package com.npc.marry.app;

/**
 * Created by Lenovo on 2/21/2017.
 */

public class Constants {
    public static final String BASE_URL = "https://m.liyathabara.com";
    public static final String BASE_PHOTO_URL = "https://content.liyathabara.com/photo/";
    public static final String BASE_FILE_URL = "https://content.liyathabara.com/attachment/";
    public static final String BASE_MAIL_URL = "https://content.liyathabara.com/mail/";

    public static final String USER_KEY = "user";
    public static final String TOKEN_KEY = "token";
    public static final String ORIENTATION_KEY = "orientation";
    public static final String USER_DATA = "user-data";
    public static final int PAGE_SIZE = 15;

    public static final String PAYPAL_CLIENT_ID = "Ac7oQ1q1D6UCCBBfUKeHM_qaXccMTFQfTJLERoFn_yvrRjGfE_fujHidW-zfPeJqz8Z304zbn110blF4";
    public static final String PAYPAL_CLIENT_SECRET = "EPmCDvS6bbUj-qC60wgvZWUFwfzOhIDeG6ZXd-1HA0qe_hswV7Vu5KNSBFftwqwQtTCfS2ij_BTT81y-";

    public static final String SOCKET_URL = "https://www.liyathabara.com";
    public static final String SOCKET_PORT = "8400";

    public static final String NETWORK_KEY = "o5t9rjef8ut7b4h0o5t9rjef8ut7b4h0";

    public static final String ABOUT_TEXT = "<div id=\"main\">\n" +
            "<div>\n" +
            "    <div class=\"testmonials col-md-12\">\n" +
            "        <div>\n" +
            "            <h1 class=\"header_big testi-head\" style=\"color:#FFF\"> About us </h1>\n" +
            "            <hr>\n" +
            "        </div>\n" +
            "        <div>\n" +
            "            We realizes that Marriage is a life together with someone special. We have made it possible by creating a easy user friendly matrimonial website to help the users find their life partners. <br>\n" +
            "            <br>\n" +
            "            Our site is a service in the Online Matrimony/Online Marriage proposal industry. Launched in December 2007, it was popular within three months and has been so ever since.<br>\n" +
            "            <br>\n" +
            "            Liyathabara is for everyone, Whether you are parent looking for bride/groom for a your son/daughter, whether you are a brother looking to help your unmarried brother/sister, Whether you are a friend who is trying to help your best mate, or even if you are along looking to get married we can help you.<br>\n" +
            "            <br>\n" +
            "            Unlike most other online matrimony/online dating sites, which focus on casual dating and quick \"hook-ups\", Liyathabara.com caters to a community of singles and parents who are looking for something serious and long-lasting.<br>\n" +
            "            <br>\n" +
            "            We have broken the continental boundaries to help users meet their soul mate. Sri Lankan's who are all over the world No matter whether you are from Australia, United States, United Kingdom etc... they all meet under our little website, they chat, they email they get to know each other and finally they decides to get married to a person who may be 10000 km away. Its the power of the today's communication technology at your doorstep. <br>\n" +
            "            <br>\n" +
            "            Liyathabara continues to focus on differentiating itself by providing a rich array of unique services and by delivering online superior customer support. Our goal is to provide an unparalleled level of services.<br>\n" +
            "            <br>\n" +
            "            Every aspect of our site has been constructed with secure matrimonial as the central focus. And navigation of the site has been made so simple, that even a first time user, will find profiles that match its interest.<br>\n" +
            "            <br>\n" +
            "            We help the users meet in pursuit of their life partner by allowing them to add their profiles for free, do a search for their life partner for free and allow them to contact their soul mate with a minimal fee. We offer basic and premium membership at a very low rate. <br>\n" +
            "            <br>\n" +
            "            <b>Success Stories</b><br>\n" +
            "            <br>\n" +
            "            Whether it was love at first sight, a marriage proposal, an interesting first meeting, a wedding, a first child, or simply something that felt like a premonition of good things to come, we can't keep up with the thousands of success stories our members have sent to us everyday.<br>\n" +
            "            <br>\n" +
            "            If you need any proof that our site works, feel free to take a look at our huge collection of happy testimonials sent to us everyday, written with love by excited our site members who thanking us for helping them find the special person they'd been searching for. <br>\n" +
            "            <br>\n" +
            "            <b>Our Goal</b><br>\n" +
            "            Marry me for reason, Even marriage proposals can begin with love.<br>\n" +
            "            <br>\n" +
            "            <b>Why Liyathabara Marriage Proposals ?</b><br>\n" +
            "            <br>\n" +
            "            We don't promise you the \"Sun and the Moon\" like other Sri Lankan websites; <br>\n" +
            "            <br>\n" +
            "            <ul><br>\n" +
            "                <li>No, you won't get married within 1 month with us. (to divorced next month), we want you to get married for life.</li> <br>\n" +
            "                <li>No, we can't do 1000+ marriages per month. But we have made few people happy, there is no pleasure than reading our members true and lovely testimonials send to us every day.</li> <br>\n" +
            "                <li> No, we don't need to pretend we are the no 1, but we know we are the best.  with over 75,000+ visits per month, over 60,000+ past members and over 10,000+ active members is proof that its not just an another website. </li><br>\n" +
            "                <li> No, we don't show 1000+ proposals at office and ask you to pay for telephone numbers, we don't treat our members as a marketing tool to make money for us.</li><br>\n" +
            "                <li> No, we don't offer any personalised services, if you want to get married you should know the best person to get married better than us.</li><br>\n" +
            "            </ul>\n" +
            "        </div>\n" +
            "    </div>\n" +
            "</div>\n" +
            "</div>";
}
