package com.npc.marry.app;

import com.npc.marry.model.User;
import com.npc.marry.model.UserData;
import com.orhanobut.hawk.Hawk;

/**
 * Created by Lenovo on 2/21/2017.
 */

public class AppSetting {
    private static final AppSetting instance = new AppSetting();
    User user;
    String token;
    UserData userData;

    private AppSetting() {
        loadData();
    }

    public static AppSetting getInstance() {
        return instance;
    }

    public UserData getUserData() {
        return userData;
    }

    public void setUserData(UserData userData) {
        this.userData = userData;
        Hawk.put(Constants.USER_DATA, userData);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
        Hawk.put(Constants.USER_KEY, user);
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
        Hawk.put(Constants.TOKEN_KEY, token);
    }

    private void loadData() {
        user = Hawk.get(Constants.USER_KEY, null);
        token = Hawk.get(Constants.TOKEN_KEY, null);
        userData = Hawk.get(Constants.USER_DATA, null);
    }

    public void logout() {
        Hawk.clear();
        loadData();
    }
}
