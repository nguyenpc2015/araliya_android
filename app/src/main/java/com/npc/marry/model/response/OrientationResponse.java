package com.npc.marry.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.npc.marry.model.Orientation;

import java.util.List;

/**
 * Created by Lenovo on 2/21/2017.
 */

public class OrientationResponse extends BaseResponse {
    @Expose
    @SerializedName("oriens")
    public List<Orientation> orientations;
}
