package com.npc.marry.model;

import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.util.List;

/**
 * Created by nguyen tran on 2/17/2018.
 */

public class DropSelect implements Serializable {
    @Expose
    public List<HeightRange> height_range;
    @Expose
    public List<WeightRange> weight_range;

    public class HeightRange implements Serializable {
        @Expose
        public int id;
        @Expose
        public String title;
        @Expose
        public String value_cm;
        @Expose
        public String value_f;
    }

    public class WeightRange implements Serializable {
        @Expose
        public int id;
        @Expose
        public String title;
        @Expose
        public String value_kg;
    }
}
