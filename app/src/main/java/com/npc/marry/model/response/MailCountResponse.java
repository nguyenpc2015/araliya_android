package com.npc.marry.model.response;

import com.google.gson.annotations.Expose;

/**
 * Created by nguyen tran on 2/27/2018.
 */

public class MailCountResponse extends BaseResponse {
    @Expose
    public int count;
}
