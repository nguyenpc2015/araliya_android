package com.npc.marry.model.request;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Created by nguyen tran on 2/17/2018.
 */

public class SecretCodeRequest implements Serializable {
    @Expose
    public String code;

    public SecretCodeRequest(String code) {
        this.code = code;
    }
}
