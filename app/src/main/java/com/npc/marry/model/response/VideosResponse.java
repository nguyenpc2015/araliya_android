package com.npc.marry.model.response;

import com.google.gson.annotations.Expose;
import com.npc.marry.model.Video;

import java.util.List;

/**
 * Created by Lenovo on 6/10/2017.
 */

public class VideosResponse extends BaseResponse {
    @Expose
    public List<Video> videos;
}
