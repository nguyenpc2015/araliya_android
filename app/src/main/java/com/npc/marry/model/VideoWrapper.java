package com.npc.marry.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

/**
 * Created by Lenovo on 6/20/2017.
 */

@Parcel
public class VideoWrapper {
    @Expose
    @SerializedName("liyathabara\\Models\\Audio")
    public Video video;

    @Expose
    public String name;

    public VideoWrapper() {
    }

    public VideoWrapper(Video video, String name) {
        this.video = video;
        this.name = name;
    }
}
