package com.npc.marry.model.request;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Created by nguyen tran on 2/17/2018.
 */

public class UpdateLocationRequest implements Serializable {
    @Expose
    public int user_id;
    @Expose
    public int country_id;
    @Expose
    public int state_id;
    @Expose
    public int city_id;

    public UpdateLocationRequest(int user_id, int country_id, int state_id, int city_id) {
        this.user_id = user_id;
        this.country_id = country_id;
        this.state_id = state_id;
        this.city_id = city_id;
    }
}
