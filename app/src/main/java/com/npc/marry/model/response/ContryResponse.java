package com.npc.marry.model.response;

import com.npc.marry.model.Country;

import java.util.List;

/**
 * Created by Lenovo on 2/25/2017.
 */

public class ContryResponse extends BaseResponse {
    public List<Country> countries;
}
