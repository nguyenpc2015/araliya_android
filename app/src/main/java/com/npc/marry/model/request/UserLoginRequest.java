package com.npc.marry.model.request;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Created by Lenovo on 2/21/2017.
 */

public class UserLoginRequest implements Serializable {
    @Expose
    public String username;
    @Expose
    public String password;

    public UserLoginRequest() {
    }

    public UserLoginRequest(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
