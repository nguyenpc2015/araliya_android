package com.npc.marry.model.response;

import com.google.gson.annotations.Expose;
import com.npc.marry.model.UserData;

import java.util.List;

/**
 * Created by Lenovo on 3/6/2017.
 */

public class UserListResponse extends BaseResponse {
    @Expose
    public List<UserData> users;
    @Expose
    public int total_pages;
    @Expose
    public int next;
    @Expose
    public int total_items;
}
