package com.npc.marry.model.request;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Created by nguyen tran on 2/26/2018.
 */

public class MailFileRequest implements Serializable {
    @Expose
    public int user_id;
    @Expose
    public String ref;

    public MailFileRequest(int user_id, String ref) {
        this.user_id = user_id;
        this.ref = ref;
    }
}
