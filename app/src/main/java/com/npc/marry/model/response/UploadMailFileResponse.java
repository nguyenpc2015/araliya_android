package com.npc.marry.model.response;

import com.google.gson.annotations.Expose;
import com.npc.marry.model.File;

/**
 * Created by nguyen tran on 2/26/2018.
 */

public class UploadMailFileResponse extends BaseResponse {
    @Expose
    public File file;
}
