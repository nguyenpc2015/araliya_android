package com.npc.marry.model;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Created by Lenovo on 2/25/2017.
 */

public class Country implements Serializable {
    @Expose
    public String country_id;
    @Expose
    public String country_title;
    @Expose
    public String code;
    @Expose
    public String country_sort;
}