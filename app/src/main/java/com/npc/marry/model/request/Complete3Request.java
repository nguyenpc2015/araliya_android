package com.npc.marry.model.request;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Created by Lenovo on 2/28/2017.
 */

public class Complete3Request implements Serializable {
    @Expose
    private int user_id;
    @Expose
    private int status_id;
    @Expose
    private int education_id;
    @Expose
    private int career_id;
    @Expose
    private int religion_id;
    @Expose
    private int ethnicity_id;
    @Expose
    private int cast_id;
    @Expose
    private int class_id;
    @Expose
    private int residency_id;
    @Expose
    private int family_id;

    public Complete3Request() {
    }

    public Complete3Request(int user_id, int status_id, int education_id, int career_id, int religion_id, int ethnicity_id, int cast_id, int class_id, int residency_id, int family_id) {
        this.user_id = user_id;
        this.status_id = status_id;
        this.education_id = education_id;
        this.career_id = career_id;
        this.religion_id = religion_id;
        this.ethnicity_id = ethnicity_id;
        this.cast_id = cast_id;
        this.class_id = class_id;
        this.residency_id = residency_id;
        this.family_id = family_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getStatus_id() {
        return status_id;
    }

    public void setStatus_id(int status_id) {
        this.status_id = status_id;
    }

    public int getEducation_id() {
        return education_id;
    }

    public void setEducation_id(int education_id) {
        this.education_id = education_id;
    }

    public int getCareer_id() {
        return career_id;
    }

    public void setCareer_id(int career_id) {
        this.career_id = career_id;
    }

    public int getReligion_id() {
        return religion_id;
    }

    public void setReligion_id(int religion_id) {
        this.religion_id = religion_id;
    }

    public int getEthnicity_id() {
        return ethnicity_id;
    }

    public void setEthnicity_id(int ethnicity_id) {
        this.ethnicity_id = ethnicity_id;
    }

    public int getCast_id() {
        return cast_id;
    }

    public void setCast_id(int cast_id) {
        this.cast_id = cast_id;
    }

    public int getClass_id() {
        return class_id;
    }

    public void setClass_id(int class_id) {
        this.class_id = class_id;
    }

    public int getResidency_id() {
        return residency_id;
    }

    public void setResidency_id(int residency_id) {
        this.residency_id = residency_id;
    }

    public int getFamily_id() {
        return family_id;
    }

    public void setFamily_id(int family_id) {
        this.family_id = family_id;
    }
}
