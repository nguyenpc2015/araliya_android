package com.npc.marry.model;

import com.google.gson.annotations.Expose;

import org.parceler.Parcel;

import java.io.Serializable;

/**
 * Created by Lenovo on 4/19/2017.
 */

@Parcel
public class Conversation implements Serializable {
    @Expose
    public String created;
    @Expose
    public String from_user_id;
    @Expose
    public String to_user_id;
    @Expose
    public String photo_from;
    @Expose
    public String photo_to;
    @Expose
    public String received;
    @Expose
    public String msg;
    @Expose
    public String from_user_name;
    @Expose
    public String to_user_name;
}
