package com.npc.marry.model.response;

import com.google.gson.annotations.Expose;
import com.npc.marry.model.PaymentPlan;

import java.util.List;

/**
 * Created by Lenovo on 3/17/2017.
 */

public class PaymentPlanResponse extends BaseResponse {
    @Expose
    public List<PaymentPlan> plans;
}
