package com.npc.marry.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.npc.marry.model.response.BaseResponse;

import org.parceler.Parcel;

/**
 * Created by Lenovo on 3/6/2017.
 */

@Parcel
public class UserData extends BaseResponse {
    @Expose
    public String user_id;
    @Expose
    public String mail;
    @Expose
    public String birth;
    @Expose
    public int age;
    @Expose
    public String city;
    @Expose
    public String career;
    @Expose
    public String religion;
    @Expose
    public String education;
    @Expose
    public String status;
    @Expose
    public String country;
    @Expose
    public String state;
    @Expose
    public String height;
    @Expose
    public String smoking;
    @Expose
    public String drinking;
    @Expose
    public String diet;
    @Expose
    public String humor;
    @Expose
    public String first_date;
    @Expose
    public String living_with;
    @Expose
    public String body;
    @Expose
    public String appearance;
    @Expose
    public String complexion;
    @Expose
    public String weight;
    @Expose
    public String ethnicity;
    @Expose
    public String cast;
    @Expose
    @SerializedName("class")
    public String classs;
    @Expose
    public String family;
    @Expose
    public String residency;
    @Expose
    public String headline;
    @Expose
    public String essay;
    @Expose
    public String orientation;
    @Expose
    public String default_photo;
    @Expose
    public String relation;
    @Expose
    public String online;
    @Expose
    public String mode;
    @Expose
    public String name;
    @Expose
    public String gender;
    @Expose
    public float rating;
    @Expose
    public String enddate;
    @Expose
    public int featured;

    @Expose
    public int viewed;

    @Expose
    public int new_interst;

    @Expose
    public int gold_days;

    @Expose
    public int count_chat;

    @Expose
    public String last_visit;

    @Expose
    public String user_message;

    @Expose
    public double latt;
    @Expose
    public double longt;
}
