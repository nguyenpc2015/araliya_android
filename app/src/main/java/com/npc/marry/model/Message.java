package com.npc.marry.model;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Created by Lenovo on 4/22/2017.
 */

public class Message implements Serializable {
    @Expose
    public String id;
    @Expose
    public String created;
    @Expose
    public String from_user;
    @Expose
    public String to_user;
    @Expose
    public String msg;
    @Expose
    public String received;
    @Expose
    public String received_on;
    @Expose
    public String from_user_closed;
    @Expose
    public String to_user_closed;
}
