package com.npc.marry.model;

import com.google.gson.annotations.Expose;

/**
 * Created by Lenovo on 2/25/2017.
 */

public class RelationShip {
    @Expose
    public String id;
    @Expose
    public String title;
}
