package com.npc.marry.model.response;

import com.google.gson.annotations.Expose;
import com.npc.marry.model.File;

import java.util.List;

/**
 * Created by nguyen tran on 2/22/2018.
 */

public class MailFileResponse extends BaseResponse {
    @Expose
    public List<File> files;
}
