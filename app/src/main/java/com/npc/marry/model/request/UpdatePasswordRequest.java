package com.npc.marry.model.request;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Created by nguyen tran on 2/17/2018.
 */

public class UpdatePasswordRequest implements Serializable {
    @Expose
    public String password;
    @Expose
    public int user_id;

    public UpdatePasswordRequest(String password, int user_id) {
        this.password = password;
        this.user_id = user_id;
    }
}
