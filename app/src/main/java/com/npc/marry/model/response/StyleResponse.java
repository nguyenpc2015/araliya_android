package com.npc.marry.model.response;

import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Lenovo on 2/27/2017.
 */

public class StyleResponse extends BaseResponse {
    @Expose
    public List<Smoking> smoking;
    @Expose
    public List<Drinking> drinking;
    @Expose
    public List<Diet> diet;
    @Expose
    public List<Persional> persional;
    @Expose
    public List<Firstdate> firstdate;
    @Expose
    public List<Living> living;

    public class Smoking implements Serializable {
        @Expose
        public String id;
        @Expose
        public String title;
    }

    public class Drinking implements Serializable {
        @Expose
        public String id;
        @Expose
        public String title;
    }

    public class Diet implements Serializable {
        @Expose
        public String id;
        @Expose
        public String title;
    }

    public class Persional implements Serializable {
        @Expose
        public String id;
        @Expose
        public String title;
    }

    public class Firstdate implements Serializable {
        @Expose
        public String id;
        @Expose
        public String title;
    }

    public class Living implements Serializable {
        @Expose
        public String id;
        @Expose
        public String title;
    }
}
