package com.npc.marry.model.request;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Created by nguyen tran on 2/28/2018.
 */

public class UpdateNearByRequest implements Serializable {
    @Expose
    public int user_id;
    @Expose
    public int near_by;

    public UpdateNearByRequest(int user_id, int near_by) {
        this.user_id = user_id;
        this.near_by = near_by;
    }
}
