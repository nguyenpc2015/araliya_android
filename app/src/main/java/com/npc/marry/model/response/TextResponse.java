package com.npc.marry.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.npc.marry.model.Text;

/**
 * Created by Lenovo on 4/10/2017.
 */

public class TextResponse extends BaseResponse {
    @Expose
    @SerializedName("text")
    public Text text;

    @Expose
    @SerializedName("text1")
    public String textEmpty;
}
