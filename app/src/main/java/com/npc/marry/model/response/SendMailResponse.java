package com.npc.marry.model.response;

import com.google.gson.annotations.Expose;
import com.npc.marry.model.Mail;

/**
 * Created by nguyen tran on 2/26/2018.
 */

public class SendMailResponse extends BaseResponse {
    @Expose
    public Mail mail;
}
