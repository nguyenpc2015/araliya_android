package com.npc.marry.model;

import com.google.gson.annotations.Expose;

import org.parceler.Parcel;

import java.io.Serializable;

/**
 * Created by Lenovo on 6/10/2017.
 */

@Parcel
public class Video implements Serializable {
    @Expose
    public String url;

    @Expose
    public String audio_name;

    @Expose
    public String description;

    @Expose
    public String audio_code;

    @Expose
    public String user_id;

    @Expose
    public char visible;

    @Expose
    public int audio_id;

    public Video() {
    }
}
