package com.npc.marry.model;

import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.util.List;

/**
 * Created by nguyen tran on 2/19/2018.
 */

public class AMultiSelect implements Serializable {
    @Expose
    public List<VarBody> var_body;
    @Expose
    public List<VarAppearance> var_appearance;
    @Expose
    public List<VarComplextion> var_complextion;
    @Expose
    public List<VarStatus> var_status;
    @Expose
    public List<VarEducation> var_education;
    @Expose
    public List<VarCareer> var_career;
    @Expose
    public List<VarReligion> var_religion;
    @Expose
    public List<VarEthnicity> var_ethnicity;
    @Expose
    public List<VarCast> var_cast;
    @Expose
    public List<VarClass> var_class;
    @Expose
    public List<VarResidency> var_residency;
    @Expose
    public List<VarFamily> var_family;
    @Expose
    public List<VarSmoking> var_smoking;
    @Expose
    public List<VarDrinking> var_drinking;
    @Expose
    public List<VarDiet> var_diet;
    @Expose
    public List<VarHumor> var_humor;

    public class AMultiWrapper implements Serializable {
        @Expose
        public int id;
        @Expose
        public String title;
        @Expose
        public boolean checked;


    }

    public class VarBody extends AMultiWrapper {

    }

    public class VarAppearance extends AMultiWrapper {

    }

    public class VarComplextion extends AMultiWrapper {

    }

    public class VarStatus extends AMultiWrapper {

    }

    public class VarEducation extends AMultiWrapper {

    }

    public class VarCareer extends AMultiWrapper {

    }

    public class VarReligion extends AMultiWrapper {

    }

    public class VarEthnicity extends AMultiWrapper {

    }

    public class VarCast extends AMultiWrapper {

    }

    public class VarClass extends AMultiWrapper {

    }

    public class VarResidency extends AMultiWrapper {

    }

    public class VarFamily extends AMultiWrapper {

    }

    public class VarSmoking extends AMultiWrapper {

    }

    public class VarDrinking extends AMultiWrapper {

    }

    public class VarDiet extends AMultiWrapper {

    }

    public class VarHumor extends AMultiWrapper {

    }
}
