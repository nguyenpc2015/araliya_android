package com.npc.marry.model.response;

import com.google.gson.annotations.Expose;

/**
 * Created by Lenovo on 6/5/2017.
 */

public class BraintreeTokenResponse extends BaseResponse {
    @Expose
    public String client_token;
}
