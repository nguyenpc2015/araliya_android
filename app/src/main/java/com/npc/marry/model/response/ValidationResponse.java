package com.npc.marry.model.response;

import com.google.gson.annotations.Expose;

/**
 * Created by Lenovo on 2/27/2017.
 */

public class ValidationResponse extends BaseResponse {
    @Expose
    public String token;
}
