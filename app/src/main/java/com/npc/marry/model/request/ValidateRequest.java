package com.npc.marry.model.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Lenovo on 2/22/2017.
 */

public class ValidateRequest implements Serializable {
    @Expose
    @SerializedName("user_id")
    String user_id;

    @Expose
    @SerializedName("code")
    String code;

    public ValidateRequest() {
    }

    public ValidateRequest(String user_id, String code) {
        this.user_id = user_id;
        this.code = code;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
