package com.npc.marry.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by nguyen tran on 2/21/2018.
 */

public class Mail implements Parcelable {
    public static final Creator<Mail> CREATOR = new Creator<Mail>() {
        @Override
        public Mail createFromParcel(Parcel in) {
            return new Mail(in);
        }

        @Override
        public Mail[] newArray(int size) {
            return new Mail[size];
        }
    };
    @Expose
    public String id;
    @Expose
    public String user_from;
    @Expose
    public String user_to;
    @SerializedName("new")
    @Expose
    public String _new;
    @Expose
    public String subject;
    @Expose
    public String text;
    @Expose
    public String date;
    @Expose
    public String status;
    @Expose
    public String user_name;
    @Expose
    public String photo;

    protected Mail(Parcel in) {
        id = in.readString();
        user_from = in.readString();
        user_to = in.readString();
        _new = in.readString();
        subject = in.readString();
        text = in.readString();
        date = in.readString();
        status = in.readString();
        user_name = in.readString();
        photo = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(user_from);
        parcel.writeString(user_to);
        parcel.writeString(_new);
        parcel.writeString(subject);
        parcel.writeString(text);
        parcel.writeString(date);
        parcel.writeString(status);
        parcel.writeString(user_name);
        parcel.writeString(photo);
    }
}
