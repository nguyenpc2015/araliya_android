package com.npc.marry.model.request;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Created by Lenovo on 4/12/2017.
 */

public class HoroscopeRequest implements Serializable {
    @Expose
    public String birth_time;

    @Expose
    public String location;

    @Expose
    public float longitude;

    @Expose
    public float latitude;

    public HoroscopeRequest(String birth_time, String location, float longitude, float latitude) {
        this.birth_time = birth_time;
        this.location = location;
        this.longitude = longitude;
        this.latitude = latitude;
    }
}
