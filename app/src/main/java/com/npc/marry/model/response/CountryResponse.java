package com.npc.marry.model.response;

import com.google.gson.annotations.Expose;
import com.npc.marry.model.Country;

import java.util.List;

/**
 * Created by Lenovo on 2/25/2017.
 */

public class CountryResponse extends BaseResponse {
    @Expose
    public List<Country> countries;
}
