package com.npc.marry.model.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by nguyen tran on 2/19/2018.
 */

public class AdvanceSearchRequest implements Serializable {
    @Expose
    public int user_id;
    @Expose
    public String is_photo;
    @Expose
    public int p_age_from;
    @Expose
    public int p_age_to;
    @Expose
    public int p_height_from;
    @Expose
    public int p_height_to;
    @Expose
    public int p_weight_from;
    @Expose
    public int p_weight_to;
    @Expose
    public List<Integer> p_education;
    @Expose
    public List<Integer> p_body;
    @Expose
    public List<Integer> p_ethnicity;
    @Expose
    public List<Integer> p_religion;
    @Expose
    public List<Integer> p_family;
    @Expose
    public List<Integer> p_career;
    @Expose
    public List<Integer> p_smoking;
    @Expose
    public List<Integer> p_drinking;
    @Expose
    public List<Integer> p_status;
    @Expose
    public List<Integer> appearance;
    @Expose
    public List<Integer> complexion;
    @Expose
    public List<Integer> cast;
    @Expose
    @SerializedName("class")
    public List<Integer> classs;
    @Expose
    public List<Integer> residency;
    @Expose
    public List<Integer> family;
    @Expose
    public List<Integer> diet;
    @Expose
    public List<Integer> humor;

    public String getIs_photo() {
        return is_photo;
    }

    public void setIs_photo(String is_photo) {
        this.is_photo = is_photo;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getP_age_from() {
        return p_age_from;
    }

    public void setP_age_from(int p_age_from) {
        this.p_age_from = p_age_from;
    }

    public int getP_age_to() {
        return p_age_to;
    }

    public void setP_age_to(int p_age_to) {
        this.p_age_to = p_age_to;
    }

    public int getP_height_from() {
        return p_height_from;
    }

    public void setP_height_from(int p_height_from) {
        this.p_height_from = p_height_from;
    }

    public int getP_height_to() {
        return p_height_to;
    }

    public void setP_height_to(int p_height_to) {
        this.p_height_to = p_height_to;
    }

    public int getP_weight_from() {
        return p_weight_from;
    }

    public void setP_weight_from(int p_weight_from) {
        this.p_weight_from = p_weight_from;
    }

    public int getP_weight_to() {
        return p_weight_to;
    }

    public void setP_weight_to(int p_weight_to) {
        this.p_weight_to = p_weight_to;
    }

    public List<Integer> getP_education() {
        return p_education;
    }

    public void setP_education(List<Integer> p_education) {
        this.p_education = p_education;
    }

    public List<Integer> getP_body() {
        return p_body;
    }

    public void setP_body(List<Integer> p_body) {
        this.p_body = p_body;
    }

    public List<Integer> getP_ethnicity() {
        return p_ethnicity;
    }

    public void setP_ethnicity(List<Integer> p_ethnicity) {
        this.p_ethnicity = p_ethnicity;
    }

    public List<Integer> getP_religion() {
        return p_religion;
    }

    public void setP_religion(List<Integer> p_religion) {
        this.p_religion = p_religion;
    }

    public List<Integer> getP_family() {
        return p_family;
    }

    public void setP_family(List<Integer> p_family) {
        this.p_family = p_family;
    }

    public List<Integer> getP_career() {
        return p_career;
    }

    public void setP_career(List<Integer> p_career) {
        this.p_career = p_career;
    }

    public List<Integer> getP_smoking() {
        return p_smoking;
    }

    public void setP_smoking(List<Integer> p_smoking) {
        this.p_smoking = p_smoking;
    }

    public List<Integer> getP_drinking() {
        return p_drinking;
    }

    public void setP_drinking(List<Integer> p_drinking) {
        this.p_drinking = p_drinking;
    }

    public List<Integer> getP_status() {
        return p_status;
    }

    public void setP_status(List<Integer> p_status) {
        this.p_status = p_status;
    }

    public List<Integer> getAppearance() {
        return appearance;
    }

    public void setAppearance(List<Integer> appearance) {
        this.appearance = appearance;
    }

    public List<Integer> getComplexion() {
        return complexion;
    }

    public void setComplexion(List<Integer> complexion) {
        this.complexion = complexion;
    }

    public List<Integer> getCast() {
        return cast;
    }

    public void setCast(List<Integer> cast) {
        this.cast = cast;
    }

    public List<Integer> getClasss() {
        return classs;
    }

    public void setClasss(List<Integer> classs) {
        this.classs = classs;
    }

    public List<Integer> getResidency() {
        return residency;
    }

    public void setResidency(List<Integer> residency) {
        this.residency = residency;
    }

    public List<Integer> getFamily() {
        return family;
    }

    public void setFamily(List<Integer> family) {
        this.family = family;
    }

    public List<Integer> getDiet() {
        return diet;
    }

    public void setDiet(List<Integer> diet) {
        this.diet = diet;
    }

    public List<Integer> getHumor() {
        return humor;
    }

    public void setHumor(List<Integer> humor) {
        this.humor = humor;
    }
}
