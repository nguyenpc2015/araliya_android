package com.npc.marry.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.io.Serializable;

/**
 * Created by Lenovo on 3/12/2017.
 */

@Parcel
public class Notification implements Serializable {
    @Expose
    int id;

    @Expose
    int user_id;

    @Expose
    int user_id_from;

    @Expose
    @SerializedName("m_date_created")
    String date;

    @Expose
    @SerializedName("m_content")
    String content;

    @Expose
    @SerializedName("m_viewed")
    int viewed;

    @Expose
    String name;

    @Expose
    String photo;

    public Notification() {
    }

    public Notification(int id, int user_id, int user_id_from, String date, String content, int viewed, String name, String photo) {
        this.id = id;
        this.user_id = user_id;
        this.user_id_from = user_id_from;
        this.date = date;
        this.content = content;
        this.viewed = viewed;
        this.name = name;
        this.photo = photo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getUser_id_from() {
        return user_id_from;
    }

    public void setUser_id_from(int user_id_from) {
        this.user_id_from = user_id_from;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getViewed() {
        return viewed;
    }

    public void setViewed(int viewed) {
        this.viewed = viewed;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
