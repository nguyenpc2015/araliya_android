package com.npc.marry.model;

import com.google.gson.annotations.Expose;

import org.parceler.Parcel;

import java.io.Serializable;

/**
 * Created by Lenovo on 3/17/2017.
 */

@Parcel
public class PaymentPlan implements Serializable {
    @Expose
    public String item;
    @Expose
    public String item_name;
    @Expose
    public String amount;
    @Expose
    public String gold_days;
    @Expose
    public String gender;
    @Expose
    public String amount_manual;
}
