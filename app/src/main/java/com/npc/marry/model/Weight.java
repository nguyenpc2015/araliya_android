package com.npc.marry.model;

import com.google.gson.annotations.Expose;

import org.parceler.Parcel;

import java.io.Serializable;

/**
 * Created by Lenovo on 2/27/2017.
 */

@Parcel

public class Weight implements Serializable {
    @Expose
    public String id;
    @Expose
    public String title;
    @Expose
    public String value_kg;
}