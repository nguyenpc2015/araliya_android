package com.npc.marry.model.response;

import com.google.gson.annotations.Expose;

/**
 * Created by nguyen tran on 2/17/2018.
 */

public class ForgotPasswordConfirmResponse extends BaseResponse {
    @Expose
    public int user_id;
}
