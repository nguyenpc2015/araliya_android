package com.npc.marry.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Lenovo on 2/27/2017.
 */

public class SocialResponse extends BaseResponse {
    @Expose
    public List<Status> status;
    @Expose
    public List<Education> education;
    @Expose
    public List<Career> career;
    @Expose
    public List<Religion> religion;
    @Expose
    public List<Ethnicity> ethnicity;
    @Expose
    public List<Cast> cast;
    @Expose
    @SerializedName("class")
    public List<Classs> classs;
    @Expose
    public List<Recidency> recidency;
    @Expose
    public List<Family> family;

    public class Status implements Serializable {
        @Expose
        public String id;
        @Expose
        public String title;
    }

    public class Education implements Serializable {
        @Expose
        public String id;
        @Expose
        public String title;
    }

    public class Career implements Serializable {
        @Expose
        public String id;
        @Expose
        public String title;
    }

    public class Religion implements Serializable {
        @Expose
        public String id;
        @Expose
        public String title;
    }

    public class Ethnicity implements Serializable {
        @Expose
        public String id;
        @Expose
        public String title;
    }

    public class Cast implements Serializable {
        @Expose
        public String id;
        @Expose
        public String title;
    }

    public class Classs implements Serializable {
        @Expose
        public String id;
        @Expose
        public String title;
    }

    public class Recidency implements Serializable {
        @Expose
        public String id;
        @Expose
        public String title;
    }

    public class Family implements Serializable {
        @Expose
        public String id;
        @Expose
        public String title;
    }

}
