package com.npc.marry.model.response;

import com.google.gson.annotations.Expose;
import com.npc.marry.model.State;

import java.util.List;

/**
 * Created by Lenovo on 2/25/2017.
 */

public class StateResponse extends BaseResponse {
    @Expose
    public List<State> states;
}
