package com.npc.marry.model.response;

import com.google.gson.annotations.Expose;

/**
 * Created by Lenovo on 3/13/2017.
 */

public class UploadPhotoResponse extends BaseResponse {
    @Expose
    String ref;

    public UploadPhotoResponse() {
    }

    public UploadPhotoResponse(String ref) {
        this.ref = ref;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }
}
