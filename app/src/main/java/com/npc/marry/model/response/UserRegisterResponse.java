package com.npc.marry.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.npc.marry.model.User;

/**
 * Created by Lenovo on 2/22/2017.
 */

public class UserRegisterResponse extends BaseResponse {

    @Expose
    @SerializedName("user")
    public User user;
}
