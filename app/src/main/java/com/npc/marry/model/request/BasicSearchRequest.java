package com.npc.marry.model.request;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Created by Lenovo on 3/10/2017.
 */

public class BasicSearchRequest implements Serializable {
    @Expose
    String user_id;
    @Expose
    int country_id;
    @Expose
    int state_id;
    @Expose
    int city_id;
    @Expose
    int status;
    @Expose
    int photo;
    @Expose
    int age_from;
    @Expose
    int age_to;

    @Expose
    String keyword;

    public BasicSearchRequest() {
    }

    public BasicSearchRequest(String user_id, int country_id, int state_id, int city_id, int status, int photo, int age_from, int age_to, String keyword) {
        this.user_id = user_id;
        this.country_id = country_id;
        this.state_id = state_id;
        this.city_id = city_id;
        this.status = status;
        this.photo = photo;
        this.age_from = age_from;
        this.age_to = age_to;
        this.keyword = keyword;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public int getCountry_id() {
        return country_id;
    }

    public void setCountry_id(int country_id) {
        this.country_id = country_id;
    }

    public int getState_id() {
        return state_id;
    }

    public void setState_id(int state_id) {
        this.state_id = state_id;
    }

    public int getCity_id() {
        return city_id;
    }

    public void setCity_id(int city_id) {
        this.city_id = city_id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getPhoto() {
        return photo;
    }

    public void setPhoto(int photo) {
        this.photo = photo;
    }

    public int getAge_from() {
        return age_from;
    }

    public void setAge_from(int age_from) {
        this.age_from = age_from;
    }

    public int getAge_to() {
        return age_to;
    }

    public void setAge_to(int age_to) {
        this.age_to = age_to;
    }
}
