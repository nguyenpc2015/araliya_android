package com.npc.marry.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.io.Serializable;

/**
 * Created by Lenovo on 6/17/2017.
 */

@Parcel
public class PhotoWrapper implements Serializable {
    @Expose
    @SerializedName("liyathabara\\Models\\Photo")
    public Photo photo;

    @Expose
    public String name;

    public PhotoWrapper(Photo photo, String name) {
        this.photo = photo;
        this.name = name;
    }

    public PhotoWrapper() {
    }
}