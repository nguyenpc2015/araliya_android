package com.npc.marry.model.request;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Created by nguyen tran on 3/2/2018.
 */

public class CheckSessionRequest implements Serializable {
    @Expose
    public int user_id;
    @Expose
    public String session_id;

    public CheckSessionRequest(int user_id, String session_id) {
        this.user_id = user_id;
        this.session_id = session_id;
    }
}
