package com.npc.marry.model.request;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Created by nguyen tran on 2/17/2018.
 */

public class UpdateEmailRequest implements Serializable {
    @Expose
    public int user_id;
    @Expose
    public String email;

    public UpdateEmailRequest(int user_id, String email) {
        this.user_id = user_id;
        this.email = email;
    }
}
