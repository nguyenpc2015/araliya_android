package com.npc.marry.model.request;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Created by Lenovo on 3/31/2017.
 */

public class ContactRequest implements Serializable {

    @Expose
    private String user_id;

    @Expose
    private String name;

    @Expose
    private String mail;

    @Expose
    private String type;

    @Expose
    private String comment;

    public ContactRequest(String user_id, String name, String mail, String type, String comment) {
        this.user_id = user_id;
        this.name = name;
        this.mail = mail;
        this.type = type;
        this.comment = comment;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
