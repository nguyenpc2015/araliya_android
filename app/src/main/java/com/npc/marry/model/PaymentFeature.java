package com.npc.marry.model;

import java.io.Serializable;

/**
 * Created by Lenovo on 3/17/2017.
 */

public class PaymentFeature implements Serializable {
    public String feature;
    public boolean free;
    public boolean verified;

    public PaymentFeature(String feature, boolean free, boolean verified) {
        this.feature = feature;
        this.free = free;
        this.verified = verified;
    }
}
