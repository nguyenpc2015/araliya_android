package com.npc.marry.model.response;

import com.google.gson.annotations.Expose;
import com.npc.marry.model.User;

/**
 * Created by Lenovo on 2/21/2017.
 */

public class UserLoginResponse extends BaseResponse {

    @Expose
    String token;

    @Expose
    User user;

    public UserLoginResponse() {
    }

    public UserLoginResponse(String token, User user) {
        this.token = token;
        this.user = user;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
