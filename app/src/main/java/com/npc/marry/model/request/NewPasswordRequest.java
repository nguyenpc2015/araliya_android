package com.npc.marry.model.request;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Created by nguyen tran on 2/17/2018.
 */

public class NewPasswordRequest implements Serializable {
    @Expose
    public int user_id;
    @Expose
    public String password;

    public NewPasswordRequest(int user_id, String password) {
        this.user_id = user_id;
        this.password = password;
    }
}
