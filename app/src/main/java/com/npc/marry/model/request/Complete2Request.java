package com.npc.marry.model.request;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Created by Lenovo on 2/28/2017.
 */

public class Complete2Request implements Serializable {
    @Expose
    private int user_id;
    @Expose
    private int relation_id;
    @Expose
    private int age_from;
    @Expose
    private int age_to;
    @Expose
    private int height_id;
    @Expose
    private int weight_id;
    @Expose
    private int body_id;
    @Expose
    private int appearance_id;
    @Expose
    private int complexion_id;

    public Complete2Request() {
    }

    public Complete2Request(int user_id, int relation_id, int age_from, int age_to, int height_id, int weight_id, int body_id, int appearance_id, int complexion_id) {
        this.user_id = user_id;
        this.relation_id = relation_id;
        this.age_from = age_from;
        this.age_to = age_to;
        this.height_id = height_id;
        this.weight_id = weight_id;
        this.body_id = body_id;
        this.appearance_id = appearance_id;
        this.complexion_id = complexion_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getRelation_id() {
        return relation_id;
    }

    public void setRelation_id(int relation_id) {
        this.relation_id = relation_id;
    }

    public int getAge_from() {
        return age_from;
    }

    public void setAge_from(int age_from) {
        this.age_from = age_from;
    }

    public int getAge_to() {
        return age_to;
    }

    public void setAge_to(int age_to) {
        this.age_to = age_to;
    }

    public int getHeight_id() {
        return height_id;
    }

    public void setHeight_id(int height_id) {
        this.height_id = height_id;
    }

    public int getWeight_id() {
        return weight_id;
    }

    public void setWeight_id(int weight_id) {
        this.weight_id = weight_id;
    }

    public int getBody_id() {
        return body_id;
    }

    public void setBody_id(int body_id) {
        this.body_id = body_id;
    }

    public int getAppearance_id() {
        return appearance_id;
    }

    public void setAppearance_id(int appearance_id) {
        this.appearance_id = appearance_id;
    }

    public int getComplexion_id() {
        return complexion_id;
    }

    public void setComplexion_id(int complexion_id) {
        this.complexion_id = complexion_id;
    }
}
