package com.npc.marry.model.request;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Created by Lenovo on 2/28/2017.
 */

public class Complete1Request implements Serializable {

    @Expose
    private int user_id;
    @Expose
    private String headline;
    @Expose
    private String essay;
    @Expose
    private int country_id;
    @Expose
    private int state_id;
    @Expose
    private int city_id;

    public Complete1Request() {
    }

    public Complete1Request(int user_id, String headline, String essay, int country_id, int state_id, int city_id) {
        this.user_id = user_id;
        this.headline = headline;
        this.essay = essay;
        this.country_id = country_id;
        this.state_id = state_id;
        this.city_id = city_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getHeadline() {
        return headline;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
    }

    public String getEssay() {
        return essay;
    }

    public void setEssay(String essay) {
        this.essay = essay;
    }

    public int getCountry_id() {
        return country_id;
    }

    public void setCountry_id(int country_id) {
        this.country_id = country_id;
    }

    public int getState_id() {
        return state_id;
    }

    public void setState_id(int state_id) {
        this.state_id = state_id;
    }

    public int getCity_id() {
        return city_id;
    }

    public void setCity_id(int city_id) {
        this.city_id = city_id;
    }
}
