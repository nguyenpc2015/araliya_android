package com.npc.marry.model.response;

import com.google.gson.annotations.Expose;
import com.npc.marry.model.DropSelect;
import com.npc.marry.model.MultiSelect;

/**
 * Created by nguyen tran on 2/17/2018.
 */

public class PartnerParamResponse extends BaseResponse {
    @Expose
    public DropSelect drop_select;
    @Expose
    public MultiSelect multi_select;
}
