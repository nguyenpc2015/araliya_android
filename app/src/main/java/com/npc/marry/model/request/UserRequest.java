package com.npc.marry.model.request;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Created by Lenovo on 2/21/2017.
 */

public class UserRequest implements Serializable {

    @Expose
    public String username;
    @Expose
    public String password;
    @Expose
    public String email;
    @Expose
    public String gender;
    @Expose
    public String orientation;
    @Expose
    public String p_orientation;
    @Expose
    public String born;

    public UserRequest() {
    }

    public UserRequest(String username, String password, String email, String gender, String orientation, String p_orientation, String born) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.gender = gender;
        this.orientation = orientation;
        this.p_orientation = p_orientation;
        this.born = born;
    }
}
