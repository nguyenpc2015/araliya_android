package com.npc.marry.model.response;

import com.google.gson.annotations.Expose;
import com.npc.marry.model.Mail;

import java.util.List;

/**
 * Created by nguyen tran on 2/21/2018.
 */

public class MailsResponse extends BaseResponse {
    @Expose
    public List<Mail> mails;
    @Expose
    public int total_pages;
    @Expose
    public int total_items;
}
