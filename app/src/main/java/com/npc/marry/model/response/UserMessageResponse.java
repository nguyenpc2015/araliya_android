package com.npc.marry.model.response;

import com.google.gson.annotations.Expose;
import com.npc.marry.model.User;

/**
 * Created by nguyen tran on 2/17/2018.
 */

public class UserMessageResponse extends BaseResponse {
    @Expose
    public User user;
}
