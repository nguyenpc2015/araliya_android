package com.npc.marry.model.request;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Created by Lenovo on 3/11/2017.
 */

public class FireBaseTokenRequest implements Serializable {
    @Expose
    int user_id;
    @Expose
    String token;

    public FireBaseTokenRequest() {
    }

    public FireBaseTokenRequest(int user_id, String token) {
        this.user_id = user_id;
        this.token = token;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
