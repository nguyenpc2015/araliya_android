package com.npc.marry.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.npc.marry.model.response.BaseResponse;

/**
 * Created by Lenovo on 2/21/2017.
 */

public class Orientation extends BaseResponse {

    @Expose
    @SerializedName("id")
    private String id;
    @Expose
    @SerializedName("title")
    private String title;
    @Expose
    @SerializedName("search")
    private String search;
    @Expose
    @SerializedName("gender")
    private String gender;
    @Expose
    @SerializedName("free")
    private String free;

    public Orientation() {
    }

    public Orientation(String id, String title, String search, String gender, String free) {
        this.id = id;
        this.title = title;
        this.search = search;
        this.gender = gender;
        this.free = free;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getFree() {
        return free;
    }

    public void setFree(String free) {
        this.free = free;
    }
}
