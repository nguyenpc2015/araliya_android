package com.npc.marry.model.request;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Created by nguyen tran on 2/20/2018.
 */

public class NearBySearchRequest implements Serializable {
    @Expose
    public int user_id;
    @Expose
    public double distance;
    @Expose
    public double latt;
    @Expose
    public double longt;

    public NearBySearchRequest(int user_id, double distance, double latt, double longt) {
        this.user_id = user_id;
        this.distance = distance;
        this.latt = latt;
        this.longt = longt;
    }
}
