package com.npc.marry.model.request;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Created by nguyen tran on 2/27/2018.
 */

public class RemoveMailFileRequest implements Serializable {
    @Expose
    public String filename;
    @Expose
    public String ref;

    public RemoveMailFileRequest(String filename, String ref) {
        this.filename = filename;
        this.ref = ref;
    }
}
