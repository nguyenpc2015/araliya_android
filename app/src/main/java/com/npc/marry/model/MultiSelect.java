package com.npc.marry.model;

import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.util.List;

/**
 * Created by nguyen tran on 2/17/2018.
 */

public class MultiSelect implements Serializable {
    @Expose
    public List<PEducation> p_education;
    @Expose
    public List<PBody> p_body;
    @Expose
    public List<PEthnicity> p_ethnicity;
    @Expose
    public List<PReligion> p_religion;
    @Expose
    public List<PFamily> p_family;
    @Expose
    public List<PCareer> p_career;
    @Expose
    public List<PSmoking> p_smoking;
    @Expose
    public List<PDrinking> p_drinking;
    @Expose
    public List<PStatus> p_status;

    public class MultiWrapper implements Serializable {
        @Expose
        public int id;
        @Expose
        public String title;
        public boolean checked;
    }

    public class PEducation extends MultiWrapper {
    }

    public class PBody extends MultiWrapper {
    }

    public class PEthnicity extends MultiWrapper {
    }

    public class PReligion extends MultiWrapper {
    }

    public class PFamily extends MultiWrapper {
    }

    public class PCareer extends MultiWrapper {
    }

    public class PSmoking extends MultiWrapper {
    }

    public class PDrinking extends MultiWrapper {
    }

    public class PStatus extends MultiWrapper {
    }
}
