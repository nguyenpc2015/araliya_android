package com.npc.marry.model;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Created by Lenovo on 4/10/2017.
 */

public class Text implements Serializable {
    @Expose
    public String id;
    @Expose
    public String user_id;
    @Expose
    public String headline;
    @Expose
    public String essay;
    @Expose
    public String date;
    @Expose
    public String status;
}
