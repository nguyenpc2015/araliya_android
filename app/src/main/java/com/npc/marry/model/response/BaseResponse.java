package com.npc.marry.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Lenovo on 2/21/2017.
 */

public class BaseResponse implements Serializable {
    @Expose
    @SerializedName("message")
    public String message;
    @Expose
    public int valid;
}
