package com.npc.marry.model.request;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Created by Lenovo on 3/5/2017.
 */

public class LogoutRequest implements Serializable {

    @Expose
    private int user_id;

    public LogoutRequest() {
    }

    public LogoutRequest(int user_id) {
        this.user_id = user_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }
}
