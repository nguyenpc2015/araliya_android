package com.npc.marry.model;

import com.google.gson.annotations.Expose;

/**
 * Created by nguyen tran on 2/22/2018.
 */

public class File {
    @Expose
    public String file_id;
    @Expose
    public String mail_msg_id;
    @Expose
    public String user_id;
    @Expose
    public String ref;
    @Expose
    public String file_name;
    @Expose
    public String date;
}
