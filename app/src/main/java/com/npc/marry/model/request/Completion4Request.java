package com.npc.marry.model.request;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Created by Lenovo on 2/28/2017.
 */

public class Completion4Request implements Serializable {
    @Expose
    private int user_id;
    @Expose
    private int smoking_id;
    @Expose
    private int drinking_id;
    @Expose
    private int diet_id;
    @Expose
    private int humor_id;
    @Expose
    private int first_date_id;
    @Expose
    private int living_with_id;

    public Completion4Request() {
    }

    public Completion4Request(int user_id, int smoking_id, int drinking_id, int diet_id, int humor_id, int first_date_id, int living_with_id) {
        this.user_id = user_id;
        this.smoking_id = smoking_id;
        this.drinking_id = drinking_id;
        this.diet_id = diet_id;
        this.humor_id = humor_id;
        this.first_date_id = first_date_id;
        this.living_with_id = living_with_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getSmoking_id() {
        return smoking_id;
    }

    public void setSmoking_id(int smoking_id) {
        this.smoking_id = smoking_id;
    }

    public int getDrinking_id() {
        return drinking_id;
    }

    public void setDrinking_id(int drinking_id) {
        this.drinking_id = drinking_id;
    }

    public int getDiet_id() {
        return diet_id;
    }

    public void setDiet_id(int diet_id) {
        this.diet_id = diet_id;
    }

    public int getHumor_id() {
        return humor_id;
    }

    public void setHumor_id(int humor_id) {
        this.humor_id = humor_id;
    }

    public int getFirst_date_id() {
        return first_date_id;
    }

    public void setFirst_date_id(int first_date_id) {
        this.first_date_id = first_date_id;
    }

    public int getLiving_with_id() {
        return living_with_id;
    }

    public void setLiving_with_id(int living_with_id) {
        this.living_with_id = living_with_id;
    }
}
