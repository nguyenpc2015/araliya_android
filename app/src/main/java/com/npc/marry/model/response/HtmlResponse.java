package com.npc.marry.model.response;

import com.google.gson.annotations.Expose;

/**
 * Created by Lenovo on 6/12/2017.
 */

public class HtmlResponse extends BaseResponse {
    @Expose
    public String html;
}
