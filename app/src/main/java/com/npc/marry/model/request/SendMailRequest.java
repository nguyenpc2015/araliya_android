package com.npc.marry.model.request;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Created by nguyen tran on 2/26/2018.
 */

public class SendMailRequest implements Serializable {
    @Expose
    public int user_id;
    @Expose
    public int user_to;
    @Expose
    public String subject;
    @Expose
    public String text;
    @Expose
    public String ref;

    public SendMailRequest(int user_id, int user_to, String subject, String text, String ref) {
        this.user_id = user_id;
        this.user_to = user_to;
        this.subject = subject;
        this.text = text;
        this.ref = ref;
    }
}
