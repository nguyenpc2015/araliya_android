package com.npc.marry.model.request;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Created by nguyen tran on 2/19/2018.
 */

public class SaveVideoRequest implements Serializable {
    @Expose
    public String url;
    @Expose
    public int user_id;
    @Expose
    public String audio_name;
    @Expose
    public String description;

    public SaveVideoRequest(String url, int user_id, String audio_name, String description) {
        this.url = url;
        this.user_id = user_id;
        this.audio_name = audio_name;
        this.description = description;
    }
}
