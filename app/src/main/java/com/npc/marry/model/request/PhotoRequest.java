package com.npc.marry.model.request;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Created by Lenovo on 3/14/2017.
 */

public class PhotoRequest implements Serializable {
    @Expose
    String user_id;
    @Expose
    String photo_name;
    @Expose
    String description;
    @Expose
    int default_photo;
    @Expose
    String ref;

    public PhotoRequest() {
    }

    public PhotoRequest(String user_id, String photo_name, String description, int default_photo, String ref) {
        this.user_id = user_id;
        this.photo_name = photo_name;
        this.description = description;
        this.default_photo = default_photo;
        this.ref = ref;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getPhoto_name() {
        return photo_name;
    }

    public void setPhoto_name(String photo_name) {
        this.photo_name = photo_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int geyDefalt_photo() {
        return default_photo;
    }

    public void setDefalt_photo(int defalt_photo) {
        this.default_photo = defalt_photo;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }
}
