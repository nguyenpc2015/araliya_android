package com.npc.marry.model.request;

import com.google.gson.annotations.Expose;
import com.npc.marry.model.ShippingAddress;

import java.io.Serializable;

/**
 * Created by Lenovo on 3/20/2017.
 */

public class VerifyPaymentRequest implements Serializable {
    @Expose
    String nonce;
    @Expose
    private String user_id;
    @Expose
    private String amount;
    @Expose
    private String currency;
    @Expose
    private int gold_days;
    @Expose
    private String item;
    @Expose
    private String item_name;
    @Expose
    private ShippingAddress shipping;

    public VerifyPaymentRequest() {
    }

    public VerifyPaymentRequest(String user_id, String amount, String currency, int gold_days, String item, String nonce, String item_name, ShippingAddress shipping) {
        this.user_id = user_id;
        this.amount = amount;
        this.currency = currency;
        this.gold_days = gold_days;
        this.item = item;
        this.nonce = nonce;
        this.item_name = item_name;
        this.shipping = shipping;
    }

    public ShippingAddress getShipping() {
        return shipping;
    }

    public void setShipping(ShippingAddress shipping) {
        this.shipping = shipping;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public int getGold_days() {
        return gold_days;
    }

    public void setGold_days(int gold_days) {
        this.gold_days = gold_days;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getNonce() {
        return nonce;
    }

    public void setNonce(String nonce) {
        this.nonce = nonce;
    }

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }
}
