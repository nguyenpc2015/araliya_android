package com.npc.marry.model.response;

import com.google.gson.annotations.Expose;
import com.npc.marry.model.AMultiSelect;
import com.npc.marry.model.DropSelect;

/**
 * Created by nguyen tran on 2/19/2018.
 */

public class AdvanceSearchParamResponse extends BaseResponse {
    @Expose
    public DropSelect drop_select;
    @Expose
    public AMultiSelect a_multi_select;
}
