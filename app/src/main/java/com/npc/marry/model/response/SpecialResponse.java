package com.npc.marry.model.response;

import com.google.gson.annotations.Expose;
import com.npc.marry.model.Appear;
import com.npc.marry.model.Body;
import com.npc.marry.model.Complex;
import com.npc.marry.model.Height;
import com.npc.marry.model.Weight;

import java.util.List;

/**
 * Created by Lenovo on 2/27/2017.
 */

public class SpecialResponse extends BaseResponse {
    @Expose
    public List<Height> heights;
    @Expose
    public List<Weight> weights;
    @Expose
    public List<Body> bodies;
    @Expose
    public List<Appear> appears;
    @Expose
    public List<Complex> complexs;
}
