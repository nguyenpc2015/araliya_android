package com.npc.marry.model;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Created by Lenovo on 2/25/2017.
 */

public class City implements Serializable {
    @Expose
    public String city_id;
    @Expose
    public String state_id;
    @Expose
    public String country_id;
    @Expose
    public String city_title;
}