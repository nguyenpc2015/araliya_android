package com.npc.marry.model.response;

import com.google.gson.annotations.Expose;
import com.npc.marry.model.Message;

import java.util.List;

/**
 * Created by Lenovo on 4/22/2017.
 */

public class MessageResponse extends BaseResponse {
    @Expose
    public List<Message> items;
    @Expose
    public int first;
    @Expose
    public int before;
    @Expose
    public int current;
    @Expose
    public int last;
    @Expose
    public int next;
    @Expose
    public int total_pages;
    @Expose
    public int total_items;
    @Expose
    public int limit;
}
