package com.npc.marry.model.response;

import com.google.gson.annotations.Expose;
import com.npc.marry.model.Conversation;

import java.util.List;

/**
 * Created by Lenovo on 4/19/2017.
 */

public class ConversationResponse extends BaseResponse {
    @Expose
    public List<Conversation> chats;
}
