package com.npc.marry.model.request;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Created by Lenovo on 3/11/2017.
 */

public class InterestRequest implements Serializable {
    @Expose
    int user_id_from;
    @Expose
    int user_id_to;

    public InterestRequest() {
    }

    public InterestRequest(int user_id_from, int user_id_to) {
        this.user_id_from = user_id_from;
        this.user_id_to = user_id_to;
    }

    public int getUser_id_from() {
        return user_id_from;
    }

    public void setUser_id_from(int user_id_from) {
        this.user_id_from = user_id_from;
    }

    public int getUser_id_to() {
        return user_id_to;
    }

    public void setUser_id_to(int user_id_to) {
        this.user_id_to = user_id_to;
    }
}
