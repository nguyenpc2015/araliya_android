package com.npc.marry.model;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Created by Lenovo on 6/6/2017.
 */

public class ShippingAddress implements Serializable {

    @Expose
    String firstName;
    @Expose
    String lastName;
    @Expose
    String company;
    @Expose
    String streetAddress;
    @Expose
    String extendedAddress;
    @Expose
    String locality;
    @Expose
    String region;
    @Expose
    String postalCode;
    @Expose
    String countryCodeAlpha2;

    public ShippingAddress() {
    }

    public ShippingAddress(String firstName, String lastName, String company, String streetAddress, String extendedAddress, String locality, String region, String postalCode, String countryCodeAlpha2) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.company = company;
        this.streetAddress = streetAddress;
        this.extendedAddress = extendedAddress;
        this.locality = locality;
        this.region = region;
        this.postalCode = postalCode;
        this.countryCodeAlpha2 = countryCodeAlpha2;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getExtendedAddress() {
        return extendedAddress;
    }

    public void setExtendedAddress(String extendedAddress) {
        this.extendedAddress = extendedAddress;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCountryCodeAlpha2() {
        return countryCodeAlpha2;
    }

    public void setCountryCodeAlpha2(String countryCodeAlpha2) {
        this.countryCodeAlpha2 = countryCodeAlpha2;
    }
}
