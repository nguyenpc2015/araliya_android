package com.npc.marry.model.request;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Created by nguyen tran on 2/28/2018.
 */

public class NewsletterRequest implements Serializable {
    @Expose
    public int user_id;
    @Expose
    public int check;

    public NewsletterRequest(int user_id, int check) {
        this.user_id = user_id;
        this.check = check;
    }
}
