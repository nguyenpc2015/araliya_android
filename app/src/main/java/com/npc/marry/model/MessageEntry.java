package com.npc.marry.model;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Created by Lenovo on 4/27/2017.
 */

public class MessageEntry implements Serializable {
    @Expose
    String msg;
    @Expose
    String to;

    public MessageEntry(String msg, String to) {
        this.msg = msg;
        this.to = to;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }
}
