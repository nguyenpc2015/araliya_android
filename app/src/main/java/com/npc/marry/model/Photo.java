package com.npc.marry.model;

import com.google.gson.annotations.Expose;

import org.parceler.Parcel;

import java.io.Serializable;

/**
 * Created by Lenovo on 3/13/2017.
 */

@Parcel
public class Photo implements Serializable {
    @Expose
    public String photo_id;
    @Expose
    public String user_id;
    @Expose
    public String main;
    @Expose
    public String photo_name;
    @Expose
    public String description;
    @Expose
    public String visible;
    @Expose
    public String ref;
    @Expose
    public String storage;
    @Expose
    public String datetime;

    public Photo() {
    }
}
