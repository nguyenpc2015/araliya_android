package com.npc.marry.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.npc.marry.model.response.BaseResponse;

/**
 * Created by Lenovo on 2/21/2017.
 */

public class User extends BaseResponse {
    @Expose
    public String user_id;
    @Expose
    public String name;
    @Expose
    public String mode;
    @Expose
    public String password;
    @Expose
    public String gender;
    @Expose
    public int gold_days;
    @Expose
    public int orientation;
    @Expose
    public int country_id;
    @Expose
    public int state_id;
    @Expose
    public int city_id;
    @Expose
    public int education;
    @Expose
    public int height;
    @Expose
    public int relation;
    @Expose
    public int body;
    @Expose
    public int complexion;
    @Expose
    public int appearance;
    @Expose
    public int religion;
    @Expose
    public int weight;
    @Expose
    public int ethnicity;
    @Expose
    public int career;
    @Expose
    public int family;
    @Expose
    public int smoking;
    @Expose
    public int drinking;
    @Expose
    public int first_date;
    @Expose
    public int diet;
    @Expose
    public int humor;
    @Expose
    public int status;
    @Expose
    public int cast;
    @Expose
    @SerializedName("class")
    public int classs;
    @Expose
    public int living_with;
    @Expose
    public int residency;
    @Expose
    public int p_age_from;
    @Expose
    public int p_age_to;
    @Expose
    public String session_id;
    @Expose
    public int online;
    @Expose
    public int verified;
    @Expose
    public int p_education;
    @Expose
    public int p_body;
    @Expose
    public int p_ethnicity;
    @Expose
    public int p_religion;
    @Expose
    public int p_family;
    @Expose
    public int p_career;
    @Expose
    public int p_smoking;
    @Expose
    public int p_drinking;
    @Expose
    public int p_status;
    @Expose
    public int p_height_from;
    @Expose
    public int p_height_to;
    @Expose
    public int p_weight_from;
    @Expose
    public int p_weight_to;
    @Expose
    public int near_by;
}
