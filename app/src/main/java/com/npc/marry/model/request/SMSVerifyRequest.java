package com.npc.marry.model.request;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Created by nguyen tran on 2/17/2018.
 */

public class SMSVerifyRequest implements Serializable {
    @Expose
    public int user_id;
    @Expose
    public String mobile;
    @Expose
    public int country_code;

    public SMSVerifyRequest(int user_id, String mobile, int country_code) {
        this.user_id = user_id;
        this.mobile = mobile;
        this.country_code = country_code;
    }
}
