package com.npc.marry.base;

import rx.subscriptions.CompositeSubscription;

/**
 * Created by Lenovo on 2/21/2017.
 */

public abstract class BasePresenter<T> {
    protected T view;
    protected CompositeSubscription compositeSubscription;

    public BasePresenter(T view, CompositeSubscription compositeSubscription) {
        initView(view);
        this.compositeSubscription = compositeSubscription;
    }

    protected void initView(T view) {
        this.view = view;
    }
}
