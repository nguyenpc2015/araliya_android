package com.npc.marry.api.retrofit;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.npc.marry.R;
import com.npc.marry.app.App;
import com.npc.marry.app.AppSetting;
import com.npc.marry.app.Constants;
import com.npc.marry.utils.TextUtils;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Collections;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import okhttp3.CipherSuite;
import okhttp3.ConnectionSpec;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.TlsVersion;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


public class RetrofitAPIService {

    private static RetrofitAPIService sInstance;
    private APIServices mService;

    private RetrofitAPIService() {
        Gson gson = new GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation()
                .disableHtmlEscaping()
                .setLenient()
                .create();

        OkHttpClient client = getClient();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(client)
                .build();
        mService = retrofit.create(APIServices.class);
    }

    public static RetrofitAPIService getInstance() {
        if (sInstance == null) {
            sInstance = new RetrofitAPIService();
        }
        return sInstance;
    }

    public static Response processResponse(Response response) {
        try {
            byte[] byteData = response.body().bytes();
            Response.Builder builder = response.newBuilder();
            String jsonString = new TextUtils(Constants.NETWORK_KEY).decrypt(byteData);
            builder.body(ResponseBody.create(MediaType.parse("application/json"), jsonString));
            return builder.build();
        } catch (Exception e) {
            Log.d("TAG", "processResponse: " + e.toString());
            return response;
        }
    }

    //This method handle because of wrong responses from anh Phu, it should be JSON, not String.
    private static String removeUnusedCharacters(String str) {
        if (str.charAt(0) == '\"' && str.charAt(str.length() - 1) == '\"') {
            str = str.substring(1, str.length() - 1);
        }
        str = str.replace("\\", "");
        return str;
    }

    public APIServices getService() {
        return mService;
    }

    private boolean addRootCertificateToCustomStore(int id, KeyStore keyStore) {
        InputStream caInput = null;
        try {
            // Load CAs from an InputStream
            // (could be from a resource or ByteArrayInputStream or ...)
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            // From https://www.washington.edu/itconnect/security/ca/load-der.crt
            caInput = new BufferedInputStream(App.getAppContext().getResources().openRawResource(id));
            Certificate ca = cf.generateCertificate(caInput);
            keyStore.setCertificateEntry("ca=" + ((X509Certificate) ca).getSubjectDN(), ca);
        } catch (CertificateException certEx) {

        } catch (KeyStoreException certEx) {

        } finally {
            if (caInput != null)
                try {
                    caInput.close();

                } catch (IOException ioe) {
                }
        }
        return true;
    }

    public OkHttpClient getClient() {
        ConnectionSpec spec = new ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
                .tlsVersions(TlsVersion.TLS_1_2)
                .cipherSuites(
                        CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
                        CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
                        CipherSuite.TLS_DHE_RSA_WITH_AES_128_GCM_SHA256)
                .build();
        HttpLoggingInterceptor logger = new HttpLoggingInterceptor();
        logger.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder()
                .connectionSpecs(Collections.singletonList(spec))
                .addInterceptor(new CustomInterceptor())
                .addInterceptor(logger)
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .build();
        return client;
    }

    public OkHttpClient getUnsafeOkHttpClient() {
        try {

            //logger
            HttpLoggingInterceptor logger = new HttpLoggingInterceptor();
            logger.setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient.Builder builder = new OkHttpClient.Builder();

            // Create a KeyStore containing our trusted CAs
            String keyStoreType = KeyStore.getDefaultType();
            KeyStore keyStore = KeyStore.getInstance(keyStoreType);
            keyStore.load(null, null);

            this.addRootCertificateToCustomStore(R.raw.c1, keyStore);
            this.addRootCertificateToCustomStore(R.raw.c2, keyStore);

            // Create a TrustManager that trusts the CAs in our KeyStore
            String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
            tmf.init(keyStore);

            // Create an SSLContext that uses our TrustManager
            SSLContext context = SSLContext.getInstance("TLS");
            context.init(null, tmf.getTrustManagers(), null);

            builder.sslSocketFactory(context.getSocketFactory());

            OkHttpClient okHttpClient = builder.addInterceptor(new CustomInterceptor())
                    .addInterceptor(logger)
                    .connectTimeout(30, TimeUnit.SECONDS)
                    .readTimeout(30, TimeUnit.SECONDS)
                    .writeTimeout(30, TimeUnit.SECONDS)
                    .build();

            return okHttpClient;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public static class CustomInterceptor implements Interceptor {

        public boolean isOnline() {
            ConnectivityManager cm =
                    (ConnectivityManager) App.getAppContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            return netInfo != null && netInfo.isConnectedOrConnecting();
        }

        @Override
        public Response intercept(Chain chain) throws IOException {
            if (!isOnline()) {
                throw new IOException("Please check your network connection");
            } else {
                Request.Builder originRequest = chain.request().newBuilder()
                        .header("Accept", "application/json")
                        .header("Content-Type", "application/json");
                if (AppSetting.getInstance().getToken() != null && !AppSetting.getInstance().getToken().isEmpty()) {
                    originRequest.addHeader("Authorization", AppSetting.getInstance().getToken());
                }

                return processResponse(chain.proceed(originRequest.build()));
            }
        }
    }
}
