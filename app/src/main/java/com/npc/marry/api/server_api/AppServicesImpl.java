package com.npc.marry.api.server_api;

import android.content.Context;

import com.google.gson.Gson;
import com.npc.marry.api.retrofit.APIServices;
import com.npc.marry.api.retrofit.RetrofitAPIService;
import com.npc.marry.base.BaseActivity;
import com.npc.marry.model.UserData;
import com.npc.marry.model.request.AdvanceSearchRequest;
import com.npc.marry.model.request.BasicSearchRequest;
import com.npc.marry.model.request.CheckSessionRequest;
import com.npc.marry.model.request.Complete1Request;
import com.npc.marry.model.request.Complete2Request;
import com.npc.marry.model.request.Complete3Request;
import com.npc.marry.model.request.Completion4Request;
import com.npc.marry.model.request.ContactRequest;
import com.npc.marry.model.request.EmailRequest;
import com.npc.marry.model.request.FireBaseTokenRequest;
import com.npc.marry.model.request.HoroscopeRequest;
import com.npc.marry.model.request.InterestRequest;
import com.npc.marry.model.request.LogoutRequest;
import com.npc.marry.model.request.NearBySearchRequest;
import com.npc.marry.model.request.NewsletterRequest;
import com.npc.marry.model.request.PhotoRequest;
import com.npc.marry.model.request.RemoveMailFileRequest;
import com.npc.marry.model.request.SMSValidateRequest;
import com.npc.marry.model.request.SMSVerifyRequest;
import com.npc.marry.model.request.SaveVideoRequest;
import com.npc.marry.model.request.SendMailRequest;
import com.npc.marry.model.request.UpdateBirthDateRequest;
import com.npc.marry.model.request.UpdateEmailRequest;
import com.npc.marry.model.request.UpdateLocationRequest;
import com.npc.marry.model.request.UpdateNearByRequest;
import com.npc.marry.model.request.UpdatePartnerRequest;
import com.npc.marry.model.request.UpdatePasswordRequest;
import com.npc.marry.model.request.UserLoginRequest;
import com.npc.marry.model.request.UserRequest;
import com.npc.marry.model.request.ValidateRequest;
import com.npc.marry.model.response.AdvanceSearchParamResponse;
import com.npc.marry.model.response.BaseResponse;
import com.npc.marry.model.response.CityResponse;
import com.npc.marry.model.response.ConversationResponse;
import com.npc.marry.model.response.CountryResponse;
import com.npc.marry.model.response.ErrorResponse;
import com.npc.marry.model.response.ForgotPasswordResponse;
import com.npc.marry.model.response.HtmlResponse;
import com.npc.marry.model.response.MailCountResponse;
import com.npc.marry.model.response.MailFileResponse;
import com.npc.marry.model.response.MailsResponse;
import com.npc.marry.model.response.MessageResponse;
import com.npc.marry.model.response.NotificationResponse;
import com.npc.marry.model.response.OrientationResponse;
import com.npc.marry.model.response.PartnerParamResponse;
import com.npc.marry.model.response.PaymentPlanResponse;
import com.npc.marry.model.response.PhotoResponse;
import com.npc.marry.model.response.RelationshipResponse;
import com.npc.marry.model.response.SendMailResponse;
import com.npc.marry.model.response.SocialResponse;
import com.npc.marry.model.response.SpecialResponse;
import com.npc.marry.model.response.StateResponse;
import com.npc.marry.model.response.StyleResponse;
import com.npc.marry.model.response.TextResponse;
import com.npc.marry.model.response.UploadMailFileResponse;
import com.npc.marry.model.response.UploadPhotoResponse;
import com.npc.marry.model.response.UserListResponse;
import com.npc.marry.model.response.UserLoginResponse;
import com.npc.marry.model.response.UserMessageResponse;
import com.npc.marry.model.response.UserPhotoResponse;
import com.npc.marry.model.response.UserRegisterResponse;
import com.npc.marry.model.response.UserVideoResponse;
import com.npc.marry.model.response.ValidationResponse;
import com.npc.marry.model.response.VideosResponse;
import com.npc.marry.ui.login.LoginActivity;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;

public class AppServicesImpl implements AppAPIService {

    private static AppServicesImpl sInstance;

    Context context;
    APIServices mAPIService;

    private AppServicesImpl(Context context) {
        this.context = context;
        mAPIService = RetrofitAPIService.getInstance().getService();
    }

    public static AppServicesImpl getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new AppServicesImpl(context);
        } else {
            sInstance.setContext(context);
        }
        return sInstance;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    private void handleResponse(Call<? extends BaseResponse> call, Subscriber subscriber) {
        try {
            Response _response = call.execute();
            if (_response.code() != 200) {
                if (_response.code() == 401) {
                    ((BaseActivity) context).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ((BaseActivity) context).startActivity(LoginActivity.class, null, true);
                        }
                    });

                } else {
                    String jsonString = _response.errorBody().string();
                    Gson g = new Gson();
                    ErrorResponse errorResponse = g.fromJson(jsonString, ErrorResponse.class);
                    if (!subscriber.isUnsubscribed()) {
                        if (errorResponse != null) {
                            subscriber.onError(new Throwable(errorResponse.message));
                        }
                        subscriber.onCompleted();
                    }
                }
            } else {
                BaseResponse response = (BaseResponse) _response.body();
                if (!subscriber.isUnsubscribed()) {
                    if (response != null) {
                        subscriber.onNext(response);
                    }
                    subscriber.onCompleted();
                }
            }
        } catch (Exception e) {
            if (!subscriber.isUnsubscribed()) {
                subscriber.onError(e);
                subscriber.onCompleted();
            }
        }
    }

    private void handleStringResponse(Call<? extends String> call, Subscriber subscriber) {
        try {
            Response _response = call.execute();
            if (_response.code() != 200) {
                if (_response.code() == 401) {
                    ((BaseActivity) context).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ((BaseActivity) context).startActivity(LoginActivity.class, null, true);
                        }
                    });

                } else {
                    String jsonString = _response.errorBody().string();
                    Gson g = new Gson();
                    ErrorResponse errorResponse = g.fromJson(jsonString, ErrorResponse.class);
                    if (!subscriber.isUnsubscribed()) {
                        if (errorResponse != null) {
                            subscriber.onError(new Throwable(errorResponse.message));
                        }
                        subscriber.onCompleted();
                    }
                }
            } else {
                String response = (String) _response.body();
                if (!subscriber.isUnsubscribed()) {
                    if (response != null) {
                        subscriber.onNext(response);
                    }
                    subscriber.onCompleted();
                }
            }
        } catch (Exception e) {
            if (!subscriber.isUnsubscribed()) {
                subscriber.onError(e);
                subscriber.onCompleted();
            }
        }
    }


    @Override
    public Observable<OrientationResponse> getOrientations() {
        return Observable.create(new Observable.OnSubscribe<OrientationResponse>() {
            @Override
            public void call(Subscriber<? super OrientationResponse> subscriber) {
                handleResponse(mAPIService.getOrientations(), subscriber);
            }
        });
    }

    @Override
    public Observable<UserRegisterResponse> register(final UserRequest request) {
        return Observable.create(new Observable.OnSubscribe<UserRegisterResponse>() {
            @Override
            public void call(Subscriber<? super UserRegisterResponse> subscriber) {
                handleResponse(mAPIService.register(request), subscriber);
            }
        });
    }

    @Override
    public Observable<UserLoginResponse> login(final UserLoginRequest userLoginRequest) {
        return Observable.create(new Observable.OnSubscribe<UserLoginResponse>() {
            @Override
            public void call(Subscriber<? super UserLoginResponse> subscriber) {
                handleResponse(mAPIService.login(userLoginRequest), subscriber);
            }
        });
    }

    @Override
    public Observable<ValidationResponse> validate(final ValidateRequest validateRequest) {
        return Observable.create(new Observable.OnSubscribe<ValidationResponse>() {
            @Override
            public void call(Subscriber<? super ValidationResponse> subscriber) {
                handleResponse(mAPIService.validate(validateRequest), subscriber);
            }
        });
    }

    @Override
    public Observable<BaseResponse> requestCode(final String user_id) {
        return Observable.create(new Observable.OnSubscribe<BaseResponse>() {
            @Override
            public void call(Subscriber<? super BaseResponse> subscriber) {
                handleResponse(mAPIService.requestCode(user_id), subscriber);
            }
        });
    }

    @Override
    public Observable<CountryResponse> getCountry() {
        return Observable.create(new Observable.OnSubscribe<CountryResponse>() {
            @Override
            public void call(Subscriber<? super CountryResponse> subscriber) {
                handleResponse(mAPIService.getCountry(), subscriber);
            }
        });
    }

    @Override
    public Observable<StateResponse> getState(final int country_id) {
        return Observable.create(new Observable.OnSubscribe<StateResponse>() {
            @Override
            public void call(Subscriber<? super StateResponse> subscriber) {
                handleResponse(mAPIService.getState(country_id), subscriber);
            }
        });
    }

    @Override
    public Observable<CityResponse> getCity(final int country_id, final int city_id) {
        return Observable.create(new Observable.OnSubscribe<CityResponse>() {
            @Override
            public void call(Subscriber<? super CityResponse> subscriber) {
                handleResponse(mAPIService.getCity(country_id, city_id), subscriber);
            }
        });
    }

    @Override
    public Observable<RelationshipResponse> getRelationship() {
        return Observable.create(new Observable.OnSubscribe<RelationshipResponse>() {
            @Override
            public void call(Subscriber<? super RelationshipResponse> subscriber) {
                handleResponse(mAPIService.getRelationship(), subscriber);
            }
        });
    }

    @Override
    public Observable<SpecialResponse> getSpecial() {
        return Observable.create(new Observable.OnSubscribe<SpecialResponse>() {
            @Override
            public void call(Subscriber<? super SpecialResponse> subscriber) {
                handleResponse(mAPIService.getSpecial(), subscriber);
            }
        });
    }

    @Override
    public Observable<SocialResponse> getSocial() {
        return Observable.create(new Observable.OnSubscribe<SocialResponse>() {
            @Override
            public void call(Subscriber<? super SocialResponse> subscriber) {
                handleResponse(mAPIService.getSocial(), subscriber);
            }
        });
    }

    @Override
    public Observable<StyleResponse> getStyle() {
        return Observable.create(new Observable.OnSubscribe<StyleResponse>() {
            @Override
            public void call(Subscriber<? super StyleResponse> subscriber) {
                handleResponse(mAPIService.getStyle(), subscriber);
            }
        });
    }

    @Override
    public Observable<BaseResponse> completeProfile1(final Complete1Request complete1Request) {
        return Observable.create(new Observable.OnSubscribe<BaseResponse>() {
            @Override
            public void call(Subscriber<? super BaseResponse> subscriber) {
                handleResponse(mAPIService.completeProfile1(complete1Request), subscriber);
            }
        });
    }

    @Override
    public Observable<BaseResponse> completeProfile2(final Complete2Request complete2Request) {
        return Observable.create(new Observable.OnSubscribe<BaseResponse>() {
            @Override
            public void call(Subscriber<? super BaseResponse> subscriber) {
                handleResponse(mAPIService.completeProfile2(complete2Request), subscriber);
            }
        });
    }

    @Override
    public Observable<BaseResponse> completeProfile3(final Complete3Request complete3Request) {
        return Observable.create(new Observable.OnSubscribe<BaseResponse>() {
            @Override
            public void call(Subscriber<? super BaseResponse> subscriber) {
                handleResponse(mAPIService.completeProfile3(complete3Request), subscriber);
            }
        });
    }

    @Override
    public Observable<UserRegisterResponse> completeProfile4(final Completion4Request completion4Request) {
        return Observable.create(new Observable.OnSubscribe<UserRegisterResponse>() {
            @Override
            public void call(Subscriber<? super UserRegisterResponse> subscriber) {
                handleResponse(mAPIService.completeProfile4(completion4Request), subscriber);
            }
        });
    }

    @Override
    public Observable<BaseResponse> logout(final LogoutRequest logoutRequest) {
        return Observable.create(new Observable.OnSubscribe<BaseResponse>() {
            @Override
            public void call(Subscriber<? super BaseResponse> subscriber) {
                handleResponse(mAPIService.logout(logoutRequest), subscriber);
            }
        });
    }

    @Override
    public Observable<UserData> getUser(final String user_id) {
        return Observable.create(new Observable.OnSubscribe<UserData>() {
            @Override
            public void call(Subscriber<? super UserData> subscriber) {
                handleResponse(mAPIService.getUser(user_id), subscriber);
            }
        });
    }

    @Override
    public Observable<UserMessageResponse> getUserData(final String user_id) {
        return Observable.create(new Observable.OnSubscribe<UserMessageResponse>() {
            @Override
            public void call(Subscriber<? super UserMessageResponse> subscriber) {
                handleResponse(mAPIService.getUserData(user_id), subscriber);
            }
        });
    }

    @Override
    public Observable<UserListResponse> getFavourUser(final String user_id, final String page) {
        return Observable.create(new Observable.OnSubscribe<UserListResponse>() {
            @Override
            public void call(Subscriber<? super UserListResponse> subscriber) {
                handleResponse(mAPIService.getFavourUser(user_id, page), subscriber);
            }
        });
    }

    @Override
    public Observable<UserListResponse> getFeaturedUser(final String user_id, final String page) {
        return Observable.create(new Observable.OnSubscribe<UserListResponse>() {
            @Override
            public void call(Subscriber<? super UserListResponse> subscriber) {
                handleResponse(mAPIService.getFeaturedUser(user_id, page), subscriber);
            }
        });
    }

    @Override
    public Observable<UserListResponse> getHoroScopeUser(final String user_id, final String page) {
        return Observable.create(new Observable.OnSubscribe<UserListResponse>() {
            @Override
            public void call(Subscriber<? super UserListResponse> subscriber) {
                handleResponse(mAPIService.getHoroScopeUser(user_id, page), subscriber);
            }
        });
    }

    @Override
    public Observable<UserListResponse> getNewestUser(final String user_id, final String page) {
        return Observable.create(new Observable.OnSubscribe<UserListResponse>() {
            @Override
            public void call(Subscriber<? super UserListResponse> subscriber) {
                handleResponse(mAPIService.getNewestUser(user_id, page), subscriber);
            }
        });
    }

    @Override
    public Observable<UserListResponse> getOnlineUser(final String user_id, final String page) {
        return Observable.create(new Observable.OnSubscribe<UserListResponse>() {
            @Override
            public void call(Subscriber<? super UserListResponse> subscriber) {
                handleResponse(mAPIService.getOnlineUser(user_id, page), subscriber);
            }
        });
    }

    @Override
    public Observable<UserListResponse> getRecentUser(final String user_id, final String page) {
        return Observable.create(new Observable.OnSubscribe<UserListResponse>() {
            @Override
            public void call(Subscriber<? super UserListResponse> subscriber) {
                handleResponse(mAPIService.getRecentUser(user_id, page), subscriber);
            }
        });
    }

    @Override
    public Observable<UserListResponse> getRecommendUser(final String user_id, final String page) {
        return Observable.create(new Observable.OnSubscribe<UserListResponse>() {
            @Override
            public void call(Subscriber<? super UserListResponse> subscriber) {
                handleResponse(mAPIService.getRecommendUser(user_id, page), subscriber);
            }
        });
    }

    @Override
    public Observable<UserListResponse> getVerifiedUser(final String user_id, final String page) {
        return Observable.create(new Observable.OnSubscribe<UserListResponse>() {
            @Override
            public void call(Subscriber<? super UserListResponse> subscriber) {
                handleResponse(mAPIService.getVerifiedUser(user_id, page), subscriber);
            }
        });
    }

    @Override
    public Observable<UserListResponse> getMatchUser(final String user_id, final String page) {
        return Observable.create(new Observable.OnSubscribe<UserListResponse>() {
            @Override
            public void call(Subscriber<? super UserListResponse> subscriber) {
                handleResponse(mAPIService.getMatchUser(user_id, page), subscriber);
            }
        });
    }

    @Override
    public Observable<UserListResponse> search(final int page, final BasicSearchRequest basicSearchRequest) {
        return Observable.create(new Observable.OnSubscribe<UserListResponse>() {
            @Override
            public void call(Subscriber<? super UserListResponse> subscriber) {
                handleResponse(mAPIService.search(String.valueOf(page), basicSearchRequest), subscriber);
            }
        });
    }

    @Override
    public Observable<BaseResponse> sendToken(final FireBaseTokenRequest fireBaseTokenRequest) {
        return Observable.create(new Observable.OnSubscribe<BaseResponse>() {
            @Override
            public void call(Subscriber<? super BaseResponse> subscriber) {
                handleResponse(mAPIService.sendToken(fireBaseTokenRequest), subscriber);
            }
        });
    }

    @Override
    public Observable<BaseResponse> sendInterest(final InterestRequest interestRequest) {
        return Observable.create(new Observable.OnSubscribe<BaseResponse>() {
            @Override
            public void call(Subscriber<? super BaseResponse> subscriber) {
                handleResponse(mAPIService.sendInterest(interestRequest), subscriber);
            }
        });
    }

    @Override
    public Observable<NotificationResponse> getNotification(final String user_id, final String page) {
        return Observable.create(new Observable.OnSubscribe<NotificationResponse>() {
            @Override
            public void call(Subscriber<? super NotificationResponse> subscriber) {
                handleResponse(mAPIService.getNotification(user_id, page), subscriber);
            }
        });
    }

    @Override
    public Observable<BaseResponse> updateNotification(final String id) {
        return Observable.create(new Observable.OnSubscribe<BaseResponse>() {
            @Override
            public void call(Subscriber<? super BaseResponse> subscriber) {
                handleResponse(mAPIService.updateNotification(id), subscriber);
            }
        });
    }

    @Override
    public Observable<PhotoResponse> getPhoto(final String user_id, final String page) {
        return Observable.create(new Observable.OnSubscribe<PhotoResponse>() {
            @Override
            public void call(Subscriber<? super PhotoResponse> subscriber) {
                handleResponse(mAPIService.getPhoto(user_id, page), subscriber);
            }
        });
    }

    @Override
    public Observable<UploadPhotoResponse> uploadPhoto(final MultipartBody.Part image, final String user_id) {
        return Observable.create(new Observable.OnSubscribe<UploadPhotoResponse>() {
            @Override
            public void call(Subscriber<? super UploadPhotoResponse> subscriber) {
                handleResponse(mAPIService.uploadPhoto(image, user_id), subscriber);
            }
        });
    }

    @Override
    public Observable<BaseResponse> savePhoto(final PhotoRequest photoRequest) {
        return Observable.create(new Observable.OnSubscribe<BaseResponse>() {
            @Override
            public void call(Subscriber<? super BaseResponse> subscriber) {
                handleResponse(mAPIService.savePhoto(photoRequest), subscriber);
            }
        });
    }

    @Override
    public Observable<PaymentPlanResponse> getPaymentPlan(final String gender) {
        return Observable.create(new Observable.OnSubscribe<PaymentPlanResponse>() {
            @Override
            public void call(Subscriber<? super PaymentPlanResponse> subscriber) {
                handleResponse(mAPIService.getPaymentPlan(gender), subscriber);
            }
        });
    }

    @Override
    public Observable<BaseResponse> addFavorite(final InterestRequest interestRequest) {
        return Observable.create(new Observable.OnSubscribe<BaseResponse>() {
            @Override
            public void call(Subscriber<? super BaseResponse> subscriber) {
                handleResponse(mAPIService.addFavorite(interestRequest), subscriber);
            }
        });
    }

    @Override
    public Observable<BaseResponse> blockUser(final InterestRequest interestRequest) {
        return Observable.create(new Observable.OnSubscribe<BaseResponse>() {
            @Override
            public void call(Subscriber<? super BaseResponse> subscriber) {
                handleResponse(mAPIService.blockUser(interestRequest), subscriber);
            }
        });
    }

    @Override
    public Observable<UserListResponse> getInterestUser(final String user_id, final String page) {
        return Observable.create(new Observable.OnSubscribe<UserListResponse>() {
            @Override
            public void call(Subscriber<? super UserListResponse> subscriber) {
                handleResponse(mAPIService.getInterestUser(user_id, page), subscriber);
            }
        });
    }

    @Override
    public Observable<UserListResponse> getInterestReceivedUser(final String user_id, final String page) {
        return Observable.create(new Observable.OnSubscribe<UserListResponse>() {
            @Override
            public void call(Subscriber<? super UserListResponse> subscriber) {
                handleResponse(mAPIService.getInterestReceivedUser(user_id, page), subscriber);
            }
        });
    }

    @Override
    public Observable<UserListResponse> getFavoriteUSer(final String user_id, final String page) {
        return Observable.create(new Observable.OnSubscribe<UserListResponse>() {
            @Override
            public void call(Subscriber<? super UserListResponse> subscriber) {
                handleResponse(mAPIService.getFavoriteUSer(user_id, page), subscriber);
            }
        });
    }

    @Override
    public Observable<UserListResponse> getBlockUsers(final String user_id, final String page) {
        return Observable.create(new Observable.OnSubscribe<UserListResponse>() {
            @Override
            public void call(Subscriber<? super UserListResponse> subscriber) {
                handleResponse(mAPIService.getBlockUsers(user_id, page), subscriber);
            }
        });
    }

    @Override
    public Observable<BaseResponse> removeInterest(final String user_id, final String user_remove_id) {
        return Observable.create(new Observable.OnSubscribe<BaseResponse>() {
            @Override
            public void call(Subscriber<? super BaseResponse> subscriber) {
                handleResponse(mAPIService.removeInterest(user_id, user_remove_id), subscriber);
            }
        });
    }

    @Override
    public Observable<BaseResponse> removeFavorite(final String user_id, final String user_remove_id) {
        return Observable.create(new Observable.OnSubscribe<BaseResponse>() {
            @Override
            public void call(Subscriber<? super BaseResponse> subscriber) {
                handleResponse(mAPIService.removeFavorite(user_id, user_remove_id), subscriber);
            }
        });
    }

    @Override
    public Observable<BaseResponse> removeBlock(final String user_id, final String user_remove_id) {
        return Observable.create(new Observable.OnSubscribe<BaseResponse>() {
            @Override
            public void call(Subscriber<? super BaseResponse> subscriber) {
                handleResponse(mAPIService.removeBlock(user_id, user_remove_id), subscriber);
            }
        });
    }

    @Override
    public Observable<BaseResponse> contactUs(final ContactRequest contactRequest) {
        return Observable.create(new Observable.OnSubscribe<BaseResponse>() {
            @Override
            public void call(Subscriber<? super BaseResponse> subscriber) {
                handleResponse(mAPIService.contactUs(contactRequest), subscriber);
            }
        });
    }

    @Override
    public Observable<TextResponse> getHeadline(final String user_id) {
        return Observable.create(new Observable.OnSubscribe<TextResponse>() {
            @Override
            public void call(Subscriber<? super TextResponse> subscriber) {
                handleResponse(mAPIService.getHeadline(user_id), subscriber);
            }
        });
    }

    @Override
    public Observable<BaseResponse> saveHoroscope(final String user_id, final HoroscopeRequest horoscopeRequest) {
        return Observable.create(new Observable.OnSubscribe<BaseResponse>() {
            @Override
            public void call(Subscriber<? super BaseResponse> subscriber) {
                handleResponse(mAPIService.saveHoroscope(user_id, horoscopeRequest), subscriber);
            }
        });
    }

    @Override
    public Observable<ConversationResponse> getConversations(final String user_id) {
        return Observable.create(new Observable.OnSubscribe<ConversationResponse>() {
            @Override
            public void call(Subscriber<? super ConversationResponse> subscriber) {
                handleResponse(mAPIService.getConversations(user_id), subscriber);
            }
        });
    }

    @Override
    public Observable<MessageResponse> getMessages(final String user_id, final String user_id_1, final int page) {
        return Observable.create(new Observable.OnSubscribe<MessageResponse>() {
            @Override
            public void call(Subscriber<? super MessageResponse> subscriber) {
                handleResponse(mAPIService.getMessages(user_id, user_id_1, page), subscriber);
            }
        });
    }

    @Override
    public Observable<UploadPhotoResponse> uploadAttachment(final MultipartBody.Part file, final String user_id) {
        return Observable.create(new Observable.OnSubscribe<UploadPhotoResponse>() {
            @Override
            public void call(Subscriber<? super UploadPhotoResponse> subscriber) {
                handleResponse(mAPIService.uploadAttachment(file, user_id), subscriber);
            }
        });
    }

    @Override
    public Observable<BaseResponse> viewUser(final InterestRequest interestRequest) {
        return Observable.create(new Observable.OnSubscribe<BaseResponse>() {
            @Override
            public void call(Subscriber<? super BaseResponse> subscriber) {
                handleResponse(mAPIService.viewUser(interestRequest), subscriber);
            }
        });
    }

    @Override
    public Observable<UserListResponse> getUserView(final String user_id) {
        return Observable.create(new Observable.OnSubscribe<UserListResponse>() {
            @Override
            public void call(Subscriber<? super UserListResponse> subscriber) {
                handleResponse(mAPIService.getUserView(user_id), subscriber);
            }
        });
    }

    @Override
    public Observable<UserListResponse> getUserMyView(final String user_id, final String page) {
        return Observable.create(new Observable.OnSubscribe<UserListResponse>() {
            @Override
            public void call(Subscriber<? super UserListResponse> subscriber) {
                handleResponse(mAPIService.getUserMyView(user_id, page), subscriber);
            }
        });
    }

    @Override
    public Observable<UserListResponse> getUserViewedMe(final String user_id, final String page) {
        return Observable.create(new Observable.OnSubscribe<UserListResponse>() {
            @Override
            public void call(Subscriber<? super UserListResponse> subscriber) {
                handleResponse(mAPIService.getUserViewedMe(user_id, page), subscriber);
            }
        });
    }

    @Override
    public Observable<UserListResponse> getUserInterest(final String user_id) {
        return Observable.create(new Observable.OnSubscribe<UserListResponse>() {
            @Override
            public void call(Subscriber<? super UserListResponse> subscriber) {
                handleResponse(mAPIService.getUserInterest(user_id), subscriber);
            }
        });
    }

    @Override
    public Observable<BaseResponse> updateView(final String user_id) {
        return Observable.create(new Observable.OnSubscribe<BaseResponse>() {
            @Override
            public void call(Subscriber<? super BaseResponse> subscriber) {
                handleResponse(mAPIService.updateView(user_id), subscriber);
            }
        });
    }

    @Override
    public Observable<BaseResponse> updateInterest(final String user_id) {
        return Observable.create(new Observable.OnSubscribe<BaseResponse>() {
            @Override
            public void call(Subscriber<? super BaseResponse> subscriber) {
                handleResponse(mAPIService.updateInterest(user_id), subscriber);
            }
        });
    }

    @Override
    public Observable<VideosResponse> getVideos(final String user_id) {
        return Observable.create(new Observable.OnSubscribe<VideosResponse>() {
            @Override
            public void call(Subscriber<? super VideosResponse> subscriber) {
                handleResponse(mAPIService.getVideos(user_id), subscriber);
            }
        });
    }

    @Override
    public Observable<BaseResponse> deleteVideo(final String video_id) {
        return Observable.create(new Observable.OnSubscribe<BaseResponse>() {
            @Override
            public void call(Subscriber<? super BaseResponse> subscriber) {
                handleResponse(mAPIService.deleteVideo(video_id), subscriber);
            }
        });
    }

    @Override
    public Observable<BaseResponse> saveVideo(final SaveVideoRequest videoRequest) {
        return Observable.create(new Observable.OnSubscribe<BaseResponse>() {
            @Override
            public void call(Subscriber<? super BaseResponse> subscriber) {
                handleResponse(mAPIService.saveVideo(videoRequest), subscriber);
            }
        });
    }

    @Override
    public Observable<HtmlResponse> getGeneratedCode(final String user_id, final String item_id) {
        return Observable.create(new Observable.OnSubscribe<HtmlResponse>() {
            @Override
            public void call(Subscriber<? super HtmlResponse> subscriber) {
                handleResponse(mAPIService.getGeneratedCode(user_id, item_id), subscriber);
            }
        });
    }

    @Override
    public Observable<UserListResponse> searchByName(final String user_id, final String keyword, final String page) {
        return Observable.create(new Observable.OnSubscribe<UserListResponse>() {
            @Override
            public void call(Subscriber<? super UserListResponse> subscriber) {
                handleResponse(mAPIService.searchByName(user_id, keyword, page), subscriber);
            }
        });
    }

    @Override
    public Observable<AdvanceSearchParamResponse> getAdvanceSearchParam() {
        return Observable.create(new Observable.OnSubscribe<AdvanceSearchParamResponse>() {
            @Override
            public void call(Subscriber<? super AdvanceSearchParamResponse> subscriber) {
                handleResponse(mAPIService.getAdvanceSearchParam(), subscriber);
            }
        });
    }

    @Override
    public Observable<UserListResponse> advanceSearch(final AdvanceSearchRequest searchRequest, final String page) {
        return Observable.create(new Observable.OnSubscribe<UserListResponse>() {
            @Override
            public void call(Subscriber<? super UserListResponse> subscriber) {
                handleResponse(mAPIService.advanceSearch(searchRequest, page), subscriber);
            }
        });
    }

    @Override
    public Observable<UserListResponse> nearBySearch(final NearBySearchRequest nearBySearchRequest) {
        return Observable.create(new Observable.OnSubscribe<UserListResponse>() {
            @Override
            public void call(Subscriber<? super UserListResponse> subscriber) {
                handleResponse(mAPIService.nearBySearch(nearBySearchRequest), subscriber);
            }
        });
    }

    @Override
    public Observable<BaseResponse> deletePhoto(final String photo_id) {
        return Observable.create(new Observable.OnSubscribe<BaseResponse>() {
            @Override
            public void call(Subscriber<? super BaseResponse> subscriber) {
                handleResponse(mAPIService.deletePhoto(photo_id), subscriber);
            }
        });
    }

    @Override
    public Observable<UserVideoResponse> getAllVideos(final String user_id, final String page) {
        return Observable.create(new Observable.OnSubscribe<UserVideoResponse>() {
            @Override
            public void call(Subscriber<? super UserVideoResponse> subscriber) {
                handleResponse(mAPIService.getAllVideos(user_id, page), subscriber);
            }
        });
    }

    @Override
    public Observable<UserPhotoResponse> getAllPhotos(final String user_id, final String page) {
        return Observable.create(new Observable.OnSubscribe<UserPhotoResponse>() {
            @Override
            public void call(Subscriber<? super UserPhotoResponse> subscriber) {
                handleResponse(mAPIService.getAllPhotos(user_id, page), subscriber);
            }
        });
    }

    @Override
    public Observable<ForgotPasswordResponse> forgotPassword(final EmailRequest emailRequest) {
        return Observable.create(new Observable.OnSubscribe<ForgotPasswordResponse>() {
            @Override
            public void call(Subscriber<? super ForgotPasswordResponse> subscriber) {
                handleResponse(mAPIService.forgotPassword(emailRequest), subscriber);
            }
        });
    }

    @Override
    public Observable<BaseResponse> deleteAccount(final LogoutRequest logoutRequest) {
        return Observable.create(new Observable.OnSubscribe<BaseResponse>() {
            @Override
            public void call(Subscriber<? super BaseResponse> subscriber) {
                handleResponse(mAPIService.deleteAccount(logoutRequest), subscriber);
            }
        });
    }

    @Override
    public Observable<BaseResponse> updateNewsletter(final NewsletterRequest logoutRequest) {
        return Observable.create(new Observable.OnSubscribe<BaseResponse>() {
            @Override
            public void call(Subscriber<? super BaseResponse> subscriber) {
                handleResponse(mAPIService.updateNewsletter(logoutRequest), subscriber);
            }
        });
    }

    @Override
    public Observable<UserMessageResponse> updateEmail(final UpdateEmailRequest emailRequest) {
        return Observable.create(new Observable.OnSubscribe<UserMessageResponse>() {
            @Override
            public void call(Subscriber<? super UserMessageResponse> subscriber) {
                handleResponse(mAPIService.updateEmail(emailRequest), subscriber);
            }
        });
    }

    @Override
    public Observable<UserMessageResponse> updatePassword(final UpdatePasswordRequest passwordRequest) {
        return Observable.create(new Observable.OnSubscribe<UserMessageResponse>() {
            @Override
            public void call(Subscriber<? super UserMessageResponse> subscriber) {
                handleResponse(mAPIService.updatePassword(passwordRequest), subscriber);
            }
        });
    }

    @Override
    public Observable<UserMessageResponse> updateLocation(final UpdateLocationRequest locationRequest) {
        return Observable.create(new Observable.OnSubscribe<UserMessageResponse>() {
            @Override
            public void call(Subscriber<? super UserMessageResponse> subscriber) {
                handleResponse(mAPIService.updateLocation(locationRequest), subscriber);
            }
        });
    }

    @Override
    public Observable<UserMessageResponse> updateBirthDate(final UpdateBirthDateRequest birthDateRequest) {
        return Observable.create(new Observable.OnSubscribe<UserMessageResponse>() {
            @Override
            public void call(Subscriber<? super UserMessageResponse> subscriber) {
                handleResponse(mAPIService.updateBirthDate(birthDateRequest), subscriber);
            }
        });
    }

    @Override
    public Observable<PartnerParamResponse> getPartnerParam() {
        return Observable.create(new Observable.OnSubscribe<PartnerParamResponse>() {
            @Override
            public void call(Subscriber<? super PartnerParamResponse> subscriber) {
                handleResponse(mAPIService.getPartnerParam(), subscriber);
            }
        });
    }

    @Override
    public Observable<BaseResponse> verifySMS(final SMSVerifyRequest smsVerifyRequest) {
        return Observable.create(new Observable.OnSubscribe<BaseResponse>() {
            @Override
            public void call(Subscriber<? super BaseResponse> subscriber) {
                handleResponse(mAPIService.verifySMS(smsVerifyRequest), subscriber);
            }
        });
    }

    @Override
    public Observable<UserMessageResponse> validateSMS(final SMSValidateRequest validateRequest) {
        return Observable.create(new Observable.OnSubscribe<UserMessageResponse>() {
            @Override
            public void call(Subscriber<? super UserMessageResponse> subscriber) {
                handleResponse(mAPIService.validateSMS(validateRequest), subscriber);
            }
        });
    }

    @Override
    public Observable<UserMessageResponse> updatePartner(final UpdatePartnerRequest partnerRequest) {
        return Observable.create(new Observable.OnSubscribe<UserMessageResponse>() {
            @Override
            public void call(Subscriber<? super UserMessageResponse> subscriber) {
                handleResponse(mAPIService.updatePartner(partnerRequest), subscriber);
            }
        });
    }

    @Override
    public Observable<MailsResponse> getMails(final String user_id, final String page) {
        return Observable.create(new Observable.OnSubscribe<MailsResponse>() {
            @Override
            public void call(Subscriber<? super MailsResponse> subscriber) {
                handleResponse(mAPIService.getMails(user_id, page), subscriber);
            }
        });
    }

    @Override
    public Observable<MailsResponse> getMailTrash(final String user_id, final String page) {
        return Observable.create(new Observable.OnSubscribe<MailsResponse>() {
            @Override
            public void call(Subscriber<? super MailsResponse> subscriber) {
                handleResponse(mAPIService.getMailTrash(user_id, page), subscriber);
            }
        });
    }

    @Override
    public Observable<MailsResponse> getMailSent(final String user_id, final String page) {
        return Observable.create(new Observable.OnSubscribe<MailsResponse>() {
            @Override
            public void call(Subscriber<? super MailsResponse> subscriber) {
                handleResponse(mAPIService.getMailSent(user_id, page), subscriber);
            }
        });
    }

    @Override
    public Observable<MailFileResponse> getMailFiels(final String mail_id) {
        return Observable.create(new Observable.OnSubscribe<MailFileResponse>() {
            @Override
            public void call(Subscriber<? super MailFileResponse> subscriber) {
                handleResponse(mAPIService.getMailFiels(mail_id), subscriber);
            }
        });
    }

    @Override
    public Observable<SendMailResponse> sendMail(final SendMailRequest mailRequest) {
        return Observable.create(new Observable.OnSubscribe<SendMailResponse>() {
            @Override
            public void call(Subscriber<? super SendMailResponse> subscriber) {
                handleResponse(mAPIService.sendMail(mailRequest), subscriber);
            }
        });
    }

    @Override
    public Observable<UploadMailFileResponse> uploadFile(final MultipartBody.Part image, final String user_id, final String ref) {
        return Observable.create(new Observable.OnSubscribe<UploadMailFileResponse>() {
            @Override
            public void call(Subscriber<? super UploadMailFileResponse> subscriber) {
                handleResponse(mAPIService.uploadFile(image, user_id, ref), subscriber);
            }
        });
    }


    @Override
    public Observable<BaseResponse> removeAttachment(final RemoveMailFileRequest removeMailFileRequest) {
        return Observable.create(new Observable.OnSubscribe<BaseResponse>() {
            @Override
            public void call(Subscriber<? super BaseResponse> subscriber) {
                handleResponse(mAPIService.removeAttachment(removeMailFileRequest), subscriber);
            }
        });
    }

    @Override
    public Observable<MailCountResponse> getNewMails(final String user_id) {
        return Observable.create(new Observable.OnSubscribe<MailCountResponse>() {
            @Override
            public void call(Subscriber<? super MailCountResponse> subscriber) {
                handleResponse(mAPIService.getNewMails(user_id), subscriber);
            }
        });
    }

    @Override
    public Observable<UserMessageResponse> updateNearBy(final UpdateNearByRequest nearByRequest) {
        return Observable.create(new Observable.OnSubscribe<UserMessageResponse>() {
            @Override
            public void call(Subscriber<? super UserMessageResponse> subscriber) {
                handleResponse(mAPIService.updateNearBy(nearByRequest), subscriber);
            }
        });
    }

    @Override
    public Observable<BaseResponse> checkSession(final CheckSessionRequest sessionRequest) {
        return Observable.create(new Observable.OnSubscribe<BaseResponse>() {
            @Override
            public void call(Subscriber<? super BaseResponse> subscriber) {
                handleResponse(mAPIService.checkSession(sessionRequest), subscriber);
            }
        });
    }
}
