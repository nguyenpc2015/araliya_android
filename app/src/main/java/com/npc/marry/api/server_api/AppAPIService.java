package com.npc.marry.api.server_api;

import com.npc.marry.model.UserData;
import com.npc.marry.model.request.AdvanceSearchRequest;
import com.npc.marry.model.request.BasicSearchRequest;
import com.npc.marry.model.request.CheckSessionRequest;
import com.npc.marry.model.request.Complete1Request;
import com.npc.marry.model.request.Complete2Request;
import com.npc.marry.model.request.Complete3Request;
import com.npc.marry.model.request.Completion4Request;
import com.npc.marry.model.request.ContactRequest;
import com.npc.marry.model.request.EmailRequest;
import com.npc.marry.model.request.FireBaseTokenRequest;
import com.npc.marry.model.request.HoroscopeRequest;
import com.npc.marry.model.request.InterestRequest;
import com.npc.marry.model.request.LogoutRequest;
import com.npc.marry.model.request.NearBySearchRequest;
import com.npc.marry.model.request.NewsletterRequest;
import com.npc.marry.model.request.PhotoRequest;
import com.npc.marry.model.request.RemoveMailFileRequest;
import com.npc.marry.model.request.SMSValidateRequest;
import com.npc.marry.model.request.SMSVerifyRequest;
import com.npc.marry.model.request.SaveVideoRequest;
import com.npc.marry.model.request.SendMailRequest;
import com.npc.marry.model.request.UpdateBirthDateRequest;
import com.npc.marry.model.request.UpdateEmailRequest;
import com.npc.marry.model.request.UpdateLocationRequest;
import com.npc.marry.model.request.UpdateNearByRequest;
import com.npc.marry.model.request.UpdatePartnerRequest;
import com.npc.marry.model.request.UpdatePasswordRequest;
import com.npc.marry.model.request.UserLoginRequest;
import com.npc.marry.model.request.UserRequest;
import com.npc.marry.model.request.ValidateRequest;
import com.npc.marry.model.response.AdvanceSearchParamResponse;
import com.npc.marry.model.response.BaseResponse;
import com.npc.marry.model.response.CityResponse;
import com.npc.marry.model.response.ConversationResponse;
import com.npc.marry.model.response.CountryResponse;
import com.npc.marry.model.response.ForgotPasswordResponse;
import com.npc.marry.model.response.HtmlResponse;
import com.npc.marry.model.response.MailCountResponse;
import com.npc.marry.model.response.MailFileResponse;
import com.npc.marry.model.response.MailsResponse;
import com.npc.marry.model.response.MessageResponse;
import com.npc.marry.model.response.NotificationResponse;
import com.npc.marry.model.response.OrientationResponse;
import com.npc.marry.model.response.PartnerParamResponse;
import com.npc.marry.model.response.PaymentPlanResponse;
import com.npc.marry.model.response.PhotoResponse;
import com.npc.marry.model.response.RelationshipResponse;
import com.npc.marry.model.response.SendMailResponse;
import com.npc.marry.model.response.SocialResponse;
import com.npc.marry.model.response.SpecialResponse;
import com.npc.marry.model.response.StateResponse;
import com.npc.marry.model.response.StyleResponse;
import com.npc.marry.model.response.TextResponse;
import com.npc.marry.model.response.UploadMailFileResponse;
import com.npc.marry.model.response.UploadPhotoResponse;
import com.npc.marry.model.response.UserListResponse;
import com.npc.marry.model.response.UserLoginResponse;
import com.npc.marry.model.response.UserMessageResponse;
import com.npc.marry.model.response.UserPhotoResponse;
import com.npc.marry.model.response.UserRegisterResponse;
import com.npc.marry.model.response.UserVideoResponse;
import com.npc.marry.model.response.ValidationResponse;
import com.npc.marry.model.response.VideosResponse;

import okhttp3.MultipartBody;
import rx.Observable;

/**
 * Created by Lenovo on 2/21/2017.
 */

public interface AppAPIService {
    Observable<OrientationResponse> getOrientations();

    Observable<UserRegisterResponse> register(UserRequest request);

    Observable<UserLoginResponse> login(UserLoginRequest userLoginRequest);

    Observable<ValidationResponse> validate(ValidateRequest validateRequest);

    Observable<BaseResponse> requestCode(String user_id);

    Observable<CountryResponse> getCountry();

    Observable<StateResponse> getState(int country_id);

    Observable<CityResponse> getCity(int country_id, int city_id);

    Observable<RelationshipResponse> getRelationship();

    Observable<SpecialResponse> getSpecial();

    Observable<SocialResponse> getSocial();

    Observable<StyleResponse> getStyle();

    Observable<BaseResponse> completeProfile1(Complete1Request complete1Request);

    Observable<BaseResponse> completeProfile2(Complete2Request complete2Request);

    Observable<BaseResponse> completeProfile3(Complete3Request complete3Request);

    Observable<UserRegisterResponse> completeProfile4(Completion4Request completion4Request);

    Observable<BaseResponse> logout(LogoutRequest logoutRequest);

    Observable<UserData> getUser(String user_id);

    Observable<UserMessageResponse> getUserData(String user_id);

    Observable<UserListResponse> getFavourUser(String user_id, String page);

    Observable<UserListResponse> getFeaturedUser(String user_id, String page);

    Observable<UserListResponse> getHoroScopeUser(String user_id, String page);

    Observable<UserListResponse> getNewestUser(String user_id, String page);

    Observable<UserListResponse> getOnlineUser(String user_id, String page);

    Observable<UserListResponse> getRecentUser(String user_id, String page);

    Observable<UserListResponse> getRecommendUser(String user_id, String page);

    Observable<UserListResponse> getVerifiedUser(String user_id, String page);

    Observable<UserListResponse> getMatchUser(String user_id, String page);

    Observable<UserListResponse> search(int page, BasicSearchRequest basicSearchRequest);

    Observable<BaseResponse> sendToken(FireBaseTokenRequest fireBaseTokenRequest);

    Observable<BaseResponse> sendInterest(InterestRequest interestRequest);

    Observable<NotificationResponse> getNotification(String user_id, String page);

    Observable<BaseResponse> updateNotification(String id);

    Observable<PhotoResponse> getPhoto(String user_id, String page);

    Observable<UploadPhotoResponse> uploadPhoto(MultipartBody.Part image, String user_id);

    Observable<BaseResponse> savePhoto(PhotoRequest photoRequest);

    Observable<PaymentPlanResponse> getPaymentPlan(String gender);

    Observable<BaseResponse> addFavorite(InterestRequest interestRequest);

    Observable<BaseResponse> blockUser(InterestRequest interestRequest);

    Observable<UserListResponse> getInterestUser(String user_id, String page);

    Observable<UserListResponse> getInterestReceivedUser(String user_id, String page);

    Observable<UserListResponse> getFavoriteUSer(String user_id, String page);

    Observable<UserListResponse> getBlockUsers(String user_id, String page);

    Observable<BaseResponse> removeInterest(String user_id, String user_remove_id);

    Observable<BaseResponse> removeFavorite(String user_id, String user_remove_id);

    Observable<BaseResponse> removeBlock(String user_id, String user_remove_id);

    Observable<BaseResponse> contactUs(ContactRequest contactRequest);

    Observable<TextResponse> getHeadline(String user_id);

    Observable<BaseResponse> saveHoroscope(String user_id, HoroscopeRequest horoscopeRequest);

    Observable<ConversationResponse> getConversations(String user_id);

    Observable<MessageResponse> getMessages(String user_id, String user_id_1, int page);

    Observable<UploadPhotoResponse> uploadAttachment(MultipartBody.Part file, String user_id);

    Observable<BaseResponse> viewUser(InterestRequest interestRequest);

    Observable<UserListResponse> getUserView(String user_id);

    Observable<UserListResponse> getUserMyView(String user_id, String page);

    Observable<UserListResponse> getUserViewedMe(String user_id, String page);

    Observable<UserListResponse> getUserInterest(String user_id);

    Observable<BaseResponse> updateView(String user_id);

    Observable<BaseResponse> updateInterest(String user_id);

    Observable<VideosResponse> getVideos(String user_id);

    Observable<BaseResponse> deleteVideo(String video_id);

    Observable<BaseResponse> saveVideo(SaveVideoRequest videoRequest);

    Observable<HtmlResponse> getGeneratedCode(String user_id, String item_id);

    Observable<UserListResponse> searchByName(String user_id, String keyword, String page);

    Observable<AdvanceSearchParamResponse> getAdvanceSearchParam();

    Observable<UserListResponse> advanceSearch(AdvanceSearchRequest searchRequest, String page);

    Observable<UserListResponse> nearBySearch(NearBySearchRequest nearBySearchRequest);

    Observable<BaseResponse> deletePhoto(String photo_id);

    Observable<UserVideoResponse> getAllVideos(String user_id, String page);

    Observable<UserPhotoResponse> getAllPhotos(String user_id, String page);

    Observable<ForgotPasswordResponse> forgotPassword(EmailRequest emailRequest);

    Observable<BaseResponse> deleteAccount(LogoutRequest logoutRequest);

    Observable<BaseResponse> updateNewsletter(NewsletterRequest logoutRequest);

    Observable<UserMessageResponse> updateEmail(UpdateEmailRequest emailRequest);

    Observable<UserMessageResponse> updatePassword(UpdatePasswordRequest passwordRequest);

    Observable<UserMessageResponse> updateLocation(UpdateLocationRequest locationRequest);

    Observable<UserMessageResponse> updateBirthDate(UpdateBirthDateRequest birthDateRequest);

    Observable<PartnerParamResponse> getPartnerParam();

    Observable<BaseResponse> verifySMS(SMSVerifyRequest smsVerifyRequest);

    Observable<UserMessageResponse> validateSMS(SMSValidateRequest validateRequest);

    Observable<UserMessageResponse> updatePartner(UpdatePartnerRequest partnerRequest);

    Observable<MailsResponse> getMails(String user_id, String page);

    Observable<MailsResponse> getMailTrash(String user_id, String page);

    Observable<MailsResponse> getMailSent(String user_id, String page);

    Observable<MailFileResponse> getMailFiels(String mail_id);

    Observable<SendMailResponse> sendMail(SendMailRequest mailRequest);

    Observable<UploadMailFileResponse> uploadFile(MultipartBody.Part image, String user_id, String ref);

    Observable<BaseResponse> removeAttachment(RemoveMailFileRequest removeMailFileRequest);

    Observable<MailCountResponse> getNewMails(String user_id);

    Observable<UserMessageResponse> updateNearBy(UpdateNearByRequest nearByRequest);

    Observable<BaseResponse> checkSession(CheckSessionRequest sessionRequest);
}
