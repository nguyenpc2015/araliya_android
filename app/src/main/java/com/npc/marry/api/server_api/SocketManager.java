package com.npc.marry.api.server_api;

import android.util.Log;

import com.npc.marry.app.AppSetting;
import com.npc.marry.app.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import io.socket.client.IO;
import io.socket.client.Manager;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import io.socket.engineio.client.Transport;

/**
 * Created by Lenovo on 4/25/2017.
 */

public class SocketManager {
    private static final SocketManager instance = new SocketManager();

    MessageListener messageListener;
    private Socket mSocket;

    private SocketManager() {
    }

    public static SocketManager getInstance() {
        return instance;
    }

    public void setMessageListener(MessageListener messageListener) {
        this.messageListener = messageListener;
    }

    public void init() {
        try {
            mSocket = IO.socket(Constants.SOCKET_URL + ":" + Constants.SOCKET_PORT);
            mSocket.io().on(Manager.EVENT_TRANSPORT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Transport transport = (Transport) args[0];

                    transport.on(Transport.EVENT_REQUEST_HEADERS, new Emitter.Listener() {
                        @Override
                        public void call(Object... args) {
                            @SuppressWarnings("unchecked")
                            Map<String, List<String>> headers = (Map<String, List<String>>) args[0];
                            // modify request headers
                            headers.put("Cookie", Arrays.asList("PHPSESSID=" + AppSetting.getInstance().getUser().session_id + ";"));
                            Log.d("TAG", "call: " + headers.toString());
                        }
                    });
                }
            });
            mSocket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    //Log.d("TAG" , "call: " + args[0]);
                    mSocket.emit("get_online_info", 1);
                    if (messageListener != null) {
                        messageListener.onConnected();
                    }
                }
            });
            mSocket.on("authenticated", new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Log.d("TAG", "call: " + args[0].toString());
                }
            });
            mSocket.on("msg", new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Log.d("TAG", "call: msg " + args[0].toString());
                    if (messageListener != null) {
                        if (args[0] instanceof JSONObject)
                            messageListener.onMsg((JSONObject) (args[0]));
                    }

                }
            });
            mSocket.on("sent", new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Log.d("TAG", "call: sent " + args[0].toString());
                    if (messageListener != null) {
                        if (args[0] instanceof JSONObject)
                            messageListener.onSent((JSONObject) (args[0]));
                    }
                }
            });
            mSocket.on("typing", new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    if (messageListener != null) {
                        messageListener.onTyping((JSONObject) (args[0]));
                    }
                }
            });
            mSocket.on("online_info", new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    if (messageListener != null) {
                        if (args[0] instanceof JSONObject) {
                            messageListener.onStatus((JSONObject) (args[0]));
                        } else {
                            messageListener.onStatus((boolean) (args[0]));
                        }
                    }
                }
            });
            if (!mSocket.connected()) {
                mSocket.connect();
            }

        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public void disconnect() {
        mSocket.disconnect();
    }

    public void sendMessage(String msg, String to) {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("msg", msg);
            jsonObject.put("to", to);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mSocket.emit("send", jsonObject);
    }

    public interface MessageListener {
        void onConnected();

        void onMsg(JSONObject messageEntry);

        void onSent(JSONObject messageEntry);

        void onTyping(JSONObject messageEntry);

        void onStatus(JSONObject status);

        void onStatus(boolean status);
    }
}
