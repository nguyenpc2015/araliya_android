package com.npc.marry.api.retrofit;


import com.npc.marry.model.UserData;
import com.npc.marry.model.request.AdvanceSearchRequest;
import com.npc.marry.model.request.BasicSearchRequest;
import com.npc.marry.model.request.CheckSessionRequest;
import com.npc.marry.model.request.Complete1Request;
import com.npc.marry.model.request.Complete2Request;
import com.npc.marry.model.request.Complete3Request;
import com.npc.marry.model.request.Completion4Request;
import com.npc.marry.model.request.ContactRequest;
import com.npc.marry.model.request.EmailRequest;
import com.npc.marry.model.request.FireBaseTokenRequest;
import com.npc.marry.model.request.HoroscopeRequest;
import com.npc.marry.model.request.InterestRequest;
import com.npc.marry.model.request.LogoutRequest;
import com.npc.marry.model.request.NearBySearchRequest;
import com.npc.marry.model.request.NewPasswordRequest;
import com.npc.marry.model.request.NewsletterRequest;
import com.npc.marry.model.request.PhotoRequest;
import com.npc.marry.model.request.RemoveMailFileRequest;
import com.npc.marry.model.request.SMSValidateRequest;
import com.npc.marry.model.request.SMSVerifyRequest;
import com.npc.marry.model.request.SaveVideoRequest;
import com.npc.marry.model.request.SecretCodeRequest;
import com.npc.marry.model.request.SendMailRequest;
import com.npc.marry.model.request.UpdateBirthDateRequest;
import com.npc.marry.model.request.UpdateEmailRequest;
import com.npc.marry.model.request.UpdateLocationRequest;
import com.npc.marry.model.request.UpdateNearByRequest;
import com.npc.marry.model.request.UpdatePartnerRequest;
import com.npc.marry.model.request.UpdatePasswordRequest;
import com.npc.marry.model.request.UserLoginRequest;
import com.npc.marry.model.request.UserRequest;
import com.npc.marry.model.request.ValidateRequest;
import com.npc.marry.model.response.AdvanceSearchParamResponse;
import com.npc.marry.model.response.BaseResponse;
import com.npc.marry.model.response.CityResponse;
import com.npc.marry.model.response.ConversationResponse;
import com.npc.marry.model.response.CountryResponse;
import com.npc.marry.model.response.ForgotPasswordConfirmResponse;
import com.npc.marry.model.response.ForgotPasswordResponse;
import com.npc.marry.model.response.HtmlResponse;
import com.npc.marry.model.response.MailCountResponse;
import com.npc.marry.model.response.MailFileResponse;
import com.npc.marry.model.response.MailsResponse;
import com.npc.marry.model.response.MessageResponse;
import com.npc.marry.model.response.NotificationResponse;
import com.npc.marry.model.response.OrientationResponse;
import com.npc.marry.model.response.PartnerParamResponse;
import com.npc.marry.model.response.PaymentPlanResponse;
import com.npc.marry.model.response.PhotoResponse;
import com.npc.marry.model.response.RelationshipResponse;
import com.npc.marry.model.response.SendMailResponse;
import com.npc.marry.model.response.SocialResponse;
import com.npc.marry.model.response.SpecialResponse;
import com.npc.marry.model.response.StateResponse;
import com.npc.marry.model.response.StyleResponse;
import com.npc.marry.model.response.TextResponse;
import com.npc.marry.model.response.UploadMailFileResponse;
import com.npc.marry.model.response.UploadPhotoResponse;
import com.npc.marry.model.response.UserListResponse;
import com.npc.marry.model.response.UserLoginResponse;
import com.npc.marry.model.response.UserMessageResponse;
import com.npc.marry.model.response.UserPhotoResponse;
import com.npc.marry.model.response.UserRegisterResponse;
import com.npc.marry.model.response.UserVideoResponse;
import com.npc.marry.model.response.ValidationResponse;
import com.npc.marry.model.response.VideosResponse;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface APIServices {

    @POST("/login")
    Call<UserLoginResponse> login(@Body UserLoginRequest userLoginRequest);

    @POST("/register")
    Call<UserRegisterResponse> register(@Body UserRequest userRequest);

    @GET("/orientation")
    Call<OrientationResponse> getOrientations();

    @POST("/validate")
    Call<ValidationResponse> validate(@Body ValidateRequest validateRequest);

    @GET("/validate/{user_id}")
    Call<BaseResponse> requestCode(@Path("user_id") String user_id);

    @GET("/country")
    Call<CountryResponse> getCountry();

    @GET("/state/{country_id}")
    Call<StateResponse> getState(@Path("country_id") int country_id);

    @GET("/city/{country_id}/{state_id}")
    Call<CityResponse> getCity(@Path("country_id") int country_id, @Path("state_id") int state_id);

    @GET("/relationship")
    Call<RelationshipResponse> getRelationship();

    @GET("/special")
    Call<SpecialResponse> getSpecial();

    @GET("/social")
    Call<SocialResponse> getSocial();

    @GET("/style")
    Call<StyleResponse> getStyle();

    @POST("/user/complete1")
    Call<BaseResponse> completeProfile1(@Body Complete1Request complete1Request);

    @POST("/user/complete2")
    Call<BaseResponse> completeProfile2(@Body Complete2Request complete2Request);

    @POST("/user/complete3")
    Call<BaseResponse> completeProfile3(@Body Complete3Request complete3Request);

    @POST("/user/complete4")
    Call<UserRegisterResponse> completeProfile4(@Body Completion4Request complete4Request);

    @POST("/logout")
    Call<BaseResponse> logout(@Body LogoutRequest logoutRequest);

    @GET("/user/{user_id}")
    Call<UserData> getUser(@Path("user_id") String user_id);

    @GET("/get_user/{userid}")
    Call<UserMessageResponse> getUserData(@Path("userid") String user_id);

    @GET("/favour_user/{user_id}/{page}")
    Call<UserListResponse> getFavourUser(@Path("user_id") String user_id, @Path("page") String page);

    @GET("/feature_user/{user_id}/{page}")
    Call<UserListResponse> getFeaturedUser(@Path("user_id") String user_id, @Path("page") String page);

    @GET("/horoscope_user/{user_id}/{page}")
    Call<UserListResponse> getHoroScopeUser(@Path("user_id") String user_id, @Path("page") String page);

    @GET("/newest_user/{user_id}/{page}")
    Call<UserListResponse> getNewestUser(@Path("user_id") String user_id, @Path("page") String page);

    @GET("/online_user/{user_id}/{page}")
    Call<UserListResponse> getOnlineUser(@Path("user_id") String user_id, @Path("page") String page);

    @GET("/recent_user/{user_id}/{page}")
    Call<UserListResponse> getRecentUser(@Path("user_id") String user_id, @Path("page") String page);

    @GET("/recommend_user/{user_id}/{page}")
    Call<UserListResponse> getRecommendUser(@Path("user_id") String user_id, @Path("page") String page);

    @GET("/verified_user/{user_id}/{page}")
    Call<UserListResponse> getVerifiedUser(@Path("user_id") String user_id, @Path("page") String page);

    @GET("/match/{user_id}/{page}")
    Call<UserListResponse> getMatchUser(@Path("user_id") String user_id, @Path("page") String page);

    @GET("/interest_user/{user_id}/{page}")
    Call<UserListResponse> getInterestUser(@Path("user_id") String user_id, @Path("page") String page);

    @GET("/interest_user_recv/{user_id}/{page}")
    Call<UserListResponse> getInterestReceivedUser(@Path("user_id") String user_id, @Path("page") String page);

    @DELETE("/interest_user/{user_id}/{user_remove_id}")
    Call<BaseResponse> removeInterest(@Path("user_id") String user_id, @Path("user_remove_id") String user_remove_id);

    @GET("/favorite_user/{user_id}/{page}")
    Call<UserListResponse> getFavoriteUSer(@Path("user_id") String user_id, @Path("page") String page);

    @DELETE("/favorite_user/{user_id}/{user_remove_id}")
    Call<BaseResponse> removeFavorite(@Path("user_id") String user_id, @Path("user_remove_id") String user_remove_id);

    @GET("/block_user/{user_id}/{page}")
    Call<UserListResponse> getBlockUsers(@Path("user_id") String user_id, @Path("page") String page);

    @DELETE("/block_user/{user_id}/{user_remove_id}")
    Call<BaseResponse> removeBlock(@Path("user_id") String user_id, @Path("user_remove_id") String user_remove_id);

    @POST("/search/{page}")
    Call<UserListResponse> search(@Path("page") String page, @Body BasicSearchRequest basicSearchRequest);

    @GET("/advance_search_param")
    Call<AdvanceSearchParamResponse> getAdvanceSearchParam();

    @POST("/advance_search/{page}")
    Call<UserListResponse> advanceSearch(@Body AdvanceSearchRequest searchRequest, @Path("page") String page);

    @POST("/near_by_search")
    Call<UserListResponse> nearBySearch(@Body NearBySearchRequest nearBySearchRequest);

    @POST("/send_token")
    Call<BaseResponse> sendToken(@Body FireBaseTokenRequest fireBaseTokenRequest);

    @POST("/send_interest")
    Call<BaseResponse> sendInterest(@Body InterestRequest interestRequest);

    @GET("/notifications/{user_id}/{page}")
    Call<NotificationResponse> getNotification(@Path("user_id") String user_id, @Path("page") String page);

    @PUT("/notification/{id}")
    Call<BaseResponse> updateNotification(@Path("id") String id);

    @GET("/photo/{user_id}/{page}")
    Call<PhotoResponse> getPhoto(@Path("user_id") String user_id, @Path("page") String page);

    @Multipart
    @POST("/upload/{user_id}")
    Call<UploadPhotoResponse> uploadPhoto(@Part MultipartBody.Part image, @Path("user_id") String user_id);

    @POST("/save_photo")
    Call<BaseResponse> savePhoto(@Body PhotoRequest photoRequest);

    @GET("/payment_plan/{gender}")
    Call<PaymentPlanResponse> getPaymentPlan(@Path("gender") String gender);

    @POST("/add_favorite")
    Call<BaseResponse> addFavorite(@Body InterestRequest interestRequest);

    @POST("/block")
    Call<BaseResponse> blockUser(@Body InterestRequest interestRequest);

    @POST("/contact_us")
    Call<BaseResponse> contactUs(@Body ContactRequest contactRequest);

    @GET("/headline/{user_id}")
    Call<TextResponse> getHeadline(@Path("user_id") String user_id);

    @POST("/horoscope/{user_id}")
    Call<BaseResponse> saveHoroscope(@Path("user_id") String user_id, @Body HoroscopeRequest horoscopeRequest);

    @GET("/conversation/{user_id}")
    Call<ConversationResponse> getConversations(@Path("user_id") String user_id);

    @GET("/conversation")
    Call<MessageResponse> getMessages(@Query("user_id") String user_id, @Query("user_id_1") String user_id_1, @Query("page") int page);

    @Multipart
    @POST("/files/upload/{user_id}")
    Call<UploadPhotoResponse> uploadAttachment(@Part MultipartBody.Part file, @Path("user_id") String user_id);

    @POST("/user/view")
    Call<BaseResponse> viewUser(@Body InterestRequest interestRequest);

    @GET("/user/view/{user_id}")
    Call<UserListResponse> getUserView(@Path("user_id") String user_id);

    @GET("/user/my_view/{user_id}/{page}")
    Call<UserListResponse> getUserMyView(@Path("user_id") String user_id, @Path("page") String page);

    @GET("/user/viewed_me/{user_id}/{page}")
    Call<UserListResponse> getUserViewedMe(@Path("user_id") String user_id, @Path("page") String page);

    @GET("/user/interest/{user_id}")
    Call<UserListResponse> getUserInterest(@Path("user_id") String user_id);

    @PUT("/user/view/{user_id}")
    Call<BaseResponse> updateView(@Path("user_id") String user_id);

    @PUT("/user/interest/{user_id}")
    Call<BaseResponse> updateInterest(@Path("user_id") String user_id);

    @GET("/payment/generate/{user_id}/{item_id}")
    Call<HtmlResponse> getGeneratedCode(@Path("user_id") String user_id, @Path("item_id") String item_id);

    @GET("/video/{user_id}")
    Call<VideosResponse> getVideos(@Path("user_id") String user_id);

    @DELETE("/video/{video_id}")
    Call<BaseResponse> deleteVideo(@Path("video_id") String video_id);

    @POST("/save_video")
    Call<BaseResponse> saveVideo(@Body SaveVideoRequest videoRequest);

    @GET("/search/{user_id}/{keyword}/{page}")
    Call<UserListResponse> searchByName(@Path("user_id") String user_id, @Path("keyword") String keyword, @Path("page") String page);

    @DELETE("/photo/{photo_id}")
    Call<BaseResponse> deletePhoto(@Path("photo_id") String photo_id);

    @GET("/videos/{user_id}/{page}")
    Call<UserVideoResponse> getAllVideos(@Path("user_id") String user_id, @Path("page") String page);

    @GET("/photos/{user_id}/{page}")
    Call<UserPhotoResponse> getAllPhotos(@Path("user_id") String user_id, @Path("page") String page);

    @POST("/forgot_password")
    Call<ForgotPasswordResponse> forgotPassword(@Body EmailRequest emailRequest);

    @POST("/forgot_password_confirm")
    Call<ForgotPasswordConfirmResponse> forgotPasswordConfirm(@Body SecretCodeRequest codeRequest);

    @POST("/forgot_password_update")
    Call<BaseResponse> updatePassword(@Body NewPasswordRequest passwordRequest);

    @POST("/delete")
    Call<BaseResponse> deleteAccount(@Body LogoutRequest logoutRequest);

    @POST("/update_newsleter")
    Call<BaseResponse> updateNewsletter(@Body NewsletterRequest logoutRequest);

    @POST("/update_email")
    Call<UserMessageResponse> updateEmail(@Body UpdateEmailRequest emailRequest);

    @POST("/update_password")
    Call<UserMessageResponse> updatePassword(@Body UpdatePasswordRequest passwordRequest);

    @POST("/update_location")
    Call<UserMessageResponse> updateLocation(@Body UpdateLocationRequest locationRequest);

    @POST("update_birthdate")
    Call<UserMessageResponse> updateBirthDate(@Body UpdateBirthDateRequest birthDateRequest);

    @GET("/get_partner_param")
    Call<PartnerParamResponse> getPartnerParam();

    @POST("/sns_verify")
    Call<BaseResponse> verifySMS(@Body SMSVerifyRequest smsVerifyRequest);

    @POST("/sns_validate")
    Call<UserMessageResponse> validateSMS(@Body SMSValidateRequest validateRequest);

    @POST("/update_partner")
    Call<UserMessageResponse> updatePartner(@Body UpdatePartnerRequest partnerRequest);

    @GET("/mail/{user_id}/{page}")
    Call<MailsResponse> getMails(@Path("user_id") String user_id, @Path("page") String page);

    @GET("/mail_trash/{user_id}/{page}")
    Call<MailsResponse> getMailTrash(@Path("user_id") String user_id, @Path("page") String page);

    @GET("/mail_sent/{user_id}/{page}")
    Call<MailsResponse> getMailSent(@Path("user_id") String user_id, @Path("page") String page);

    @GET("/mail_file/{mail_id}")
    Call<MailFileResponse> getMailFiels(@Path("mail_id") String mail_id);

    @POST("/send_mail")
    Call<SendMailResponse> sendMail(@Body SendMailRequest mailRequest);

    @Multipart
    @POST("/send_file/{user_id}/{ref}")
    Call<UploadMailFileResponse> uploadFile(@Part MultipartBody.Part image, @Path("user_id") String user_id, @Path("ref") String ref);

    @POST("/remove_attachment")
    Call<BaseResponse> removeAttachment(@Body RemoveMailFileRequest removeMailFileRequest);

    @GET("/mail_count/{user_id}")
    Call<MailCountResponse> getNewMails(@Path("user_id") String user_id);

    @POST("/update_near_by_option")
    Call<UserMessageResponse> updateNearBy(@Body UpdateNearByRequest nearByRequest);

    @POST("/check_session")
    Call<BaseResponse> checkSession(@Body CheckSessionRequest sessionRequest);

}