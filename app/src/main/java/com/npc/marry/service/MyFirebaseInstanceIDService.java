package com.npc.marry.service;

import android.content.Intent;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.npc.marry.api.server_api.AppServicesImpl;
import com.npc.marry.app.AppSetting;
import com.npc.marry.model.request.FireBaseTokenRequest;
import com.npc.marry.model.response.BaseResponse;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Lenovo on 3/11/2017.
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        sendRegistrationToServer(refreshedToken);
    }

    void sendRegistrationToServer(String refreshedToken) {
        if (AppSetting.getInstance().getUser() != null) {
            AppServicesImpl.getInstance(this).sendToken(new FireBaseTokenRequest(Integer.valueOf(AppSetting.getInstance().getUser().user_id), refreshedToken))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<BaseResponse>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {
                            Intent intent = new Intent("com.result.notification");
                            intent.putExtra("result", false);
                            getApplicationContext().sendBroadcast(intent);
                        }

                        @Override
                        public void onNext(BaseResponse baseResponse) {
                            Intent intent = new Intent("com.result.notification");
                            intent.putExtra("result", true);
                            getApplicationContext().sendBroadcast(intent);
                        }
                    });
        }

    }

}
