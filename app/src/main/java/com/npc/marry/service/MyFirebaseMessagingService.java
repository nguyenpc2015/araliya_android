package com.npc.marry.service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.npc.marry.R;
import com.npc.marry.model.Notification;
import com.npc.marry.ui.profile.ProfileActivity;

import org.parceler.Parcels;

import java.util.Map;

/**
 * Created by Lenovo on 3/11/2017.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Map<String, String> data = remoteMessage.getData();
        Notification notification = new Notification();
        notification.setId(Integer.valueOf(data.get("id")));
        notification.setContent(data.get("content"));
        notification.setDate(data.get("date"));
        notification.setPhoto(data.get("photo"));
        notification.setUser_id_from(Integer.valueOf(data.get("user_id_from")));

        Intent intent = new Intent("com.user.notification");
        intent.putExtra("data", Parcels.wrap(notification));
        getApplicationContext().sendBroadcast(intent);
        showNotification(data.get("content"), data.get("user_id_from"));
    }

    private void showNotification(String msg, String user_id) {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.icon) // notification icon
                .setContentTitle(getString(R.string.app_name)) // title for notification
                .setContentText(msg) // message for notification
                .setAutoCancel(true); // clear notification after click
        Intent intent = new Intent(this, ProfileActivity.class);
        intent.putExtra("data", user_id);
        PendingIntent pi = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(pi);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(0, mBuilder.build());
    }
}
