package com.npc.marry.utils;

import android.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by Lenovo on 7/13/2017.
 */

public class TextUtils {
    Cipher ecipher;
    Cipher dcipher;
    byte[] salt = new byte[8];
    int iterationCount = 200;

    public TextUtils(String passPhrase) {
        try {
            // generate a random salt

            // Create the key
            SecretKey keySpec = new SecretKeySpec(passPhrase.getBytes(), "AES");
            dcipher = Cipher.getInstance("AES/ECB/PKCS7Padding", "BC");
            ecipher = Cipher.getInstance("AES/ECB/PKCS7Padding", "BC");
            // Create the ciphers
            dcipher.init(Cipher.DECRYPT_MODE, keySpec);
            ecipher.init(Cipher.ENCRYPT_MODE, keySpec);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String encrypt(String str) {
        try {
            // Encode the string into bytes using utf-8
            byte[] utf8 = str.getBytes("UTF8");

            // Encrypt
            byte[] enc = ecipher.doFinal(utf8);

            // Encode bytes to base64 to get a string
            return Base64.encodeToString(enc, Base64.DEFAULT);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public String decrypt(byte[] dec) {
        try {

            // Decrypt
            byte[] utf8 = dcipher.doFinal(dec);

            // Decode using utf-8
            return new String(utf8, "UTF8");
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public int getIterationCount() {
        return iterationCount;
    }

    public String getSalt() {
        return Base64.encodeToString(salt, Base64.DEFAULT);
    }
}
