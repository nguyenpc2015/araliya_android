package com.npc.marry.ui.account;

import android.content.Context;

import com.npc.marry.api.server_api.AppServicesImpl;
import com.npc.marry.app.AppSetting;
import com.npc.marry.base.BasePresenter;
import com.npc.marry.model.request.UpdateBirthDateRequest;
import com.npc.marry.model.request.UpdateEmailRequest;
import com.npc.marry.model.request.UpdateLocationRequest;
import com.npc.marry.model.request.UpdatePasswordRequest;
import com.npc.marry.model.response.CityResponse;
import com.npc.marry.model.response.CountryResponse;
import com.npc.marry.model.response.StateResponse;
import com.npc.marry.model.response.UserMessageResponse;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by nguyen tran on 2/17/2018.
 */

public class EditAccountPresenter extends BasePresenter<EditAccountView> {
    AppSetting appSetting;
    AppServicesImpl appServices;

    public EditAccountPresenter(EditAccountView view, Context context, CompositeSubscription compositeSubscription) {
        super(view, compositeSubscription);
        appServices = AppServicesImpl.getInstance(context);
        appSetting = AppSetting.getInstance();
    }

    public void getCountry() {
        compositeSubscription.add(appServices.getCountry()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CountryResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(CountryResponse countryResponse) {
                        if (view != null) {
                            view.onGetCountrySuccuess(countryResponse.countries);
                        }
                    }
                }));
    }

    public void getState(int country_id) {
        compositeSubscription.add(appServices.getState(country_id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<StateResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(StateResponse stateResponse) {
                        if (view != null) {
                            view.onGetStateSuccess(stateResponse.states);
                        }
                    }
                }));
    }

    public void getCity(int country_id, int city_id) {
        compositeSubscription.add(appServices.getCity(country_id, city_id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CityResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(CityResponse cityResponse) {
                        if (view != null) {
                            view.onGetCitySuccess(cityResponse.cities);
                        }
                    }
                }));
    }

    public void updateEmail(String email) {
        compositeSubscription.add(
                appServices.updateEmail(new UpdateEmailRequest(Integer.valueOf(AppSetting.getInstance().getUser().user_id), email))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<UserMessageResponse>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable throwable) {
                                if (view != null) {
                                    view.onError(throwable.getMessage());
                                }
                            }

                            @Override
                            public void onNext(UserMessageResponse baseResponse) {
                                AppSetting.getInstance().setUser(baseResponse.user);
                                if (view != null) {
                                    view.onUpdateEmailSuccess(baseResponse.message);
                                }
                            }
                        })
        );
    }

    public void updateLocation(int country_id, int state_id, int city_id) {
        compositeSubscription.add(
                appServices.updateLocation(new UpdateLocationRequest(Integer.valueOf(AppSetting.getInstance().getUser().user_id), country_id, state_id, city_id))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<UserMessageResponse>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable throwable) {
                                if (view != null) {
                                    view.onError(throwable.getMessage());
                                }
                            }

                            @Override
                            public void onNext(UserMessageResponse baseResponse) {
                                AppSetting.getInstance().setUser(baseResponse.user);
                                if (view != null) {
                                    view.onUpdateLocationSuccess(baseResponse.message);
                                }
                            }
                        })
        );
    }

    public void updatePassword(String password) {
        compositeSubscription.add(
                appServices.updatePassword(new UpdatePasswordRequest(password, Integer.valueOf(AppSetting.getInstance().getUser().user_id)))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<UserMessageResponse>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable throwable) {
                                if (view != null) {
                                    view.onError(throwable.getMessage());
                                }
                            }

                            @Override
                            public void onNext(UserMessageResponse baseResponse) {
                                AppSetting.getInstance().setUser(baseResponse.user);
                                if (view != null) {
                                    view.onUpdatePasswordSuccess(baseResponse.message);
                                }
                            }
                        })
        );
    }

    public void updateBirthdate(String birthdate) {
        compositeSubscription.add(
                appServices.updateBirthDate(new UpdateBirthDateRequest(Integer.valueOf(AppSetting.getInstance().getUser().user_id), birthdate))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<UserMessageResponse>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable throwable) {
                                if (view != null) {
                                    view.onError(throwable.getMessage());
                                }
                            }

                            @Override
                            public void onNext(UserMessageResponse userMessageResponse) {
                                AppSetting.getInstance().setUser(userMessageResponse.user);
                                if (view != null) {
                                    view.onUpdateBirthdateSuccess(userMessageResponse.message);
                                }
                            }
                        })
        );
    }
}
