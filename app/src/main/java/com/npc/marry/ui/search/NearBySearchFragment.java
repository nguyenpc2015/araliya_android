package com.npc.marry.ui.search;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.VisibleRegion;
import com.npc.marry.R;
import com.npc.marry.app.AppSetting;
import com.npc.marry.base.BaseActivity;
import com.npc.marry.base.BaseFragment;
import com.npc.marry.model.City;
import com.npc.marry.model.Country;
import com.npc.marry.model.State;
import com.npc.marry.model.UserData;
import com.npc.marry.ui.custom.CircleTransform;
import com.npc.marry.ui.main.MainActivity;
import com.npc.marry.ui.profile.ProfileActivity;
import com.npc.marry.utils.UIUtils;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by nguyen tran on 2/19/2018.
 */

public class NearBySearchFragment extends BaseFragment implements SearchView, OnMapReadyCallback, LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, GoogleMap.OnMarkerClickListener {

    private static final long INTERVAL = 1000 * 10;
    private static final long FASTEST_INTERVAL = 1000 * 5;
    @BindView(R.id.cbCheck)
    CheckBox cbCheck;
    LocationManager locationManager;
    NearBySearchPresenter presenter;
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    Location mCurrentLocation;
    String mLastUpdateTime;

    double latt, longt;

    GoogleMap googleMap;

    boolean searched = false;
    private BroadcastReceiver locationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            boolean success = intent.getBooleanExtra("status", false);
            if (success) {
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    com.google.android.gms.common.api.PendingResult<Status> pendingResult = LocationServices.FusedLocationApi.requestLocationUpdates(
                            mGoogleApiClient, mLocationRequest, NearBySearchFragment.this);
                }

            } else {
                Toast.makeText(getContext(), "App doesnt have permission to continue!...", Toast.LENGTH_SHORT).show();
            }
        }
    };
    private int i;

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createLocationRequest();
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        latt = AppSetting.getInstance().getUserData().latt;
        longt = AppSetting.getInstance().getUserData().longt;
        presenter = new NearBySearchPresenter(this, compositeSubscription, getContext());
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().getApplicationContext().registerReceiver(locationReceiver, new IntentFilter("location.search"));

    }

    protected void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, MainActivity.GPS_PERMISSION_REQUEST);
        } else {
            PendingResult<Status> pendingResult = LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search_nearby, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        cbCheck.setChecked(AppSetting.getInstance().getUser().near_by == 1);
        cbCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                presenter.updateNearBy(cbCheck.isChecked() ? 1 : 0);
            }
        });
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().getApplicationContext().unregisterReceiver(locationReceiver);
    }

    @Override
    public void onError(String msg) {

    }

    @Override
    public void onGetCountrySuccuess(List<Country> countries) {

    }

    @Override
    public void onGetStateSuccess(List<State> states) {

    }

    @Override
    public void onGetCitySuccess(List<City> cities) {

    }

    @Override
    public void onGetSearchResult(final List<UserData> userDatas, int totalPage) {
        ((BaseActivity) getActivity()).dismissProgressDialog();
        googleMap.clear();

        for (i = 0; i < userDatas.size(); i++) {
            final UserData userData = userDatas.get(i);
            if (!userData.gender.equals(AppSetting.getInstance().getUser().gender)) {
                Picasso.with(getContext()).load(userData.default_photo).resize(UIUtils.dpToPx(getContext(), 70), UIUtils.dpToPx(getContext(), 70)).transform(new CircleTransform(userData.name)).into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        LatLng pos = new LatLng(userData.latt, userData.longt);
                        MarkerOptions markerOptions = new MarkerOptions().position(pos)
                                .title(userData.name).icon(BitmapDescriptorFactory.fromBitmap(bitmap));
                        Marker marker = googleMap.addMarker(markerOptions);
                        //marker.showInfoWindow();
                        marker.setTag(userData);
                    }

                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {

                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {

                    }
                });

            }
        }
    }

    @Override
    public void onGetSearchResultNext(List<UserData> userDatas, int totalPage) {

    }

    @Override
    public void onUpdateNearByOption(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        searched = false;
        this.googleMap = googleMap;
        googleMap.setOnMarkerClickListener(this);
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latt, longt), 14));
        this.googleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
                if (latt != 0 && longt != 0) {
                    float[] results = new float[1];
                    Location.distanceBetween(latt, longt,
                            googleMap.getCameraPosition().target.latitude, googleMap.getCameraPosition().target.longitude, results);
                    presenter.nearBySearch(latt, longt, results[0] + calculateVisibleRadius(googleMap.getProjection().getVisibleRegion()));
                }
            }
        });
        mGoogleApiClient.connect();
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (mGoogleApiClient.isConnected()) {
            startLocationUpdates();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            searched = true;
            latt = location.getLatitude();
            longt = location.getLongitude();
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latt, longt), 14));
            presenter.nearBySearch(latt, longt, calculateVisibleRadius(googleMap.getProjection().getVisibleRegion()));
            stopLocationUpdates();
        }
    }

    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient, this);
    }

    private double calculateVisibleRadius(VisibleRegion visibleRegion) {
        float[] distanceWidth = new float[1];
        float[] distanceHeight = new float[1];

        LatLng farRight = visibleRegion.farRight;
        LatLng farLeft = visibleRegion.farLeft;
        LatLng nearRight = visibleRegion.nearRight;
        LatLng nearLeft = visibleRegion.nearLeft;

        //calculate the distance width (left <-> right of map on screen)
        Location.distanceBetween(
                (farLeft.latitude + nearLeft.latitude) / 2,
                farLeft.longitude,
                (farRight.latitude + nearRight.latitude) / 2,
                farRight.longitude,
                distanceWidth
        );

        //calculate the distance height (top <-> bottom of map on screen)
        Location.distanceBetween(
                farRight.latitude,
                (farRight.longitude + farLeft.longitude) / 2,
                nearRight.latitude,
                (nearRight.longitude + nearLeft.longitude) / 2,
                distanceHeight
        );

        //visible radius is (smaller distance) / 2:
        return (distanceWidth[0] < distanceHeight[0]) ? distanceWidth[0] / 2 : distanceHeight[0] / 2;
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        UserData userData = (UserData) marker.getTag();
        if (userData != null) {
            ((BaseActivity) getActivity()).startActivity(ProfileActivity.class, userData.user_id, false);
        }
        return false;
    }
}
