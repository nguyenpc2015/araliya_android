package com.npc.marry.ui.partner.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.npc.marry.R;
import com.npc.marry.app.App;
import com.npc.marry.app.AppSetting;
import com.npc.marry.model.MultiSelect;
import com.npc.marry.model.User;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by nguyen tran on 2/17/2018.
 */

public class StyleCheckPartnerAdapter extends RecyclerView.Adapter<StyleCheckPartnerAdapter.StyleCheckPartnerHolder> {

    String tag;
    List<MultiSelect.MultiWrapper> multiWrapperList;

    public StyleCheckPartnerAdapter() {
        multiWrapperList = new ArrayList<>();
    }

    public void setMultiWrapperList(List<MultiSelect.MultiWrapper> multiWrapperList) {
        this.multiWrapperList = multiWrapperList;
        notifyDataSetChanged();
    }


    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public List<Integer> getParamResult() {
        List<Integer> integers = new ArrayList<>();
        for (MultiSelect.MultiWrapper wrapper : multiWrapperList) {
            if (wrapper.checked) {
                integers.add(wrapper.id);
            }
        }
        return integers;
    }

    @Override
    public StyleCheckPartnerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new StyleCheckPartnerHolder(LayoutInflater.from(App.getAppContext()).inflate(R.layout.item_check_partner_style, parent, false));
    }

    @Override
    public void onBindViewHolder(StyleCheckPartnerHolder holder, int position) {
        holder.bind(multiWrapperList.get(position), position);
    }

    @Override
    public int getItemCount() {
        return multiWrapperList.size();
    }

    class StyleCheckPartnerHolder extends RecyclerView.ViewHolder implements CompoundButton.OnCheckedChangeListener {

        @BindView(R.id.cbCheck)
        CheckBox cbCheck;
        int position;

        public StyleCheckPartnerHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            cbCheck.setOnCheckedChangeListener(this);
            position = 0;
        }

        public void bind(MultiSelect.MultiWrapper wrapper, int position) {
            this.position = position;
            cbCheck.setText(wrapper.title);
            cbCheck.setTag(wrapper);
            User user = AppSetting.getInstance().getUser();
            if (wrapper instanceof MultiSelect.PEducation) {
                if ((user.p_education & (1 << (wrapper.id - 1))) > 0) {
                    cbCheck.setChecked(true);
                } else {
                    cbCheck.setChecked(false);
                }
            } else if (wrapper instanceof MultiSelect.PBody) {
                if ((user.p_body & (1 << (wrapper.id - 1))) > 0) {
                    cbCheck.setChecked(true);
                } else {
                    cbCheck.setChecked(false);
                }
            } else if (wrapper instanceof MultiSelect.PEthnicity) {
                if ((user.p_ethnicity & (1 << (wrapper.id - 1))) > 0) {
                    cbCheck.setChecked(true);
                } else {
                    cbCheck.setChecked(false);
                }
            } else if (wrapper instanceof MultiSelect.PReligion) {
                if ((user.p_religion & (1 << (wrapper.id - 1))) > 0) {
                    cbCheck.setChecked(true);
                } else {
                    cbCheck.setChecked(false);
                }
            } else if (wrapper instanceof MultiSelect.PFamily) {
                if ((user.p_family & (1 << (wrapper.id - 1))) > 0) {
                    cbCheck.setChecked(true);
                } else {
                    cbCheck.setChecked(false);
                }
            } else if (wrapper instanceof MultiSelect.PCareer) {
                if ((user.p_career & (1 << (wrapper.id - 1))) > 0) {
                    cbCheck.setChecked(true);
                } else {
                    cbCheck.setChecked(false);
                }
            } else if (wrapper instanceof MultiSelect.PSmoking) {
                if ((user.p_smoking & (1 << (wrapper.id - 1))) > 0) {
                    cbCheck.setChecked(true);
                } else {
                    cbCheck.setChecked(false);
                }
            } else if (wrapper instanceof MultiSelect.PDrinking) {
                if ((user.p_drinking & (1 << (wrapper.id - 1))) > 0) {
                    cbCheck.setChecked(true);
                } else {
                    cbCheck.setChecked(false);
                }
            } else if (wrapper instanceof MultiSelect.PStatus) {
                if ((user.p_status & (1 << (wrapper.id - 1))) > 0) {
                    cbCheck.setChecked(true);
                } else {
                    cbCheck.setChecked(false);
                }
            }
        }

        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            MultiSelect.MultiWrapper wrapper = (MultiSelect.MultiWrapper) cbCheck.getTag();
            if (wrapper != null) {
                wrapper.checked = b;
                multiWrapperList.set(position, wrapper);
            }
        }
    }
}
