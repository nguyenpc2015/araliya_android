package com.npc.marry.ui.chat;

import android.support.v4.widget.SwipeRefreshLayout;

import com.npc.marry.model.Conversation;
import com.npc.marry.ui.adapter.ConversationAdapter;

import java.util.List;

/**
 * Created by Lenovo on 4/19/2017.
 */

public interface MyChatView extends ConversationAdapter.ConversationListener, SwipeRefreshLayout.OnRefreshListener {
    void onError(String msg);

    void onGetConversation(List<Conversation> conversationList);
}
