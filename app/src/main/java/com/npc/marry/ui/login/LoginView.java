package com.npc.marry.ui.login;

import com.mobsandgeeks.saripaar.Validator;

/**
 * Created by Lenovo on 2/21/2017.
 */

public interface LoginView extends Validator.ValidationListener {
    void onLoginSucess();

    void onLoginFail(String msg);

    void onSendTokenSuccess();

    void onError(String msg);

    void onActiveAccount();

}
