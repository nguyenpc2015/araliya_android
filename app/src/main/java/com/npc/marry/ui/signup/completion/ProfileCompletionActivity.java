package com.npc.marry.ui.signup.completion;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.npc.marry.R;
import com.npc.marry.base.BaseActivity;
import com.npc.marry.ui.custom.CustomSwipeViewPager;
import com.npc.marry.ui.main.MainActivity;
import com.npc.marry.ui.splash.ViewPagerAdapter;

import butterknife.BindView;
import me.relex.circleindicator.CircleIndicator;

public class ProfileCompletionActivity extends BaseActivity {

    @BindView(R.id.viewpager)
    CustomSwipeViewPager viewPager;
    @BindView(R.id.indicator)
    CircleIndicator indicator;

    ViewPagerAdapter adapter;
    String mode = null;

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getIntExtra("step", 0)) {
                case 0:
                    viewPager.setCurrentItem(0);
                    break;
                case 1:
                    viewPager.setCurrentItem(1);
                    break;
                case 2:
                    viewPager.setCurrentItem(2);
                    break;
                case 3:
                    viewPager.setCurrentItem(3);
                    break;
                case 4:
                    if (mode != null) {
                        ProfileCompletionActivity.this.finish();
                    } else {
                        startActivity(MainActivity.class, null, true);
                    }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_completion);
        if (getIntent() != null) {
            mode = getIntent().getStringExtra("data");
        }
        viewPager.setSwipeLocked(true);
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        Fragment fragment1 = new Completion1Fragment();
        Fragment fragment2 = new Completion2Fragment();
        Fragment fragment3 = new Completion3Fragment();
        Fragment fragment4 = new Completion4Fragment();
        Bundle bundle = new Bundle();
        if (mode != null) {
            bundle.putString("mode", mode);
            fragment1.setArguments(bundle);
            fragment2.setArguments(bundle);
            fragment3.setArguments(bundle);
            fragment4.setArguments(bundle);
        }
        adapter.addFragment(fragment1);
        adapter.addFragment(fragment2);
        adapter.addFragment(fragment3);
        adapter.addFragment(fragment4);
        indicator.setViewPager(viewPager);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getApplicationContext().registerReceiver(mReceiver, new IntentFilter("com.npc.complete"));
    }

    @Override
    protected void onPause() {
        super.onPause();
        getApplicationContext().unregisterReceiver(mReceiver);
    }
}
