package com.npc.marry.ui.signup.completion;

import android.widget.AdapterView;

import com.npc.marry.model.RelationShip;
import com.npc.marry.model.response.SpecialResponse;

import java.util.List;

/**
 * Created by Lenovo on 2/27/2017.
 */

public interface Completion2View extends AdapterView.OnItemSelectedListener {
    void onError(String msg);

    void onGetRelationshipSuccess(List<RelationShip> relationShipList);

    void onGetSpecialSuccess(SpecialResponse specialResponses);

    void onComppleteStep();
}
