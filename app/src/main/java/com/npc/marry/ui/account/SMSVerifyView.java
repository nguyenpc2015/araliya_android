package com.npc.marry.ui.account;

import com.npc.marry.model.User;

/**
 * Created by nguyen tran on 2/17/2018.
 */

public interface SMSVerifyView {
    void onError(String msg);

    void onSentSMSSuccess(String msg);

    void onValidateSuccess(String msg);

    void onGetUser(User user);
}
