package com.npc.marry.ui.match;

import android.content.Context;

import com.npc.marry.api.server_api.AppServicesImpl;
import com.npc.marry.app.AppSetting;
import com.npc.marry.base.BasePresenter;
import com.npc.marry.model.response.UserListResponse;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Lenovo on 3/29/2017.
 */

public class MatchPresenter extends BasePresenter<MatchView> {

    AppServicesImpl appServices;

    public MatchPresenter(MatchView view, CompositeSubscription compositeSubscription, Context context) {
        super(view, compositeSubscription);
        appServices = AppServicesImpl.getInstance(context);
    }

    public void getUserMatch(final int page) {
        compositeSubscription.add(appServices.getMatchUser(String.valueOf(AppSetting.getInstance().getUser().user_id), String.valueOf(page))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<UserListResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(UserListResponse userListResponse) {
                        if (view != null) {
                            if (page == 1) {
                                view.onGetUserSuccess(userListResponse.users, userListResponse.total_pages);
                            } else {
                                view.onGetUserSuccessNext(userListResponse.users, userListResponse.total_pages);
                            }
                        }
                    }
                }));
    }


}
