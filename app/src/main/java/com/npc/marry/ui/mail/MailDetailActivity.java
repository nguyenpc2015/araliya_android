package com.npc.marry.ui.mail;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.npc.marry.R;
import com.npc.marry.app.AppSetting;
import com.npc.marry.base.BaseActivity;
import com.npc.marry.model.File;
import com.npc.marry.model.Mail;
import com.npc.marry.ui.custom.BlackTextView;
import com.npc.marry.ui.payment.PaymentActivity;
import com.npc.marry.utils.DateUtils;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class MailDetailActivity extends BaseActivity implements MyMailView {

    @BindView(R.id.tvTitle)
    BlackTextView tvTitle;
    @BindView(R.id.ivUser)
    CircleImageView ivUser;
    @BindView(R.id.tvUser)
    BlackTextView tvUser;
    @BindView(R.id.tvMyUser)
    BlackTextView tvMyUser;
    @BindView(R.id.tvTime)
    BlackTextView tvTime;
    @BindView(R.id.tvText)
    BlackTextView tvText;
    @BindView(R.id.rvAttach)
    RecyclerView rvAttach;
    @BindView(R.id.tvAttName)
    BlackTextView tvAttName;
    @BindView(R.id.tvReply)
    BlackTextView tvReply;

    MailFileAdapter fileAdapter;

    MyMailPresenter presenter;

    Mail mail;

    int cmd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mail_detail);
        mail = getIntent().getParcelableExtra("mail");
        cmd = getIntent().getIntExtra("mode", 1);
        switch (cmd) {
            case 1:
                tvUser.setText(String.format("From %s", mail.user_name));
                tvMyUser.setText(String.format("To %s", AppSetting.getInstance().getUser().name));
                break;
            case 2:
                tvUser.setText(mail.user_from.equals(AppSetting.getInstance().getUser().user_id) ?
                        String.format("From %s", AppSetting.getInstance().getUser().name) : String.format("From %s", mail.user_name));
                tvMyUser.setText(mail.user_from.equals(AppSetting.getInstance().getUser().user_id) ?
                        String.format("To %s", mail.user_name) : String.format("To %s", AppSetting.getInstance().getUser().name));
                tvReply.setVisibility(View.GONE);
                break;
            case 3:
                tvUser.setText(String.format("From %s", AppSetting.getInstance().getUser().name));
                tvMyUser.setText(String.format("To %s", mail.user_name));
                tvReply.setVisibility(View.GONE);
                break;
        }
        tvTitle.setText(mail.subject);
        tvTime.setText(DateUtils.getDisplayTime(this, mail.date));
        Picasso.with(this).load(mail.photo).into(ivUser);
        tvText.setText(mail.text);
        fileAdapter = new MailFileAdapter();
        rvAttach.setLayoutManager(new LinearLayoutManager(this));
        rvAttach.setAdapter(fileAdapter);
        presenter = new MyMailPresenter(this, compositeSubscription, this);
        presenter.getMailFiles(cmd == 3 ? String.valueOf(Integer.valueOf(mail.id) - 1) : mail.id);
        showProgressDialog(false);
    }


    @Override
    public void onError(String msg) {
        dismissProgressDialog();
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onGetMailList(List<Mail> mailList, int totalPage) {
        dismissProgressDialog();
    }

    @Override
    public void onGetMailFiles(List<File> fileList) {
        dismissProgressDialog();
        if (fileList == null || fileList.size() == 0) {
            tvAttName.setVisibility(View.GONE);
        } else {
            fileAdapter.setFileList(fileList);
        }
    }

    @OnClick(R.id.ivBack)
    public void onBack() {
        finish();
    }

    @OnClick(R.id.tvReply)
    public void onReply() {
        if (AppSetting.getInstance().getUserData() != null && AppSetting.getInstance().getUserData().gold_days > 0) {
            Intent intent = new Intent(this, MailComposeActivity.class);
            intent.putExtra("user_to", mail.user_from.equals(AppSetting.getInstance().getUser().user_id) ? mail.user_to : mail.user_from);
            intent.putExtra("user_name", mail.user_name);
            intent.putExtra("subject", "rep: " + mail.subject);
            startActivity(intent);
        } else {
            Toast.makeText(this, "Please upgrade to use this feature", Toast.LENGTH_SHORT).show();
            startActivity(PaymentActivity.class, null, false);
        }
    }
}
