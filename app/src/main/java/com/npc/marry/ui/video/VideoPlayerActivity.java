package com.npc.marry.ui.video;

import android.os.Bundle;
import android.widget.Toast;

import com.jaedongchicken.ytplayer.YoutubePlayerView;
import com.npc.marry.R;
import com.npc.marry.base.BaseActivity;
import com.npc.marry.model.Video;

import org.parceler.Parcels;

import butterknife.BindView;

public class VideoPlayerActivity extends BaseActivity implements YoutubePlayerView.YouTubeListener {

    @BindView(R.id.youtubePlayerView)
    YoutubePlayerView youtubePlayerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_player);

        Video video = Parcels.unwrap(getIntent().getParcelableExtra("data"));

        youtubePlayerView.setAutoPlayerHeight(this);
        youtubePlayerView.initialize(video.audio_code, this);
        youtubePlayerView.pause();
        youtubePlayerView.play();
    }

    @Override
    public void onReady() {

    }

    @Override
    public void onStateChange(YoutubePlayerView.STATE state) {

    }

    @Override
    public void onPlaybackQualityChange(String arg) {

    }

    @Override
    public void onPlaybackRateChange(String arg) {

    }

    @Override
    public void onError(String arg) {
        Toast.makeText(this, arg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onApiChange(String arg) {

    }

    @Override
    public void onCurrentSecond(double second) {

    }

    @Override
    public void onDuration(double duration) {

    }

    @Override
    public void logs(String log) {

    }
}
