package com.npc.marry.ui.adapter;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LevelListDrawable;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.npc.marry.R;
import com.npc.marry.app.AppSetting;
import com.npc.marry.model.Conversation;
import com.npc.marry.ui.chat.ChatActivity;
import com.npc.marry.ui.custom.BlackTextView;
import com.npc.marry.ui.custom.BoldTextView;
import com.npc.marry.utils.DateUtils;
import com.squareup.picasso.Picasso;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Lenovo on 4/19/2017.
 */

public class ConversationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int ITEM_TYPE = 1;
    public static final int HEADER_TYPE = 2;

    List<Conversation> conversationList;
    ConversationListener conversationListener;

    public ConversationAdapter() {
        conversationList = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == HEADER_TYPE) {
            return new HeaderHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.header_user_list, parent, false));
        }
        return new ConversationHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_conversation, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ConversationHolder) {
            ConversationHolder conversationHolder = (ConversationHolder) holder;
            conversationHolder.bind(conversationList.get(position));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0 && conversationList.size() == 1 && conversationList.get(0) == null) {
            return HEADER_TYPE;
        }

        return ITEM_TYPE;
    }

    @Override
    public int getItemCount() {
        return conversationList.size();
    }

    public void setConversationListener(ConversationListener conversationListener) {
        this.conversationListener = conversationListener;
    }

    public void setConversationList(List<Conversation> conversationList) {
        this.conversationList = conversationList;
        notifyDataSetChanged();
    }

    public interface ConversationListener {
        void onItemClick(Conversation conversation);
    }

    class ConversationHolder extends RecyclerView.ViewHolder implements Html.ImageGetter {

        @BindView(R.id.ivUser)
        CircleImageView ivUser;
        @BindView(R.id.tvName)
        BoldTextView tvName;
        @BindView(R.id.tvTime)
        BlackTextView tvTime;
        @BindView(R.id.tvMsg)
        BlackTextView tvMsg;

        View itemView;

        public ConversationHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            ButterKnife.bind(this, itemView);
        }

        public void bind(final Conversation conversation) {
            if (conversation != null) {
                boolean from = !conversation.from_user_id.equals(AppSetting.getInstance().getUser().user_id);
                if (from) {
                    if (conversation.from_user_name != null) {
                        tvName.setText(conversation.from_user_name);
                    }
                    if (conversation.photo_from != null && !conversation.photo_from.isEmpty()) {
                        Picasso.with(itemView.getContext()).load(conversation.photo_from).into(ivUser);
                    }
                } else {
                    if (conversation.to_user_name != null) {
                        tvName.setText(conversation.to_user_name);
                    }
                    if (conversation.photo_to != null && !conversation.photo_to.isEmpty()) {
                        Picasso.with(itemView.getContext()).load(conversation.photo_to).into(ivUser);
                    }
                }
                if (conversation.msg != null) {
                    tvMsg.setText(Html.fromHtml(conversation.msg, this, null));
                    if (conversation.received.equals("0") && !conversation.from_user_id.equals(AppSetting.getInstance().getUser().user_id)) {
                        tvMsg.setTextColor(tvMsg.getResources().getColor(R.color.colorPrimary));
                    }
                }
                if (conversation.received != null) {
                    tvTime.setText(DateUtils.getDisplayTime(itemView.getContext(), conversation.created, DateUtils.DATE_FORMAT_ISO_8601));
                }
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (conversationListener != null) {
                            conversationListener.onItemClick(conversation);
                        }
                    }
                });
            }
        }

        @Override
        public Drawable getDrawable(String source) {
            if (source.contains("http")) {
                LevelListDrawable d = new LevelListDrawable();
                Drawable empty = tvMsg.getContext().getResources().getDrawable(R.drawable.logo);
                d.addLevel(0, 0, empty);
                d.setBounds(0, 0, empty.getIntrinsicWidth(), empty.getIntrinsicHeight());

                new LoadImage().execute(source, d);

                return d;
            } else {
                int id = ChatActivity.smilesMapReplaces.get(source);
                Drawable d = tvMsg.getContext().getResources().getDrawable(id);
                d.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
                return d;
            }
        }

        class LoadImage extends AsyncTask<Object, Void, Bitmap> {

            private LevelListDrawable mDrawable;

            @Override
            protected Bitmap doInBackground(Object... params) {
                String source = (String) params[0];
                mDrawable = (LevelListDrawable) params[1];
                try {
                    InputStream is = new URL(source).openStream();
                    return BitmapFactory.decodeStream(is);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Bitmap bitmap) {
                if (bitmap != null) {
                    BitmapDrawable d = new BitmapDrawable(bitmap);
                    mDrawable.addLevel(1, 1, d);
                    mDrawable.setBounds(0, 0, bitmap.getWidth() * 3, bitmap.getHeight() * 3);
                    mDrawable.setLevel(1);
                    // i don't know yet a better way to refresh TextView
                    // mTv.invalidate() doesn't work as expected
                    CharSequence t = tvMsg.getText();
                    tvMsg.setText(t);
                }
            }
        }
    }

    class HeaderHolder extends RecyclerView.ViewHolder {

        public HeaderHolder(View itemView) {
            super(itemView);
        }
    }
}
