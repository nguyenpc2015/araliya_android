package com.npc.marry.ui.chat;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.npc.marry.R;
import com.npc.marry.base.BaseFragment;
import com.npc.marry.ui.interest.MyInterestFragment;
import com.npc.marry.ui.mail.MyMailFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Lenovo on 3/1/2017.
 */

public class ChatFragment extends BaseFragment implements TabLayout.OnTabSelectedListener {
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chat, container, false);
        ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tabLayout.addTab(tabLayout.newTab().setText("My Chats"));
        tabLayout.addTab(tabLayout.newTab().setText("My Mails"));
        tabLayout.addTab(tabLayout.newTab().setText("My Interests"));
        tabLayout.addOnTabSelectedListener(this);
        tabLayout.getTabAt(0).select();
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        switch (tab.getPosition()) {

            case 0:
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.llContainer11, new MyChatFragment()).commit();
                break;
            case 1:
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.llContainer11, new MyMailFragment()).commit();
                break;
            case 2:
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.llContainer11, new MyInterestFragment()).commit();
                break;
        }
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
        switch (tab.getPosition()) {

            case 0:
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.llContainer11, new MyChatFragment()).commit();
                break;
            case 1:
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.llContainer11, new MyMailFragment()).commit();
                break;
            case 2:
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.llContainer11, new MyInterestFragment()).commit();
                break;
        }
    }
}
