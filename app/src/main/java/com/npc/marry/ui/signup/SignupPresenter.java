package com.npc.marry.ui.signup;

import android.content.Context;

import com.npc.marry.api.server_api.AppServicesImpl;
import com.npc.marry.app.AppSetting;
import com.npc.marry.base.BasePresenter;
import com.npc.marry.model.Orientation;
import com.npc.marry.model.request.UserRequest;
import com.npc.marry.model.request.ValidateRequest;
import com.npc.marry.model.response.OrientationResponse;
import com.npc.marry.model.response.UserRegisterResponse;
import com.npc.marry.model.response.ValidationResponse;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Lenovo on 2/21/2017.
 */

public class SignupPresenter extends BasePresenter<SignupView> {

    AppServicesImpl appServices;
    AppSetting appSetting;

    public SignupPresenter(SignupView view, Context context, CompositeSubscription compositeSubscription) {
        super(view, compositeSubscription);
        appServices = AppServicesImpl.getInstance(context);
        appSetting = AppSetting.getInstance();
    }

    public void register(String username, String password, String email, String born, Orientation orientation) {
        compositeSubscription.add(appServices.register(new UserRequest(username, password, email, orientation.getGender(), orientation.getId(), orientation.getSearch(), born))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<UserRegisterResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onSignupFail(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(UserRegisterResponse user) {
                        appSetting.setUser(user.user);
                        if (view != null) {
                            view.onSignupSuccess();
                        }
                    }
                }));
    }

    public void submitCode(String code) {
        compositeSubscription.add(appServices.validate(new ValidateRequest(appSetting.getUser().user_id, code))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ValidationResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onValidateFail(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(ValidationResponse validationResponse) {
                        appSetting.setToken(validationResponse.token);
                        if (view != null) {
                            view.onValidateSuccess();
                        }
                    }
                }));
    }

    public void getOrientation() {
        compositeSubscription.add(appServices.getOrientations()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<OrientationResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onOrientationFail(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(OrientationResponse orientationResponse) {
                        if (view != null) {
                            view.onOrientationSuccess(orientationResponse.orientations);
                        }
                    }
                }));
    }
}
