package com.npc.marry.ui.interest;

import android.content.Context;

import com.npc.marry.api.server_api.AppServicesImpl;
import com.npc.marry.app.AppSetting;
import com.npc.marry.base.BasePresenter;
import com.npc.marry.model.response.BaseResponse;
import com.npc.marry.model.response.UserListResponse;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Lenovo on 3/29/2017.
 */

public class InterestPresenter extends BasePresenter<InterestView> {
    AppServicesImpl appServices;

    public InterestPresenter(InterestView view, CompositeSubscription compositeSubscription, Context context) {
        super(view, compositeSubscription);
        appServices = AppServicesImpl.getInstance(context);
    }

    public void getInterestUser(final int page) {
        compositeSubscription.add(appServices.getInterestUser(AppSetting.getInstance().getUser().user_id, String.valueOf(page))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<UserListResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(UserListResponse userListResponse) {
                        if (view != null) {
                            if (page == 1) {
                                view.onGetUserList(userListResponse.users, userListResponse.total_pages);
                            } else {
                                view.onGetUserListNext(userListResponse.users, userListResponse.total_pages);
                            }
                        }
                    }
                }));
    }

    public void getInterestReceivedUser(final int page) {
        compositeSubscription.add(appServices.getInterestReceivedUser(AppSetting.getInstance().getUser().user_id, String.valueOf(page))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<UserListResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(UserListResponse userListResponse) {
                        if (view != null) {
                            if (page == 1) {
                                view.onGetUserList(userListResponse.users, userListResponse.total_pages);
                            } else {
                                view.onGetUserListNext(userListResponse.users, userListResponse.total_pages);
                            }
                        }
                    }
                }));
    }

    public void removeInterest(final String user_id) {
        compositeSubscription.add(appServices.removeInterest(AppSetting.getInstance().getUser().user_id, user_id)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<BaseResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(BaseResponse baseResponse) {
                        if (view != null) {
                            view.onRemoveSuccess(baseResponse.message, user_id);
                        }
                    }
                }));
    }
}
