package com.npc.marry.ui.chat;

import android.content.Context;

import com.npc.marry.api.server_api.AppServicesImpl;
import com.npc.marry.app.AppSetting;
import com.npc.marry.base.BasePresenter;
import com.npc.marry.model.response.ConversationResponse;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Lenovo on 4/19/2017.
 */

public class MyChatPresenter extends BasePresenter<MyChatView> {

    AppServicesImpl appServices;

    public MyChatPresenter(MyChatView view, CompositeSubscription compositeSubscription, Context context) {
        super(view, compositeSubscription);
        appServices = AppServicesImpl.getInstance(context);
    }

    public void getConvetsations() {
        compositeSubscription.add(appServices.getConversations(AppSetting.getInstance().getUser().user_id)
                .subscribeOn(Schedulers.io()).subscribeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<ConversationResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(ConversationResponse conversationResponse) {
                        if (conversationResponse != null) {
                            if (view != null) {
                                view.onGetConversation(conversationResponse.chats);
                            }
                        }
                    }
                }));
    }
}
