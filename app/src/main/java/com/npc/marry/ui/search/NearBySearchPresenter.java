package com.npc.marry.ui.search;

import android.content.Context;

import com.npc.marry.api.server_api.AppServicesImpl;
import com.npc.marry.app.AppSetting;
import com.npc.marry.base.BasePresenter;
import com.npc.marry.model.request.NearBySearchRequest;
import com.npc.marry.model.request.UpdateNearByRequest;
import com.npc.marry.model.response.UserListResponse;
import com.npc.marry.model.response.UserMessageResponse;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by nguyen tran on 2/19/2018.
 */

public class NearBySearchPresenter extends BasePresenter<SearchView> {
    AppServicesImpl appServices;

    public NearBySearchPresenter(SearchView view, CompositeSubscription compositeSubscription, Context context) {
        super(view, compositeSubscription);
        appServices = AppServicesImpl.getInstance(context);
    }

    public void nearBySearch(double latt, double longt, double distance) {
        compositeSubscription.add(
                appServices.nearBySearch(new NearBySearchRequest(Integer.valueOf(AppSetting.getInstance().getUser().user_id), distance, latt, longt))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<UserListResponse>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable throwable) {
                                if (view != null) {
                                    view.onError(throwable.getMessage());
                                }
                            }

                            @Override
                            public void onNext(UserListResponse userListResponse) {
                                if (view != null) {
                                    view.onGetSearchResult(userListResponse.users, 0);
                                }
                            }
                        })
        );
    }

    public void updateNearBy(int near_by) {
        compositeSubscription.add(
                appServices.updateNearBy(new UpdateNearByRequest(Integer.valueOf(AppSetting.getInstance().getUser().user_id), near_by))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<UserMessageResponse>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable throwable) {
                                if (view != null) {
                                    view.onError(throwable.getMessage());
                                }
                            }

                            @Override
                            public void onNext(UserMessageResponse userMessageResponse) {
                                AppSetting.getInstance().setUser(userMessageResponse.user);
                                if (view != null) {
                                    view.onUpdateNearByOption(userMessageResponse.message);
                                }
                            }
                        })
        );
    }
}
