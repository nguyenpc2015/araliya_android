package com.npc.marry.ui.mail;

import com.npc.marry.model.File;
import com.npc.marry.model.Mail;

import java.util.List;

/**
 * Created by nguyen tran on 2/21/2018.
 */

public interface MyMailView {
    void onError(String msg);

    void onGetMailList(List<Mail> mailList, int totalPage);

    void onGetMailFiles(List<File> fileList);
}
