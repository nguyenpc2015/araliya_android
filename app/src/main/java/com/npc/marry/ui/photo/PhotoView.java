package com.npc.marry.ui.photo;

import com.npc.marry.model.Photo;
import com.npc.marry.model.PhotoWrapper;
import com.npc.marry.ui.adapter.PhotoAdapter;
import com.npc.marry.ui.adapter.PhotoWrapperAdapter;

import java.util.List;

/**
 * Created by Lenovo on 3/13/2017.
 */

public interface PhotoView extends UploadPhotoDialog.SavePhotoListener, PhotoAdapter.PhotoListener, PhotoWrapperAdapter.PhotoWrapperListener {
    void onError(String msg);

    void onGetPhotos(List<Photo> photoList, int totalPage, int totalItem);

    void onGetPhotosNext(List<Photo> photoList, int totalPage);

    void onUploadSuccess(String msg);

    void onSaveSuccess(String msg);

    void onDeleteResult(String msg);

    void onGetAllPhotos(List<PhotoWrapper> photoList, int totalPage, int totalItem);

    void onGetAllPhotosNext(List<PhotoWrapper> photoList, int totalPage);
}
