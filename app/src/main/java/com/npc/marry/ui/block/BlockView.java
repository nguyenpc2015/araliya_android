package com.npc.marry.ui.block;

import com.npc.marry.model.UserData;
import com.npc.marry.ui.adapter.UserManagerAdapter;

import java.util.List;

/**
 * Created by Lenovo on 3/31/2017.
 */

public interface BlockView extends UserManagerAdapter.RemoveListner {
    void onError(String msg);

    void onGetUserSuccess(List<UserData> userDataList, int totalPage);

    void onGetUserSuccessNext(List<UserData> userDataList, int totalPage);

    void onRemoveSuccess(String msg, String user_id);
}
