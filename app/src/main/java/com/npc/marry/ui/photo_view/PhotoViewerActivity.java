package com.npc.marry.ui.photo_view;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.npc.marry.R;
import com.npc.marry.base.BaseActivity;
import com.npc.marry.model.Photo;
import com.npc.marry.model.PhotoWrapper;
import com.npc.marry.ui.adapter.PhotoAdapter;
import com.npc.marry.ui.adapter.PhotoViewerAdapter;
import com.npc.marry.ui.adapter.ViewPagerFragmentAdapter;

import org.parceler.Parcels;

import java.util.List;

import butterknife.BindView;

public class PhotoViewerActivity extends BaseActivity implements PhotoAdapter.PhotoListener {
    @BindView(R.id.viewpager)
    ViewPager viewPager;

    @BindView(R.id.rvPhoto)
    RecyclerView rvPhoto;

    PhotoViewerAdapter photoAdapter;

    ViewPagerFragmentAdapter fragmentAdapter;
    List<Photo> photoList;
    List<PhotoWrapper> photoWrapperList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_viewer);

        rvPhoto.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        photoAdapter = new PhotoViewerAdapter();
        photoAdapter.setPhotoListener(this);
        rvPhoto.setAdapter(photoAdapter);
        fragmentAdapter = new ViewPagerFragmentAdapter(getSupportFragmentManager());
        viewPager.setAdapter(fragmentAdapter);
        Object object = Parcels.unwrap(getIntent().getParcelableExtra("photo"));
        PhotoWrapper photoWrapper = null;
        Photo photo = null;
        if (object instanceof Photo) {
            photo = (Photo) object;
            photoList = Parcels.unwrap(getIntent().getParcelableExtra("photo-list"));
            photoAdapter.setPhotoList(photoList);
        } else {
            photoWrapper = (PhotoWrapper) object;
            photoWrapperList = Parcels.unwrap(getIntent().getParcelableExtra("photo-list"));
            photoAdapter.setPhotoList1(photoWrapperList);
        }


        int size = photoList != null ? photoList.size() : photoWrapperList.size();
        int index = 0;
        for (int i = 0; i < size; ++i) {
            PhotoViewerFragment photoViewerFragment = new PhotoViewerFragment();
            Bundle bundle = new Bundle();
            if (photoList != null) {
                bundle.putParcelable("photo", Parcels.wrap(photoList.get(i)));
            } else {
                bundle.putParcelable("photo", Parcels.wrap(photoWrapperList.get(i)));
            }
            photoViewerFragment.setArguments(bundle);
            fragmentAdapter.addFragment(photoViewerFragment, "");
            if (photoList != null && photoList.get(i) != null && photo.ref.contains(photoList.get(i).ref)) {
                index = i;
            } else if (photoWrapperList != null && photoWrapperList.get(i) != null && photoWrapper.photo.ref.contains(photoWrapperList.get(i).photo.ref)) {
                index = i;
            }
        }
        viewPager.setCurrentItem(index);

    }


    @Override
    public void onPhotoWrapperClick(PhotoWrapper photoWrapper, List<PhotoWrapper> photoWrapperList) {
        int index = 0;
        for (int i = 0; i < photoWrapperList.size(); ++i) {
            if (photoWrapperList.get(i) != null && photoWrapperList.get(i).photo.ref.equals(photoWrapper.photo.ref)) {
                index = i;
            }
        }
        viewPager.setCurrentItem(index);
    }

    @Override
    public void onDeletePhoto(Photo photo) {

    }

    @Override
    public void onPhotoClick(Photo photo, List<Photo> photoList) {
        int index = 0;
        for (int i = 0; i < photoList.size(); ++i) {
            if (photoList.get(i) != null && photoList.get(i).ref.equals(photo.ref)) {
                index = i;
            }
        }
        viewPager.setCurrentItem(index);
    }
}
