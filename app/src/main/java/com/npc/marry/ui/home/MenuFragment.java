package com.npc.marry.ui.home;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;

import com.npc.marry.R;
import com.npc.marry.app.AppSetting;
import com.npc.marry.base.BaseActivity;
import com.npc.marry.base.BaseFragment;
import com.npc.marry.model.UserData;
import com.npc.marry.ui.account.MyAccountActivity;
import com.npc.marry.ui.block.BlockActivity;
import com.npc.marry.ui.chat.MyChatFragment;
import com.npc.marry.ui.custom.BlackTextView;
import com.npc.marry.ui.favorite.FavoriteActivity;
import com.npc.marry.ui.login.LoginActivity;
import com.npc.marry.ui.mail.MyMailFragment;
import com.npc.marry.ui.match.UserMatchActivity;
import com.npc.marry.ui.myview.MyViewActivity;
import com.npc.marry.ui.partner.MyPartnerActivity;
import com.npc.marry.ui.payment.PaymentActivity;
import com.npc.marry.ui.photo.PhotoActivity;
import com.npc.marry.ui.profile.ProfileActivity;
import com.npc.marry.ui.user.UsersActivity;
import com.npc.marry.ui.video.VideoActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import jp.wasabeef.picasso.transformations.BlurTransformation;
import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by Lenovo on 3/1/2017.
 */

public class MenuFragment extends BaseFragment implements MenuView {


    @BindView(R.id.ivBG)
    ImageView ivBG;
    @BindView(R.id.profile_image)
    CircleImageView ivUser;
    @BindView(R.id.tvName)
    BlackTextView tvName;
    @BindView(R.id.rbRating)
    RatingBar rbRating;
    @BindView(R.id.tvDate)
    BlackTextView tvDate;
    @BindView(R.id.btnUpgrade)
    FancyButton btnUpgrade;
    @BindView(R.id.tvMail)
    BlackTextView tvMail;
    @BindView(R.id.tvInterestCount)
    BlackTextView tvInterestCount;
    @BindView(R.id.tvChatCount)
    BlackTextView tvChatCount;
    @BindView(R.id.tvUserMessage)
    BlackTextView tvUserMessage;

    List<UserData> interestList;
    List<UserData> viewList;

    MenuPresenter menuPresenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Picasso.with(getContext()).load(R.drawable.sp1).transform(new BlurTransformation(getContext(), 3)).into(ivBG);
        menuPresenter = new MenuPresenter(this, getContext(), compositeSubscription);
        menuPresenter.getUser();
    }

    @Override
    public void onResume() {
        super.onResume();
        menuPresenter.getUserData(AppSetting.getInstance().getUser().user_id);
        menuPresenter.getUserInterest(AppSetting.getInstance().getUser().user_id);
        menuPresenter.getNewMails();
    }

    @OnClick(R.id.btnPartner)
    public void onPartner() {
        ((BaseActivity) getActivity()).startActivity(MyPartnerActivity.class, null, false);
    }

    @OnClick(R.id.btnAccount)
    public void onEditAccount() {
        ((BaseActivity) getActivity()).startActivity(MyAccountActivity.class, null, false);
    }

    @OnClick(R.id.btnMyView)
    public void onMyView() {
        ((BaseActivity) getActivity()).startActivity(MyViewActivity.class, null, false);
    }

    @OnClick(R.id.btnBlock)
    public void onBlock() {
        ((BaseActivity) getActivity()).startActivity(BlockActivity.class, null, false);
    }

    @OnClick(R.id.btnFavorite)
    public void onFavorite() {
        ((BaseActivity) getActivity()).startActivity(FavoriteActivity.class, null, false);
    }

    @OnClick(R.id.btnMatch)
    public void onMatch() {
        ((BaseActivity) getActivity()).startActivity(UserMatchActivity.class, null, false);
    }


    @OnClick(R.id.btnProfile)
    public void onProfileClick() {
        ((BaseActivity) getActivity()).startActivity(ProfileActivity.class, AppSetting.getInstance().getUserData(), false);
    }

    @OnClick(R.id.btnUpgrade)
    public void onUpgrade() {
        ((BaseActivity) getActivity()).startActivity(PaymentActivity.class, null, false);
    }

    @OnClick(R.id.llChatCount)
    public void onChatCount() {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.llPhotos, new MyChatFragment());
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @OnClick(R.id.llNewMails)
    public void onNewMails() {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.llPhotos, new MyMailFragment());
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @OnClick(R.id.llInterest)
    public void onInterestCount() {
        menuPresenter.updateInterest(AppSetting.getInstance().getUser().user_id);
    }


    @OnClick(R.id.btnVideo)
    public void onVideoClick() {
        VideoActivity photoActivity = new VideoActivity();
        Bundle bundle = new Bundle();
        bundle.putString("data", AppSetting.getInstance().getUser().user_id);
        bundle.putInt("mode", 1);
        photoActivity.setArguments(bundle);
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.llPhotos, photoActivity).addToBackStack(null).commit();
    }

    @OnClick(R.id.btnPhoto)
    public void onPhotoClick() {
        PhotoActivity photoActivity = new PhotoActivity();
        Bundle bundle = new Bundle();
        bundle.putString("data", AppSetting.getInstance().getUser().user_id);
        bundle.putInt("mode", 1);
        photoActivity.setArguments(bundle);
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.llPhotos, photoActivity).addToBackStack(null).commit();
    }

    @Override
    public void onError(String msg) {
        ((BaseActivity) getActivity()).dismissProgressDialog();
        new AlertDialog.Builder(getContext())
                .setTitle("Alert")
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    @Override
    public void onLogoutSuccess() {
        //((BaseActivity)getActivity()).dismissProgressDialog();
        AppSetting.getInstance().logout();
        ((BaseActivity) getActivity()).startActivity(LoginActivity.class, null, true);
    }

    @Override
    public void onGetDataSuccess(UserData userData) {
        ((BaseActivity) getActivity()).dismissProgressDialog();
        if (userData != null) {
            tvUserMessage.setText(userData.user_message);
            if (userData.user_message == null || userData.user_message.isEmpty()) {
                tvUserMessage.setVisibility(View.GONE);
            } else {
                tvUserMessage.setVisibility(View.VISIBLE);
            }
            if (userData.gold_days > 0) {
                String endate = userData.enddate.replace("0000-00-00", "Free");
                tvDate.setText(endate);
                btnUpgrade.setVisibility(View.GONE);
            }

            tvChatCount.setText(userData.count_chat + "");
            if (userData.default_photo != null && !userData.default_photo.isEmpty()) {
                // String url = userData.default_photo.replace("m", "b");
                Picasso.with(getActivity()).load(userData.default_photo).into(ivUser);
            }
            if (userData.name != null) {
                tvName.setText(userData.name);
            }
            rbRating.setRating(userData.rating);
        }
    }

    @Override
    public void onGetUsersList(List<UserData> userList) {
        this.viewList = userList;

    }

    @Override
    public void onGetUserListInterest(List<UserData> userDatas) {
        this.interestList = userDatas;
        if (userDatas != null) {
            tvInterestCount.setText(userDatas.size() + "");
        }
    }

    @Override
    public void onUpdateSuccess(int code) {
        if (code == 1) {
            if (viewList != null && viewList.size() > 0) {
                ((BaseActivity) getActivity()).startActivity(UsersActivity.class, viewList, false);
            }
        } else {
            if (interestList != null) {
                ((BaseActivity) getActivity()).startActivity(UsersActivity.class, interestList, false);
            }
        }
    }

    @Override
    public void onGetNewMails(int count) {
        tvMail.setText(String.valueOf(count));
    }
}
