package com.npc.marry.ui.chat;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.npc.marry.R;
import com.npc.marry.app.AppSetting;
import com.npc.marry.base.BaseActivity;
import com.npc.marry.base.BaseFragment;
import com.npc.marry.model.Conversation;
import com.npc.marry.ui.adapter.ConversationAdapter;
import com.npc.marry.ui.payment.PaymentActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Lenovo on 3/3/2017.
 */

public class MyChatFragment extends BaseFragment implements MyChatView {

    @BindView(R.id.swipe)
    SwipeRefreshLayout swipe;

    @BindView(R.id.rvChat)
    RecyclerView rvChat;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    MyChatPresenter myChatPresenter;
    ConversationAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_chat, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter = new ConversationAdapter();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        rvChat.setLayoutManager(layoutManager);
        adapter.setConversationListener(this);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvChat.getContext(),
                LinearLayoutManager.VERTICAL);
        rvChat.addItemDecoration(dividerItemDecoration);
        rvChat.setAdapter(adapter);
        swipe.setOnRefreshListener(this);
        myChatPresenter = new MyChatPresenter(this, compositeSubscription, getActivity());

    }

    @Override
    public void onResume() {
        super.onResume();
        myChatPresenter.getConvetsations();
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onError(final String msg) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                swipe.setRefreshing(false);
                progressBar.setVisibility(View.GONE);
                ((BaseActivity) getActivity()).showDialog(msg);
            }
        });
    }

    @Override
    public void onGetConversation(final List<Conversation> conversationList) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                swipe.setRefreshing(false);
                progressBar.setVisibility(View.GONE);
                if (conversationList != null && conversationList.size() > 0) {
                    List<Conversation> conversations = new ArrayList<Conversation>();
                    for (Conversation conversation : conversationList) {
                        if (!conversation.from_user_id.trim().isEmpty() && !conversation.to_user_id.trim().isEmpty()) {
                            conversations.add(conversation);
                        }
                    }
                    adapter.setConversationList(conversations);
                } else {
                    List<Conversation> conversationLists = new ArrayList<Conversation>();
                    conversationLists.add(null);
                    adapter.setConversationList(conversationLists);
                }
            }
        });
    }

    @Override
    public void onItemClick(Conversation conversation) {
        if (AppSetting.getInstance().getUserData().gold_days > 0) {
            ((BaseActivity) getActivity()).startActivity(ChatActivity.class, conversation, false);
        } else {
            ((BaseActivity) getActivity()).startActivity(PaymentActivity.class, null, false);
            Toast.makeText(getContext(), "Please upgrade to use chat feature", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onRefresh() {
        myChatPresenter.getConvetsations();
    }
}
