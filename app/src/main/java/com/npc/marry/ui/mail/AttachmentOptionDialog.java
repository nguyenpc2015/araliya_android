package com.npc.marry.ui.mail;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.npc.marry.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by nguyen tran on 2/28/2018.
 */

public class AttachmentOptionDialog extends DialogFragment {
    AttachmentOptionListener attachmentOptionListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_attachment_option, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @OnClick(R.id.tvCamera)
    public void onCamera() {
        if (attachmentOptionListener != null) {
            attachmentOptionListener.onCamera();
            dismiss();
        }
    }

    @OnClick(R.id.tvFile)
    public void onFile() {
        if (attachmentOptionListener != null) {
            attachmentOptionListener.onFile();
            dismiss();
        }
    }

    public void setAttachmentOptionListener(AttachmentOptionListener attachmentOptionListener) {
        this.attachmentOptionListener = attachmentOptionListener;
    }

    public interface AttachmentOptionListener {
        void onCamera();

        void onFile();
    }
}
