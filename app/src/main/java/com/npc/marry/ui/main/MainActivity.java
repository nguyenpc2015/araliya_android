package com.npc.marry.ui.main;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AlertDialog;

import com.npc.marry.R;
import com.npc.marry.base.BaseActivity;
import com.npc.marry.ui.chat.ChatFragment;
import com.npc.marry.ui.gallery.GalleryFragment;
import com.npc.marry.ui.home.MenuFragment;
import com.npc.marry.ui.logout.LogoutFragment;
import com.npc.marry.ui.notification.NotificationFragment;
import com.npc.marry.ui.search.SearchFragment;
import com.npc.marry.ui.users.HomeFragment;

import butterknife.BindView;

public class MainActivity extends BaseActivity implements TabLayout.OnTabSelectedListener {

    public static final int GPS_PERMISSION_REQUEST = 55;

    @BindView(R.id.tabLayout)
    TabLayout tabLayout;

    boolean isAdvance = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tabLayout.addOnTabSelectedListener(this);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.ic_action_home).setText("Home"));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.ic_forum).setText("Messages"));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.ic_action_user).setText("Users"));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.ic_action_search).setText("Search"));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.ic_photo_album).setText("Gallery"));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.ic_notifications_active).setText("Notifications"));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.ic_settings).setText("Settings"));
    }

    public boolean isAdvance() {
        return isAdvance;
    }

    public void setAdvance(boolean advance) {
        isAdvance = advance;
    }

    public TabLayout getTabLayout() {
        return tabLayout;
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            super.onBackPressed();
        } else {
            new AlertDialog.Builder(this).setMessage("Do you really want to exit?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            MainActivity.super.onBackPressed();
                        }
                    }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            }).show();
        }
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        selectTab(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
        selectTab(tab.getPosition());
    }

    private void selectTab(int position) {
        switch (position) {
            case 0:
                replaceFragment(new MenuFragment(), R.id.llContainer);
                break;
            case 1:
                replaceFragment(new ChatFragment(), R.id.llContainer);
                break;
            case 2:
                replaceFragment(new HomeFragment(), R.id.llContainer);
                break;
            case 3:
                replaceFragment(new SearchFragment(), R.id.llContainer);
                break;
            case 4:
                replaceFragment(new GalleryFragment(), R.id.llContainer);
                break;
            case 5:
                replaceFragment(new NotificationFragment(), R.id.llContainer);
                break;
            case 6:
                replaceFragment(new LogoutFragment(), R.id.llContainer);
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        // super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == GPS_PERMISSION_REQUEST) {
            Intent intent = new Intent("location.search");
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                intent.putExtra("status", true);
            } else {
                intent.putExtra("status", false);
            }
            getApplicationContext().sendBroadcast(intent);
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}
