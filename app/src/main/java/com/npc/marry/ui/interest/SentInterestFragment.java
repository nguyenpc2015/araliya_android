package com.npc.marry.ui.interest;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.npc.marry.R;
import com.npc.marry.base.BaseActivity;
import com.npc.marry.base.BaseFragment;
import com.npc.marry.model.UserData;
import com.npc.marry.ui.adapter.UserManagerAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SentInterestFragment extends BaseFragment implements InterestView {

    @BindView(R.id.rvUser)
    RecyclerView rvUser;

    @BindView(R.id.pbProgress)
    ProgressBar pbProgress;

    UserManagerAdapter userListAdapter;
    InterestPresenter homePresenter;
    int currentPage = 0;

    boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;

    boolean isNext;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_interest, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        isNext = false;
        setUpRecycleView();
        homePresenter = new InterestPresenter(this, compositeSubscription, getContext());
        currentPage = 1;
        homePresenter.getInterestUser(currentPage);
        pbProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void onError(String msg) {
        pbProgress.setVisibility(View.GONE);
        ((BaseActivity) getActivity()).dismissProgressDialog();
        ((BaseActivity) getActivity()).showDialog(msg);
    }

    @Override
    public void onGetUserList(List<UserData> userDataList, int totalPage) {
        pbProgress.setVisibility(View.GONE);
        this.loading = true;
        if (userDataList != null) {
            isNext = currentPage < totalPage;
            if (isNext || userDataList.size() == 0) {
                userDataList.add(null);
            }
            userListAdapter.setUserList(userDataList);
        }
    }

    @Override
    public void onGetUserListNext(List<UserData> userDataList, int totalPage) {
        this.loading = true;
        if (userDataList != null) {
            isNext = currentPage < totalPage;
            if (isNext) {
                userDataList.add(null);
                userListAdapter.addMediaList(userDataList);
            } else {
                userListAdapter.addMediaList(userDataList);
            }
        }
    }

    @Override
    public void onRemoveSuccess(String msg, String user_id) {
        ((BaseActivity) getActivity()).dismissProgressDialog();
        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
        userListAdapter.removeUser(user_id);
    }

    private void setUpRecycleView() {
        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        rvUser.setLayoutManager(mLayoutManager);
        userListAdapter = new UserManagerAdapter();
        userListAdapter.setHide_Remove(true);
        userListAdapter.setRemoveListner(this);
        rvUser.setAdapter(userListAdapter);
        rvUser.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) { //check for scroll down{
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            if (isNext) {
                                currentPage++;
                                homePresenter.getInterestUser(currentPage);
                            }
                        }
                    }
                }
            }
        });
    }

    @Override
    public void onItemRemove(UserData userData) {
        homePresenter.removeInterest(userData.user_id);
        ((BaseActivity) getActivity()).showProgressDialog(false);
    }
}
