package com.npc.marry.ui.user;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ProgressBar;

import com.npc.marry.R;
import com.npc.marry.base.BaseActivity;
import com.npc.marry.model.UserData;
import com.npc.marry.ui.adapter.UsersAdapter;

import org.parceler.Parcels;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class UsersActivity extends BaseActivity {
    @BindView(R.id.rvUser)
    RecyclerView rvUser;

    @BindView(R.id.pbProgress)
    ProgressBar pbProgress;

    UsersAdapter usersAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users);

        usersAdapter = new UsersAdapter();
        rvUser.setLayoutManager(new LinearLayoutManager(this));
        rvUser.setAdapter(usersAdapter);

        usersAdapter.setUserList((List<UserData>) Parcels.unwrap(getIntent().getParcelableExtra("data")));
    }

    @OnClick(R.id.ivBack)
    public void onBack() {
        finish();
    }


}
