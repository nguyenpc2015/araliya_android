package com.npc.marry.ui.signup;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.ConfirmEmail;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;
import com.npc.marry.R;
import com.npc.marry.base.BaseActivity;
import com.npc.marry.model.Orientation;
import com.npc.marry.ui.custom.BlackEditText;
import com.npc.marry.ui.login.LoginActivity;
import com.npc.marry.ui.signup.completion.ProfileCompletionActivity;
import com.npc.marry.ui.splash.SplashActivity;
import com.npc.marry.ui.term.TermActivity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class SignupActivity extends BaseActivity implements SignupView {

    @NotEmpty
    @BindView(R.id.edtUsername)
    BlackEditText edtUsername;

    @Password(min = 6)
    @BindView(R.id.edtPassword)
    BlackEditText edtPassword;

    @ConfirmPassword
    @BindView(R.id.edtrPassword)
    BlackEditText edtrPassword;

    @Email
    @BindView(R.id.edtEmail)
    BlackEditText edtEmail;

    @ConfirmEmail
    @BindView(R.id.edtrEmail)
    BlackEditText edtrEmail;

    @NotEmpty
    @BindView(R.id.edtBorn)
    BlackEditText edtBorn;


    @BindView(R.id.spIam)
    Spinner spIam;

    Validator validator;
    SignupPresenter signupPresenter;
    Orientation orientation = null;
    List<Orientation> orientations = null;

    int year = 0;
    private DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {
            // arg1 = year
            // arg2 = month
            // arg3 = day
            String date = arg1 + "-" + (arg2 + 1) + "-" + arg3;
            edtBorn.setText(date);
            Calendar calendar = Calendar.getInstance();
            year = calendar.get(Calendar.YEAR) - arg1;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        validator = new Validator(this);
        validator.setValidationListener(this);
        signupPresenter = new SignupPresenter(this, this, compositeSubscription);
        spIam.setOnItemSelectedListener(this);
        signupPresenter.getOrientation();
    }

    @OnClick(R.id.ivBack)
    public void onBack() {
        startActivity(SplashActivity.class, null, true);
    }

    @OnClick(R.id.btnRegister)
    public void onRegister() {
        validator.validate();
    }

    @OnClick(R.id.tvLogin)
    public void onLogin() {
        startActivity(LoginActivity.class, null, true);
    }

    @OnClick(R.id.tvTerm)
    public void onTerm() {
        startActivity(TermActivity.class, "a", false);
    }

    @OnClick(R.id.tvPolicy)
    public void onPolicy() {
        startActivity(TermActivity.class, "b", false);
    }

    @OnClick(R.id.edtBorn)
    public void onBorn() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR) - 18;
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, myDateListener, year, month, day);
        datePickerDialog.show();
    }

    @Override
    public void onValidationSucceeded() {
        if (edtUsername.getText().toString().trim().contains(" ")) {
            edtUsername.setError("Not allow space");
        } else if (orientation == null) {
            Toast.makeText(this, "Please select on dropdown option", Toast.LENGTH_LONG).show();
        } else if (edtUsername.getText().toString().trim().length() < 6) {
            edtUsername.setError("Username already taken or Invalid, please enter a different username.");
        } else if (year < 18) {
            edtBorn.setError("You must be greater than or equal 18 years old");
        } else {
            String username = edtUsername.getText().toString();
            String password = edtPassword.getText().toString();
            String email = edtEmail.getText().toString().trim();
            String born = edtBorn.getText().toString();
            signupPresenter.register(username, password, email, born, orientation);
            showProgressDialog(false);
        }
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onSignupSuccess() {
        showProgressDialog(false);
        String code = "23@32";
        signupPresenter.submitCode(code);
        //dismissProgressDialog();
    }

    @Override
    public void onSignupFail(String msg) {
        dismissProgressDialog();
        showDialog(msg);
    }

    @Override
    public void onOrientationSuccess(List<Orientation> orientationList) {
        this.orientations = orientationList;
        List<String> datas = new ArrayList<String>();
        datas.add("Please Choose");
        if (orientationList != null) {
            for (Orientation orientation : orientationList) {
                datas.add(orientation.getTitle());
            }
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, datas);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spIam.setAdapter(dataAdapter);
    }

    @Override
    public void onOrientationFail(String msg) {
        Toast.makeText(this, "Can not get orientations", Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (position > 0) {
            this.orientation = this.orientations != null ? this.orientations.get(position - 1) : null;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onValidateFail(String msg) {
        dismissProgressDialog();
        showDialog(msg);
    }

    @Override
    public void onValidateSuccess() {
        dismissProgressDialog();
        startActivity(ProfileCompletionActivity.class, null, true);
    }
}
