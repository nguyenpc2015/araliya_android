package com.npc.marry.ui.forgot_password;

/**
 * Created by nguyen tran on 2/16/2018.
 */

public interface ForgotPasswordView {
    void onError(String msg);

    void onSentMailSuccess(String msg);
}
