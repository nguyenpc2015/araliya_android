package com.npc.marry.ui.payment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.npc.marry.R;
import com.npc.marry.app.AppSetting;
import com.npc.marry.base.BaseActivity;
import com.npc.marry.model.PaymentFeature;
import com.npc.marry.model.PaymentPlan;
import com.npc.marry.ui.adapter.PaymentFeatureAdapter;

import org.parceler.Parcels;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class PaymentActivity extends BaseActivity implements PaymentView {

    @BindView(R.id.rvFeature)
    RecyclerView rvFeature;

    @BindView(R.id.webview)
    WebView webView;

    PaymentPresenter paymentPresenter;

    PaymentFeatureAdapter paymentFeatureAdapter;

    PaymentPlan paymentPlan;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        paymentPresenter = new PaymentPresenter(this, this, compositeSubscription);

        rvFeature.setLayoutManager(new LinearLayoutManager(this));
        paymentFeatureAdapter = new PaymentFeatureAdapter();
        paymentFeatureAdapter.setPaymentListener(this);
        rvFeature.setAdapter(paymentFeatureAdapter);
        initPaymentFeature();
    }

    @OnClick(R.id.ivBack)
    public void onBack() {
        finish();
    }

    @Override
    public void onError(String msg) {
        dismissProgressDialog();
        showDialog(msg);
    }

    @Override
    public void onGetPaymentPlan(List<PaymentPlan> paymentPlanList) {
        dismissProgressDialog();
        if (paymentPlanList != null) {
            PaymentPlanDialog paymentPlanDialog = new PaymentPlanDialog();
            paymentPlanDialog.setPaymentPlanListener(this);
            Bundle bundle = new Bundle();
            bundle.putParcelable("plans", Parcels.wrap(paymentPlanList));
            paymentPlanDialog.setArguments(bundle);
            paymentPlanDialog.show(getSupportFragmentManager(), "Payment Plan");
        }
    }

    @Override
    public void onGetGeneratedCode(final String html) {
        dismissProgressDialog();
        rvFeature.setVisibility(View.GONE);
        webView.setVisibility(View.VISIBLE);
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait while we connect the payment gateway");
        webView.setWebViewClient(new WebViewClient() {


            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.contains("marry://app")) {
                    PaymentActivity.this.finish();
                } else {
                    view.loadUrl(url);
                }
                return super.shouldOverrideUrlLoading(view, url);
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                super.onReceivedError(view, errorCode, description, failingUrl);
                Log.d("TAG", "onReceivedError: " + description);
            }
        });
        WebSettings settings = webView.getSettings();
        settings.setDomStorageEnabled(true);

        settings.setDefaultTextEncodingName("utf-8");
        settings.setJavaScriptEnabled(true);
        webView.loadData(html, "text/html", "utf-8");
        progressDialog.show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                progressDialog.dismiss();
            }
        }, 5000);
    }


    public void initPaymentFeature() {
        paymentFeatureAdapter.addFeature(new PaymentFeature("", false, false));
        paymentFeatureAdapter.addFeature(new PaymentFeature("Unlimited Receive Interests from other members (*)", true, true));
        paymentFeatureAdapter.addFeature(new PaymentFeature("Unlimited Receive Mails from other members (*)", true, true));
        paymentFeatureAdapter.addFeature(new PaymentFeature("Unlimited Send Interests to other members (*)", true, true));
        paymentFeatureAdapter.addFeature(new PaymentFeature("Unlimited Receive Instant Messages from online members (*)", true, true));
        paymentFeatureAdapter.addFeature(new PaymentFeature("Unlimited Send Instant Messages to other online members (*)", false, true));
        paymentFeatureAdapter.addFeature(new PaymentFeature("Search Results Priority over free members", false, true));
        paymentFeatureAdapter.addFeature(new PaymentFeature("Approval Priority over free members", false, true));
        paymentFeatureAdapter.addFeature(new PaymentFeature("Featured Listing in the member home pages", false, true));
        paymentFeatureAdapter.addFeature(new PaymentFeature("Featured Badge in the your profile", false, true));
        paymentFeatureAdapter.addFeature(new PaymentFeature("", false, false));
    }

    @Override
    public void onPaymentClick() {
        paymentPresenter.getPaymentPlan();
        showProgressDialog(false);
    }

    @Override
    public void onChoosePlan(final PaymentPlan paymentPlan) {
        this.paymentPlan = paymentPlan;
        paymentPresenter.getGeneratedCode(AppSetting.getInstance().getUserData().user_id, paymentPlan.item);
        showProgressDialog(false);
    }


}
