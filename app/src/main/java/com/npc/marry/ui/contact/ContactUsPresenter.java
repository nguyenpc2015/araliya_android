package com.npc.marry.ui.contact;

import android.content.Context;

import com.npc.marry.api.server_api.AppServicesImpl;
import com.npc.marry.app.AppSetting;
import com.npc.marry.base.BasePresenter;
import com.npc.marry.model.request.ContactRequest;
import com.npc.marry.model.response.BaseResponse;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Lenovo on 3/31/2017.
 */

public class ContactUsPresenter extends BasePresenter<ContactUsView> {

    AppServicesImpl appServices;

    public ContactUsPresenter(ContactUsView view, CompositeSubscription compositeSubscription, Context context) {
        super(view, compositeSubscription);
        appServices = AppServicesImpl.getInstance(context);
    }

    public void contactUs(String name, String mail, String type, String comment) {
        compositeSubscription.add(appServices.contactUs(new ContactRequest(AppSetting.getInstance().getUser().user_id,
                name, mail, type, comment)).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<BaseResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(BaseResponse baseResponse) {
                        if (view != null) {
                            view.onContactSuccess(baseResponse.message);
                        }
                    }
                }));
    }
}
