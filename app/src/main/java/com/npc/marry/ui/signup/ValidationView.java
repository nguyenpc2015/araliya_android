package com.npc.marry.ui.signup;

import com.mobsandgeeks.saripaar.Validator;

/**
 * Created by Lenovo on 2/21/2017.
 */

public interface ValidationView extends Validator.ValidationListener {
    void onValidateSuccess();

    void onValidateFail(String msg);

    void onRequestCodeSuccess(String msg);
}
