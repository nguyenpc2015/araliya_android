package com.npc.marry.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.npc.marry.R;
import com.npc.marry.base.BaseActivity;
import com.npc.marry.model.UserData;
import com.npc.marry.ui.custom.BlackTextView;
import com.npc.marry.ui.profile.ProfileActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Lenovo on 6/4/2017.
 */

public class UsersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<UserData> userList;

    public UsersAdapter() {
        userList = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new UserHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        UserHolder userHolder = (UserHolder) holder;
        userHolder.bind(userList.get(position));
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    public void setUserList(List<UserData> userList) {
        this.userList = userList;
        notifyDataSetChanged();
    }

    class UserHolder extends RecyclerView.ViewHolder {

        Context context;

        @BindView(R.id.ivUser)
        CircleImageView ivUser;


        @BindView(R.id.tvName)
        BlackTextView tvName;
        @BindView(R.id.tvAge)
        BlackTextView tvAge;
        @BindView(R.id.tvCity)
        BlackTextView tvCity;
        @BindView(R.id.tvEducation)
        BlackTextView tvEducation;
        @BindView(R.id.tvReligion)
        BlackTextView tvReligion;
        @BindView(R.id.tvCareer)
        BlackTextView tvCareer;
        @BindView(R.id.tvStatus)
        BlackTextView tvStatus;

        public UserHolder(View itemView) {
            super(itemView);
            this.context = itemView.getContext();
            ButterKnife.bind(this, itemView);
        }

        public void bind(final UserData user) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((BaseActivity) context).startActivity(ProfileActivity.class, userList, user, false);
                }
            });
            tvName.setText(user.name);
            Picasso.with(context).load(user.default_photo).into(ivUser);
            tvAge.setText(user.age + "");
            tvCity.setText(user.city + " in " + user.country);
            tvEducation.setText(user.education);
            tvReligion.setText(user.religion);
            tvCareer.setText(user.career);
            tvStatus.setText(user.status);
        }
    }
}
