package com.npc.marry.ui.signup;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.npc.marry.R;
import com.npc.marry.base.BaseActivity;
import com.npc.marry.ui.custom.BlackEditText;
import com.npc.marry.ui.signup.completion.ProfileCompletionActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class ConfirmSignupActivity extends BaseActivity implements ValidationView {

    @NotEmpty
    @BindView(R.id.edtCode)
    BlackEditText edtCode;

    Validator validator;
    ValidationPresenter validationPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_signup);
        validator = new Validator(this);
        validator.setValidationListener(this);
        validationPresenter = new ValidationPresenter(this, this, compositeSubscription);
        validationPresenter.requestCode();
    }

    @OnClick(R.id.btnConfirm)
    public void onConfirm() {
        validator.validate();
    }

    @Override
    public void onValidationSucceeded() {
        String code = edtCode.getText().toString().trim();
        validationPresenter.submitCode(code);
        showProgressDialog(false);
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onValidateSuccess() {
        dismissProgressDialog();
        startActivity(ProfileCompletionActivity.class, null, true);
    }

    @Override
    public void onValidateFail(String msg) {
        dismissProgressDialog();
        showDialog(msg);
    }

    @Override
    public void onRequestCodeSuccess(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
}
