package com.npc.marry.ui.forgot_password;

import android.os.Bundle;

import com.npc.marry.R;
import com.npc.marry.base.BaseActivity;
import com.npc.marry.ui.custom.BlackTextView;

import butterknife.BindView;
import butterknife.OnClick;

public class ForgotPasswordConfirmActivity extends BaseActivity {

    @BindView(R.id.tvEmailConfirm)
    BlackTextView tvEmailConfirm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password_confirm);
        String email = getIntent().getStringExtra("data");
        tvEmailConfirm.setText(String.format(getResources().getString(R.string.forgot_password_confirm), email));
    }

    @OnClick(R.id.btnGo)
    public void onGo() {
        finish();
    }

    @OnClick(R.id.ivBack)
    public void onBack() {
        finish();
    }
}
