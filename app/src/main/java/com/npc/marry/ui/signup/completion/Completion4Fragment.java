package com.npc.marry.ui.signup.completion;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.npc.marry.R;
import com.npc.marry.app.AppSetting;
import com.npc.marry.base.BaseActivity;
import com.npc.marry.base.BaseFragment;
import com.npc.marry.model.response.StyleResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Lenovo on 2/25/2017.
 */

public class Completion4Fragment extends BaseFragment implements Completion4View {

    @BindView(R.id.spSmoking)
    Spinner spSmoking;
    @BindView(R.id.spDrinking)
    Spinner spDrinking;
    @BindView(R.id.spDiet)
    Spinner spDiet;
    @BindView(R.id.spPersonal)
    Spinner spPersonal;
    @BindView(R.id.spFirst)
    Spinner spFirsy;
    @BindView(R.id.spLiving)
    Spinner spFamily;

    List<StyleResponse.Smoking> smokings;
    List<StyleResponse.Drinking> drinkings;
    List<StyleResponse.Diet> diets;
    List<StyleResponse.Persional> persionals;
    List<StyleResponse.Firstdate> firstdates;
    List<StyleResponse.Living> livings;

    StyleResponse.Smoking smoking;
    StyleResponse.Drinking drinking;
    StyleResponse.Diet diet;
    StyleResponse.Persional persional;
    StyleResponse.Firstdate firstdate;
    StyleResponse.Living living;

    Completion4Presenter completion4Presenter;

    String mode = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_completion_4, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        spSmoking.setOnItemSelectedListener(this);
        spDrinking.setOnItemSelectedListener(this);
        spDiet.setOnItemSelectedListener(this);
        spPersonal.setOnItemSelectedListener(this);
        spFirsy.setOnItemSelectedListener(this);
        spFamily.setOnItemSelectedListener(this);
        if (getArguments() != null) {
            mode = getArguments().getString("mode");
        }
        completion4Presenter = new Completion4Presenter(this, getContext(), compositeSubscription);
        completion4Presenter.getStyle();
    }

    @OnClick(R.id.btnNext)
    public void onNext() {
        if (smoking != null && drinking != null && diet != null &&
                persional != null && firstdate != null && living != null) {
            completion4Presenter.sendProfile(Integer.parseInt(smoking.id), Integer.parseInt(drinking.id), Integer.parseInt(diet.id),
                    Integer.parseInt(persional.id), Integer.parseInt(firstdate.id), Integer.parseInt(living.id));
            ((BaseActivity) getActivity()).showProgressDialog(false);
        } else {
            Toast.makeText(getActivity(), "Please select all fields", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onError(String msg) {
        ((BaseActivity) getActivity()).dismissProgressDialog();
        new AlertDialog.Builder(getActivity())
                .setTitle("Alert")
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    @Override
    public void onGetStyleSuccess(StyleResponse styleResponse) {
        this.smokings = styleResponse.smoking;
        this.drinkings = styleResponse.drinking;
        this.diets = styleResponse.diet;
        this.persionals = styleResponse.persional;
        this.firstdates = styleResponse.firstdate;
        this.livings = styleResponse.living;

        String smoking1 = String.valueOf(AppSetting.getInstance().getUser().smoking);
        String drinking1 = String.valueOf(AppSetting.getInstance().getUser().drinking);
        String diet1 = String.valueOf(AppSetting.getInstance().getUser().diet);
        String personal1 = String.valueOf(AppSetting.getInstance().getUser().humor);
        String firstdate1 = String.valueOf(AppSetting.getInstance().getUser().first_date);
        String living1 = String.valueOf(AppSetting.getInstance().getUser().living_with);

        List<String> datas1 = new ArrayList<>();
        datas1.add("Please Choose");
        for (StyleResponse.Smoking smoking : styleResponse.smoking) {
            datas1.add(smoking.title);
        }
        ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, datas1);
        dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spSmoking.setAdapter(dataAdapter1);

        List<String> datas2 = new ArrayList<>();
        datas2.add("Please Choose");
        for (StyleResponse.Drinking drinking : styleResponse.drinking) {
            datas2.add(drinking.title);
        }
        ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, datas2);
        dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spDrinking.setAdapter(dataAdapter2);

        List<String> datas3 = new ArrayList<>();
        datas3.add("Please Choose");
        for (StyleResponse.Diet diet : styleResponse.diet) {
            datas3.add(diet.title);
        }
        ArrayAdapter<String> dataAdapter3 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, datas3);
        dataAdapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spDiet.setAdapter(dataAdapter3);

        List<String> datas4 = new ArrayList<>();
        datas4.add("Please Choose");
        for (StyleResponse.Firstdate firstdate : styleResponse.firstdate) {
            datas4.add(firstdate.title);
        }
        ArrayAdapter<String> dataAdapter4 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, datas4);
        dataAdapter4.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spFirsy.setAdapter(dataAdapter4);

        List<String> datas5 = new ArrayList<>();
        datas5.add("Please Choose");
        for (StyleResponse.Persional persional : styleResponse.persional) {
            datas5.add(persional.title);
        }
        ArrayAdapter<String> dataAdapter5 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, datas5);
        dataAdapter5.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spPersonal.setAdapter(dataAdapter5);

        List<String> datas6 = new ArrayList<>();
        datas6.add("Please Choose");
        for (StyleResponse.Living living : styleResponse.living) {
            datas6.add(living.title);
        }
        ArrayAdapter<String> dataAdapter6 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, datas6);
        dataAdapter6.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spFamily.setAdapter(dataAdapter6);

        if (mode != null) {
            for (StyleResponse.Smoking smoking : styleResponse.smoking) {
                if (smoking.id.equals(smoking1)) {
                    spSmoking.setSelection(styleResponse.smoking.indexOf(smoking) + 1);
                    break;
                }
            }

            for (StyleResponse.Drinking drinking : styleResponse.drinking) {
                if (drinking.id.equals(drinking1)) {
                    spDrinking.setSelection(styleResponse.drinking.indexOf(drinking) + 1);
                    break;
                }
            }

            for (StyleResponse.Diet diet : styleResponse.diet) {
                if (diet.id.equals(diet1)) {
                    spDiet.setSelection(styleResponse.diet.indexOf(diet) + 1);
                    break;
                }
            }

            for (StyleResponse.Firstdate firstdate : styleResponse.firstdate) {
                if (firstdate.id.equals(firstdate1)) {
                    spFirsy.setSelection(styleResponse.firstdate.indexOf(firstdate) + 1);
                    break;
                }
            }

            for (StyleResponse.Living living : styleResponse.living) {
                if (living.id.equals(living1)) {
                    spFamily.setSelection(styleResponse.living.indexOf(living) + 1);
                }
            }

            for (StyleResponse.Persional persional : styleResponse.persional) {
                if (persional.id.equals(personal1)) {
                    spPersonal.setSelection(styleResponse.persional.indexOf(persional) + 1);
                    break;
                }
            }
        }
    }

    @Override
    public void onCompleteStep() {
        if (mode != null) {
            Intent intent = new Intent();
            intent.setAction("com.npc.complete");
            intent.putExtra("step", 4);
            getContext().sendBroadcast(intent);
        } else {
            completion4Presenter.login(AppSetting.getInstance().getUser().name, AppSetting.getInstance().getUser().password);
        }
    }

    @Override
    public void onLoginFail(final String msg) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((BaseActivity) getActivity()).dismissProgressDialog();
                ((BaseActivity) getActivity()).showDialog(msg);
            }
        });

    }

    @Override
    public void onLoginSucess() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((BaseActivity) getActivity()).dismissProgressDialog();
                ((BaseActivity) getActivity()).dismissProgressDialog();
                Intent intent = new Intent();
                intent.setAction("com.npc.complete");
                intent.putExtra("step", 4);
                getContext().sendBroadcast(intent);
            }
        });

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (position > 0) {
            switch (parent.getId()) {
                case R.id.spSmoking:
                    if (smokings != null) {
                        smoking = smokings.get(position - 1);
                    }
                    break;
                case R.id.spDrinking:
                    if (drinkings != null) {
                        drinking = drinkings.get(position - 1);
                    }
                    break;
                case R.id.spDiet:
                    if (diets != null) {
                        diet = diets.get(position - 1);
                    }
                    break;
                case R.id.spPersonal:
                    if (persionals != null) {
                        persional = persionals.get(position - 1);
                    }
                    break;
                case R.id.spFirst:
                    if (firstdates != null) {
                        firstdate = firstdates.get(position - 1);
                    }
                    break;
                case R.id.spLiving:
                    if (livings != null) {
                        living = livings.get(position - 1);
                    }
                    break;
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
