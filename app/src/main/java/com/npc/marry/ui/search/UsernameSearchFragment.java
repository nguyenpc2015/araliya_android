package com.npc.marry.ui.search;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.npc.marry.R;
import com.npc.marry.base.BaseFragment;
import com.npc.marry.model.City;
import com.npc.marry.model.Country;
import com.npc.marry.model.State;
import com.npc.marry.model.UserData;
import com.npc.marry.ui.adapter.UserListAdapter;
import com.npc.marry.ui.custom.BlackEditText;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by Lenovo on 6/15/2017.
 */

public class UsernameSearchFragment extends BaseFragment implements SearchView, Validator.ValidationListener {

    @BindView(R.id.edtKeyword)
    @NotEmpty
    BlackEditText edtKeyword;

    @BindView(R.id.llControl)
    LinearLayout llControl;

    @BindView(R.id.rvResult)
    RecyclerView rvResult;

    @BindView(R.id.pbLoading)
    ProgressBar pbLoading;

    @BindView(R.id.btnShow)
    FancyButton btnShow;

    SearchPresenter searchPresenter;

    UserListAdapter userListAdapter;

    Validator validator;

    int currentPage;

    boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;

    boolean isNext;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_search, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        validator = new Validator(this);
        validator.setValidationListener(this);
        setUpRecycleView();
        searchPresenter = new SearchPresenter(this, getContext(), compositeSubscription);
    }

    @OnClick(R.id.btnSearch)
    public void onSearch() {
        validator.validate();
    }

    @OnClick(R.id.btnShow)
    public void onSearchAgain() {
        llControl.setVisibility(View.VISIBLE);
        pbLoading.setVisibility(View.GONE);
        rvResult.setVisibility(View.GONE);
        btnShow.setVisibility(View.GONE);
    }

    @Override
    public void onError(String msg) {

    }

    @Override
    public void onGetCountrySuccuess(List<Country> countries) {

    }

    @Override
    public void onGetStateSuccess(List<State> states) {

    }

    @Override
    public void onGetCitySuccess(List<City> cities) {

    }

    public void onGetSearchResult(List<UserData> userDatas, int totalPage) {
        pbLoading.setVisibility(View.GONE);
        btnShow.setVisibility(View.VISIBLE);
        this.loading = true;
        if (userDatas != null) {
            isNext = currentPage < totalPage;
            if (isNext || userDatas.size() == 0) {
                userDatas.add(null);
            }
            userListAdapter.setUserList(userDatas);
        }
    }

    @Override
    public void onGetSearchResultNext(List<UserData> userDatas, int totalPage) {
        this.loading = true;
        if (userDatas != null) {
            isNext = currentPage < totalPage;
            if (isNext) {
                userDatas.add(null);
                userListAdapter.addMediaList(userDatas);
            } else {
                userListAdapter.addMediaList(userDatas);
            }
        }
    }

    @Override
    public void onUpdateNearByOption(String msg) {

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onValidationSucceeded() {
        currentPage = 1;
        isNext = false;
        loading = false;
        searchPresenter.searchByName(currentPage, edtKeyword.getText().toString().trim());
        llControl.setVisibility(View.GONE);
        pbLoading.setVisibility(View.VISIBLE);
        rvResult.setVisibility(View.VISIBLE);
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
            }
        }
    }

    private void setUpRecycleView() {
        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rvResult.setLayoutManager(mLayoutManager);
        userListAdapter = new UserListAdapter();
        rvResult.setAdapter(userListAdapter);
        rvResult.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) { //check for scroll down{
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            if (isNext) {
                                currentPage++;
                                searchPresenter.searchByName(currentPage, edtKeyword.getText().toString().trim());
                            }
                        }
                    }
                }
            }
        });
    }
}
