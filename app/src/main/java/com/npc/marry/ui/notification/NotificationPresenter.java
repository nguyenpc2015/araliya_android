package com.npc.marry.ui.notification;

import android.content.Context;

import com.npc.marry.api.server_api.AppServicesImpl;
import com.npc.marry.app.AppSetting;
import com.npc.marry.base.BasePresenter;
import com.npc.marry.model.response.BaseResponse;
import com.npc.marry.model.response.NotificationResponse;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Lenovo on 3/13/2017.
 */

public class NotificationPresenter extends BasePresenter<NotificationView> {

    AppServicesImpl appServices;

    public NotificationPresenter(NotificationView view, Context context, CompositeSubscription compositeSubscription) {
        super(view, compositeSubscription);
        appServices = AppServicesImpl.getInstance(context);
    }

    public void updateNotification(String id) {
        compositeSubscription.add(appServices.updateNotification(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<BaseResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(BaseResponse baseResponse) {
                        if (view != null) {
                            view.onUpdateSuccess();
                        }
                    }
                }));
    }

    public void getNotification(final String page) {
        compositeSubscription.add(appServices.getNotification(AppSetting.getInstance().getUser().user_id, page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<NotificationResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(NotificationResponse notificationResponse) {
                        if (view != null) {
                            if (page.equals("1")) {
                                view.onGetNotification(notificationResponse.items, notificationResponse.total_pages);
                            } else {
                                view.onGetNotificationNext(notificationResponse.items, notificationResponse.total_pages);
                            }
                        }
                    }
                }));
    }
}
