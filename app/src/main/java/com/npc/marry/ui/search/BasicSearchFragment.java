package com.npc.marry.ui.search;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Toast;

import com.npc.marry.R;
import com.npc.marry.base.BaseActivity;
import com.npc.marry.base.BaseFragment;
import com.npc.marry.model.City;
import com.npc.marry.model.Country;
import com.npc.marry.model.State;
import com.npc.marry.model.UserData;
import com.npc.marry.ui.adapter.UserListAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by Lenovo on 3/1/2017.
 */

public class BasicSearchFragment extends BaseFragment implements SearchView {

    @BindView(R.id.llSearch)
    ScrollView llSearch;
    @BindView(R.id.rvResult)
    RecyclerView rvResult;

    @BindView(R.id.spFrom)
    Spinner spFrom;
    @BindView(R.id.spTo)
    Spinner spTo;
    @BindView(R.id.spCountry)
    Spinner spCountry;
    @BindView(R.id.spState)
    Spinner spState;
    @BindView(R.id.spCity)
    Spinner spCity;
    @BindView(R.id.spStatus)
    Spinner spStatus;
    @BindView(R.id.cbPhoto)
    CheckBox cbPhoto;

    @BindView(R.id.pbLoading)
    ProgressBar pbLoading;

    @BindView(R.id.btnShow)
    FancyButton btnShow;

    List<Country> countryList = null;
    List<State> stateList = null;
    List<City> cityList = null;

    Country country = null;
    State state = null;
    City city = null;

    int ageFrom = 0;
    int ageTo = 0;

    int status = 5;

    int currentPage;

    boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;

    boolean isNext;

    UserListAdapter userListAdapter;

    SearchPresenter searchPresenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        List<String> datas = new ArrayList<>();
        datas.add("Select...");
        for (int i = 18; i < 56; ++i) {
            datas.add(String.valueOf(i));
        }
        ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, datas);
        dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spFrom.setAdapter(dataAdapter1);


        ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, datas);
        dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spTo.setAdapter(dataAdapter2);

        datas = new ArrayList<>();
        datas.add("All Country");
        ArrayAdapter<String> dataAdapter3 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, datas);
        dataAdapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCountry.setAdapter(dataAdapter3);

        datas = new ArrayList<>();
        datas.add("All State");
        ArrayAdapter<String> dataAdapter4 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, datas);
        dataAdapter4.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spState.setAdapter(dataAdapter4);

        datas = new ArrayList<>();
        datas.add("All City");
        ArrayAdapter<String> dataAdapter5 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, datas);
        dataAdapter5.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCity.setAdapter(dataAdapter5);

        datas = new ArrayList<>();
        datas.add("Online Members");
        datas.add("Newest Members");
        datas.add("Verified Members");
        datas.add("Recommend Members");
        datas.add("Feature Members");
        datas.add("Doesn't Matter");
        ArrayAdapter<String> dataAdapter6 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, datas);
        dataAdapter6.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spStatus.setAdapter(dataAdapter6);

        isNext = false;
        setUpRecycleView();

        spCountry.setOnItemSelectedListener(this);
        spState.setOnItemSelectedListener(this);
        spCity.setOnItemSelectedListener(this);
        spFrom.setOnItemSelectedListener(this);
        spTo.setOnItemSelectedListener(this);
        spStatus.setOnItemSelectedListener(this);

        spStatus.setSelection(5);
        spTo.setSelection(38);
        spFrom.setSelection(1);

        searchPresenter = new SearchPresenter(this, getActivity(), compositeSubscription);
        searchPresenter.getCountry();

    }

    @OnClick(R.id.btnShow)
    public void onShow() {
        llSearch.setVisibility(View.VISIBLE);
        pbLoading.setVisibility(View.GONE);
        rvResult.setVisibility(View.GONE);
        btnShow.setVisibility(View.GONE);
        country = null;
        state = null;
        city = null;
    }

    @OnClick(R.id.btnSearch)
    public void onSearch() {
        currentPage = 1;
        isNext = false;
        loading = false;
        if (ageFrom == 0) {
            Toast.makeText(getActivity(), "Please select age from ...", Toast.LENGTH_SHORT).show();
        } else if (ageTo == 0) {
            Toast.makeText(getActivity(), "Please select age to ...", Toast.LENGTH_SHORT).show();
        } else {
            searchPresenter.search(currentPage, country != null ? Integer.valueOf(country.country_id) : -1,
                    state != null ? Integer.valueOf(state.state_id) : -1, city != null ? Integer.valueOf(city.city_id) : -1,
                    status, cbPhoto.isChecked() ? 1 : 0, ageFrom, ageTo);
            llSearch.setVisibility(View.GONE);
            pbLoading.setVisibility(View.VISIBLE);
            rvResult.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onError(String msg) {
        ((BaseActivity) getActivity()).showDialog(msg);
    }

    @Override
    public void onGetCountrySuccuess(List<Country> countries) {
        this.countryList = countries;
        List<String> datas = new ArrayList<>();
        datas.add("All Country");
        for (Country country : countries) {
            datas.add(country.country_title);
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, datas);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCountry.setAdapter(dataAdapter);

        datas = new ArrayList<>();
        datas.add("All State");
        ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, datas);
        dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spState.setAdapter(dataAdapter1);

        datas = new ArrayList<>();
        datas.add("All City");
        ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, datas);
        dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCity.setAdapter(dataAdapter2);
    }

    @Override
    public void onGetStateSuccess(List<State> states) {
        this.stateList = states;
        List<String> datas = new ArrayList<>();
        datas.add("All State");
        for (State state : states) {
            datas.add(state.state_title);
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, datas);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spState.setAdapter(dataAdapter);

        datas = new ArrayList<>();
        datas.add("All City");
        ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, datas);
        dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCity.setAdapter(dataAdapter1);
    }

    @Override
    public void onGetCitySuccess(List<City> cities) {
        this.cityList = cities;
        List<String> datas = new ArrayList<>();
        datas.add("All City");
        for (City city : cities) {
            datas.add(city.city_title);
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, datas);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCity.setAdapter(dataAdapter);
    }

    @Override
    public void onGetSearchResult(List<UserData> userDatas, int totalPage) {
        pbLoading.setVisibility(View.GONE);
        btnShow.setVisibility(View.VISIBLE);
        this.loading = true;
        if (userDatas != null) {
            isNext = currentPage < totalPage;
            if (isNext || userDatas.size() == 0) {
                userDatas.add(null);
            }
            userListAdapter.setUserList(userDatas);
        }
    }

    @Override
    public void onGetSearchResultNext(List<UserData> userDatas, int totalPage) {
        this.loading = true;
        if (userDatas != null) {
            isNext = currentPage < totalPage;
            if (isNext) {
                userDatas.add(null);
                userListAdapter.addMediaList(userDatas);
            } else {
                userListAdapter.addMediaList(userDatas);
            }
        }
    }

    @Override
    public void onUpdateNearByOption(String msg) {

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {
            case R.id.spFrom:
                if (position > 0)
                    ageFrom = position + 17;
                break;
            case R.id.spTo:
                if (position > 0)
                    ageTo = position + 17;
                break;
            case R.id.spCountry:
                if (countryList != null && position > 0) {
                    country = countryList.get(position - 1);
                    searchPresenter.getState(Integer.parseInt(country.country_id));
                }
                break;
            case R.id.spState:
                if (stateList != null && position > 0) {
                    state = stateList.get(position - 1);
                    searchPresenter.getCity(Integer.parseInt(country.country_id), Integer.parseInt(state.state_id));
                }
                break;
            case R.id.spCity:
                if (cityList != null && position > 0) {
                    city = cityList.get(position - 1);
                }
                break;
            case R.id.spStatus:
                status = position;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void setUpRecycleView() {
        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rvResult.setLayoutManager(mLayoutManager);
        userListAdapter = new UserListAdapter();
        rvResult.setAdapter(userListAdapter);
        rvResult.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) { //check for scroll down{
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            if (isNext) {
                                currentPage++;
                                searchPresenter.search(currentPage, country != null ? Integer.valueOf(country.country_id) : -1,
                                        state != null ? Integer.valueOf(state.state_id) : -1, city != null ? Integer.valueOf(city.city_id) : -1,
                                        status, cbPhoto.isChecked() ? 1 : 0, ageFrom, ageTo);
                            }
                        }
                    }
                }
            }
        });
    }
}
