package com.npc.marry.ui.home;

import android.content.Context;

import com.npc.marry.api.server_api.AppServicesImpl;
import com.npc.marry.app.AppSetting;
import com.npc.marry.base.BasePresenter;
import com.npc.marry.model.UserData;
import com.npc.marry.model.request.LogoutRequest;
import com.npc.marry.model.response.BaseResponse;
import com.npc.marry.model.response.MailCountResponse;
import com.npc.marry.model.response.UserListResponse;
import com.npc.marry.model.response.UserMessageResponse;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Lenovo on 3/5/2017.
 */

public class MenuPresenter extends BasePresenter<MenuView> {

    AppServicesImpl appServices;
    AppSetting appSetting;

    public MenuPresenter(MenuView view, Context context, CompositeSubscription compositeSubscription) {
        super(view, compositeSubscription);
        appSetting = AppSetting.getInstance();
        appServices = AppServicesImpl.getInstance(context);
    }

    public void logout() {
        compositeSubscription.add(appServices.logout(new LogoutRequest(Integer.parseInt(appSetting.getUser().user_id)))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<BaseResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(BaseResponse baseResponse) {
                        appSetting.logout();
                        if (view != null) {
                            view.onLogoutSuccess();
                        }
                    }
                }));
    }

    public void getUser() {
        compositeSubscription.add(
                appServices.getUserData(AppSetting.getInstance().getUser().user_id)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<UserMessageResponse>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable throwable) {
                                if (view != null) {
                                    view.onError(throwable.getMessage());
                                }
                            }

                            @Override
                            public void onNext(UserMessageResponse userMessageResponse) {
                                AppSetting.getInstance().setUser(userMessageResponse.user);
                            }
                        })
        );
    }

    public void getUserData(String user_id) {
        compositeSubscription.add(appServices.getUser(user_id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<UserData>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(UserData userData) {
                        appSetting.setUserData(userData);
                        if (view != null) {
                            view.onGetDataSuccess(userData);
                        }
                    }
                }));
    }

    public void getNewMails() {
        compositeSubscription.add(
                appServices.getNewMails(AppSetting.getInstance().getUser().user_id)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<MailCountResponse>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable throwable) {
                                if (view != null) {
                                    view.onError(throwable.getMessage());
                                }
                            }

                            @Override
                            public void onNext(MailCountResponse mailCountResponse) {
                                if (view != null) {
                                    view.onGetNewMails(mailCountResponse.count);
                                }
                            }
                        })
        );
    }

    public void getUserView(String user_id) {
        compositeSubscription.add(appServices.getUserView(user_id).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new Observer<UserListResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(UserListResponse userListResponse) {
                        if (view != null) {
                            view.onGetUsersList(userListResponse.users);
                        }
                    }
                }));
    }

    public void getUserInterest(String user_id) {
        compositeSubscription.add(appServices.getUserInterest(user_id).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new Observer<UserListResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(UserListResponse userListResponse) {
                        if (view != null) {
                            view.onGetUserListInterest(userListResponse.users);
                        }
                    }
                }));
    }


    public void updateView(String user_id) {
        compositeSubscription.add(appServices.updateView(user_id).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<BaseResponse>() {
                    @Override
                    public void onCompleted() {
                        if (view != null) {
                            view.onUpdateSuccess(1);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(BaseResponse baseResponse) {

                    }
                }));
    }

    public void updateInterest(String user_id) {
        compositeSubscription.add(appServices.updateInterest(user_id).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<BaseResponse>() {
                    @Override
                    public void onCompleted() {
                        if (view != null) {
                            view.onUpdateSuccess(2);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(BaseResponse baseResponse) {
                    }
                }));
    }
}
