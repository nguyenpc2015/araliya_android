package com.npc.marry.ui.mail;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.npc.marry.R;
import com.npc.marry.app.Constants;
import com.npc.marry.model.File;
import com.npc.marry.ui.custom.BlackTextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by nguyen tran on 2/22/2018.
 */

public class MailFileAdapter extends RecyclerView.Adapter<MailFileAdapter.MailFileHolder> {

    List<File> fileList;

    public MailFileAdapter() {
        fileList = new ArrayList<>();
    }

    public void setFileList(List<File> fileList) {
        this.fileList = fileList;
        notifyDataSetChanged();
    }

    @Override
    public MailFileHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MailFileHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_mail_file, parent, false));
    }

    @Override
    public void onBindViewHolder(MailFileHolder holder, int position) {
        holder.bind(fileList.get(position), position);
    }

    @Override
    public int getItemCount() {
        return fileList.size();
    }

    class MailFileHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.tvAttach)
        BlackTextView tvAttach;

        public MailFileHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        public void bind(File file, int position) {
            if (file != null) {
                tvAttach.setText(String.format("Download Attach File %d", position + 1));
                tvAttach.setTag(Constants.BASE_MAIL_URL + file.file_name);
            }
        }


        @Override
        public void onClick(View view) {
            String fileUrl = (String) tvAttach.getTag();
            if (fileUrl != null) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(fileUrl));
                tvAttach.getContext().startActivity(i);
            }
        }
    }
}
