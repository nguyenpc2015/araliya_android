package com.npc.marry.ui.chat;

import android.content.Context;

import com.npc.marry.api.server_api.AppServicesImpl;
import com.npc.marry.app.AppSetting;
import com.npc.marry.base.BasePresenter;
import com.npc.marry.model.request.CheckSessionRequest;
import com.npc.marry.model.response.BaseResponse;
import com.npc.marry.model.response.MessageResponse;
import com.npc.marry.model.response.UploadPhotoResponse;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Lenovo on 4/23/2017.
 */

public class ChatPresenter extends BasePresenter<ChatView> {

    AppServicesImpl appServices;

    public ChatPresenter(ChatView view, CompositeSubscription compositeSubscription, Context context) {
        super(view, compositeSubscription);
        appServices = AppServicesImpl.getInstance(context);
    }

    public void checkSession(String sessionId) {
        compositeSubscription.add(appServices.checkSession(new CheckSessionRequest(Integer.valueOf(AppSetting.getInstance().getUser().user_id), sessionId))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<BaseResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable throwable) {
                        if (view != null) {
                            view.onError(throwable.getMessage());
                        }
                    }

                    @Override
                    public void onNext(BaseResponse baseResponse) {
                        if (view != null) {
                            view.onCheckSessionResult(baseResponse.valid);
                        }
                    }
                }));
    }

    public void getMessages(String user_id_1, final int page) {
        compositeSubscription.add(appServices.getMessages(AppSetting.getInstance().getUser().user_id, user_id_1, page)
                .observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribe(new Observer<MessageResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(MessageResponse messageResponse) {
                        if (view != null) {
                            if (page == 1) {
                                view.onGetMessageFirst(messageResponse.items, messageResponse.total_pages);
                            } else {
                                view.onGetMessage(messageResponse.items, messageResponse.total_pages);
                            }
                        }
                    }
                }));
    }

    public void uploadFile(File file) {
        RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("Filedata", file.getName(), requestFile);
        compositeSubscription.add(appServices.uploadAttachment(body, AppSetting.getInstance().getUser().user_id).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new Observer<UploadPhotoResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(UploadPhotoResponse uploadPhotoResponse) {
                        if (view != null) {
                            view.onUploadFileSuccess(uploadPhotoResponse.getRef());
                        }
                    }
                }));
    }
}
