package com.npc.marry.ui.photo_view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.npc.marry.R;
import com.npc.marry.app.Constants;
import com.npc.marry.base.BaseActivity;
import com.npc.marry.base.BaseFragment;
import com.npc.marry.model.Photo;
import com.npc.marry.model.PhotoWrapper;
import com.npc.marry.ui.profile.ProfileActivity;
import com.squareup.picasso.Picasso;

import org.parceler.Parcels;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Lenovo on 5/29/2017.
 */

public class PhotoViewerFragment extends BaseFragment {

    @BindView(R.id.ivPhoto)
    ImageView ivPhoto;

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.tvUsername)
    TextView tvUsername;


    Photo photo;

    PhotoWrapper photoWrapper;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Object o = Parcels.unwrap(getArguments().getParcelable("photo"));
        if (o instanceof Photo) {
            photo = (Photo) o;
        } else {
            photoWrapper = (PhotoWrapper) o;
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_photo_viewer, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @OnClick(R.id.tvUsername)
    public void onNameClick() {
        ((BaseActivity) getActivity()).startActivity(ProfileActivity.class, photo != null ? photo.user_id : photoWrapper.photo.user_id, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (photo != null) {
            if (photo.ref != null) {
                String url = Constants.BASE_PHOTO_URL + photo.ref + "_b.jpg";
                Picasso.with(getContext()).load(url).into(ivPhoto);
            }
            if (photo.user_id != null) {

            }
            if (photo.description != null) {
                tvTitle.setText(photo.description);
            }
        } else {
            if (photoWrapper != null) {
                if (photoWrapper.photo != null && photoWrapper.photo.ref != null) {
                    String url = Constants.BASE_PHOTO_URL + photoWrapper.photo.ref + "_b.jpg";
                    Picasso.with(getContext()).load(url).into(ivPhoto);
                }
                if (photoWrapper.photo != null && photoWrapper.photo.user_id != null) {
                    tvUsername.setText(photoWrapper.name);
                }
                if (photoWrapper.photo != null && photoWrapper.photo.description != null) {
                    tvTitle.setText(photoWrapper.photo.description);
                }
            }
        }
    }
}
