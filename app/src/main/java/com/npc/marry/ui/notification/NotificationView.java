package com.npc.marry.ui.notification;

import com.npc.marry.model.Notification;
import com.npc.marry.ui.adapter.NotificationAdapter;

import java.util.List;

/**
 * Created by Lenovo on 3/13/2017.
 */

public interface NotificationView extends NotificationAdapter.NotificationListener {
    void onError(String msg);

    void onGetNotification(List<Notification> notificationList, int totalPage);

    void onGetNotificationNext(List<Notification> notificationList, int totalPage);

    void onUpdateSuccess();
}
