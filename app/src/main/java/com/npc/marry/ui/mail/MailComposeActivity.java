package com.npc.marry.ui.mail;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.nbsp.materialfilepicker.MaterialFilePicker;
import com.nbsp.materialfilepicker.ui.FilePickerActivity;
import com.npc.marry.R;
import com.npc.marry.app.AppSetting;
import com.npc.marry.base.BaseActivity;
import com.npc.marry.model.Mail;
import com.npc.marry.ui.custom.BlackEditText;
import com.npc.marry.ui.custom.BlackTextView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.OnClick;

public class MailComposeActivity extends BaseActivity implements MailComposeView, Validator.ValidationListener,
        MailAttachmentAdapter.MailAttachmentListener, AttachmentOptionDialog.AttachmentOptionListener {

    private static final int MY_PERMISSIONS_REQUEST_CODE = 55;
    private static final int SELECT_FILE = 66;

    @NotEmpty
    @BindView(R.id.edtSubject)
    BlackEditText edtSubject;
    @NotEmpty
    @BindView(R.id.edtText)
    BlackEditText edtText;
    @BindView(R.id.rvAttach)
    RecyclerView rvAttach;
    @BindView(R.id.tvFrom)
    BlackTextView tvFrom;
    @BindView(R.id.tvTo)
    BlackTextView tvTo;

    Validator validator;

    MailAttachmentAdapter attachmentAdapter;

    MailComposePresenter presenter;

    int user_to;

    String user_name;

    String ref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mail_compose);
        rvAttach.setLayoutManager(new LinearLayoutManager(this));
        attachmentAdapter = new MailAttachmentAdapter();
        attachmentAdapter.setAttachmentListener(this);
        rvAttach.setAdapter(attachmentAdapter);
        user_to = Integer.valueOf(getIntent().getStringExtra("user_to"));
        user_name = getIntent().getStringExtra("user_name");
        tvFrom.setText(AppSetting.getInstance().getUser().name);
        tvTo.setText(user_name);
        String subject = getIntent().getStringExtra("subject");
        if (subject != null && !subject.isEmpty()) {
            edtSubject.setText(subject);
            edtSubject.clearFocus();
        }
        validator = new Validator(this);
        validator.setValidationListener(this);
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyHHmmss");
        ref = sdf.format(new Date()) + String.valueOf(new Random().nextInt(100000));
        presenter = new MailComposePresenter(this, compositeSubscription);
    }

    @OnClick(R.id.btnAttach)
    public void onAttachFile() {
        AttachmentOptionDialog attachmentOptionDialog = new AttachmentOptionDialog();
        attachmentOptionDialog.setAttachmentOptionListener(this);
        attachmentOptionDialog.show(getFragmentManager(), "");

    }

    @OnClick(R.id.llSend)
    public void onSend() {
        validator.validate();
    }

    @OnClick(R.id.ivBack)
    public void onBack() {
        finish();
    }

    @Override
    public void onError(String msg) {
        dismissProgressDialog();
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onUploadFileSuccess(com.npc.marry.model.File file, String msg) {
        dismissProgressDialog();
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        attachmentAdapter.addFile(file);
    }

    @Override
    public void onSendMailSuccess(Mail mail) {
        dismissProgressDialog();
        Toast.makeText(this, "sent mail successfully", Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void onRemoveAttachmentSuccess(String msg) {
        dismissProgressDialog();
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onValidationSucceeded() {
        presenter.sendMail(user_to, edtSubject.getText().toString(), edtText.getText().toString(), ref);
        showProgressDialog(false);
    }

    @Override
    public void onValidationFailed(List<ValidationError> list) {
        for (ValidationError error : list) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == MY_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, SELECT_FILE);
            } else {
                Toast.makeText(this, "You didn't allow pick photo from Galerry", Toast.LENGTH_SHORT).show();
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch (requestCode) {
            case 1:
                if (resultCode == RESULT_OK) {
                    String filePath = imageReturnedIntent.getStringExtra(FilePickerActivity.RESULT_FILE_PATH);
                    presenter.sendFile(new File(filePath), ref);
                    showProgressDialog(false);
                }
                break;
            case SELECT_FILE:
                if (resultCode == Activity.RESULT_OK) {
                    try {
                        Uri uri = imageReturnedIntent.getData();
                        String[] projection = {MediaStore.Images.Media.DATA};

                        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
                        cursor.moveToFirst();
                        int columnIndex = cursor.getColumnIndex(projection[0]);
                        String picturePath = cursor.getString(columnIndex); // returns null
                        cursor.close();
                        File file = new File(picturePath);
                        String name = file.getName();
                        presenter.sendFile(file, ref);
                        showProgressDialog(false);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
        }
    }

    @Override
    public void onFileRemove(com.npc.marry.model.File file) {
        presenter.removeAttachFile(file);
        showProgressDialog(false);
    }

    @Override
    public void onCamera() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_CODE);
        } else {
            Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
            photoPickerIntent.setType("image/*");
            startActivityForResult(photoPickerIntent, SELECT_FILE);
        }
    }

    @Override
    public void onFile() {
        new MaterialFilePicker()
                .withActivity(this)
                .withRequestCode(1)
                .withFilterDirectories(true) // Set directories filterable (false by default)
                .withHiddenFiles(true) // Show hidden files and folders
                .start();
    }
}
