package com.npc.marry.ui.splash;

import android.os.Bundle;
import android.support.v4.view.ViewPager;

import com.npc.marry.R;
import com.npc.marry.app.AppSetting;
import com.npc.marry.base.BaseActivity;
import com.npc.marry.ui.login.LoginActivity;
import com.npc.marry.ui.main.MainActivity;
import com.npc.marry.ui.signup.SignupActivity;

import butterknife.BindView;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator;

public class SplashActivity extends BaseActivity {

    @BindView(R.id.viewpager)
    ViewPager viewPager;
    @BindView(R.id.indicator)
    CircleIndicator indicator;

    ViewPagerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String token = AppSetting.getInstance().getToken();
        if (token != null && !token.isEmpty()) {
            startActivity(MainActivity.class, null, true);
        }
        setContentView(R.layout.activity_splash);

        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new Slide1Fragment());
        adapter.addFragment(new Slide2Fragment());
        adapter.addFragment(new Slide3Fragment());
        viewPager.setAdapter(adapter);
        indicator.setViewPager(viewPager);
    }

    @OnClick(R.id.tvLogin)
    public void onLogin() {
        startActivity(LoginActivity.class, null, true);
    }

    @OnClick(R.id.tvSignup)
    public void onSignup() {
        startActivity(SignupActivity.class, null, true);
    }
}
