package com.npc.marry.ui.signup.completion;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.npc.marry.R;
import com.npc.marry.app.AppSetting;
import com.npc.marry.base.BaseActivity;
import com.npc.marry.base.BaseFragment;
import com.npc.marry.model.response.SocialResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by Lenovo on 2/25/2017.
 */

public class Completion3Fragment extends BaseFragment implements Completion3View {

    @BindView(R.id.spStatus)
    Spinner spStatus;
    @BindView(R.id.spEdu)
    Spinner spEdu;
    @BindView(R.id.spCarrer)
    Spinner spCarrer;
    @BindView(R.id.spReligion)
    Spinner spReligion;
    @BindView(R.id.spEth)
    Spinner spEth;
    @BindView(R.id.spCaste)
    Spinner spCaste;
    @BindView(R.id.spClass)
    Spinner spClass;
    @BindView(R.id.spResidency)
    Spinner spResidency;
    @BindView(R.id.spFamily)
    Spinner spFamily;

    @BindView(R.id.btnDone)
    FancyButton btnDone;

    List<SocialResponse.Status> statuss;
    List<SocialResponse.Education> educations;
    List<SocialResponse.Career> careers;
    List<SocialResponse.Religion> religions;
    List<SocialResponse.Ethnicity> eths;
    List<SocialResponse.Cast> casts;
    List<SocialResponse.Classs> classss;
    List<SocialResponse.Recidency> recidencys;
    List<SocialResponse.Family> familys;

    SocialResponse.Status status;
    SocialResponse.Education education;
    SocialResponse.Career career;
    SocialResponse.Religion religion;
    SocialResponse.Ethnicity eth;
    SocialResponse.Cast cast;
    SocialResponse.Classs classs;
    SocialResponse.Recidency recidency;
    SocialResponse.Family family;

    Completion3Presenter completion3Presenter;

    String mode = null;
    boolean isDone = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_completion_3, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        completion3Presenter = new Completion3Presenter(this, getContext(), compositeSubscription);
        spStatus.setOnItemSelectedListener(this);
        spEdu.setOnItemSelectedListener(this);
        spCarrer.setOnItemSelectedListener(this);
        spReligion.setOnItemSelectedListener(this);
        spEth.setOnItemSelectedListener(this);
        spCaste.setOnItemSelectedListener(this);
        spClass.setOnItemSelectedListener(this);
        spResidency.setOnItemSelectedListener(this);
        spFamily.setOnItemSelectedListener(this);
        if (getArguments() != null) {
            mode = getArguments().getString("mode");
        } else {
            btnDone.setVisibility(View.GONE);
        }
        completion3Presenter.getSocial();
    }

    @OnClick(R.id.btnNext)
    public void onNext() {
        if (status != null && education != null && career != null &&
                religion != null && eth != null && cast != null &&
                classs != null && recidency != null && family != null) {
            completion3Presenter.sendProfile(Integer.parseInt(status.id), Integer.parseInt(education.id), Integer.parseInt(career.id),
                    Integer.parseInt(religion.id), Integer.parseInt(eth.id), Integer.parseInt(cast.id),
                    Integer.parseInt(classs.id), Integer.parseInt(recidency.id), Integer.parseInt(family.id));
            ((BaseActivity) getActivity()).showProgressDialog(false);
        } else {
            Toast.makeText(getActivity(), "Please select all fields", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.btnDone)
    public void onDone() {
        isDone = true;
        onNext();
    }

    @Override
    public void onError(String msg) {
        ((BaseActivity) getActivity()).dismissProgressDialog();
        new AlertDialog.Builder(getActivity())
                .setTitle("Alert")
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    @Override
    public void onGetSocialSuccess(SocialResponse socialResponse) {
        this.statuss = socialResponse.status;
        this.educations = socialResponse.education;
        this.careers = socialResponse.career;
        this.religions = socialResponse.religion;
        this.eths = socialResponse.ethnicity;
        this.casts = socialResponse.cast;
        this.classss = socialResponse.classs;
        this.recidencys = socialResponse.recidency;
        this.familys = socialResponse.family;
        List<String> datas1 = new ArrayList<>();
        datas1.add("Please Choose");
        for (SocialResponse.Status status : socialResponse.status) {
            datas1.add(status.title);
        }
        ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, datas1);
        dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spStatus.setAdapter(dataAdapter1);

        List<String> datas2 = new ArrayList<>();
        datas2.add("Please Choose");
        for (SocialResponse.Education education : socialResponse.education) {
            datas2.add(education.title);
        }
        ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, datas2);
        dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spEdu.setAdapter(dataAdapter2);

        List<String> datas3 = new ArrayList<>();
        datas3.add("Please Choose");
        for (SocialResponse.Career career : socialResponse.career) {
            datas3.add(career.title);
        }
        ArrayAdapter<String> dataAdapter3 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, datas3);
        dataAdapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCarrer.setAdapter(dataAdapter3);

        List<String> datas4 = new ArrayList<>();
        datas4.add("Please Choose");
        for (SocialResponse.Religion religion : socialResponse.religion) {
            datas4.add(religion.title);
        }
        ArrayAdapter<String> dataAdapter4 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, datas4);
        dataAdapter4.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spReligion.setAdapter(dataAdapter4);

        List<String> datas5 = new ArrayList<>();
        datas5.add("Please Choose");
        for (SocialResponse.Ethnicity ethnicity : socialResponse.ethnicity) {
            datas5.add(ethnicity.title);
        }
        ArrayAdapter<String> dataAdapter5 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, datas5);
        dataAdapter5.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spEth.setAdapter(dataAdapter5);

        List<String> datas6 = new ArrayList<>();
        datas6.add("Please Choose");
        for (SocialResponse.Cast status : socialResponse.cast) {
            datas6.add(status.title);
        }
        ArrayAdapter<String> dataAdapter6 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, datas6);
        dataAdapter6.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCaste.setAdapter(dataAdapter6);

        List<String> datas7 = new ArrayList<>();
        datas7.add("Please Choose");
        for (SocialResponse.Classs classs : socialResponse.classs) {
            datas7.add(classs.title);
        }
        ArrayAdapter<String> dataAdapter7 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, datas7);
        dataAdapter7.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spClass.setAdapter(dataAdapter7);

        List<String> datas8 = new ArrayList<>();
        datas8.add("Please Choose");
        for (SocialResponse.Recidency status : socialResponse.recidency) {
            datas8.add(status.title);
        }
        ArrayAdapter<String> dataAdapter8 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, datas8);
        dataAdapter8.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spResidency.setAdapter(dataAdapter8);

        List<String> datas9 = new ArrayList<>();
        datas9.add("Please Choose");
        for (SocialResponse.Family status : socialResponse.family) {
            datas9.add(status.title);
        }
        ArrayAdapter<String> dataAdapter9 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, datas9);
        dataAdapter9.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spFamily.setAdapter(dataAdapter9);

        if (mode != null) {
            String status = String.valueOf(AppSetting.getInstance().getUser().status);
            String edu = String.valueOf(AppSetting.getInstance().getUser().education);
            String career = String.valueOf(AppSetting.getInstance().getUser().career);
            String religion = String.valueOf(AppSetting.getInstance().getUser().religion);
            String eth = String.valueOf(AppSetting.getInstance().getUser().ethnicity);
            String caste = String.valueOf(AppSetting.getInstance().getUser().cast);
            String classs = String.valueOf(AppSetting.getInstance().getUser().classs);
            String residency = String.valueOf(AppSetting.getInstance().getUser().residency);
            String family = String.valueOf(AppSetting.getInstance().getUser().family);

            for (SocialResponse.Status status1 : socialResponse.status) {
                if (status1.id.equals(status)) {
                    spStatus.setSelection(1 + socialResponse.status.indexOf(status1));
                    break;
                }
            }

            for (SocialResponse.Education education1 : socialResponse.education) {
                if (education1.id.equals(edu)) {
                    spEdu.setSelection(1 + socialResponse.education.indexOf(education1));
                    break;
                }
            }

            for (SocialResponse.Career career1 : socialResponse.career) {
                if (career1.id.equals(career)) {
                    spCarrer.setSelection(1 + socialResponse.career.indexOf(career1));
                    break;
                }
            }

            for (SocialResponse.Religion religion1 : socialResponse.religion) {
                if (religion1.id.equals(religion)) {
                    spReligion.setSelection(1 + socialResponse.religion.indexOf(religion1));
                    break;
                }
            }

            for (SocialResponse.Ethnicity ethnicity : socialResponse.ethnicity) {
                if (ethnicity.id.equals(eth)) {
                    spEth.setSelection(socialResponse.ethnicity.indexOf(ethnicity) + 1);
                    break;
                }
            }

            for (SocialResponse.Cast cast : socialResponse.cast) {
                if (cast.id.equals(caste)) {
                    spCaste.setSelection(socialResponse.cast.indexOf(cast) + 1);
                }
            }

            for (SocialResponse.Classs classs1 : socialResponse.classs) {
                if (classs1.id.equals(classs)) {
                    spClass.setSelection(socialResponse.classs.indexOf(classs1) + 1);
                    break;
                }
            }

            for (SocialResponse.Recidency recidency : socialResponse.recidency) {
                if (recidency.id.equals(residency)) {
                    spResidency.setSelection(socialResponse.recidency.indexOf(recidency) + 1);
                    break;
                }
            }

            for (SocialResponse.Family family1 : socialResponse.family) {
                if (family1.id.equals(family)) {
                    spFamily.setSelection(socialResponse.family.indexOf(family1) + 1);
                    break;
                }
            }

        }
    }

    @Override
    public void onCompleteStep() {
        if (isDone) {
            Intent intent = new Intent();
            intent.setAction("com.npc.complete");
            intent.putExtra("step", 4);
            getContext().sendBroadcast(intent);
        } else {
            ((BaseActivity) getActivity()).dismissProgressDialog();
            Intent intent = new Intent();
            intent.setAction("com.npc.complete");
            intent.putExtra("step", 3);
            getContext().sendBroadcast(intent);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (position > 0) {
            switch (parent.getId()) {
                case R.id.spStatus:
                    if (statuss != null) {
                        status = statuss.get(position - 1);
                    }
                    break;
                case R.id.spEdu:
                    if (educations != null) {
                        education = educations.get(position - 1);
                    }
                    break;
                case R.id.spCarrer:
                    if (careers != null) {
                        career = careers.get(position - 1);
                    }
                    break;
                case R.id.spReligion:
                    if (religions != null) {
                        religion = religions.get(position - 1);
                    }
                    break;
                case R.id.spEth:
                    if (eths != null) {
                        eth = eths.get(position - 1);
                    }
                    break;
                case R.id.spCaste:
                    if (casts != null) {
                        cast = casts.get(position - 1);
                    }
                    break;
                case R.id.spClass:
                    if (classss != null) {
                        classs = classss.get(position - 1);
                    }
                    break;
                case R.id.spResidency:
                    if (recidencys != null) {
                        recidency = recidencys.get(position - 1);
                    }
                    break;
                case R.id.spFamily:
                    if (familys != null) {
                        family = familys.get(position - 1);
                    }
                    break;
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
