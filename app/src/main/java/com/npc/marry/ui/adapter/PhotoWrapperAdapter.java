package com.npc.marry.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.npc.marry.R;
import com.npc.marry.app.Constants;
import com.npc.marry.model.PhotoWrapper;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Lenovo on 6/17/2017.
 */

public class PhotoWrapperAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int ITEM_TYPE = 1;
    private static final int FOOTER_TYPE = 2;

    PhotoWrapperListener photoWrapperListener;
    List<PhotoWrapper> photoWrapperList;

    public PhotoWrapperAdapter(PhotoWrapperListener photoWrapperListener) {
        this.photoWrapperListener = photoWrapperListener;
        this.photoWrapperList = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case ITEM_TYPE:
                return new PhotoWrapperHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_photo_wrapper, parent, false));
            default:
                return new FooterHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.footer_user_list, parent, false));
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof PhotoWrapperHolder) {
            PhotoWrapperHolder photoWrapperHolder = (PhotoWrapperHolder) holder;
            photoWrapperHolder.bind(photoWrapperList.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return photoWrapperList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (photoWrapperList.size() > 0 && photoWrapperList.size() > Constants.PAGE_SIZE &&
                position == photoWrapperList.size() - 1 && photoWrapperList.get(position) == null) {
            return FOOTER_TYPE;
        }

        return ITEM_TYPE;
    }

    public void addMediaList(List<PhotoWrapper> photos) {
        int size = this.photoWrapperList.size();
        this.photoWrapperList.remove(size - 1);
        notifyItemRemoved(size - 1);
        size = this.photoWrapperList.size();
        this.photoWrapperList.addAll(photos);
        notifyItemRangeInserted(size, photos.size());
    }

    public void setPhotoList(List<PhotoWrapper> photoList) {
        this.photoWrapperList = photoList;
        notifyDataSetChanged();
    }

    public interface PhotoWrapperListener {
        void onPhotoWrapperClick(PhotoWrapper photoWrapper, List<PhotoWrapper> photoWrappers);

        void onNameclick(String user_id);
    }

    class PhotoWrapperHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.ivPhoto1)
        ImageView ivPhoto;

        @BindView(R.id.llName)
        LinearLayout llName;

        @BindView(R.id.tvName)
        TextView tvName;

        public PhotoWrapperHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(final PhotoWrapper photoWrapper) {
            if (photoWrapper != null) {
                if (photoWrapper.photo != null) {
                    if (photoWrapper.photo.ref != null) {
                        String url = Constants.BASE_PHOTO_URL + photoWrapper.photo.ref + "_b.jpg";
                        Picasso.with(ivPhoto.getContext()).load(url).into(ivPhoto);
                    }
                }
                if (photoWrapper.name != null) {
                    tvName.setText(photoWrapper.name);
                }
                llName.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (photoWrapperListener != null) {
                            photoWrapperListener.onNameclick(photoWrapper.photo.user_id);
                        }

                    }
                });
                ivPhoto.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (photoWrapperListener != null) {
                            photoWrapperListener.onPhotoWrapperClick(photoWrapper, photoWrapperList);
                        }
                    }
                });
            }
        }

    }

    class FooterHolder extends RecyclerView.ViewHolder {

        public FooterHolder(View itemView) {
            super(itemView);
        }
    }
}
