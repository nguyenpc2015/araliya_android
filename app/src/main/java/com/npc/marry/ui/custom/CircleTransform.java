package com.npc.marry.ui.custom;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import com.npc.marry.app.App;
import com.npc.marry.utils.UIUtils;
import com.squareup.picasso.Transformation;

/**
 * Created by nguyen tran on 2/28/2018.
 */

public class CircleTransform implements Transformation {
    String text;

    public CircleTransform(String text) {
        this.text = text;
    }

    @Override
    public Bitmap transform(Bitmap source) {
        int size = Math.min(source.getWidth(), source.getHeight());

        int x = (source.getWidth() - size) / 2;
        int y = (source.getHeight() - size) / 2;

        Bitmap squaredBitmap = Bitmap.createBitmap(source, x, y, size, size);
        if (squaredBitmap != source) {
            source.recycle();
        }

        Bitmap bitmap = Bitmap.createBitmap(size, size, source.getConfig());

        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint();
        BitmapShader shader = new BitmapShader(squaredBitmap,
                BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP);
        paint.setShader(shader);
        paint.setAntiAlias(true);

        float r = size / 2f;
        canvas.drawCircle(r, r, r, paint);

        Paint paint1 = new Paint();
        paint1.setTextAlign(Paint.Align.CENTER);
        paint1.setStyle(Paint.Style.FILL);
        paint1.setColor(Color.parseColor("#ffffff"));
        canvas.drawRect(0, 3 * size / 4 + size / 16, size, size, paint1);

        Paint paint2 = new Paint();
        paint2.setTextAlign(Paint.Align.CENTER);
        paint2.setStrokeWidth(5);
        paint2.setTextSize(UIUtils.dpToPx(App.getAppContext(), 10));
        paint2.setColor(Color.parseColor("#82080e"));
        canvas.drawText(text, r, size - size / 16, paint2);
        squaredBitmap.recycle();
        return bitmap;
    }

    @Override
    public String key() {
        return "circle" + text;
    }
}