package com.npc.marry.ui.chat;

import com.github.angads25.filepicker.controller.DialogSelectionListener;
import com.npc.marry.api.server_api.SocketManager;
import com.npc.marry.model.Message;
import com.npc.marry.ui.adapter.SmileAdapter;

import java.util.List;

/**
 * Created by Lenovo on 4/23/2017.
 */

public interface ChatView extends SocketManager.MessageListener, SmileAdapter.SmileListener, DialogSelectionListener {
    void onError(String msg);

    void onGetMessageFirst(List<Message> messageList, int totalPage);

    void onGetMessage(List<Message> messageList, int totalPage);

    void onUploadFileSuccess(String ref);

    void onCheckSessionResult(int valid);
}
