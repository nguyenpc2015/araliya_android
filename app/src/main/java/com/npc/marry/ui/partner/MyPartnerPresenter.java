package com.npc.marry.ui.partner;

import android.content.Context;

import com.npc.marry.api.server_api.AppServicesImpl;
import com.npc.marry.app.AppSetting;
import com.npc.marry.base.BasePresenter;
import com.npc.marry.model.request.UpdatePartnerRequest;
import com.npc.marry.model.response.PartnerParamResponse;
import com.npc.marry.model.response.UserMessageResponse;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by nguyen tran on 2/17/2018.
 */

public class MyPartnerPresenter extends BasePresenter<MyPartnerView> {
    AppSetting appSetting;
    AppServicesImpl appServices;

    public MyPartnerPresenter(MyPartnerView view, CompositeSubscription compositeSubscription, Context context) {
        super(view, compositeSubscription);
        appServices = AppServicesImpl.getInstance(context);
        appSetting = AppSetting.getInstance();
    }

    public void updatePartner(UpdatePartnerRequest partnerRequest) {
        compositeSubscription.add(
                appServices.updatePartner(partnerRequest)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<UserMessageResponse>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable throwable) {
                                if (view != null) {
                                    view.onError(throwable.getMessage());
                                }
                            }

                            @Override
                            public void onNext(UserMessageResponse userMessageResponse) {
                                AppSetting.getInstance().setUser(userMessageResponse.user);
                                if (view != null) {
                                    view.onUpdatePartnerSuccess(userMessageResponse.message);
                                }
                            }
                        })
        );
    }

    public void getPartnerInfo() {
        compositeSubscription.add(
                appServices.getPartnerParam()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<PartnerParamResponse>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable throwable) {
                                if (view != null) {
                                    view.onError(throwable.getMessage());
                                }
                            }

                            @Override
                            public void onNext(PartnerParamResponse baseResponse) {
                                if (view != null) {
                                    view.onGetPartnerInfo(baseResponse.drop_select, baseResponse.multi_select);
                                }
                            }
                        })
        );
    }
}
