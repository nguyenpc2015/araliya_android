package com.npc.marry.ui.partner;

import android.widget.AdapterView;

import com.npc.marry.model.DropSelect;
import com.npc.marry.model.MultiSelect;

/**
 * Created by nguyen tran on 2/17/2018.
 */

public interface MyPartnerView extends AdapterView.OnItemSelectedListener {
    void onError(String msg);

    void onGetPartnerInfo(DropSelect dropSelect, MultiSelect multiSelect);

    void onUpdatePartnerSuccess(String msg);
}
