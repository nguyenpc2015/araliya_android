package com.npc.marry.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.npc.marry.R;
import com.npc.marry.app.Constants;
import com.npc.marry.model.Video;
import com.npc.marry.model.VideoWrapper;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Lenovo on 6/17/2017.
 */

public class VideoWrapperAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int ITEM_TYPE = 1;
    private static final int FOOTER_TYPE = 2;


    VideoListener videoListener;
    List<VideoWrapper> videoWrapperList;

    public VideoWrapperAdapter(VideoListener videoListener) {
        this.videoListener = videoListener;
        videoWrapperList = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == ITEM_TYPE) {
            return new VideoHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_video_wrapper, parent, false));
        } else {
            return new FooterHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.footer_user_list, parent, false));
        }
    }

    public int getItemViewType(int position) {
        if (videoWrapperList.size() > 0 && videoWrapperList.size() > Constants.PAGE_SIZE &&
                position == videoWrapperList.size() - 1 && videoWrapperList.get(position) == null) {
            return FOOTER_TYPE;
        }

        return ITEM_TYPE;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof VideoHolder) {
            VideoHolder videoHolder = (VideoHolder) holder;
            videoHolder.bind(videoWrapperList.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return videoWrapperList.size();
    }

    public void addMediaList(List<VideoWrapper> videoWrappers) {
        int size = this.videoWrapperList.size();
        this.videoWrapperList.remove(size - 1);
        notifyItemRemoved(size - 1);
        size = this.videoWrapperList.size();
        this.videoWrapperList.addAll(videoWrappers);
        notifyItemRangeInserted(size, videoWrappers.size());
    }

    public void setPhotoList(List<VideoWrapper> photoList) {
        this.videoWrapperList = photoList;
        notifyDataSetChanged();
    }

    public interface VideoListener {
        void onVideoClick(com.npc.marry.model.Video video);

        void onUserClick(String user_id);
    }

    class VideoHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvName)
        TextView tvName;

        @BindView(R.id.ivThumb)
        ImageView ivThumb;

        @BindView(R.id.tvUserName)
        TextView tvUsername;

        View itemView;

        public VideoHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            ButterKnife.bind(this, itemView);
        }

        public void bind(final VideoWrapper videoWrapper) {
            if (videoWrapper != null) {
                final Video video = videoWrapper.video;
                tvName.setText(video == null ? "" : video.audio_name);
                if (video.audio_code != null && !video.audio_code.isEmpty()) {
                    String url = "https://i3.ytimg.com/vi/" + video.audio_code + "/default.jpg";
                    Picasso.with(ivThumb.getContext()).load(url).error(R.drawable.ic_video).into(ivThumb);
                }
                if (videoWrapper.name != null) {
                    tvUsername.setText(videoWrapper.name);
                }
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (videoListener != null) {
                            videoListener.onVideoClick(video);
                        }
                    }
                });
                tvUsername.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (videoListener != null) {
                            videoListener.onUserClick(video.user_id);
                        }
                    }
                });
            }
        }
    }

    class FooterHolder extends RecyclerView.ViewHolder {

        public FooterHolder(View itemView) {
            super(itemView);
        }
    }
}
