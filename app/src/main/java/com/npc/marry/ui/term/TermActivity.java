package com.npc.marry.ui.term;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.npc.marry.R;
import com.npc.marry.base.BaseActivity;
import com.npc.marry.ui.custom.BlackTextView;

import butterknife.BindView;
import butterknife.OnClick;

public class TermActivity extends BaseActivity {

    @BindView(R.id.wvTerm)
    WebView wvTerm;

    @BindView(R.id.tvTitle)
    BlackTextView tvTitle;

    String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_term);
        String cmd = getIntent().getStringExtra("data");
        if (cmd.equals("a")) {
            url = "https://www.liyathabara.com/content/term_cond.html";
            tvTitle.setText("Term and Condition");
        } else {
            url = "https://www.liyathabara.com/content/priv_policy.html";
            tvTitle.setText("Privacy Policy");
        }
        wvTerm.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);

        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();

        wvTerm.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            public void onPageFinished(WebView view, String url) {

            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(TermActivity.this, "Oh no! " + description, Toast.LENGTH_SHORT).show();
                alertDialog.setTitle("Error");
                alertDialog.setMessage(description);
                alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        return;
                    }
                });
                alertDialog.show();
            }
        });
        wvTerm.loadUrl(url);
    }

    @OnClick(R.id.ivBack)
    public void onBack() {
        finish();
    }
}
