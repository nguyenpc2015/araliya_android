package com.npc.marry.ui.match;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.npc.marry.R;
import com.npc.marry.base.BaseActivity;
import com.npc.marry.model.UserData;
import com.npc.marry.ui.adapter.UserListAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class UserMatchActivity extends BaseActivity implements MatchView {

    @BindView(R.id.rvUser)
    RecyclerView rvUser;

    @BindView(R.id.pbProgress)
    ProgressBar pbProgress;

    UserListAdapter userListAdapter;
    MatchPresenter homePresenter;
    int currentPage = 0;

    boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;

    boolean isNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_match);
        isNext = false;
        setUpRecycleView();
        homePresenter = new MatchPresenter(this, compositeSubscription, this);
        currentPage = 1;
        homePresenter.getUserMatch(currentPage);
        pbProgress.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.ivBack)
    public void onBack() {
        finish();
    }

    @Override
    public void onError(String msg) {
        pbProgress.setVisibility(View.GONE);
        showDialog(msg);
    }

    @Override
    public void onGetUserSuccess(List<UserData> userDataList, int totalPage) {
        pbProgress.setVisibility(View.GONE);
        this.loading = true;
        if (userDataList != null) {
            isNext = currentPage < totalPage;
            if (isNext || userDataList.size() == 0) {
                userDataList.add(null);
            }
            userListAdapter.setUserList(userDataList);
        }
    }

    @Override
    public void onGetUserSuccessNext(List<UserData> userDataList, int totalPage) {
        this.loading = true;
        if (userDataList != null) {
            isNext = currentPage < totalPage;
            if (isNext) {
                userDataList.add(null);
                userListAdapter.addMediaList(userDataList);
            } else {
                userListAdapter.addMediaList(userDataList);
            }
        }
    }

    private void setUpRecycleView() {
        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        rvUser.setLayoutManager(mLayoutManager);
        userListAdapter = new UserListAdapter();
        rvUser.setAdapter(userListAdapter);
        rvUser.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) { //check for scroll down{
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            if (isNext) {
                                currentPage++;
                                homePresenter.getUserMatch(currentPage);
                            }
                        }
                    }
                }
            }
        });
    }
}
