package com.npc.marry.ui.contact;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.npc.marry.R;
import com.npc.marry.base.BaseActivity;
import com.npc.marry.ui.custom.BlackEditText;
import com.npc.marry.ui.custom.BlackTextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class ContactUsActivity extends BaseActivity implements ContactUsView {

    @BindView(R.id.spSubject)
    Spinner spSubject;

    @Email
    @BindView(R.id.edtEmail)
    BlackEditText edtEmail;

    @NotEmpty
    @BindView(R.id.edtName)
    BlackEditText edtName;

    @NotEmpty
    @BindView(R.id.edtComment)
    BlackEditText edtComment;

    @BindView(R.id.tvThank)
    BlackTextView tvThank;

    @BindView(R.id.llContact)
    LinearLayout llContact;

    List<String> data;

    String type;

    Validator validator;

    ContactUsPresenter contactUsPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        validator = new Validator(this);
        validator.setValidationListener(this);
        contactUsPresenter = new ContactUsPresenter(this, compositeSubscription, this);

        data = new ArrayList<>();
        data.add("General Inquiry");
        data.add("Advertising Inquiry");
        data.add("Comments/Feedback");
        data.add("Bugs/Problems");
        data.add("Questions/Help");
        data.add("Feature Request");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, data);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spSubject.setAdapter(dataAdapter);
        spSubject.setOnItemSelectedListener(this);
    }

    @OnClick(R.id.ivBack)
    public void onBack() {
        finish();
    }

    @OnClick(R.id.btnSubmit)
    public void onSubmit() {
        validator.validate();
    }


    @Override
    public void onValidationSucceeded() {
        String name = edtName.getText().toString();
        String email = edtEmail.getText().toString();
        String comment = edtComment.getText().toString();

        if (type != null) {
            contactUsPresenter.contactUs(name, email, type, comment);
            showProgressDialog(false);
        } else {
            Toast.makeText(this, "Please select a subject", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onError(String msg) {
        dismissProgressDialog();
        showDialog(msg);
    }

    @Override
    public void onContactSuccess(String msg) {
        dismissProgressDialog();
        llContact.setVisibility(View.GONE);
        tvThank.setVisibility(View.VISIBLE);
        tvThank.setText(msg);
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        type = data.get(position);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
