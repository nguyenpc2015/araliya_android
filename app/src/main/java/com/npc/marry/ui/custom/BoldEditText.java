package com.npc.marry.ui.custom;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by Lenovo on 2/20/2017.
 */

public class BoldEditText extends EditText {
    public BoldEditText(Context context) {
        super(context);
    }

    public BoldEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        Typeface typeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/roboto_bold.ttf");
        setTypeface(typeface);
    }

    public BoldEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
