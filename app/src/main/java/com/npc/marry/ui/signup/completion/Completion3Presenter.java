package com.npc.marry.ui.signup.completion;

import android.content.Context;

import com.npc.marry.api.server_api.AppServicesImpl;
import com.npc.marry.app.AppSetting;
import com.npc.marry.base.BasePresenter;
import com.npc.marry.model.request.Complete3Request;
import com.npc.marry.model.response.BaseResponse;
import com.npc.marry.model.response.SocialResponse;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Lenovo on 2/27/2017.
 */

public class Completion3Presenter extends BasePresenter<Completion3View> {

    AppServicesImpl appServices;

    public Completion3Presenter(Completion3View view, Context context, CompositeSubscription compositeSubscription) {
        super(view, compositeSubscription);
        appServices = AppServicesImpl.getInstance(context);
    }

    public void getSocial() {
        compositeSubscription.add(appServices.getSocial()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<SocialResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(SocialResponse socialResponse) {
                        if (view != null) {
                            view.onGetSocialSuccess(socialResponse);
                        }
                    }
                }));
    }

    public void sendProfile(int statusid, int educationid, int careerid, int religionid, int ethid, int castid, int classid, int recidencyid, int familyid) {
        compositeSubscription.add(appServices.completeProfile3(new Complete3Request(Integer.parseInt(AppSetting.getInstance().getUser().user_id), statusid, educationid, careerid, religionid, ethid,
                castid, classid, recidencyid, familyid))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<BaseResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(BaseResponse baseResponse) {
                        if (view != null) {
                            view.onCompleteStep();
                        }
                    }
                }));

    }
}
