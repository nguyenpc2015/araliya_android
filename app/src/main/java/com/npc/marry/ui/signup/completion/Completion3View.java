package com.npc.marry.ui.signup.completion;

import android.widget.AdapterView;

import com.npc.marry.model.response.SocialResponse;

/**
 * Created by Lenovo on 2/27/2017.
 */

public interface Completion3View extends AdapterView.OnItemSelectedListener {
    void onError(String msg);

    void onGetSocialSuccess(SocialResponse socialResponse);

    void onCompleteStep();
}
