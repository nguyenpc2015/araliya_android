package com.npc.marry.ui.partner;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.npc.marry.R;
import com.npc.marry.app.AppSetting;
import com.npc.marry.base.BaseActivity;
import com.npc.marry.model.DropSelect;
import com.npc.marry.model.MultiSelect;
import com.npc.marry.model.User;
import com.npc.marry.model.request.UpdatePartnerRequest;
import com.npc.marry.ui.custom.BlackTextView;
import com.npc.marry.ui.partner.adapter.StyleCheckPartnerAdapter;
import com.npc.marry.utils.UIUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class MyPartnerActivity extends BaseActivity implements MyPartnerView {

    @BindView(R.id.llCheckboxSection)
    LinearLayout llCheckboxSection;
    @BindView(R.id.spAgeFrom)
    Spinner spAgeFrom;
    @BindView(R.id.spAgeTo)
    Spinner spAgeTo;
    @BindView(R.id.spHeightFrom)
    Spinner spHeightFrom;
    @BindView(R.id.spHeightTo)
    Spinner spHeightTo;
    @BindView(R.id.spWeightFrom)
    Spinner spWeightFrom;
    @BindView(R.id.spWeightTo)
    Spinner spWeightTo;

    List<Integer> ageList;
    List<DropSelect.HeightRange> heightRangeList;
    List<DropSelect.WeightRange> weightRangeList;

    List<StyleCheckPartnerAdapter> adapterList;
    MyPartnerPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_partner);
        adapterList = new ArrayList<>();
        presenter = new MyPartnerPresenter(this, compositeSubscription, this);
        presenter.getPartnerInfo();
        showProgressDialog(false);
        spAgeFrom.setOnItemSelectedListener(this);
        spAgeTo.setOnItemSelectedListener(this);
        spHeightFrom.setOnItemSelectedListener(this);
        spHeightTo.setOnItemSelectedListener(this);
        spWeightFrom.setOnItemSelectedListener(this);
        spWeightTo.setOnItemSelectedListener(this);

    }

    @OnClick(R.id.ivBack)
    public void onBack() {
        finish();
    }

    @OnClick(R.id.btnUpdate)
    public void onUpdate() {
        UpdatePartnerRequest partnerRequest = new UpdatePartnerRequest();
        partnerRequest.setUser_id(Integer.valueOf(AppSetting.getInstance().getUser().user_id));
        for (StyleCheckPartnerAdapter adapter : adapterList) {
            if (adapter.getTag().equals("p_education")) {
                partnerRequest.setP_education(adapter.getParamResult());
            } else if (adapter.getTag().equals("p_body")) {
                partnerRequest.setP_body(adapter.getParamResult());
            } else if (adapter.getTag().equals("p_ethnicity")) {
                partnerRequest.setP_ethnicity(adapter.getParamResult());
            } else if (adapter.getTag().equals("p_religion")) {
                partnerRequest.setP_religion(adapter.getParamResult());
            } else if (adapter.getTag().equals("p_family")) {
                partnerRequest.setP_family(adapter.getParamResult());
            } else if (adapter.getTag().equals("p_career")) {
                partnerRequest.setP_career(adapter.getParamResult());
            } else if (adapter.getTag().equals("p_smoking")) {
                partnerRequest.setP_smoking(adapter.getParamResult());
            } else if (adapter.getTag().equals("p_drinking")) {
                partnerRequest.setP_drinking(adapter.getParamResult());
            } else if (adapter.getTag().equals("p_status")) {
                partnerRequest.setP_status(adapter.getParamResult());
            }
        }
        if (spAgeFrom.getTag() == null) {
            Toast.makeText(this, "Please select age from partner", Toast.LENGTH_SHORT).show();
        } else if (spAgeTo.getTag() == null) {
            Toast.makeText(this, "Please select age to partner", Toast.LENGTH_SHORT).show();
        } else if (spWeightFrom.getTag() == null) {
            Toast.makeText(this, "Please select weight from partner", Toast.LENGTH_SHORT).show();
        } else if (spWeightTo.getTag() == null) {
            Toast.makeText(this, "Please select weight to partner", Toast.LENGTH_SHORT).show();
        } else if (spHeightFrom.getTag() == null) {
            Toast.makeText(this, "Please select height from partner", Toast.LENGTH_SHORT).show();
        } else if (spHeightTo.getTag() == null) {
            Toast.makeText(this, "Please select height to partner", Toast.LENGTH_SHORT).show();
        } else {
            partnerRequest.setP_age_from((Integer) spAgeFrom.getTag());
            partnerRequest.setP_ago_to((Integer) spAgeTo.getTag());
            partnerRequest.setP_weight_from((Integer) spWeightFrom.getTag());
            partnerRequest.setP_weight_to((Integer) spWeightTo.getTag());
            partnerRequest.setP_height_from((Integer) spHeightFrom.getTag());
            partnerRequest.setP_height_to((Integer) spHeightTo.getTag());
            presenter.updatePartner(partnerRequest);
            showProgressDialog(false);
        }
    }

    @Override
    public void onError(String msg) {
        dismissProgressDialog();
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onGetPartnerInfo(DropSelect dropSelect, MultiSelect multiSelect) {
        dismissProgressDialog();
        setUpPartnerLooks(dropSelect);
        setUpPartnerLifeStyles(multiSelect);
    }

    @Override
    public void onUpdatePartnerSuccess(String msg) {
        dismissProgressDialog();
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    private void setUpPartnerLooks(DropSelect dropSelect) {
        this.heightRangeList = dropSelect.height_range;
        this.weightRangeList = dropSelect.weight_range;
        this.ageList = new ArrayList<>();
        User user = AppSetting.getInstance().getUser();
        for (int i = 18; i < 56; ++i) {
            ageList.add(i);
        }


        List<String> datas = new ArrayList<>();
        datas.add("Please Choose");
        for (DropSelect.HeightRange heightRange : heightRangeList) {
            datas.add(heightRange.title);
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, datas);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spHeightFrom.setAdapter(dataAdapter);
        if (user.p_height_from != 0) {
            for (int i = 0; i < heightRangeList.size(); ++i) {
                if (user.p_height_from == heightRangeList.get(i).id) {
                    spHeightFrom.setSelection(i + 1);
                    break;
                }
            }
        }

        ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, datas);
        dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spHeightTo.setAdapter(dataAdapter1);
        if (user.p_height_to != 0) {
            for (int i = 0; i < heightRangeList.size(); ++i) {
                if (user.p_height_to == heightRangeList.get(i).id) {
                    spHeightTo.setSelection(i + 1);
                    break;
                }
            }
        }
        List<String> datas1 = new ArrayList<>();
        datas1.add("Please Choose");
        for (DropSelect.WeightRange weightRange : weightRangeList) {
            datas1.add(weightRange.title);
        }
        ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, datas1);
        dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spWeightFrom.setAdapter(dataAdapter2);
        if (user.p_weight_from != 0) {
            for (int i = 0; i < weightRangeList.size(); ++i) {
                if (user.p_weight_from == weightRangeList.get(i).id) {
                    spWeightFrom.setSelection(i + 1);
                    break;
                }
            }
        }
        ArrayAdapter<String> dataAdapter3 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, datas1);
        dataAdapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spWeightTo.setAdapter(dataAdapter3);
        if (user.p_weight_to != 0) {
            for (int i = 0; i < weightRangeList.size(); ++i) {
                if (user.p_weight_to == weightRangeList.get(i).id) {
                    spWeightTo.setSelection(i + 1);
                    break;
                }
            }
        }
        List<String> datas2 = new ArrayList<>();
        datas2.add("Please Choose");
        for (Integer integer : ageList) {
            datas2.add(String.valueOf(integer));
        }
        ArrayAdapter<String> dataAdapter4 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, datas2);
        dataAdapter4.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spAgeFrom.setAdapter(dataAdapter4);
        if (user.p_age_from != 0) {
            spAgeFrom.setSelection(user.p_age_from - 18 + 1);
        }
        ArrayAdapter<String> dataAdapter5 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, datas2);
        dataAdapter5.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spAgeTo.setAdapter(dataAdapter5);
        if (user.p_age_to != 0) {
            spAgeTo.setSelection(user.p_age_to - 18 + 1);
        }
    }

    private void bindPartnerLifeStyleSection(List<MultiSelect.MultiWrapper> multiWrapper, String title, String tag) {
        View view = LayoutInflater.from(this).inflate(R.layout.view_partner_lifestyle, null);
        BlackTextView tvStyleTitle = (BlackTextView) view.findViewById(R.id.tvStyleTitle);
        RecyclerView rvStylePartner = (RecyclerView) view.findViewById(R.id.rvStylePartner);
        tvStyleTitle.setText(title);
        StyleCheckPartnerAdapter adapter = new StyleCheckPartnerAdapter();
        rvStylePartner.setLayoutManager(new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false));
        rvStylePartner.setAdapter(adapter);
        adapter.setMultiWrapperList(multiWrapper);
        adapter.setTag(tag);
        adapterList.add(adapter);
        if (title.equals("Partner Education")) {
            rvStylePartner.setMinimumHeight(UIUtils.dpToPx(this, 250));
        }
        llCheckboxSection.addView(view, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
    }

    private void setUpPartnerLifeStyles(MultiSelect multiSelect) {
        bindPartnerLifeStyleSection(new ArrayList<MultiSelect.MultiWrapper>(multiSelect.p_education), "Partner Education", "p_education");
        bindPartnerLifeStyleSection(new ArrayList<MultiSelect.MultiWrapper>(multiSelect.p_body), "Partner Body", "p_body");
        bindPartnerLifeStyleSection(new ArrayList<MultiSelect.MultiWrapper>(multiSelect.p_ethnicity), "Partner Ethnicity", "p_ethnicity");
        bindPartnerLifeStyleSection(new ArrayList<MultiSelect.MultiWrapper>(multiSelect.p_religion), "Partner Religion", "p_religion");
        bindPartnerLifeStyleSection(new ArrayList<MultiSelect.MultiWrapper>(multiSelect.p_family), "Partner Family", "p_family");
        bindPartnerLifeStyleSection(new ArrayList<MultiSelect.MultiWrapper>(multiSelect.p_career), "Partner Career", "p_career");
        bindPartnerLifeStyleSection(new ArrayList<MultiSelect.MultiWrapper>(multiSelect.p_smoking), "Partner Smoking", "p_smoking");
        bindPartnerLifeStyleSection(new ArrayList<MultiSelect.MultiWrapper>(multiSelect.p_drinking), "Partner Drinking", "p_drinking");
        bindPartnerLifeStyleSection(new ArrayList<MultiSelect.MultiWrapper>(multiSelect.p_status), "Partner Status", "p_status");
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        if (i > 0) {
            switch (adapterView.getId()) {
                case R.id.spAgeFrom:
                    spAgeFrom.setTag(18 + i - 1);
                    break;
                case R.id.spAgeTo:
                    spAgeTo.setTag(18 + i - 1);
                    break;
                case R.id.spHeightFrom:
                    spHeightFrom.setTag(heightRangeList.get(i - 1).id);
                    break;
                case R.id.spHeightTo:
                    spHeightTo.setTag(heightRangeList.get(i - 1).id);
                    break;
                case R.id.spWeightFrom:
                    spWeightFrom.setTag(weightRangeList.get(i - 1).id);
                    break;
                case R.id.spWeightTo:
                    spWeightTo.setTag(weightRangeList.get(i - 1).id);
                    break;
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
