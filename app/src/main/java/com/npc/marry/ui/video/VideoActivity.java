package com.npc.marry.ui.video;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.npc.marry.R;
import com.npc.marry.app.AppSetting;
import com.npc.marry.base.BaseActivity;
import com.npc.marry.base.BaseFragment;
import com.npc.marry.model.Video;
import com.npc.marry.model.VideoWrapper;
import com.npc.marry.ui.adapter.VideoWrapperAdapter;
import com.npc.marry.ui.adapter.VideosAdapter;
import com.npc.marry.ui.custom.BlackTextView;
import com.npc.marry.ui.profile.ProfileActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mehdi.sakout.fancybuttons.FancyButton;

public class VideoActivity extends BaseFragment implements VideoView {

    @BindView(R.id.rvVideo)
    RecyclerView rvVideo;
    @BindView(R.id.tvCount)
    BlackTextView tvCount;
    @BindView(R.id.btnUpload)
    FancyButton btnUpload;

    VideosAdapter videosAdapter;

    VideoWrapperAdapter videoWrapperAdapter;

    VideoPresenter videoPresenter;

    int currentPage = 0;

    boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;

    boolean isNext;
    int mode = 1;
    String user_id;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_video, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        user_id = getArguments().getString("data");
        mode = getArguments().getInt("mode");
        setUpRecycleView();
        btnUpload.setVisibility((mode == 1) && user_id.equals(AppSetting.getInstance().getUser().user_id) ? View.VISIBLE : View.GONE);
        videoPresenter = new VideoPresenter(this, compositeSubscription, getContext());
        if (mode == 1) {
            videoPresenter.getVideos(user_id);
        } else {
            currentPage = 1;
            videoPresenter.getAllVideos(currentPage);
        }
        //((BaseActivity)getActivity()).showProgressDialog(false);
    }

    @OnClick(R.id.btnUpload)
    public void onUpload() {
        UploadVideoDialog videoDialog = new UploadVideoDialog();
        videoDialog.setVideoListener(this);
        videoDialog.show(getActivity().getSupportFragmentManager(), "");
    }

    @Override
    public void onError(String msg) {
        ((BaseActivity) getActivity()).dismissProgressDialog();
        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onGetVideoList(List<Video> videoList) {
        ((BaseActivity) getActivity()).dismissProgressDialog();
        if (!AppSetting.getInstance().getUser().user_id.equals(user_id)) {
            for (Video video : videoList) {
                if (video.visible == 'N') {
                    videoList.remove(videoList.indexOf(video));
                }
            }
        }
        videosAdapter.setVideoList(videoList);
        tvCount.setText(String.format("%d videos", videoList.size()));
    }

    @Override
    public void onGetVideoList(List<VideoWrapper> videoList, int totalPage, int items) {
        this.loading = true;
        tvCount.setText(String.format("%d videos", items));
        if (videoList != null && videoList.size() > 0) {
            List<VideoWrapper> videoWrappers = new ArrayList<>();
            for (VideoWrapper videoWrapper : videoList) {
                if (videoWrapper != null && videoWrapper.video != null && videoWrapper.video.audio_code != null) {
                    videoWrappers.add(videoWrapper);
                }
            }
            isNext = currentPage < totalPage;
            if (isNext) {
                videoWrappers.add(null);
            }
            videoWrapperAdapter.setPhotoList(videoWrappers);
        }
    }

    @Override
    public void onGetVideoNext(List<VideoWrapper> videoList, int totalPage) {
        this.loading = true;
        if (videoList != null && videoList.size() > 0) {
            List<VideoWrapper> videoWrappers = new ArrayList<>();
            for (VideoWrapper videoWrapper : videoList) {
                if (videoWrapper != null && videoWrapper.video != null && videoWrapper.video.audio_code != null) {
                    videoWrappers.add(videoWrapper);
                }
            }
            isNext = currentPage < totalPage;
            if (isNext) {
                videoWrappers.add(null);
                videoWrapperAdapter.addMediaList(videoWrappers);
            } else {
                videoWrapperAdapter.addMediaList(videoWrappers);
            }
        }
    }

    @Override
    public void onDeleteVideoSuccess(String msg) {
        ((BaseActivity) getActivity()).dismissProgressDialog();
        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
        videoPresenter.getVideos(user_id);
    }

    @Override
    public void onUploadVideoSuccess(String msg) {
        ((BaseActivity) getActivity()).dismissProgressDialog();
        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
        videoPresenter.getVideos(user_id);
    }

    @Override
    public void onVideoClick(Video video) {
        ((BaseActivity) getActivity()).startActivity(VideoPlayerActivity.class, video, false);
    }

    @Override
    public void onVideoDelete(Video video) {
        videoPresenter.deleteVideo(String.valueOf(video.audio_id));
        ((BaseActivity) getActivity()).showProgressDialog(false);
    }

    @Override
    public void onUserClick(String user_id) {
        ((BaseActivity) getActivity()).startActivity(ProfileActivity.class, user_id, false);
    }

    private void setUpRecycleView() {
        final GridLayoutManager mLayoutManager = new GridLayoutManager(getContext(), 3, LinearLayoutManager.VERTICAL, false);
        rvVideo.setLayoutManager(mLayoutManager);
        videosAdapter = new VideosAdapter();
        videosAdapter.setVideoListener(this);

        videoWrapperAdapter = new VideoWrapperAdapter(this);
        rvVideo.setAdapter(mode == 1 ? videosAdapter : videoWrapperAdapter);
        rvVideo.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) { //check for scroll down{
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            if (isNext) {
                                currentPage++;
                                videoPresenter.getAllVideos(currentPage);
                            }
                        }
                    }
                }
            }
        });
    }

    @Override
    public void onSaveVideo(String name, String description, String url) {
        videoPresenter.uploadVideo(url, name, description);
        ((BaseActivity) getActivity()).showProgressDialog(false);
    }
}
