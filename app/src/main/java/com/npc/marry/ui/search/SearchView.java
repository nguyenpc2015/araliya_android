package com.npc.marry.ui.search;

import android.widget.AdapterView;

import com.npc.marry.model.City;
import com.npc.marry.model.Country;
import com.npc.marry.model.State;
import com.npc.marry.model.UserData;

import java.util.List;

/**
 * Created by Lenovo on 3/10/2017.
 */

public interface SearchView extends AdapterView.OnItemSelectedListener {
    void onError(String msg);

    void onGetCountrySuccuess(List<Country> countries);

    void onGetStateSuccess(List<State> states);

    void onGetCitySuccess(List<City> cities);

    void onGetSearchResult(List<UserData> userDatas, int totalPage);

    void onGetSearchResultNext(List<UserData> userDatas, int totalPage);

    void onUpdateNearByOption(String msg);
}
