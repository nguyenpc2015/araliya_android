package com.npc.marry.ui.account;

import android.content.Context;

import com.npc.marry.api.server_api.AppServicesImpl;
import com.npc.marry.app.AppSetting;
import com.npc.marry.base.BasePresenter;
import com.npc.marry.model.request.SMSValidateRequest;
import com.npc.marry.model.request.SMSVerifyRequest;
import com.npc.marry.model.response.BaseResponse;
import com.npc.marry.model.response.UserMessageResponse;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by nguyen tran on 2/17/2018.
 */

public class SMSVerifyPresenter extends BasePresenter<SMSVerifyView> {
    AppServicesImpl appServices;

    public SMSVerifyPresenter(SMSVerifyView view, CompositeSubscription compositeSubscription, Context context) {
        super(view, compositeSubscription);
        appServices = AppServicesImpl.getInstance(context);
    }

    public void getUser() {
        compositeSubscription.add(
                appServices.getUserData(AppSetting.getInstance().getUser().user_id)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<UserMessageResponse>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable throwable) {
                                if (view != null) {
                                    view.onError(throwable.getMessage());
                                }
                            }

                            @Override
                            public void onNext(UserMessageResponse userMessageResponse) {
                                AppSetting.getInstance().setUser(userMessageResponse.user);
                                if (view != null) {
                                    view.onGetUser(userMessageResponse.user);
                                }
                            }
                        })
        );
    }

    public void validateSMS(String code) {
        compositeSubscription.add(
                appServices.validateSMS(new SMSValidateRequest(Integer.valueOf(AppSetting.getInstance().getUser().user_id), code))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<UserMessageResponse>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable throwable) {
                                if (view != null) {
                                    view.onError(throwable.getMessage());
                                }
                            }

                            @Override
                            public void onNext(UserMessageResponse baseResponse) {
                                AppSetting.getInstance().setUser(baseResponse.user);
                                if (view != null) {
                                    view.onValidateSuccess(baseResponse.message);
                                }
                            }
                        })
        );
    }

    public void sendSMSVerify(int country_code, String mobile) {
        compositeSubscription.add(
                appServices.verifySMS(new SMSVerifyRequest(Integer.valueOf(AppSetting.getInstance().getUser().user_id), mobile, country_code))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<BaseResponse>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable throwable) {
                                if (view != null) {
                                    view.onError(throwable.getMessage());
                                }
                            }

                            @Override
                            public void onNext(BaseResponse baseResponse) {
                                if (view != null) {
                                    view.onSentSMSSuccess(baseResponse.message);
                                }
                            }
                        })
        );
    }
}
