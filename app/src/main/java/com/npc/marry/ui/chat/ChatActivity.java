package com.npc.marry.ui.chat;

import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.github.angads25.filepicker.model.DialogConfigs;
import com.github.angads25.filepicker.model.DialogProperties;
import com.github.angads25.filepicker.view.FilePickerDialog;
import com.npc.marry.R;
import com.npc.marry.api.server_api.SocketManager;
import com.npc.marry.app.AppSetting;
import com.npc.marry.app.Constants;
import com.npc.marry.base.BaseActivity;
import com.npc.marry.model.Conversation;
import com.npc.marry.model.Message;
import com.npc.marry.model.MessageEntry;
import com.npc.marry.ui.adapter.ChatAdapter;
import com.npc.marry.ui.adapter.SmileAdapter;
import com.npc.marry.ui.custom.BlackEditText;
import com.npc.marry.ui.custom.BlackTextView;
import com.npc.marry.ui.custom.BoldTextView;
import com.npc.marry.ui.login.LoginActivity;
import com.npc.marry.ui.profile.ProfileActivity;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;
import org.parceler.Parcels;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class ChatActivity extends BaseActivity implements ChatView {

    public static HashMap<Integer, String> smilesMap = new HashMap<>();
    public static HashMap<String, Integer> smilesMapReplaces = new HashMap<>();
    @BindView(R.id.ivUser)
    CircleImageView ivUser;
    @BindView(R.id.tvUsername)
    BoldTextView tvName;
    @BindView(R.id.tvStatus)
    BlackTextView tvStatus;
    @BindView(R.id.rvChat)
    RecyclerView rvChat;
    @BindView(R.id.edtChat)
    BlackEditText edtChat;
    @BindView(R.id.btnSend)
    Button btnSend;
    @BindView(R.id.rvSmile)
    RecyclerView rvSmile;
    @BindView(R.id.llSmile)
    LinearLayout llSmile;
    @BindView(R.id.pbProgress)
    ProgressBar progressBar;
    FilePickerDialog dialog;

    Conversation conversation;

    ChatPresenter chatPresenter;

    ChatAdapter chatAdapter;

    SmileAdapter smileAdapter;

    int currentPage;

    boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;

    boolean isNext;

    boolean showSmile = false;

    String chat_user_id = "";

    SocketManager socketManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        socketManager = SocketManager.getInstance();
        socketManager.setMessageListener(this);
        isNext = false;
        conversation = Parcels.unwrap(getIntent().getParcelableExtra("data"));
        setSmileData();
        showData();
        setUpRecycleView();
        currentPage = 1;
        chatPresenter = new ChatPresenter(this, compositeSubscription, this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        chatPresenter.checkSession(AppSetting.getInstance().getUser().session_id);
    }

    @Override
    protected void onStop() {
        super.onStop();
        SocketManager.getInstance().disconnect();
    }

    @OnClick(R.id.ivUser)
    public void onUserClick() {
        startActivity(ProfileActivity.class, AppSetting.getInstance().getUserData().user_id.equals(conversation.from_user_id) ? conversation.to_user_id : conversation.from_user_id, false);
    }

    @OnClick(R.id.ivFile)
    public void onFile() {
        DialogProperties properties = new DialogProperties();
        properties.selection_mode = DialogConfigs.SINGLE_MODE;
        properties.selection_type = DialogConfigs.FILE_SELECT;
        properties.root = new File(DialogConfigs.DEFAULT_DIR);
        properties.error_dir = new File(DialogConfigs.DEFAULT_DIR);
        properties.offset = new File(DialogConfigs.DEFAULT_DIR);
        properties.extensions = null;
        dialog = new FilePickerDialog(this, properties);
        dialog.setTitle("Select a File");
        dialog.setDialogSelectionListener(this);
        dialog.show();
    }

    @OnClick(R.id.ivSmile)
    public void onSmile() {
        showSmile = !showSmile;
        if (showSmile) {
            llSmile.setVisibility(View.VISIBLE);
        } else {
            llSmile.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.ivBack)
    public void onBack() {
        this.finish();
    }

    public void showData() {
        if (conversation != null) {
            if (AppSetting.getInstance().getUser().user_id.equals(conversation.from_user_id)) {
                tvName.setText(conversation.to_user_name);
                Picasso.with(this).load(conversation.photo_to).into(ivUser);
            } else {
                tvName.setText(conversation.from_user_name);
                Picasso.with(this).load(conversation.photo_from).into(ivUser);
            }
        }
    }

    @Override
    public void onError(String msg) {
        showDialog(msg);
    }

    @Override
    public void onGetMessageFirst(List<Message> messageList, int totalPage) {
        progressBar.setVisibility(View.GONE);
        this.loading = true;
        if (messageList != null && messageList.size() > 0) {
            isNext = currentPage < totalPage;
            if (isNext || messageList.size() == 0) {
                messageList.add(null);
            }
            chatAdapter.setMessages(messageList);
        }
    }

    @Override
    public void onGetMessage(List<Message> messageList, int totalPage) {
        this.loading = true;
        if (messageList != null && messageList.size() > 0) {
            isNext = currentPage < totalPage;
            if (isNext) {
                messageList.add(null);
                chatAdapter.addMessageList(messageList);
            } else {
                chatAdapter.addMessageList(messageList);
            }
        }
    }

    @Override
    public void onUploadFileSuccess(String ref) {
        chatAdapter.replaceFileMessage();
        String msg = Constants.BASE_FILE_URL + ref;
        String to = "";
        if (AppSetting.getInstance().getUserData().user_id.equals(conversation.from_user_id)) {
            to = conversation.to_user_name;
        } else {
            to = conversation.from_user_name;
        }
        if (!msg.isEmpty() && !to.isEmpty()) {
            socketManager.sendMessage(msg, to);
        }

    }

    @Override
    public void onCheckSessionResult(int valid) {
        if (valid == 1) {
            socketManager.init();
        } else {
            AppSetting.getInstance().logout();
            Toast.makeText(this, "Invalid Session", Toast.LENGTH_LONG).show();
            startActivity(LoginActivity.class, null, true);
        }
    }

    private void setUpRecycleView() {
        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setReverseLayout(true);
        rvChat.setLayoutManager(mLayoutManager);
        if (AppSetting.getInstance().getUser().user_id.equals(conversation.from_user_id)) {
            chatAdapter = new ChatAdapter(this, conversation.photo_to, conversation.photo_from);
        } else {
            chatAdapter = new ChatAdapter(this, conversation.photo_from, conversation.photo_to);
        }

        rvChat.setAdapter(chatAdapter);
        rvChat.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy < 0) { //check for scroll down{
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            if (isNext) {
                                currentPage++;
                                if (AppSetting.getInstance().getUser().user_id.equals(conversation.from_user_id)) {
                                    chatPresenter.getMessages(conversation.to_user_id, currentPage);
                                } else {
                                    chatPresenter.getMessages(conversation.from_user_id, currentPage);
                                }
                            }
                        }
                    }
                }
            }
        });
    }

    private void setSmileData() {
        rvSmile.setLayoutManager(new GridLayoutManager(this, 5, LinearLayoutManager.VERTICAL, false));
        smileAdapter = new SmileAdapter();
        smileAdapter.setSmileListener(this);
        rvSmile.setAdapter(smileAdapter);
        smilesMapReplaces.put(":angel", R.drawable.angel);
        smilesMapReplaces.put(":devil", R.drawable.devil);
        smilesMapReplaces.put(":angry", R.drawable.angry);
        smilesMapReplaces.put(":cute", R.drawable.cute);
        smilesMapReplaces.put(":desire", R.drawable.desire);

        smilesMapReplaces.put(":drink:", R.drawable.drink);
        smilesMapReplaces.put(":girl:", R.drawable.girl);
        smilesMapReplaces.put(":kiss", R.drawable.kiss);
        smilesMapReplaces.put(":love", R.drawable.love);
        smilesMapReplaces.put(":love-over", R.drawable.love_over);

        smilesMapReplaces.put(":D", R.drawable.laugh);
        smilesMapReplaces.put(":mobile:", R.drawable.mobile);
        smilesMapReplaces.put(":present:", R.drawable.present);
        smilesMapReplaces.put(":rose", R.drawable.rose);
        smilesMapReplaces.put(":flower:", R.drawable.rose);

        smilesMapReplaces.put(":shock", R.drawable.shock);
        smilesMapReplaces.put(":shutup", R.drawable.shut_mouth);
        smilesMapReplaces.put(":)", R.drawable.smile);
        smilesMapReplaces.put(":mean", R.drawable.mean);
        smilesMapReplaces.put(":neutral", R.drawable.neutral);

        smilesMapReplaces.put(":(", R.drawable.weep);
        smilesMapReplaces.put(";)", R.drawable.wink);
        smilesMapReplaces.put(":P", R.drawable.tongue);
        smilesMapReplaces.put(":(", R.drawable.sad);
        smilesMapReplaces.put(":rotfl:", R.drawable.rotfl);

        smilesMap.put(R.drawable.angel, ":angel");
        smilesMap.put(R.drawable.devil, ":devil");
        smilesMap.put(R.drawable.angry, ":angry");
        smilesMap.put(R.drawable.cute, ":cute");
        smilesMap.put(R.drawable.desire, ":desire");

        smilesMap.put(R.drawable.drink, ":drink:");
        smilesMap.put(R.drawable.girl, ":girl:");
        smilesMap.put(R.drawable.kiss, ":kiss");
        smilesMap.put(R.drawable.love, ":love");
        smilesMap.put(R.drawable.love_over, ":love-over");

        smilesMap.put(R.drawable.laugh, ":D");
        smilesMap.put(R.drawable.mobile, ":mobile:");
        smilesMap.put(R.drawable.present, ":present:");
        smilesMap.put(R.drawable.rose, ":rose");
        smilesMap.put(R.drawable.rose, ":flower:");


        smilesMap.put(R.drawable.shock, ":shock");
        smilesMap.put(R.drawable.shut_mouth, ":shutup");
        smilesMap.put(R.drawable.smile, ":)");
        smilesMap.put(R.drawable.mean, ":mean");
        smilesMap.put(R.drawable.neutral, ":neutral");

        smilesMap.put(R.drawable.weep, ";(");
        smilesMap.put(R.drawable.wink, ";)");
        smilesMap.put(R.drawable.tongue, ":P");
        smilesMap.put(R.drawable.sad, ":(");
        smilesMap.put(R.drawable.rotfl, ":rotfl:");

        List<Integer> integers = new ArrayList<>();
        integers.add(R.drawable.angel);
        integers.add(R.drawable.devil);
        integers.add(R.drawable.angry);
        integers.add(R.drawable.cute);
        integers.add(R.drawable.desire);

        integers.add(R.drawable.drink);
        integers.add(R.drawable.girl);
        integers.add(R.drawable.kiss);
        integers.add(R.drawable.love);
        integers.add(R.drawable.love_over);

        integers.add(R.drawable.laugh);
        integers.add(R.drawable.mobile);
        integers.add(R.drawable.present);
        integers.add(R.drawable.rose);
        integers.add(R.drawable.rose);

        integers.add(R.drawable.shock);
        integers.add(R.drawable.shut_mouth);
        integers.add(R.drawable.smile);
        integers.add(R.drawable.mean);
        integers.add(R.drawable.neutral);

        integers.add(R.drawable.weep);
        integers.add(R.drawable.wink);
        integers.add(R.drawable.tongue);
        integers.add(R.drawable.sad);
        integers.add(R.drawable.rotfl);
        smileAdapter.setSmiles(integers);
    }

    @Override
    public void onConnected() {
        if (AppSetting.getInstance().getUser().user_id.equals(conversation.from_user_id)) {
            chatPresenter.getMessages(conversation.to_user_id, currentPage);
            chat_user_id = conversation.to_user_id;
        } else {
            chatPresenter.getMessages(conversation.from_user_id, currentPage);
            chat_user_id = conversation.from_user_id;
        }
    }

    @Override
    public void onMsg(final JSONObject messageEntry) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                MessageEntry messageEntry1 = null;
                try {
                    String chat_msg_from_id = messageEntry.getJSONObject("profile").getString("id");
                    //only if you make sure they are the same user id that u chat from and to
                    if (chat_user_id.equals(chat_msg_from_id)) {
                        messageEntry1 = new MessageEntry(messageEntry.getString("msg"), messageEntry.getString("from"));
                        Message message = new Message();
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        message.to_user = AppSetting.getInstance().getUserData().user_id;
                        message.from_user = conversation.to_user_id.equals(AppSetting.getInstance().getUserData().user_id) ?
                                conversation.from_user_id : conversation.to_user_id;
                        message.msg = messageEntry1.getMsg();
                        message.created = simpleDateFormat.format(new Date());
                        chatAdapter.addMessage(message);
                        rvChat.scrollToPosition(0);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

    }

    @Override
    public void onSent(final JSONObject messageEntry) {
        Log.d("TAG", "sent: " + messageEntry.toString());
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (messageEntry.toString().equals("false")) {
                    if (AppSetting.getInstance().getUser().user_id.equals(conversation.from_user_id)) {
                        chatPresenter.getMessages(conversation.to_user_id, currentPage);
                    } else {
                        chatPresenter.getMessages(conversation.from_user_id, currentPage);
                    }
                } else {
                    try {
                        MessageEntry messageEntry1 = new MessageEntry(messageEntry.getString("msg"), messageEntry.getString("to"));
                        Message message = new Message();
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        message.to_user = conversation.to_user_id.equals(AppSetting.getInstance().getUserData().user_id) ? conversation.from_user_id : conversation.to_user_id;
                        message.from_user = AppSetting.getInstance().getUserData().user_id;
                        message.msg = messageEntry1.getMsg();
                        message.created = simpleDateFormat.format(new Date());
                        chatAdapter.addMessage(message);
                        rvChat.scrollToPosition(0);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

    }

    @Override
    public void onTyping(JSONObject jsonObject) {

        Log.d("TAG", "onTyping: " + jsonObject.toString());
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

            }
        });
    }

    @Override
    public void onStatus(JSONObject status) {
        Log.d("TAG", "onStt " + status.toString());
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

            }
        });
    }

    @Override
    public void onStatus(boolean jsonObject) {
        Log.d("TAG", "onStt: " + jsonObject);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

            }
        });
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    @OnClick(R.id.btnSend)
    public void onSend() {
        if (isOnline()) {
            String msg = edtChat.getText().toString().trim();
            String to = "";
            if (AppSetting.getInstance().getUser().user_id.equals(conversation.from_user_id)) {
                to = conversation.to_user_name;
            } else {
                to = conversation.from_user_name;
            }
            if (!msg.isEmpty() && !to.isEmpty()) {
                socketManager.sendMessage(msg, to);
                edtChat.getText().clear();
            }
        } else {
            showDialog("Please check your internet connection");
        }
    }


    @Override
    public void onSmileClick(Integer integer) {
        showSmile = false;
        llSmile.setVisibility(View.GONE);
        String tmpText = edtChat.getText().toString();
        tmpText = tmpText + " " + smilesMap.get(integer) + " ";
        edtChat.setText(tmpText);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case FilePickerDialog.EXTERNAL_READ_PERMISSION_GRANT: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (dialog != null) {   //Show dialog if the read permission has been granted.
                        dialog.show();
                    }
                } else {
                    //Permission has not been granted. Notify the user.
                    Toast.makeText(this, "Permission is Required for getting list of files", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    @Override
    public void onSelectedFilePaths(String[] files) {
        File file = new File(files[0]);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Message message = new Message();
        message.from_user = AppSetting.getInstance().getUserData().user_id;
        message.to_user = conversation.to_user_id.equals(AppSetting.getInstance().getUserData().user_id) ? conversation.from_user_id : conversation.to_user_id;
        message.created = simpleDateFormat.format(new Date());
        message.msg = "::FILE::" + file.getName();
        chatAdapter.addMessage(message);
        rvChat.scrollToPosition(0);
        chatPresenter.uploadFile(file);
    }
}
