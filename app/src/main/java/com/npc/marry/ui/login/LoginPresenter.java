package com.npc.marry.ui.login;

import android.content.Context;

import com.google.firebase.iid.FirebaseInstanceId;
import com.npc.marry.api.server_api.AppServicesImpl;
import com.npc.marry.app.AppSetting;
import com.npc.marry.base.BasePresenter;
import com.npc.marry.model.request.FireBaseTokenRequest;
import com.npc.marry.model.request.UserLoginRequest;
import com.npc.marry.model.response.BaseResponse;
import com.npc.marry.model.response.UserLoginResponse;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Lenovo on 2/21/2017.
 */

public class LoginPresenter extends BasePresenter<LoginView> {

    AppServicesImpl appAPIService;
    AppSetting appSetting;

    public LoginPresenter(LoginView view, Context context, CompositeSubscription compositeSubscription) {
        super(view, compositeSubscription);
        appSetting = AppSetting.getInstance();
        appAPIService = AppServicesImpl.getInstance(context);
    }

    public void sendToken() {
        compositeSubscription.add(appAPIService.sendToken(new FireBaseTokenRequest(Integer.valueOf(AppSetting.getInstance().getUser().user_id), FirebaseInstanceId.getInstance().getToken()))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<BaseResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(BaseResponse baseResponse) {
                        if (view != null) {
                            view.onSendTokenSuccess();
                        }
                    }
                }));
    }

    public void login(String username, String password) {
        compositeSubscription.add(appAPIService.login(new UserLoginRequest(username, password))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<UserLoginResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onLoginFail(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(UserLoginResponse user) {
                        appSetting.setUser(user.getUser());
                        if (user.message == null) {
                            appSetting.setToken(user.getToken());
                            if (view != null) {
                                view.onLoginSucess();
                            }
                        } else {
                            if (view != null) {
                                view.onActiveAccount();
                            }
                        }

                    }
                }));
    }

}
