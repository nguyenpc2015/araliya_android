package com.npc.marry.ui.notification;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.npc.marry.R;
import com.npc.marry.base.BaseActivity;
import com.npc.marry.base.BaseFragment;
import com.npc.marry.model.Notification;
import com.npc.marry.ui.adapter.NotificationAdapter;
import com.npc.marry.ui.profile.ProfileActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Lenovo on 3/1/2017.
 */

public class NotificationFragment extends BaseFragment implements NotificationView {

    @BindView(R.id.rvNotification)
    RecyclerView rvNotification;

    @BindView(R.id.pbProgress)
    ProgressBar pbProgress;

    NotificationAdapter notificationAdapter;

    NotificationPresenter notificationPresenter;

    int currentPage = 1;

    boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;

    boolean isNext;

    Notification notification = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notification, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setUpRecycleView();
        notificationPresenter = new NotificationPresenter(this, getContext(), compositeSubscription);
        currentPage = 1;
        loading = false;
        notificationPresenter.getNotification(String.valueOf(currentPage));
        pbProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void onError(String msg) {
        ((BaseActivity) getActivity()).dismissProgressDialog();
        ((BaseActivity) getActivity()).showDialog(msg);
    }

    @Override
    public void onGetNotification(List<Notification> notificationList, int totalPage) {
        pbProgress.setVisibility(View.GONE);
        this.loading = true;
        if (notificationList != null && notificationList.size() > 0) {
            isNext = currentPage < totalPage;
            if (isNext) {
                notificationList.add(null);
            }
            notificationAdapter.setNotificationList(notificationList);
        }
    }

    @Override
    public void onGetNotificationNext(List<Notification> notificationList, int totalPage) {
        this.loading = true;
        if (notificationList != null) {
            isNext = currentPage < totalPage;
            if (isNext) {
                notificationList.add(null);
                notificationAdapter.addNotificationList(notificationList);
            } else {
                notificationAdapter.addNotificationList(notificationList);
            }
        }
    }

    @Override
    public void onUpdateSuccess() {
        ((BaseActivity) getActivity()).dismissProgressDialog();
        if (notification != null) {
            ((BaseActivity) getActivity()).startActivity(ProfileActivity.class, String.valueOf(notification.getUser_id_from()), false);
        }
    }

    private void setUpRecycleView() {
        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rvNotification.setLayoutManager(mLayoutManager);
        android.support.v7.widget.DividerItemDecoration dividerItemDecoration = new android.support.v7.widget.DividerItemDecoration(rvNotification.getContext(),
                LinearLayoutManager.VERTICAL);
        rvNotification.addItemDecoration(dividerItemDecoration);
        notificationAdapter = new NotificationAdapter();
        notificationAdapter.setNotificationListener(this);
        rvNotification.setAdapter(notificationAdapter);
        rvNotification.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) { //check for scroll down{
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            if (isNext) {
                                currentPage++;
                                notificationPresenter.getNotification(String.valueOf(currentPage));
                            }
                        }
                    }
                }
            }
        });
    }

    @Override
    public void onItemClick(Notification notification) {
        this.notification = notification;
        notificationPresenter.updateNotification(String.valueOf(notification.getId()));
        ((BaseActivity) getActivity()).showProgressDialog(false);
    }
}
