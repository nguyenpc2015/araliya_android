package com.npc.marry.ui.video;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.npc.marry.R;
import com.npc.marry.ui.custom.BlackEditText;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by nguyen tran on 2/19/2018.
 */

public class UploadVideoDialog extends DialogFragment implements Validator.ValidationListener {
    @NotEmpty
    @BindView(R.id.edtName)
    BlackEditText edtName;
    @NotEmpty
    @BindView(R.id.edtDescription)
    BlackEditText edtDescription;
    @NotEmpty
    @BindView(R.id.edtUrl)
    BlackEditText edtUrl;

    Validator validator;

    SaveVideoListener videoListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_upload_videos, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        edtName.setText("My video");
        validator = new Validator(this);
        validator.setValidationListener(this);

    }

    @OnClick(R.id.btnSave)
    public void onSaveVideo() {
        validator.validate();
    }

    @Override
    public void onValidationSucceeded() {
        if (videoListener != null) {
            videoListener.onSaveVideo(edtName.getText().toString().trim(), edtDescription.getText().toString().trim(), edtUrl.getText().toString().trim());
            dismiss();
        }
    }

    @Override
    public void onValidationFailed(List<ValidationError> list) {
        for (ValidationError error : list) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
            }
        }
    }

    public void setVideoListener(SaveVideoListener videoListener) {
        this.videoListener = videoListener;
    }

    public interface SaveVideoListener {
        void onSaveVideo(String name, String description, String url);
    }
}
