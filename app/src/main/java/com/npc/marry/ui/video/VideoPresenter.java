package com.npc.marry.ui.video;

import android.content.Context;

import com.npc.marry.api.server_api.AppServicesImpl;
import com.npc.marry.app.AppSetting;
import com.npc.marry.base.BasePresenter;
import com.npc.marry.model.request.SaveVideoRequest;
import com.npc.marry.model.response.BaseResponse;
import com.npc.marry.model.response.UserVideoResponse;
import com.npc.marry.model.response.VideosResponse;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Lenovo on 6/10/2017.
 */

public class VideoPresenter extends BasePresenter<VideoView> {

    AppServicesImpl appServices;

    public VideoPresenter(VideoView view, CompositeSubscription compositeSubscription, Context context) {
        super(view, compositeSubscription);
        appServices = AppServicesImpl.getInstance(context);
    }

    public void getAllVideos(final int page) {
        compositeSubscription.add(
                appServices.getAllVideos(AppSetting.getInstance().getUser().user_id, String.valueOf(page))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<UserVideoResponse>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable e) {
                                if (view != null) {
                                    view.onError(e.getMessage());
                                }
                            }

                            @Override
                            public void onNext(UserVideoResponse userVideoResponse) {
                                if (view != null) {
                                    if (page == 1) {
                                        view.onGetVideoList(userVideoResponse.items, userVideoResponse.total_pages, userVideoResponse.total_items);
                                    } else {
                                        view.onGetVideoNext(userVideoResponse.items, userVideoResponse.total_pages);
                                    }
                                }
                            }
                        })
        );
    }

    public void getVideos(String user_id) {
        compositeSubscription.add(appServices.getVideos(user_id)
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<VideosResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(VideosResponse videosResponse) {
                        if (view != null) {
                            view.onGetVideoList(videosResponse.videos);
                        }
                    }
                }));
    }

    public void deleteVideo(String video_id) {
        compositeSubscription.add(
                appServices.deleteVideo(video_id)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<BaseResponse>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable throwable) {
                                if (view != null) {
                                    view.onError(throwable.getMessage());
                                }
                            }

                            @Override
                            public void onNext(BaseResponse baseResponse) {
                                if (view != null) {
                                    view.onDeleteVideoSuccess(baseResponse.message);
                                }
                            }
                        })
        );
    }

    public void uploadVideo(String url, String audio_name, String description) {
        compositeSubscription.add(
                appServices.saveVideo(new SaveVideoRequest(url, Integer.valueOf(AppSetting.getInstance().getUser().user_id), audio_name, description))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<BaseResponse>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable throwable) {
                                if (view != null) {
                                    view.onError(throwable.getMessage());
                                }
                            }

                            @Override
                            public void onNext(BaseResponse baseResponse) {
                                if (view != null) {
                                    view.onUploadVideoSuccess(baseResponse.message);
                                }
                            }
                        })
        );
    }
}
