package com.npc.marry.ui.photo;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.npc.marry.R;
import com.npc.marry.ui.custom.BlackEditText;
import com.npc.marry.ui.custom.BlackTextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Lenovo on 3/14/2017.
 */

public class UploadPhotoDialog extends DialogFragment implements Validator.ValidationListener {

    @NotEmpty
    @BindView(R.id.edtName)
    BlackEditText edtName;
    @NotEmpty
    @BindView(R.id.edtDescription)
    BlackEditText edtDescription;

    @BindView(R.id.tvPath)
    BlackTextView tvPath;

    @BindView(R.id.cbDefault)
    CheckBox cbDefault;

    Validator validator;
    String ref = "";
    String path = "";
    SavePhotoListener savePhotoListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_upload_photo, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle bundle = getArguments();
        ref = bundle.getString("ref");
        path = bundle.getString("path");
        String name = bundle.getString("name");
        tvPath.setText("Photo: " + path);
        edtName.setText("My photo");

        validator = new Validator(this);
        validator.setValidationListener(this);
    }

    @OnClick(R.id.btnSave)
    public void onSave() {
        validator.validate();
    }

    @Override
    public void onValidationSucceeded() {
        if (savePhotoListener != null) {
            savePhotoListener.onSavePhoto(edtName.getText().toString(), edtDescription.getText().toString(), ref, cbDefault.isChecked());
            dismiss();
        }
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
            }
        }
    }

    public void setSavePhotoListener(SavePhotoListener savePhotoListener) {
        this.savePhotoListener = savePhotoListener;
    }

    public interface SavePhotoListener {
        void onSavePhoto(String name, String description, String ref, boolean default_photo);
    }
}
