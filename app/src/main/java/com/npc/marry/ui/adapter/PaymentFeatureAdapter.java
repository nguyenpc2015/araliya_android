package com.npc.marry.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.npc.marry.R;
import com.npc.marry.model.PaymentFeature;
import com.npc.marry.ui.custom.BlackTextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by Lenovo on 3/17/2017.
 */

public class PaymentFeatureAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int HEADER_TYPE = 1;
    public static final int ITEM_TYPE = 2;
    public static final int FOOTER_TYPE = 3;


    List<PaymentFeature> paymentPlanList;
    PaymentListener paymentListener;

    public PaymentFeatureAdapter() {
        paymentPlanList = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case HEADER_TYPE:
                return new HeaderHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.header_payment_plan, parent, false));
            case FOOTER_TYPE:
                return new FooterHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.footer_payment_feature, parent, false));
            default:
                return new ItemHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_payment_plan, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemHolder) {
            ItemHolder itemHolder = (ItemHolder) holder;
            itemHolder.bind(paymentPlanList.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return paymentPlanList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return HEADER_TYPE;
        }
        if (position == paymentPlanList.size() - 1) {
            return FOOTER_TYPE;
        }

        return ITEM_TYPE;
    }

    public void addFeature(PaymentFeature paymentFeature) {
        paymentPlanList.add(paymentFeature);
        notifyDataSetChanged();
    }

    public void setPaymentListener(PaymentListener paymentListener) {
        this.paymentListener = paymentListener;
    }

    public interface PaymentListener {
        void onPaymentClick();
    }

    class HeaderHolder extends RecyclerView.ViewHolder {

        public HeaderHolder(View itemView) {
            super(itemView);
        }
    }

    class ItemHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvFeature)
        BlackTextView tvFeature;
        @BindView(R.id.ivFree)
        ImageView ivFree;
        @BindView(R.id.ivVerified)
        ImageView ivVerified;

        public ItemHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(PaymentFeature paymentPlan) {
            tvFeature.setText(paymentPlan.feature);
            ivFree.setImageResource(paymentPlan.free ? R.drawable.verified : R.drawable.noicon);
            ivVerified.setImageResource(paymentPlan.verified ? R.drawable.verified : R.drawable.noicon);
        }
    }

    class FooterHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.btnPayment)
        FancyButton btnPayment;

        public FooterHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.btnPayment)
        public void onPayment() {
            if (paymentListener != null) {
                paymentListener.onPaymentClick();
            }
        }
    }
}
