package com.npc.marry.ui.search;

import com.npc.marry.model.AMultiSelect;
import com.npc.marry.model.DropSelect;
import com.npc.marry.model.UserData;

import java.util.List;

/**
 * Created by nguyen tran on 2/19/2018.
 */

public interface AdvanceSearchView {
    void onError(String msg);

    void onGetAdvanceSearchParam(DropSelect dropSelect, AMultiSelect aMultiSelect);

    void onAdvanceSearchResultFirst(List<UserData> userDataList, int totalPage);

    void onAdvanceSearchResultNext(List<UserData> userDataList, int totalPage);
}
