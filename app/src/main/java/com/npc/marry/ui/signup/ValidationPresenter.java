package com.npc.marry.ui.signup;

import android.content.Context;

import com.npc.marry.api.server_api.AppServicesImpl;
import com.npc.marry.app.AppSetting;
import com.npc.marry.base.BasePresenter;
import com.npc.marry.model.request.ValidateRequest;
import com.npc.marry.model.response.BaseResponse;
import com.npc.marry.model.response.ValidationResponse;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Lenovo on 2/21/2017.
 */

public class ValidationPresenter extends BasePresenter<ValidationView> {

    AppServicesImpl appServices;
    AppSetting appSetting;

    public ValidationPresenter(ValidationView view, Context context, CompositeSubscription compositeSubscription) {
        super(view, compositeSubscription);
        appSetting = AppSetting.getInstance();
        appServices = AppServicesImpl.getInstance(context);
    }

    public void requestCode() {
        compositeSubscription.add(appServices.requestCode(appSetting.getUser().user_id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<BaseResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onValidateFail(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(BaseResponse baseResponse) {
                        if (view != null) {
                            view.onRequestCodeSuccess(baseResponse.message);
                        }
                    }
                }));
    }

    public void submitCode(String code) {
        compositeSubscription.add(appServices.validate(new ValidateRequest(appSetting.getUser().user_id, code))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ValidationResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onValidateFail(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(ValidationResponse validationResponse) {
                        appSetting.setToken(validationResponse.token);
                        if (view != null) {
                            view.onValidateSuccess();
                        }
                    }
                }));
    }
}
