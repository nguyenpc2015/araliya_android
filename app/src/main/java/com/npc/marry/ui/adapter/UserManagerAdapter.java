package com.npc.marry.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.npc.marry.R;
import com.npc.marry.app.Constants;
import com.npc.marry.base.BaseActivity;
import com.npc.marry.model.UserData;
import com.npc.marry.ui.custom.BlackTextView;
import com.npc.marry.ui.profile.ProfileActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Lenovo on 3/31/2017.
 */

public class UserManagerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int ITEM_TYPE = 1;
    private static final int FOOTER_TYPE = 2;
    private static final int HEADER = 3;

    List<UserData> userList;

    RemoveListner removeListner;

    boolean hide_Remove = false;

    public UserManagerAdapter() {
        userList = new ArrayList<>();
    }

    public void setRemoveListner(RemoveListner removeListner) {
        this.removeListner = removeListner;
    }

    public void setUserList(List<UserData> userList) {
        this.userList = userList;
        notifyDataSetChanged();
    }

    public void setHide_Remove(boolean hide_Remove) {
        this.hide_Remove = hide_Remove;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case FOOTER_TYPE:
                return new FooterHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.footer_user_list, parent, false));
            case HEADER:
                return new HeaderHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.header_user_list, parent, false));
            default:
                return new UserHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user_manager, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof UserHolder) {
            UserHolder userHolder = (UserHolder) holder;
            userHolder.bind(userList.get(position));
        }
    }

    public void addMediaList(List<UserData> mediaList) {
        int size = this.userList.size();
        this.userList.remove(size - 1);
        notifyItemRemoved(size - 1);
        size = this.userList.size();
        this.userList.addAll(mediaList);
        notifyItemRangeInserted(size, mediaList.size());
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0 && userList.size() == 1 && userList.get(0) == null) {
            return HEADER;
        } else if (userList.size() > 0 && userList.size() > Constants.PAGE_SIZE &&
                position == userList.size() - 1 && userList.get(position) == null) {
            return FOOTER_TYPE;
        }

        return ITEM_TYPE;
    }

    public void removeUser(String user_id) {
        for (Iterator<UserData> iterator = userList.iterator(); iterator.hasNext(); ) {
            UserData userData = iterator.next();
            if (userData != null && userData.user_id.equals(user_id)) {
                iterator.remove();
            }
        }
        notifyDataSetChanged();
    }

    public interface RemoveListner {
        void onItemRemove(UserData userData);
    }

    class UserHolder extends RecyclerView.ViewHolder {

        Context context;

        @BindView(R.id.ivUser)
        CircleImageView ivUser;


        @BindView(R.id.tvName)
        BlackTextView tvName;
        @BindView(R.id.tvAge)
        BlackTextView tvAge;
        @BindView(R.id.tvCity)
        BlackTextView tvCity;
        @BindView(R.id.tvEducation)
        BlackTextView tvEducation;
        @BindView(R.id.tvReligion)
        BlackTextView tvReligion;
        @BindView(R.id.tvCareer)
        BlackTextView tvCareer;
        @BindView(R.id.tvStatus)
        BlackTextView tvStatus;
        @BindView(R.id.ivRemove)
        ImageView ivRemove;

        UserData userData = null;


        public UserHolder(View itemView) {
            super(itemView);
            this.context = itemView.getContext();
            ButterKnife.bind(this, itemView);
            ivRemove.setColorFilter(ivRemove.getContext().getResources().getColor(R.color.colorAccent));
        }

        @OnClick(R.id.ivRemove)
        public void onRemove() {
            if (userData != null) {
                removeListner.onItemRemove(userData);
            }
        }

        public void bind(final UserData user) {
            this.userData = user;
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((BaseActivity) context).startActivity(ProfileActivity.class, userList, user, false);
                }
            });
            if (hide_Remove) {
                ivRemove.setVisibility(View.GONE);
            }
            tvName.setText(user.name);
            Picasso.with(context).load(user.default_photo).into(ivUser);
            tvAge.setText(user.age + "");
            tvCity.setText(user.city + " in " + user.country);
            tvEducation.setText(user.education);
            tvReligion.setText(user.religion);
            tvCareer.setText(user.career);
            tvStatus.setText(user.status);
        }
    }

    class FooterHolder extends RecyclerView.ViewHolder {

        public FooterHolder(View itemView) {
            super(itemView);
        }
    }

    class HeaderHolder extends RecyclerView.ViewHolder {

        public HeaderHolder(View itemView) {
            super(itemView);
        }
    }


}
