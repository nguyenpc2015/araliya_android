package com.npc.marry.ui.search;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Toast;

import com.npc.marry.R;
import com.npc.marry.app.AppSetting;
import com.npc.marry.base.BaseActivity;
import com.npc.marry.base.BaseFragment;
import com.npc.marry.model.AMultiSelect;
import com.npc.marry.model.DropSelect;
import com.npc.marry.model.UserData;
import com.npc.marry.model.request.AdvanceSearchRequest;
import com.npc.marry.ui.adapter.UserListAdapter;
import com.npc.marry.ui.custom.BlackTextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by Lenovo on 6/3/2017.
 */

public class AdvanceSearchFragment extends BaseFragment implements AdvanceSearchView, AdapterView.OnItemSelectedListener {

    @BindView(R.id.llCheckboxSection)
    LinearLayout llCheckboxSection;
    @BindView(R.id.spAgeFrom)
    Spinner spAgeFrom;
    @BindView(R.id.spAgeTo)
    Spinner spAgeTo;
    @BindView(R.id.spHeightFrom)
    Spinner spHeightFrom;
    @BindView(R.id.spHeightTo)
    Spinner spHeightTo;
    @BindView(R.id.spWeightFrom)
    Spinner spWeightFrom;
    @BindView(R.id.spWeightTo)
    Spinner spWeightTo;
    @BindView(R.id.llResult)
    LinearLayout llResult;
    @BindView(R.id.llSearch)
    ScrollView llSearch;
    @BindView(R.id.rvResult)
    RecyclerView rvResult;
    @BindView(R.id.pbLoading)
    ProgressBar pbLoading;
    @BindView(R.id.cbPhoto)
    CheckBox cbPhoto;

    @BindView(R.id.btnShow)
    FancyButton btnShow;

    UserListAdapter userListAdapter;
    int currentPage;

    boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;

    boolean isNext;

    List<Integer> ageList;
    List<DropSelect.HeightRange> heightRangeList;
    List<DropSelect.WeightRange> weightRangeList;

    AdvanceSearchPresenter presenter;
    List<AdvanceSearchCheckStyleAdapter> adapterList;
    AdvanceSearchRequest partnerRequest;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_advance_search, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapterList = new ArrayList<>();
        spAgeFrom.setOnItemSelectedListener(this);
        spAgeTo.setOnItemSelectedListener(this);
        spHeightFrom.setOnItemSelectedListener(this);
        spHeightTo.setOnItemSelectedListener(this);
        spWeightFrom.setOnItemSelectedListener(this);
        spWeightTo.setOnItemSelectedListener(this);
        setUpRecycleView();
        presenter = new AdvanceSearchPresenter(this, compositeSubscription, getContext());
        presenter.getAdvanceSearchParam();
    }

    @OnClick(R.id.btnShow)
    public void onSearchAgain() {
        llSearch.setVisibility(View.VISIBLE);
        llResult.setVisibility(View.GONE);
    }

    @OnClick(R.id.btnSearch)
    public void onUpdate() {
        partnerRequest = new AdvanceSearchRequest();
        partnerRequest.setUser_id(Integer.valueOf(AppSetting.getInstance().getUser().user_id));
        partnerRequest.setIs_photo(cbPhoto.isChecked() ? "yes" : "no");
        for (AdvanceSearchCheckStyleAdapter adapter : adapterList) {
            if (adapter.getTag().equals("appearance")) {
                partnerRequest.setAppearance(adapter.getParamResult());
            } else if (adapter.getTag().equals("complextion")) {
                partnerRequest.setComplexion(adapter.getParamResult());
            } else if (adapter.getTag().equals("cast")) {
                partnerRequest.setCast(adapter.getParamResult());
            } else if (adapter.getTag().equals("class")) {
                partnerRequest.setClasss(adapter.getParamResult());
            } else if (adapter.getTag().equals("residency")) {
                partnerRequest.setResidency(adapter.getParamResult());
            } else if (adapter.getTag().equals("family")) {
                partnerRequest.setFamily(adapter.getParamResult());
            } else if (adapter.getTag().equals("diet")) {
                partnerRequest.setDiet(adapter.getParamResult());
            } else if (adapter.getTag().equals("humor")) {
                partnerRequest.setHumor(adapter.getParamResult());
            } else if (adapter.getTag().equals("p_education")) {
                partnerRequest.setP_education(adapter.getParamResult());
            } else if (adapter.getTag().equals("p_body")) {
                partnerRequest.setP_body(adapter.getParamResult());
            } else if (adapter.getTag().equals("p_ethnicity")) {
                partnerRequest.setP_ethnicity(adapter.getParamResult());
            } else if (adapter.getTag().equals("p_religion")) {
                partnerRequest.setP_religion(adapter.getParamResult());
            } else if (adapter.getTag().equals("p_family")) {
                partnerRequest.setP_family(adapter.getParamResult());
            } else if (adapter.getTag().equals("p_career")) {
                partnerRequest.setP_career(adapter.getParamResult());
            } else if (adapter.getTag().equals("p_smoking")) {
                partnerRequest.setP_smoking(adapter.getParamResult());
            } else if (adapter.getTag().equals("p_drinking")) {
                partnerRequest.setP_drinking(adapter.getParamResult());
            } else if (adapter.getTag().equals("p_status")) {
                partnerRequest.setP_status(adapter.getParamResult());
            }
        }
        if (spAgeFrom.getTag() == null) {
            Toast.makeText(getContext(), "Please select age from", Toast.LENGTH_SHORT).show();
        } else if (spAgeTo.getTag() == null) {
            Toast.makeText(getContext(), "Please select age to", Toast.LENGTH_SHORT).show();
        } else if (spWeightFrom.getTag() == null) {
            Toast.makeText(getContext(), "Please select weight from", Toast.LENGTH_SHORT).show();
        } else if (spWeightTo.getTag() == null) {
            Toast.makeText(getContext(), "Please select weight to", Toast.LENGTH_SHORT).show();
        } else if (spHeightFrom.getTag() == null) {
            Toast.makeText(getContext(), "Please select height from", Toast.LENGTH_SHORT).show();
        } else if (spHeightTo.getTag() == null) {
            Toast.makeText(getContext(), "Please select height to", Toast.LENGTH_SHORT).show();
        } else {
            partnerRequest.setP_age_from((Integer) spAgeFrom.getTag());
            partnerRequest.setP_age_to((Integer) spAgeTo.getTag());
            partnerRequest.setP_weight_from((Integer) spWeightFrom.getTag());
            partnerRequest.setP_weight_to((Integer) spWeightTo.getTag());
            partnerRequest.setP_height_from((Integer) spHeightFrom.getTag());
            partnerRequest.setP_height_to((Integer) spHeightTo.getTag());
            currentPage = 1;
            llSearch.setVisibility(View.GONE);
            llResult.setVisibility(View.VISIBLE);
            pbLoading.setVisibility(View.VISIBLE);
            btnShow.setVisibility(View.VISIBLE);
            presenter.advanceSearch(partnerRequest, currentPage);
        }
    }

    @Override
    public void onError(String msg) {
        ((BaseActivity) getActivity()).dismissProgressDialog();
        Toast.makeText(this.getContext(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onGetAdvanceSearchParam(DropSelect dropSelect, AMultiSelect aMultiSelect) {
        ((BaseActivity) getActivity()).dismissProgressDialog();
        setUpPartnerLooks(dropSelect);
        setUpPartnerLifeStyles(aMultiSelect);
    }

    @Override
    public void onAdvanceSearchResultFirst(List<UserData> userDataList, int totalPage) {
        ((BaseActivity) getActivity()).dismissProgressDialog();
        this.loading = true;
        pbLoading.setVisibility(View.GONE);
        if (userDataList != null) {
            isNext = currentPage < totalPage;
            if (isNext || userDataList.size() == 0) {
                userDataList.add(null);
            }
            userListAdapter.setUserList(userDataList);
        }
    }

    @Override
    public void onAdvanceSearchResultNext(List<UserData> userDataList, int totalPage) {
        ((BaseActivity) getActivity()).dismissProgressDialog();
        this.loading = true;
        if (userDataList != null) {
            isNext = currentPage < totalPage;
            if (isNext) {
                userDataList.add(null);
                userListAdapter.addMediaList(userDataList);
            } else {
                userListAdapter.addMediaList(userDataList);
            }
        }
    }

    private void bindPartnerLifeStyleSection(List<AMultiSelect.AMultiWrapper> multiWrapper, String title, String tag) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.view_partner_lifestyle, null);
        BlackTextView tvStyleTitle = (BlackTextView) view.findViewById(R.id.tvStyleTitle);
        RecyclerView rvStylePartner = (RecyclerView) view.findViewById(R.id.rvStylePartner);
        tvStyleTitle.setText(title);
        AdvanceSearchCheckStyleAdapter adapter = new AdvanceSearchCheckStyleAdapter();
        rvStylePartner.setLayoutManager(new GridLayoutManager(getContext(), 2, GridLayoutManager.VERTICAL, false));
        rvStylePartner.setAdapter(adapter);
        adapter.setMultiWrapperList(multiWrapper);
        adapter.setTag(tag);
        adapterList.add(adapter);
        llCheckboxSection.addView(view, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
    }

    private void setUpPartnerLifeStyles(AMultiSelect multiSelect) {
        bindPartnerLifeStyleSection(new ArrayList<AMultiSelect.AMultiWrapper>(multiSelect.var_appearance), "Personal Appearance", "appearance");
        bindPartnerLifeStyleSection(new ArrayList<AMultiSelect.AMultiWrapper>(multiSelect.var_complextion), "Personal Complexion", "complextion");
        bindPartnerLifeStyleSection(new ArrayList<AMultiSelect.AMultiWrapper>(multiSelect.var_cast), "Personal Cast", "cast");
        bindPartnerLifeStyleSection(new ArrayList<AMultiSelect.AMultiWrapper>(multiSelect.var_class), "Personal Class", "class");
        bindPartnerLifeStyleSection(new ArrayList<AMultiSelect.AMultiWrapper>(multiSelect.var_residency), "Personal Residency", "residency");
        bindPartnerLifeStyleSection(new ArrayList<AMultiSelect.AMultiWrapper>(multiSelect.var_family), "Personal Family", "family");
        bindPartnerLifeStyleSection(new ArrayList<AMultiSelect.AMultiWrapper>(multiSelect.var_diet), "Personal Diet", "diet");
        bindPartnerLifeStyleSection(new ArrayList<AMultiSelect.AMultiWrapper>(multiSelect.var_humor), "Personal Humor", "humor");
        bindPartnerLifeStyleSection(new ArrayList<AMultiSelect.AMultiWrapper>(multiSelect.var_education), "Partner Education", "p_education");
        bindPartnerLifeStyleSection(new ArrayList<AMultiSelect.AMultiWrapper>(multiSelect.var_body), "Partner Body", "p_body");
        bindPartnerLifeStyleSection(new ArrayList<AMultiSelect.AMultiWrapper>(multiSelect.var_ethnicity), "Partner Ethnicity", "p_ethnicity");
        bindPartnerLifeStyleSection(new ArrayList<AMultiSelect.AMultiWrapper>(multiSelect.var_religion), "Partner Religion", "p_religion");
        bindPartnerLifeStyleSection(new ArrayList<AMultiSelect.AMultiWrapper>(multiSelect.var_family), "Partner Family", "p_family");
        bindPartnerLifeStyleSection(new ArrayList<AMultiSelect.AMultiWrapper>(multiSelect.var_career), "Partner Career", "p_career");
        bindPartnerLifeStyleSection(new ArrayList<AMultiSelect.AMultiWrapper>(multiSelect.var_smoking), "Partner Smoking", "p_smoking");
        bindPartnerLifeStyleSection(new ArrayList<AMultiSelect.AMultiWrapper>(multiSelect.var_drinking), "Partner Drinking", "p_drinking");
        bindPartnerLifeStyleSection(new ArrayList<AMultiSelect.AMultiWrapper>(multiSelect.var_status), "Partner Status", "p_status");
    }

    private void setUpPartnerLooks(DropSelect dropSelect) {
        this.heightRangeList = dropSelect.height_range;
        this.weightRangeList = dropSelect.weight_range;
        this.ageList = new ArrayList<>();
        for (int i = 18; i < 56; ++i) {
            ageList.add(i);
        }


        List<String> datas = new ArrayList<>();
        datas.add("Please Choose");
        for (DropSelect.HeightRange heightRange : heightRangeList) {
            datas.add(heightRange.title);
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, datas);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spHeightFrom.setAdapter(dataAdapter);
        spHeightFrom.setSelection(1);

        ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, datas);
        dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spHeightTo.setAdapter(dataAdapter1);
        spHeightTo.setSelection(heightRangeList.size());
        List<String> datas1 = new ArrayList<>();
        datas1.add("Please Choose");
        for (DropSelect.WeightRange weightRange : weightRangeList) {
            datas1.add(weightRange.title);
        }
        ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, datas1);
        dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spWeightFrom.setAdapter(dataAdapter2);
        spWeightFrom.setSelection(1);
        ArrayAdapter<String> dataAdapter3 = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, datas1);
        dataAdapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spWeightTo.setAdapter(dataAdapter3);
        spWeightTo.setSelection(weightRangeList.size());
        List<String> datas2 = new ArrayList<>();
        datas2.add("Please Choose");
        for (Integer integer : ageList) {
            datas2.add(String.valueOf(integer));
        }
        ArrayAdapter<String> dataAdapter4 = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, datas2);
        dataAdapter4.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spAgeFrom.setAdapter(dataAdapter4);
        spAgeFrom.setSelection(1);
        ArrayAdapter<String> dataAdapter5 = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, datas2);
        dataAdapter5.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spAgeTo.setAdapter(dataAdapter5);
        spAgeTo.setSelection(ageList.size());
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        if (i > 0) {
            switch (adapterView.getId()) {
                case R.id.spAgeFrom:
                    spAgeFrom.setTag(18 + i - 1);
                    break;
                case R.id.spAgeTo:
                    spAgeTo.setTag(18 + i - 1);
                    break;
                case R.id.spHeightFrom:
                    spHeightFrom.setTag(heightRangeList.get(i - 1).id);
                    break;
                case R.id.spHeightTo:
                    spHeightTo.setTag(heightRangeList.get(i - 1).id);
                    break;
                case R.id.spWeightFrom:
                    spWeightFrom.setTag(weightRangeList.get(i - 1).id);
                    break;
                case R.id.spWeightTo:
                    spWeightTo.setTag(weightRangeList.get(i - 1).id);
                    break;
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private void setUpRecycleView() {
        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rvResult.setLayoutManager(mLayoutManager);
        userListAdapter = new UserListAdapter();
        rvResult.setAdapter(userListAdapter);
        rvResult.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) { //check for scroll down{
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            if (isNext) {
                                currentPage++;
                                presenter.advanceSearch(partnerRequest, currentPage);
                            }
                        }
                    }
                }
            }
        });
    }
}
