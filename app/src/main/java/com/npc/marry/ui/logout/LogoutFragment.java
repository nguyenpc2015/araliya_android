package com.npc.marry.ui.logout;

import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.npc.marry.R;
import com.npc.marry.app.AppSetting;
import com.npc.marry.base.BaseActivity;
import com.npc.marry.base.BaseFragment;
import com.npc.marry.ui.about.AboutActivity;
import com.npc.marry.ui.contact.ContactUsActivity;
import com.npc.marry.ui.login.LoginActivity;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Lenovo on 6/16/2017.
 */

public class LogoutFragment extends BaseFragment implements LogoutView, NewsLetterDialog.NewsletterListener {

    LogoutPresenter logoutPresenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_logout, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        logoutPresenter = new LogoutPresenter(this, getContext(), compositeSubscription);
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    @OnClick(R.id.btnNewLetter)
    public void onUpdateNewsletter() {
        NewsLetterDialog newsLetterDialog = new NewsLetterDialog();
        newsLetterDialog.setNewsletterListener(this);
        newsLetterDialog.show(getActivity().getSupportFragmentManager(), "");
    }

    @OnClick(R.id.btnDeleteAccount)
    public void onDeleteAccount() {
        new AlertDialog.Builder(getContext())
                .setTitle("Delete account")
                .setMessage("Just remember, this action is irreversible!  If you change your mind, you will have to create a brand new profile ..! All your data including any remaining gold days, profile badges, chat/mail messages will be all lost")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        logoutPresenter.deleteAccount();
                        ((BaseActivity) getActivity()).showProgressDialog(false);
                    }
                }).setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    @OnClick(R.id.btnLogout)
    public void onLogout() {
        if (isOnline()) {
            logoutPresenter.logout();
            ((BaseActivity) getActivity()).showProgressDialog(false);
        } else {
            AppSetting.getInstance().logout();
            ((BaseActivity) getActivity()).startActivity(LoginActivity.class, null, true);
        }
    }

    @OnClick(R.id.btnAbout)
    public void onAbout() {
        ((BaseActivity) getActivity()).startActivity(AboutActivity.class, null, false);
    }

    @OnClick(R.id.btnContact)
    public void onContact() {
        ((BaseActivity) getActivity()).startActivity(ContactUsActivity.class, null, false);
    }

    @Override
    public void onLogoutSuccess() {
        AppSetting.getInstance().logout();
        ((BaseActivity) getActivity()).startActivity(LoginActivity.class, null, true);
    }

    @Override
    public void onValidationSucceeded() {

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {

    }

    @Override
    public void onLogoutFail(final String msg) {
        ((BaseActivity) getActivity()).dismissProgressDialog();
        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onUpdateNewsletterSuccess(String msg) {
        ((BaseActivity) getActivity()).dismissProgressDialog();
        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDeleteAccountSuccess(String msg) {
        ((BaseActivity) getActivity()).dismissProgressDialog();
        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
        ((BaseActivity) getActivity()).startActivity(LoginActivity.class, null, true);
    }

    @Override
    public void onError(String msg) {
        ((BaseActivity) getActivity()).dismissProgressDialog();
        new AlertDialog.Builder(getContext())
                .setTitle("Alert")
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    @Override
    public void onSubscribe(int check) {
        logoutPresenter.updateNewsletter(check);
        ((BaseActivity) getActivity()).showProgressDialog(false);
    }
}
