package com.npc.marry.ui.signup.completion;

import android.widget.AdapterView;

import com.mobsandgeeks.saripaar.Validator;
import com.npc.marry.model.City;
import com.npc.marry.model.Country;
import com.npc.marry.model.State;
import com.npc.marry.model.Text;

import java.util.List;

/**
 * Created by Lenovo on 2/27/2017.
 */

public interface Completion1View extends Validator.ValidationListener, AdapterView.OnItemSelectedListener {
    void onError(String msg);

    void onGetCountrySuccuess(List<Country> countries);

    void onGetStateSuccess(List<State> states);

    void onGetCitySuccess(List<City> cities);

    void onCompleteStep();

    void onGetHeadline(Text text);
}
