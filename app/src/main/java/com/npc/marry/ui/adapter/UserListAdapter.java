package com.npc.marry.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.npc.marry.R;
import com.npc.marry.base.BaseActivity;
import com.npc.marry.model.UserData;
import com.npc.marry.ui.custom.BlackTextView;
import com.npc.marry.ui.profile.ProfileActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Lenovo on 3/3/2017.
 */

public class UserListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int ITEM_TYPE = 1;
    private static final int FOOTER_TYPE = 2;
    private static final int HEADER = 3;

    List<UserData> userList;

    public UserListAdapter() {
        userList = new ArrayList<>();
    }

    public void setUserList(List<UserData> userList) {
        this.userList = userList;
        notifyDataSetChanged();
    }

    public void clear() {
        this.userList.clear();
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case FOOTER_TYPE:
                return new FooterHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.footer_user_list, parent, false));
            case HEADER:
                return new HeaderHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.header_user_list, parent, false));
            default:
                return new UserHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof UserHolder) {
            UserHolder userHolder = (UserHolder) holder;
            userHolder.bind(userList.get(position));
        }
    }

    public void addMediaList(List<UserData> mediaList) {
        if (this.userList.size() == 0 && mediaList != null && mediaList.size() == 1 && mediaList.get(0) == null) {
            setUserList(mediaList);
        } else if (this.userList.size() > 0 && mediaList != null && mediaList.size() == 1 && mediaList.get(0) == null) {
            int size = this.userList.size();
            this.userList.remove(size - 1);
            notifyItemRemoved(size - 1);
        } else if (this.userList.size() == 0 && mediaList.size() > 1) {
            this.userList = mediaList;
            notifyDataSetChanged();
        } else {
            int size = this.userList.size();
            this.userList.remove(size - 1);
            notifyItemRemoved(size - 1);
            size = this.userList.size();
            this.userList.addAll(mediaList);
            notifyItemRangeInserted(size, mediaList.size());
        }
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0 && userList.size() == 1 && userList.get(0) == null) {
            return HEADER;
        } else if (userList.size() > 1 &&
                position == userList.size() - 1 && userList.get(position) == null) {
            return FOOTER_TYPE;
        }

        return ITEM_TYPE;
    }

    class UserHolder extends RecyclerView.ViewHolder {

        Context context;

        @BindView(R.id.ivUser)
        CircleImageView ivUser;


        @BindView(R.id.tvName)
        BlackTextView tvName;
        @BindView(R.id.tvAge)
        BlackTextView tvAge;
        @BindView(R.id.tvCity)
        BlackTextView tvCity;
        @BindView(R.id.tvEducation)
        BlackTextView tvEducation;
        @BindView(R.id.tvReligion)
        BlackTextView tvReligion;
        @BindView(R.id.tvCareer)
        BlackTextView tvCareer;
        @BindView(R.id.tvStatus)
        BlackTextView tvStatus;

        public UserHolder(View itemView) {
            super(itemView);
            this.context = itemView.getContext();
            ButterKnife.bind(this, itemView);
        }

        public void bind(final UserData user) {
            if (user != null) {
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ((BaseActivity) context).startActivity(ProfileActivity.class, userList, user, false);
                    }
                });
                tvName.setText(user.name);
                Picasso.with(context).load(user.default_photo).into(ivUser);
                tvAge.setText(user.age + "");
                tvCity.setText(user.city + " in " + user.country);
                tvEducation.setText(user.education);
                tvReligion.setText(user.religion);
                tvCareer.setText(user.career);
                tvStatus.setText(user.status);
            }
        }
    }

    class FooterHolder extends RecyclerView.ViewHolder {

        public FooterHolder(View itemView) {
            super(itemView);
        }
    }

    class HeaderHolder extends RecyclerView.ViewHolder {

        public HeaderHolder(View itemView) {
            super(itemView);
        }
    }
}
