package com.npc.marry.ui.mail;

import com.npc.marry.api.server_api.AppServicesImpl;
import com.npc.marry.app.App;
import com.npc.marry.app.AppSetting;
import com.npc.marry.base.BasePresenter;
import com.npc.marry.model.request.RemoveMailFileRequest;
import com.npc.marry.model.request.SendMailRequest;
import com.npc.marry.model.response.BaseResponse;
import com.npc.marry.model.response.SendMailResponse;
import com.npc.marry.model.response.UploadMailFileResponse;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by nguyen tran on 2/26/2018.
 */

public class MailComposePresenter extends BasePresenter<MailComposeView> {

    AppServicesImpl appServices;

    public MailComposePresenter(MailComposeView view, CompositeSubscription compositeSubscription) {
        super(view, compositeSubscription);
        appServices = AppServicesImpl.getInstance(App.getAppContext());
    }

    public void sendMail(int user_to, String subject, String text, String ref) {
        compositeSubscription.add(
                appServices.sendMail(new SendMailRequest(Integer.valueOf(AppSetting.getInstance().getUser().user_id), user_to, subject, text, ref))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<SendMailResponse>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable throwable) {
                                if (view != null) {
                                    view.onError(throwable.getMessage());
                                }
                            }

                            @Override
                            public void onNext(SendMailResponse baseResponse) {
                                if (view != null) {
                                    view.onSendMailSuccess(baseResponse.mail);
                                }
                            }
                        })
        );
    }

    public void sendFile(File file, String ref) {
        RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("Filedata", file.getName(), requestFile);
        compositeSubscription.add(
                appServices.uploadFile(body, AppSetting.getInstance().getUser().user_id, ref)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<UploadMailFileResponse>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable throwable) {
                                if (view != null) {
                                    view.onError(throwable.getMessage());
                                }
                            }

                            @Override
                            public void onNext(UploadMailFileResponse uploadMailFileResponse) {
                                if (view != null) {
                                    view.onUploadFileSuccess(uploadMailFileResponse.file, uploadMailFileResponse.message);
                                }
                            }
                        })
        );
    }

    public void removeAttachFile(com.npc.marry.model.File file) {
        compositeSubscription.add(
                appServices.removeAttachment(new RemoveMailFileRequest(file.file_name, file.ref))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<BaseResponse>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable throwable) {
                                if (view != null) {
                                    view.onError(throwable.getMessage());
                                }
                            }

                            @Override
                            public void onNext(BaseResponse baseResponse) {
                                if (view != null) {
                                    view.onRemoveAttachmentSuccess(baseResponse.message);
                                }
                            }
                        })
        );
    }
}
