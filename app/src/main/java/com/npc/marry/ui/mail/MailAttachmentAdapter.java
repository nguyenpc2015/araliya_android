package com.npc.marry.ui.mail;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.npc.marry.R;
import com.npc.marry.app.App;
import com.npc.marry.model.File;
import com.npc.marry.ui.custom.BlackTextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by nguyen tran on 2/27/2018.
 */

public class MailAttachmentAdapter extends RecyclerView.Adapter<MailAttachmentAdapter.MailAttachmentHolder> {

    List<File> fileList;

    MailAttachmentListener attachmentListener;

    public MailAttachmentAdapter() {
        fileList = new ArrayList<>();
    }

    public void setFileList(List<File> fileList) {
        this.fileList = fileList;
        notifyDataSetChanged();
    }

    public void addFile(File file) {
        this.fileList.add(file);
        notifyDataSetChanged();
    }

    public void setAttachmentListener(MailAttachmentListener attachmentListener) {
        this.attachmentListener = attachmentListener;
    }

    @Override
    public MailAttachmentHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MailAttachmentHolder(LayoutInflater.from(App.getAppContext()).inflate(R.layout.item_mail_attachment, parent, false));
    }

    @Override
    public void onBindViewHolder(MailAttachmentHolder holder, int position) {
        holder.bind(fileList.get(position));
    }

    @Override
    public int getItemCount() {
        return fileList.size();
    }

    interface MailAttachmentListener {
        void onFileRemove(File file);
    }

    class MailAttachmentHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.ivRemove)
        ImageView ivRemove;
        @BindView(R.id.tvAttach)
        BlackTextView tvAttach;

        public MailAttachmentHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(File mail) {
            if (mail != null) {
                tvAttach.setText(mail.file_name);
                tvAttach.setTag(mail);
            }
        }

        @OnClick(R.id.ivRemove)
        public void onRemove() {
            File file = (File) tvAttach.getTag();
            if (file != null) {
                if (attachmentListener != null) {
                    attachmentListener.onFileRemove(file);
                }
                fileList.remove(file);
                notifyDataSetChanged();
            }
        }
    }
}
