package com.npc.marry.ui.myview;

import com.npc.marry.model.UserData;

import java.util.List;

/**
 * Created by nguyen tran on 1/30/2018.
 */

public interface MyViewView {
    void onError(String msg);

    void onGetUserList(List<UserData> userDataList, int totalPage);

    void onGetUserListNext(List<UserData> userDataList, int totalPage);
}
