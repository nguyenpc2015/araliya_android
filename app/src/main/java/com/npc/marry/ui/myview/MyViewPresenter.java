package com.npc.marry.ui.myview;

import android.content.Context;

import com.npc.marry.api.server_api.AppServicesImpl;
import com.npc.marry.app.AppSetting;
import com.npc.marry.base.BasePresenter;
import com.npc.marry.model.response.UserListResponse;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by nguyen tran on 1/30/2018.
 */

public class MyViewPresenter extends BasePresenter<MyViewView> {
    AppServicesImpl appServices;

    public MyViewPresenter(MyViewView view, CompositeSubscription compositeSubscription, Context context) {
        super(view, compositeSubscription);
        appServices = AppServicesImpl.getInstance(context);
    }

    public void getUserMyView(final int page) {
        compositeSubscription.add(appServices.getUserMyView(AppSetting.getInstance().getUser().user_id, String.valueOf(page))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<UserListResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable throwable) {
                        if (view != null) {
                            view.onError(throwable.getMessage());
                        }
                    }

                    @Override
                    public void onNext(UserListResponse userListResponse) {
                        if (view != null) {
                            if (page == 1) {
                                view.onGetUserList(userListResponse.users, userListResponse.total_pages);
                            } else {
                                view.onGetUserListNext(userListResponse.users, userListResponse.total_pages);
                            }
                        }
                    }
                }));
    }
}
