package com.npc.marry.ui.logout;

import android.content.Context;

import com.npc.marry.api.server_api.AppServicesImpl;
import com.npc.marry.app.AppSetting;
import com.npc.marry.base.BasePresenter;
import com.npc.marry.model.request.LogoutRequest;
import com.npc.marry.model.request.NewsletterRequest;
import com.npc.marry.model.response.BaseResponse;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Lenovo on 2/21/2017.
 */

public class LogoutPresenter extends BasePresenter<LogoutView> {

    AppServicesImpl appServices;
    AppSetting appSetting;

    public LogoutPresenter(LogoutView view, Context context, CompositeSubscription compositeSubscription) {
        super(view, compositeSubscription);
        appSetting = AppSetting.getInstance();
        appServices = AppServicesImpl.getInstance(context);
    }

    public void updateNewsletter(int check) {
        compositeSubscription.add(
                appServices.updateNewsletter(new NewsletterRequest(Integer.parseInt(appSetting.getUser().user_id), check))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<BaseResponse>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable throwable) {
                                if (view != null) {
                                    view.onError(throwable.getMessage());
                                }
                            }

                            @Override
                            public void onNext(BaseResponse baseResponse) {
                                if (view != null) {
                                    view.onUpdateNewsletterSuccess(baseResponse.message);
                                }
                            }
                        })
        );
    }

    public void deleteAccount() {
        compositeSubscription.add(
                appServices.deleteAccount(new LogoutRequest(Integer.parseInt(appSetting.getUser().user_id)))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<BaseResponse>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable throwable) {
                                if (view != null) {
                                    view.onError(throwable.getMessage());
                                }
                            }

                            @Override
                            public void onNext(BaseResponse baseResponse) {
                                if (view != null) {
                                    view.onDeleteAccountSuccess(baseResponse.message);
                                }
                            }
                        })
        );
    }

    public void logout() {
        compositeSubscription.add(appServices.logout(new LogoutRequest(Integer.parseInt(appSetting.getUser().user_id)))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<BaseResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(BaseResponse baseResponse) {
                        appSetting.logout();
                        if (view != null) {
                            view.onLogoutSuccess();
                        }
                    }
                }));
    }
}
