package com.npc.marry.ui.users;

import android.content.Context;

import com.npc.marry.api.server_api.AppServicesImpl;
import com.npc.marry.app.AppSetting;
import com.npc.marry.base.BasePresenter;
import com.npc.marry.model.response.UserListResponse;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Lenovo on 3/5/2017.
 */

public class HomePresenter extends BasePresenter<HomeView> {

    AppServicesImpl appServices;
    AppSetting appSetting;

    public HomePresenter(HomeView view, Context context, CompositeSubscription compositeSubscription) {
        super(view, compositeSubscription);
        appSetting = AppSetting.getInstance();
        appServices = AppServicesImpl.getInstance(context);
    }

    public void getUserViewedMe(final int page) {
        compositeSubscription.add(appServices.getUserViewedMe(String.valueOf(appSetting.getUser().user_id), String.valueOf(page))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<UserListResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(UserListResponse userListResponse) {
                        if (view != null) {
                            if (page == 1) {
                                view.onGetUsersSuccess(userListResponse.users, userListResponse.total_pages);
                            } else {
                                view.onGetUsersSuccessNext(userListResponse.users, userListResponse.total_pages);
                            }
                        }
                    }
                }));
    }

    public void getFavourUser(final int page) {
        compositeSubscription.add(appServices.getFavourUser(String.valueOf(appSetting.getUser().user_id), String.valueOf(page))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<UserListResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(UserListResponse userListResponse) {
                        if (view != null) {
                            if (page == 1) {
                                view.onGetUsersSuccess(userListResponse.users, userListResponse.total_pages);
                            } else {
                                view.onGetUsersSuccessNext(userListResponse.users, userListResponse.total_pages);
                            }
                        }
                    }
                }));
    }

    public void getUserFeature(final int page) {
        compositeSubscription.add(appServices.getFeaturedUser(String.valueOf(appSetting.getUser().user_id), String.valueOf(page))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<UserListResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(UserListResponse userListResponse) {
                        if (view != null) {
                            if (page == 1) {
                                view.onGetUsersSuccess(userListResponse.users, userListResponse.total_pages);
                            } else {
                                view.onGetUsersSuccessNext(userListResponse.users, userListResponse.total_pages);
                            }
                        }
                    }
                }));
    }

    public void getHoroscopeUser(final int page) {
        compositeSubscription.add(appServices.getHoroScopeUser(String.valueOf(appSetting.getUser().user_id), String.valueOf(page))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<UserListResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(UserListResponse userListResponse) {
                        if (view != null) {
                            if (page == 1) {
                                view.onGetUsersSuccess(userListResponse.users, userListResponse.total_pages);
                            } else {
                                view.onGetUsersSuccessNext(userListResponse.users, userListResponse.total_pages);
                            }
                        }
                    }
                }));
    }

    public void getNewestUser(final int page) {
        compositeSubscription.add(appServices.getNewestUser(String.valueOf(appSetting.getUser().user_id), String.valueOf(page))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<UserListResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(UserListResponse userListResponse) {
                        if (view != null) {
                            if (page == 1) {
                                view.onGetUsersSuccess(userListResponse.users, userListResponse.total_pages);
                            } else {
                                view.onGetUsersSuccessNext(userListResponse.users, userListResponse.total_pages);
                            }
                        }
                    }
                }));
    }

    public void getOnlineUser(final int page) {
        compositeSubscription.add(appServices.getOnlineUser(String.valueOf(appSetting.getUser().user_id), String.valueOf(page))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<UserListResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(UserListResponse userListResponse) {
                        if (view != null) {
                            if (page == 1) {
                                view.onGetUsersSuccess(userListResponse.users, userListResponse.total_pages);
                            } else {
                                view.onGetUsersSuccessNext(userListResponse.users, userListResponse.total_pages);
                            }
                        }
                    }
                }));
    }

    public void getRecentUser(final int page) {
        compositeSubscription.add(appServices.getRecentUser(String.valueOf(appSetting.getUser().user_id), String.valueOf(page))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<UserListResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(UserListResponse userListResponse) {
                        if (view != null) {
                            if (page == 1) {
                                view.onGetUsersSuccess(userListResponse.users, userListResponse.total_pages);
                            } else {
                                view.onGetUsersSuccessNext(userListResponse.users, userListResponse.total_pages);
                            }
                        }
                    }
                }));
    }

    public void getRecommendUser(final int page) {
        compositeSubscription.add(appServices.getRecommendUser(String.valueOf(AppSetting.getInstance().getUser().user_id), String.valueOf(page))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<UserListResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(UserListResponse userListResponse) {
                        if (view != null) {
                            if (page == 1) {
                                view.onGetUsersSuccess(userListResponse.users, userListResponse.total_pages);
                            } else {
                                view.onGetUsersSuccessNext(userListResponse.users, userListResponse.total_pages);
                            }
                        }
                    }
                }));
    }

    public void getVerifiedUser(final int page) {
        compositeSubscription.add(appServices.getVerifiedUser(String.valueOf(AppSetting.getInstance().getUser().user_id), String.valueOf(page))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<UserListResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(UserListResponse userListResponse) {
                        if (view != null) {
                            if (page == 1) {
                                view.onGetUsersSuccess(userListResponse.users, userListResponse.total_pages);
                            } else {
                                view.onGetUsersSuccessNext(userListResponse.users, userListResponse.total_pages);
                            }
                        }
                    }
                }));
    }

}
