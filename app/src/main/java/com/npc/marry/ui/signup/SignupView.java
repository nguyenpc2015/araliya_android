package com.npc.marry.ui.signup;

import android.widget.AdapterView;

import com.mobsandgeeks.saripaar.Validator;
import com.npc.marry.model.Orientation;

import java.util.List;

/**
 * Created by Lenovo on 2/21/2017.
 */

public interface SignupView extends Validator.ValidationListener, AdapterView.OnItemSelectedListener {
    void onSignupSuccess();

    void onSignupFail(String msg);

    void onOrientationSuccess(List<Orientation> orientationList);

    void onOrientationFail(String msg);

    void onValidateSuccess();

    void onValidateFail(String msg);
}
