package com.npc.marry.ui.horoscope;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.mobsandgeeks.saripaar.Validator;

/**
 * Created by Lenovo on 4/12/2017.
 */

public interface HoroscopeView extends OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, Validator.ValidationListener {
    void onError(String msg);

    void onSaveResult(String msg);
}
