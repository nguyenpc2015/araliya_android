package com.npc.marry.ui.contact;

import android.widget.AdapterView;

import com.mobsandgeeks.saripaar.Validator;

/**
 * Created by Lenovo on 3/31/2017.
 */

public interface ContactUsView extends Validator.ValidationListener, AdapterView.OnItemSelectedListener {
    void onError(String msg);

    void onContactSuccess(String msg);
}
