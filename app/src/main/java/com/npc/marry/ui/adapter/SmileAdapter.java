package com.npc.marry.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.npc.marry.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Lenovo on 5/2/2017.
 */

public class SmileAdapter extends RecyclerView.Adapter<SmileAdapter.SmileHolder> {

    List<Integer> smiles;

    SmileListener smileListener;

    public SmileAdapter() {
        smiles = new ArrayList<>();
    }

    public void setSmileListener(SmileListener smileListener) {
        this.smileListener = smileListener;
    }

    @Override
    public SmileHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SmileHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_smile, parent, false));
    }

    @Override
    public void onBindViewHolder(SmileHolder holder, int position) {
        holder.bind(smiles.get(position));
    }

    @Override
    public int getItemCount() {
        return smiles.size();
    }

    public void setSmiles(List<Integer> integers) {
        smiles = integers;
        notifyDataSetChanged();
    }

    public interface SmileListener {
        void onSmileClick(Integer integer);
    }

    class SmileHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.ivSmile)
        ImageView ivSmile;

        public SmileHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(final int smileRes) {
            ivSmile.setImageResource(smileRes);
            ivSmile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    smileListener.onSmileClick(new Integer(smileRes));
                }
            });
        }
    }
}
