package com.npc.marry.ui.chat;

import android.support.v4.widget.SwipeRefreshLayout;

import com.npc.marry.model.UserData;
import com.npc.marry.ui.adapter.ContactAdapter;

import java.util.List;

/**
 * Created by Lenovo on 4/20/2017.
 */

public interface ChatOnlineMemberView extends ContactAdapter.RemoveListner, SwipeRefreshLayout.OnRefreshListener {
    void onError(String msg);

    void onGetUsersSuccess(List<UserData> userDataList, int totalPage);

    void onGetUsersSuccessNext(List<UserData> userDataList, int totalPage);
}
