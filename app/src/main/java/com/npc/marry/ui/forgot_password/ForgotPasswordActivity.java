package com.npc.marry.ui.forgot_password;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.npc.marry.R;
import com.npc.marry.base.BaseActivity;
import com.npc.marry.ui.custom.BlackEditText;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class ForgotPasswordActivity extends BaseActivity implements ForgotPasswordView, Validator.ValidationListener {

    @Email
    @BindView(R.id.edtEmail)
    BlackEditText edtEmail;
    Validator validator;

    ForgotPasswordPresenter passwordPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        validator = new Validator(this);
        validator.setValidationListener(this);
        passwordPresenter = new ForgotPasswordPresenter(this, compositeSubscription, this);
    }

    @OnClick(R.id.btnGo)
    public void onGo() {
        validator.validate();
    }

    @Override
    public void onValidationSucceeded() {
        passwordPresenter.sendEmailToServer(edtEmail.getText().toString());
        showProgressDialog(false);
    }

    @Override
    public void onValidationFailed(List<ValidationError> list) {
        for (ValidationError error : list) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onError(String msg) {
        dismissProgressDialog();
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSentMailSuccess(String msg) {
        dismissProgressDialog();
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        startActivity(ForgotPasswordConfirmActivity.class, edtEmail.getText().toString(), true);
    }

    @OnClick(R.id.ivBack)
    public void onBack() {
        finish();
    }
}
