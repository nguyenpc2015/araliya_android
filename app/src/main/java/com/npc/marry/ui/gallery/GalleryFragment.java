package com.npc.marry.ui.gallery;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.npc.marry.R;
import com.npc.marry.app.AppSetting;
import com.npc.marry.base.BaseFragment;
import com.npc.marry.ui.adapter.ViewPagerFragmentAdapter;
import com.npc.marry.ui.photo.PhotoActivity;
import com.npc.marry.ui.video.VideoActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Lenovo on 6/16/2017.
 */

public class GalleryFragment extends BaseFragment {

    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.viewpager)
    ViewPager viewPager;

    ViewPagerFragmentAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_gallery, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter = new ViewPagerFragmentAdapter(getActivity().getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(7);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabMode(TabLayout.MODE_FIXED);

        PhotoActivity photoActivity = new PhotoActivity();
        VideoActivity videoActivity = new VideoActivity();
        Bundle bundle = new Bundle();
        bundle.putString("data", AppSetting.getInstance().getUser().user_id);
        bundle.putInt("mode", 2);
        photoActivity.setArguments(bundle);
        videoActivity.setArguments(bundle);
        adapter.addFragment(photoActivity, "Photos");
        adapter.addFragment(videoActivity, "Videos");
    }
}
