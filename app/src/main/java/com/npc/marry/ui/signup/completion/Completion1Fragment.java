package com.npc.marry.ui.signup.completion;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.npc.marry.R;
import com.npc.marry.app.AppSetting;
import com.npc.marry.base.BaseActivity;
import com.npc.marry.base.BaseFragment;
import com.npc.marry.model.City;
import com.npc.marry.model.Country;
import com.npc.marry.model.State;
import com.npc.marry.model.Text;
import com.npc.marry.ui.custom.BlackEditText;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by Lenovo on 2/25/2017.
 */

public class Completion1Fragment extends BaseFragment implements Completion1View {

    @NotEmpty
    @BindView(R.id.edtHeadline)
    BlackEditText edtHeadline;
    @NotEmpty
    @BindView(R.id.edtEssay)
    BlackEditText edtEssay;

    @BindView(R.id.spCountry)
    Spinner spCountry;
    @BindView(R.id.spState)
    Spinner spState;
    @BindView(R.id.spCity)
    Spinner spCity;

    @BindView(R.id.btnDone)
    FancyButton btnDone;

    Validator validator;
    Completion1Presenter completion1Presenter;

    List<Country> countryList = null;
    List<State> stateList = null;
    List<City> cityList = null;

    Country country = null;
    State state = null;
    City city = null;

    String mode = null;
    boolean isDone = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_completion_1, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null) {
            mode = getArguments().getString("mode");
        }
        validator = new Validator(this);
        validator.setValidationListener(this);
        spCountry.setOnItemSelectedListener(this);
        spState.setOnItemSelectedListener(this);
        spCity.setOnItemSelectedListener(this);
        completion1Presenter = new Completion1Presenter(this, getContext(), compositeSubscription);
        completion1Presenter.getCountry();
        if (mode != null) {
            completion1Presenter.getHeadline();
        } else {
            btnDone.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.btnNext)
    public void onNext() {
        isDone = false;
        validator.validate();
    }

    @OnClick(R.id.btnDone)
    public void onDone() {
        isDone = true;
        validator.validate();
    }

    @Override
    public void onError(String msg) {
        ((BaseActivity) getActivity()).dismissProgressDialog();
        new AlertDialog.Builder(getActivity())
                .setTitle("Alert")
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    @Override
    public void onGetCountrySuccuess(List<Country> countries) {
        this.countryList = countries;
        List<String> datas = new ArrayList<>();
        datas.add("Please Choose");
        for (Country country : countries) {
            datas.add(country.country_title);
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, datas);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCountry.setAdapter(dataAdapter);
        ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, new ArrayList<String>());
        dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spState.setAdapter(dataAdapter1);
        ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, new ArrayList<String>());
        dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCity.setAdapter(dataAdapter2);
        if (mode != null) {
            int country_id = AppSetting.getInstance().getUser().country_id;
            for (Country country : countries) {
                if (country.country_id.equals(String.valueOf(country_id))) {
                    spCountry.setSelection(countries.indexOf(country) + 1);
                    break;
                }
            }
        }
    }

    @Override
    public void onGetStateSuccess(List<State> states) {
        this.stateList = states;
        List<String> datas = new ArrayList<>();
        datas.add("Please Choose");
        for (State state : states) {
            datas.add(state.state_title);
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, datas);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spState.setAdapter(dataAdapter);
        ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, new ArrayList<String>());
        dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCity.setAdapter(dataAdapter1);

        if (mode != null && country != null && country.country_id.equals(String.valueOf(AppSetting.getInstance().getUser().country_id))) {
            int state_id = AppSetting.getInstance().getUser().state_id;
            for (State state : states) {
                if (state.state_id.equals(String.valueOf(state_id))) {
                    spState.setSelection(states.indexOf(state) + 1);
                    break;
                }
            }
        }
    }

    @Override
    public void onGetCitySuccess(List<City> cities) {
        this.cityList = cities;
        List<String> datas = new ArrayList<>();
        datas.add("Please Choose");
        for (City city : cities) {
            datas.add(city.city_title);
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, datas);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCity.setAdapter(dataAdapter);

        if (mode != null && country != null && state != null &&
                country.country_id.equals(String.valueOf(AppSetting.getInstance().getUser().country_id)) && state.state_id.equals(String.valueOf(AppSetting.getInstance().getUser().state_id))) {
            int city_id = AppSetting.getInstance().getUser().city_id;
            for (City city : cities) {
                if (city.city_id.equals(String.valueOf(city_id))) {
                    spCity.setSelection(cities.indexOf(city) + 1);
                    break;
                }
            }
        }
    }

    @Override
    public void onCompleteStep() {
        ((BaseActivity) getActivity()).dismissProgressDialog();
        if (isDone) {
            Intent intent = new Intent();
            intent.setAction("com.npc.complete");
            intent.putExtra("step", 4);
            getContext().sendBroadcast(intent);
        } else {
            Intent intent = new Intent();
            intent.setAction("com.npc.complete");
            intent.putExtra("step", 1);
            getContext().sendBroadcast(intent);
        }
    }

    @Override
    public void onGetHeadline(Text text) {
        if (text != null) {
            if (text.headline != null) {
                String headline = text.headline.replace("\\r\\n", "");
                edtHeadline.setText(headline);
            }
            if (text.essay != null) {
                String essay = text.essay.replace("\\r\\n", "");
                edtEssay.setText(essay);
            }
        }
    }

    @Override
    public void onValidationSucceeded() {

        String headline = edtHeadline.getText().toString().trim();
        String essay = edtEssay.getText().toString().trim();
        if (headline.length() < 11) {
            edtHeadline.setError("Please write good profile headline");
            Toast.makeText(getContext(), "You must enter at least 10 characters", Toast.LENGTH_LONG).show();
        } else if (essay.length() < 200) {
            edtEssay.setError("Please enter a descriptive marriage proposal");
            Toast.makeText(getContext(), "You must enter at least 200 characters", Toast.LENGTH_LONG).show();
        } else if (country != null) {
            if (state != null) {
                if (city != null) {
                    completion1Presenter.sendProfule1(edtHeadline.getText().toString().trim(),
                            edtEssay.getText().toString().trim(), Integer.parseInt(country.country_id), Integer.parseInt(state.state_id), Integer.parseInt(city.city_id));
                    ((BaseActivity) getActivity()).showProgressDialog(false);
                } else {
                    Toast.makeText(getContext(), "You must select city", Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(getContext(), "You must select state", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(getContext(), "You must select country", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (position > 0) {
            switch (parent.getId()) {
                case R.id.spCountry:
                    if (countryList != null) {
                        country = countryList.get(position - 1);
                        completion1Presenter.getState(Integer.parseInt(country.country_id));
                    }
                    break;
                case R.id.spState:
                    if (stateList != null) {
                        state = stateList.get(position - 1);
                        completion1Presenter.getCity(Integer.parseInt(country.country_id), Integer.parseInt(state.state_id));
                    }
                    break;
                case R.id.spCity:
                    if (cityList != null) {
                        city = cityList.get(position - 1);
                    }
                    break;
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
