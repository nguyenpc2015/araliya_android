package com.npc.marry.ui.home;

import com.npc.marry.model.UserData;

import java.util.List;

/**
 * Created by Lenovo on 3/5/2017.
 */

public interface MenuView {
    void onError(String msg);

    void onLogoutSuccess();

    void onGetDataSuccess(UserData userData);

    void onGetUsersList(List<UserData> userList);

    void onGetUserListInterest(List<UserData> userDatas);

    void onUpdateSuccess(int code);

    void onGetNewMails(int count);
}
