package com.npc.marry.ui.payment;

import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.npc.marry.R;
import com.npc.marry.base.BaseActivity;

import butterknife.BindView;
import butterknife.OnClick;

public class CheckoutActivity extends BaseActivity {

    @BindView(R.id.webview)
    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        String html = getIntent().getStringExtra("data");

        WebSettings settings = webView.getSettings();
        settings.setDefaultTextEncodingName("utf-8");
        settings.setJavaScriptEnabled(true);
        webView.loadDataWithBaseURL(null, html, "text/html", "utf-8", null);
    }

    @OnClick(R.id.ivBack)
    public void onBack() {
        finish();
    }
}
