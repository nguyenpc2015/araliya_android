package com.npc.marry.ui.search;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.npc.marry.R;
import com.npc.marry.base.BaseFragment;
import com.npc.marry.ui.adapter.ViewPagerFragmentAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by nguyen tran on 2/19/2018.
 */

public class SearchFragment extends BaseFragment {

    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.viewpager)
    ViewPager viewPager;

    ViewPagerFragmentAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search_main, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter = new ViewPagerFragmentAdapter(getActivity().getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        adapter.addFragment(new BasicSearchFragment(), "Basic");
        adapter.addFragment(new UsernameSearchFragment(), "Username");
        adapter.addFragment(new AdvanceSearchFragment(), "Advanced");
        adapter.addFragment(new NearBySearchFragment(), "Near By");
    }
}
