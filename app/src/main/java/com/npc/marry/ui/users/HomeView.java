package com.npc.marry.ui.users;

import com.npc.marry.model.UserData;

import java.util.List;

/**
 * Created by Lenovo on 3/5/2017.
 */

public interface HomeView {
    void onError(String msg);

    void onGetUsersSuccess(List<UserData> userDataList, int totalPage);

    void onGetUsersSuccessNext(List<UserData> userDataList, int totalPage);
}
