package com.npc.marry.ui.account;

import android.widget.AdapterView;

import com.mobsandgeeks.saripaar.Validator;
import com.npc.marry.model.City;
import com.npc.marry.model.Country;
import com.npc.marry.model.State;
import com.npc.marry.model.Text;

import java.util.List;

/**
 * Created by nguyen tran on 2/17/2018.
 */

public interface EditAccountView extends Validator.ValidationListener, AdapterView.OnItemSelectedListener {
    void onError(String msg);

    void onGetCountrySuccuess(List<Country> countries);

    void onGetStateSuccess(List<State> states);

    void onGetCitySuccess(List<City> cities);

    void onCompleteStep();

    void onGetHeadline(Text text);

    void onUpdatePasswordSuccess(String msg);

    void onUpdateLocationSuccess(String msg);

    void onUpdateEmailSuccess(String msg);

    void onUpdateBirthdateSuccess(String msg);
}
