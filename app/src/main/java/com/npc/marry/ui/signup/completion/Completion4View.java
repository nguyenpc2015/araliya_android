package com.npc.marry.ui.signup.completion;

import android.widget.AdapterView;

import com.npc.marry.model.response.StyleResponse;

/**
 * Created by Lenovo on 2/25/2017.
 */

public interface Completion4View extends AdapterView.OnItemSelectedListener {
    void onError(String msg);

    void onGetStyleSuccess(StyleResponse styleResponse);

    void onCompleteStep();

    void onLoginFail(String msg);

    void onLoginSucess();
}
