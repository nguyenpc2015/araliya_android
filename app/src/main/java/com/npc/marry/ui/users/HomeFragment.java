package com.npc.marry.ui.users;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.npc.marry.R;
import com.npc.marry.ui.adapter.ViewPagerFragmentAdapter;
import com.npc.marry.ui.chat.ChatOnlineMemberFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Lenovo on 3/1/2017.
 */

public class HomeFragment extends Fragment {

    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.viewpager)
    ViewPager viewPager;

    ViewPagerFragmentAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter = new ViewPagerFragmentAdapter(getActivity().getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(7);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        adapter.addFragment(new ChatOnlineMemberFragment(), "Online Members");
        adapter.addFragment(new UserFeaturedFragment(), "Featured");
        adapter.addFragment(new UserFavourFragment(), "Favours Me");
        adapter.addFragment(new UserNewestFragment(), "Newest");
        adapter.addFragment(new UserRecentFragment(), "Recent");
        adapter.addFragment(new UserRecommendFragment(), "Recommend");
        adapter.addFragment(new UserVerifiedFragment(), "Verified");
        adapter.addFragment(new UserViewedMeFragment(), "Viewed Me");
    }
}
