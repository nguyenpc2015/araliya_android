package com.npc.marry.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.npc.marry.R;
import com.npc.marry.app.Constants;
import com.npc.marry.model.Photo;
import com.npc.marry.model.PhotoWrapper;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Lenovo on 5/29/2017.
 */

public class PhotoViewerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int ITEM_TYPE = 1;

    List<Photo> photoList;
    List<PhotoWrapper> photoWrapperList;

    PhotoAdapter.PhotoListener photoListener;

    public PhotoViewerAdapter() {
        photoWrapperList = new ArrayList<>();
        photoList = new ArrayList<>();
    }

    public void setPhotoListener(PhotoAdapter.PhotoListener photoListener) {
        this.photoListener = photoListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PhotoHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_photo_viewer, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof PhotoHolder) {
            PhotoHolder photoHolder = (PhotoHolder) holder;
            if (photoWrapperList.size() > 0) {
                photoHolder.bind(photoWrapperList.get(position));
            } else {
                photoHolder.bind(photoList.get(position));
            }
        }
    }

    @Override
    public int getItemCount() {
        if (photoWrapperList.size() > 0) {
            return photoWrapperList.size();
        }
        return photoList.size();
    }

    public void addMediaList1(List<PhotoWrapper> photos) {
        int size = this.photoWrapperList.size();
        this.photoWrapperList.addAll(photos);
        notifyItemRangeInserted(size, photos.size());
    }

    public void setPhotoList1(List<PhotoWrapper> photoList) {
        this.photoWrapperList = photoList;
        notifyDataSetChanged();
    }

    public void addMediaList(List<Photo> photos) {
        int size = this.photoList.size();
        this.photoList.addAll(photos);
        notifyItemRangeInserted(size, photos.size());
    }

    public void setPhotoList(List<Photo> photoList) {
        this.photoList = photoList;
        notifyDataSetChanged();
    }


    class PhotoHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.ivPhoto)
        ImageView ivPhoto;


        Context context;

        public PhotoHolder(View itemView) {
            super(itemView);
            context = itemView.getContext();
            ButterKnife.bind(this, itemView);
        }

        public void bind(final Photo photo) {
            if (photo != null) {
                if (photo.ref != null) {
                    String url = Constants.BASE_PHOTO_URL + photo.ref + "_b.jpg";
                    Picasso.with(context).load(url).into(ivPhoto);
                    ivPhoto.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            photoListener.onPhotoClick(photo, photoList);
                        }
                    });
                }
            }
        }

        public void bind(final PhotoWrapper photoWrapper) {
            if (photoWrapper != null) {
                Photo photo = photoWrapper.photo;
                if (photo != null) {
                    if (photo.ref != null) {
                        String url = Constants.BASE_PHOTO_URL + photo.ref + "_b.jpg";
                        Picasso.with(context).load(url).into(ivPhoto);
                        ivPhoto.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                photoListener.onPhotoWrapperClick(photoWrapper, photoWrapperList);
                            }
                        });
                    }
                }
            }
        }
    }

    class FooterHolder extends RecyclerView.ViewHolder {

        public FooterHolder(View itemView) {
            super(itemView);
        }
    }
}
