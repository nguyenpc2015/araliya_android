package com.npc.marry.ui.payment;

import android.content.Context;

import com.npc.marry.api.server_api.AppServicesImpl;
import com.npc.marry.app.AppSetting;
import com.npc.marry.base.BasePresenter;
import com.npc.marry.model.response.HtmlResponse;
import com.npc.marry.model.response.PaymentPlanResponse;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Lenovo on 3/17/2017.
 */

public class PaymentPresenter extends BasePresenter<PaymentView> {
    AppServicesImpl appServices;

    public PaymentPresenter(PaymentView view, Context context, CompositeSubscription compositeSubscription) {
        super(view, compositeSubscription);
        appServices = AppServicesImpl.getInstance(context);
    }

    public void getPaymentPlan() {
        compositeSubscription.add(appServices.getPaymentPlan(AppSetting.getInstance().getUser().gender)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<PaymentPlanResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(PaymentPlanResponse paymentPlanResponse) {
                        if (view != null) {
                            view.onGetPaymentPlan(paymentPlanResponse.plans);
                        }
                    }
                }));
    }

    public void getGeneratedCode(String user_id, String item_id) {
        compositeSubscription.add(appServices.getGeneratedCode(user_id, item_id).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<HtmlResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(HtmlResponse htmlResponse) {
                        if (view != null) {
                            view.onGetGeneratedCode(htmlResponse.html);
                        }
                    }
                }));
    }
}
