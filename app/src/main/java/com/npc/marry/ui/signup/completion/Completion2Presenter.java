package com.npc.marry.ui.signup.completion;

import android.content.Context;

import com.npc.marry.api.server_api.AppServicesImpl;
import com.npc.marry.app.AppSetting;
import com.npc.marry.base.BasePresenter;
import com.npc.marry.model.request.Complete2Request;
import com.npc.marry.model.response.BaseResponse;
import com.npc.marry.model.response.RelationshipResponse;
import com.npc.marry.model.response.SpecialResponse;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Lenovo on 2/27/2017.
 */

public class Completion2Presenter extends BasePresenter<Completion2View> {

    AppServicesImpl appServices;

    public Completion2Presenter(Completion2View view, Context context, CompositeSubscription compositeSubscription) {
        super(view, compositeSubscription);
        appServices = AppServicesImpl.getInstance(context);
    }

    public void getRelationship() {
        compositeSubscription.add(appServices.getRelationship()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<RelationshipResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(RelationshipResponse relationshipResponse) {
                        if (view != null) {
                            view.onGetRelationshipSuccess(relationshipResponse.relations);
                        }
                    }
                }));
    }

    public void getSpecial() {
        compositeSubscription.add(appServices.getSpecial()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<SpecialResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(SpecialResponse specialResponse) {
                        if (view != null) {
                            view.onGetSpecialSuccess(specialResponse);
                        }
                    }
                }));
    }

    public void sendProfile(int relationid, int agefrom, int ageto, int heightid, int weighr, int body, int appear, int complex) {
        compositeSubscription.add(appServices.completeProfile2(new Complete2Request(Integer.parseInt(AppSetting.getInstance().getUser().user_id), relationid, agefrom, ageto, heightid, weighr, body, appear, complex))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<BaseResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(BaseResponse baseResponse) {
                        if (view != null) {
                            view.onComppleteStep();
                        }
                    }
                }));
    }

}
