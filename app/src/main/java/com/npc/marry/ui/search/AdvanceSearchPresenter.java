package com.npc.marry.ui.search;

import android.content.Context;

import com.npc.marry.api.server_api.AppServicesImpl;
import com.npc.marry.base.BasePresenter;
import com.npc.marry.model.request.AdvanceSearchRequest;
import com.npc.marry.model.response.AdvanceSearchParamResponse;
import com.npc.marry.model.response.UserListResponse;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by nguyen tran on 2/19/2018.
 */

public class AdvanceSearchPresenter extends BasePresenter<AdvanceSearchView> {
    AppServicesImpl appServices;

    public AdvanceSearchPresenter(AdvanceSearchView view, CompositeSubscription compositeSubscription, Context context) {
        super(view, compositeSubscription);
        appServices = AppServicesImpl.getInstance(context);
    }

    public void getAdvanceSearchParam() {
        compositeSubscription.add(
                appServices.getAdvanceSearchParam()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<AdvanceSearchParamResponse>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable throwable) {
                                if (view != null) {
                                    view.onError(throwable.getMessage());
                                }
                            }

                            @Override
                            public void onNext(AdvanceSearchParamResponse baseResponse) {
                                if (view != null) {
                                    view.onGetAdvanceSearchParam(baseResponse.drop_select, baseResponse.a_multi_select);
                                }
                            }
                        })
        );
    }

    public void advanceSearch(final AdvanceSearchRequest searchRequest, final int page) {
        compositeSubscription.add(
                appServices.advanceSearch(searchRequest, String.valueOf(page))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<UserListResponse>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable throwable) {
                                if (view != null) {
                                    view.onError(throwable.getMessage());
                                }
                            }

                            @Override
                            public void onNext(UserListResponse userListResponse) {
                                if (view != null) {
                                    if (page == 1) {
                                        view.onAdvanceSearchResultFirst(userListResponse.users, userListResponse.total_pages);
                                    } else {
                                        view.onAdvanceSearchResultNext(userListResponse.users, userListResponse.total_pages);
                                    }
                                }
                            }
                        })
        );
    }
}
