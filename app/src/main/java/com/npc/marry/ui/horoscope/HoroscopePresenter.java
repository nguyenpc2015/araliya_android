package com.npc.marry.ui.horoscope;

import android.content.Context;

import com.npc.marry.api.server_api.AppServicesImpl;
import com.npc.marry.app.AppSetting;
import com.npc.marry.base.BasePresenter;
import com.npc.marry.model.request.HoroscopeRequest;
import com.npc.marry.model.response.BaseResponse;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Lenovo on 4/12/2017.
 */

public class HoroscopePresenter extends BasePresenter<HoroscopeView> {

    AppServicesImpl appServices;

    public HoroscopePresenter(HoroscopeView view, CompositeSubscription compositeSubscription, Context context) {
        super(view, compositeSubscription);
        appServices = AppServicesImpl.getInstance(context);
    }

    public void saveHoroscope(String birth_time, float longtitue, float latitute, String location) {
        compositeSubscription.add(appServices.saveHoroscope(AppSetting.getInstance().getUser().user_id, new HoroscopeRequest(birth_time, location, longtitue, latitute))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<BaseResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(BaseResponse baseResponse) {
                        if (view != null) {
                            view.onSaveResult(baseResponse.message);
                        }
                    }
                }));
    }


}
