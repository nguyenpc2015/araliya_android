package com.npc.marry.ui.photo;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.npc.marry.R;
import com.npc.marry.app.AppSetting;
import com.npc.marry.base.BaseActivity;
import com.npc.marry.base.BaseFragment;
import com.npc.marry.model.Photo;
import com.npc.marry.model.PhotoWrapper;
import com.npc.marry.ui.adapter.PhotoAdapter;
import com.npc.marry.ui.adapter.PhotoWrapperAdapter;
import com.npc.marry.ui.custom.BlackTextView;
import com.npc.marry.ui.photo_view.PhotoViewerActivity;
import com.npc.marry.ui.profile.ProfileActivity;

import org.parceler.Parcels;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mehdi.sakout.fancybuttons.FancyButton;

public class PhotoActivity extends BaseFragment implements PhotoView {

    private final int SELECT_PHOTO = 1;
    private final int MY_PERMISSIONS_REQUEST_CODE = 5;
    @BindView(R.id.rvPhoto)
    RecyclerView rvPhoto;

    @BindView(R.id.pbLoading)
    ProgressBar pbLoading;

    @BindView(R.id.btnUpload)
    FancyButton btnUpload;

    @BindView(R.id.tvCount)
    BlackTextView tvCount;

    PhotoPresenter photoPresenter;

    PhotoAdapter photoAdapter;

    PhotoWrapperAdapter photoWrapperAdapter;

    String user_id;

    int currentPage = 0;

    boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;

    boolean isNext;
    int mode = 1;
    String picturePath = "";
    String name;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_photo, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        user_id = getArguments().getString("data");
        mode = getArguments().getInt("mode");
        if (mode == 2 || !user_id.equals(AppSetting.getInstance().getUser().user_id)) {
            btnUpload.setVisibility(View.GONE);
        }
        setUpRecycleView();
        photoPresenter = new PhotoPresenter(this, getActivity(), compositeSubscription);
        currentPage = 1;
        loading = false;
        if (mode == 1) {
            photoPresenter.getPhoto(user_id, String.valueOf(currentPage));
        } else {
            photoPresenter.getAllPhoto(String.valueOf(currentPage));
        }
        pbLoading.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.btnUpload)
    public void onUpload() {
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_CODE);
        } else {
            Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
            photoPickerIntent.setType("image/*");
            startActivityForResult(photoPickerIntent, SELECT_PHOTO);
        }
    }

    @Override
    public void onError(String msg) {
        ((BaseActivity) getActivity()).dismissProgressDialog();
        pbLoading.setVisibility(View.GONE);
        ((BaseActivity) getActivity()).showDialog(msg);
    }

    @Override
    public void onGetPhotos(List<Photo> photoList, int totalPage, int totalItem) {
        pbLoading.setVisibility(View.GONE);
        tvCount.setText("Total: " + totalItem + " photos");
        this.loading = true;
        if (photoList != null && photoList.size() > 0) {
            List<Photo> photos = new ArrayList<>();
            if (!photoList.get(0).user_id.equals(AppSetting.getInstance().getUserData().user_id)) {
                for (Photo photo : photoList) {
                    if (photo.visible.equals("Y")) {
                        photos.add(photo);
                    }
                }
            } else {
                photos = photoList;
            }
            isNext = currentPage < totalPage;
            if (isNext) {
                photos.add(null);
            }
            photoAdapter.setPhotoList(photos);
        } else {
            photoAdapter.setPhotoList(new ArrayList<Photo>());
        }
    }

    @Override
    public void onGetPhotosNext(List<Photo> photoList, int totalPage) {
        this.loading = true;
        if (photoList != null && photoList.size() > 0) {
            List<Photo> photos = new ArrayList<>();
            if (!photoList.get(0).user_id.equals(AppSetting.getInstance().getUserData().user_id)) {
                for (Photo photo : photoList) {
                    if (photo.visible.equals("Y")) {
                        photos.add(photo);
                    }
                }
            } else {
                photos = photoList;
            }
            isNext = currentPage < totalPage;
            if (isNext) {
                photos.add(null);
                photoAdapter.addMediaList(photos);
            } else {
                photoAdapter.addMediaList(photos);
            }
        }
    }

    @Override
    public void onUploadSuccess(String msg) {
        ((BaseActivity) getActivity()).dismissProgressDialog();
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
        UploadPhotoDialog uploadPhotoDialog = new UploadPhotoDialog();
        uploadPhotoDialog.setSavePhotoListener(this);
        Bundle bundle = new Bundle();
        bundle.putString("ref", msg);
        bundle.putString("path", picturePath);
        bundle.putString("name", name);
        uploadPhotoDialog.setArguments(bundle);
        uploadPhotoDialog.show(getActivity().getFragmentManager(), "save photo");
    }

    @Override
    public void onSaveSuccess(String msg) {
        ((BaseActivity) getActivity()).dismissProgressDialog();
        pbLoading.setVisibility(View.GONE);
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
        currentPage = 1;
        loading = false;
        photoPresenter.getPhoto(user_id, String.valueOf(currentPage));
        pbLoading.setVisibility(View.VISIBLE);
    }

    @Override
    public void onDeleteResult(String msg) {
        ((BaseActivity) getActivity()).dismissProgressDialog();
        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
        currentPage = 1;
        photoPresenter.getPhoto(user_id, String.valueOf(currentPage));
    }

    @Override
    public void onGetAllPhotos(List<PhotoWrapper> photoList, int totalPage, int totalItem) {
        pbLoading.setVisibility(View.GONE);
        //tvCount.setText("Total: "+ totalItem + "0+ photos");
        this.loading = true;
        if (photoList != null && photoList.size() > 0) {

            isNext = currentPage < totalPage;
            if (isNext) {
                photoList.add(null);
            }
            photoWrapperAdapter.setPhotoList(photoList);
        }
    }

    @Override
    public void onGetAllPhotosNext(List<PhotoWrapper> photoList, int totalPage) {
        this.loading = true;
        if (photoList != null && photoList.size() > 0) {
            isNext = currentPage < totalPage;
            if (isNext) {
                photoList.add(null);
                photoWrapperAdapter.addMediaList(photoList);
            } else {
                photoWrapperAdapter.addMediaList(photoList);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == MY_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, SELECT_PHOTO);
            } else {
                Toast.makeText(getContext(), "You didn't allow pick photo from Galerry", Toast.LENGTH_SHORT).show();
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        switch (requestCode) {
            case SELECT_PHOTO:
                if (resultCode == Activity.RESULT_OK) {
                    try {
                        Uri uri = imageReturnedIntent.getData();
                        String[] projection = {MediaStore.Images.Media.DATA};

                        Cursor cursor = getContext().getContentResolver().query(uri, projection, null, null, null);
                        cursor.moveToFirst();
                        int columnIndex = cursor.getColumnIndex(projection[0]);
                        picturePath = cursor.getString(columnIndex); // returns null
                        cursor.close();
                        File file = new File(picturePath);
                        name = file.getName();
                        photoPresenter.uploadPhoto(file);
                        ((BaseActivity) getActivity()).showProgressDialog(false);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
        }
    }


    private void setUpRecycleView() {
        final GridLayoutManager mLayoutManager = new GridLayoutManager(getContext(), 3, LinearLayoutManager.VERTICAL, false);
        rvPhoto.setLayoutManager(mLayoutManager);
        photoAdapter = new PhotoAdapter();
        photoAdapter.setPhotoListener(this);
        photoWrapperAdapter = new PhotoWrapperAdapter(this);
        rvPhoto.setAdapter(mode == 1 ? photoAdapter : photoWrapperAdapter);
        rvPhoto.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) { //check for scroll down{
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            if (isNext) {
                                currentPage++;
                                if (mode == 1) {
                                    photoPresenter.getPhoto(user_id, String.valueOf(currentPage));
                                } else {
                                    photoPresenter.getAllPhoto(String.valueOf(currentPage));
                                }
                            }
                        }
                    }
                }
            }
        });
    }

    @Override
    public void onSavePhoto(String name, String description, String ref, boolean default_photo) {
        photoPresenter.savePhoto(name, description, ref, default_photo);
    }

    @Override
    public void onDeletePhoto(Photo photo) {
        photoPresenter.deletePhoto(photo.photo_id);
        ((BaseActivity) getActivity()).showProgressDialog(false);
    }

    @Override
    public void onPhotoClick(Photo photo, List<Photo> photoList) {
        Intent intent = new Intent(getContext(), PhotoViewerActivity.class);
        intent.putExtra("photo", Parcels.wrap(photo));
        intent.putExtra("photo-list", Parcels.wrap(photoList));
        startActivity(intent);
    }

    @Override
    public void onPhotoWrapperClick(PhotoWrapper photoWrapper, List<PhotoWrapper> photoWrappers) {
        List<Photo> photos = new ArrayList<>();
        for (PhotoWrapper photoWrapper1 : photoWrappers) {
            if (photoWrapper1 != null)
                photos.add(photoWrapper1.photo);
        }
        Intent intent = new Intent(getContext(), PhotoViewerActivity.class);
        intent.putExtra("photo", Parcels.wrap(photoWrapper));
        intent.putExtra("photo-list", Parcels.wrap(photoWrappers));
        startActivity(intent);
    }

    @Override
    public void onNameclick(String user_id) {
        ((BaseActivity) getActivity()).startActivity(ProfileActivity.class, user_id, false);
    }
}
