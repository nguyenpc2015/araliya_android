package com.npc.marry.ui.favorite;

import android.content.Context;

import com.npc.marry.api.server_api.AppServicesImpl;
import com.npc.marry.app.AppSetting;
import com.npc.marry.base.BasePresenter;
import com.npc.marry.model.response.BaseResponse;
import com.npc.marry.model.response.UserListResponse;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Lenovo on 3/31/2017.
 */

public class FavoritePresenter extends BasePresenter<FavoriteView> {

    AppServicesImpl appServices;

    public FavoritePresenter(FavoriteView view, CompositeSubscription compositeSubscription, Context context) {
        super(view, compositeSubscription);
        appServices = AppServicesImpl.getInstance(context);
    }

    public void getFavoriteUser(final int page) {
        compositeSubscription.add(appServices.getFavoriteUSer(AppSetting.getInstance().getUser().user_id, String.valueOf(page))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<UserListResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(UserListResponse userListResponse) {
                        if (view != null) {
                            if (page == 1) {
                                view.onGetUserSuccess(userListResponse.users, userListResponse.total_pages);
                            } else {
                                view.onGetUserSuccessNext(userListResponse.users, userListResponse.total_pages);
                            }
                        }
                    }
                }));
    }

    public void removeFavorite(final String user_id) {
        compositeSubscription.add(appServices.removeFavorite(AppSetting.getInstance().getUser().user_id, user_id)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<BaseResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(BaseResponse baseResponse) {
                        if (view != null) {
                            view.onRemoveSuccess(baseResponse.message, user_id);
                        }
                    }
                }));
    }

}
