package com.npc.marry.ui.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lenovo on 3/1/2017.
 */

public class ViewPagerFragmentAdapter extends FragmentStatePagerAdapter {

    List<String> tittles;
    List<Fragment> fragments;

    public ViewPagerFragmentAdapter(FragmentManager fm) {
        super(fm);
        tittles = new ArrayList<>();
        fragments = new ArrayList<>();
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    public void setPosition(int position, Fragment fragment) {
        fragments.set(position, fragment);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tittles.get(position);
    }

    public void addFragment(Fragment fragment, String title) {
        tittles.add(title);
        fragments.add(fragment);
        notifyDataSetChanged();
    }
}
