package com.npc.marry.ui.payment;

import com.npc.marry.model.PaymentPlan;
import com.npc.marry.ui.adapter.PaymentFeatureAdapter;

import java.util.List;

/**
 * Created by Lenovo on 3/17/2017.
 */

public interface PaymentView extends PaymentFeatureAdapter.PaymentListener, PaymentPlanDialog.PaymentPlanListener {
    void onError(String msg);

    void onGetPaymentPlan(List<PaymentPlan> paymentPlanList);

    void onGetGeneratedCode(String html);
}
