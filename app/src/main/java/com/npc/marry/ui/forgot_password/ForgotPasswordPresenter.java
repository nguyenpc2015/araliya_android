package com.npc.marry.ui.forgot_password;

import android.content.Context;

import com.npc.marry.api.server_api.AppServicesImpl;
import com.npc.marry.base.BasePresenter;
import com.npc.marry.model.request.EmailRequest;
import com.npc.marry.model.response.ForgotPasswordResponse;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by nguyen tran on 2/16/2018.
 */

public class ForgotPasswordPresenter extends BasePresenter<ForgotPasswordView> {

    AppServicesImpl appAPIService;

    public ForgotPasswordPresenter(ForgotPasswordView view, CompositeSubscription compositeSubscription, Context context) {
        super(view, compositeSubscription);
        appAPIService = AppServicesImpl.getInstance(context);
    }

    public void sendEmailToServer(String email) {
        compositeSubscription.add(
                appAPIService.forgotPassword(new EmailRequest(email))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<ForgotPasswordResponse>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable throwable) {
                                if (view != null) {
                                    view.onError(throwable.getMessage());
                                }
                            }

                            @Override
                            public void onNext(ForgotPasswordResponse forgotPasswordResponse) {
                                if (view != null) {
                                    view.onSentMailSuccess(forgotPasswordResponse.message);
                                }
                            }
                        })
        );
    }
}
