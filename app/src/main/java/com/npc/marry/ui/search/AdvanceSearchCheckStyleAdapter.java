package com.npc.marry.ui.search;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.npc.marry.R;
import com.npc.marry.app.App;
import com.npc.marry.model.AMultiSelect;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by nguyen tran on 2/19/2018.
 */

public class AdvanceSearchCheckStyleAdapter extends RecyclerView.Adapter<AdvanceSearchCheckStyleAdapter.StyleCheckPartnerHolder> {

    List<AMultiSelect.AMultiWrapper> multiWrapperList;

    String tag;

    public AdvanceSearchCheckStyleAdapter() {
        multiWrapperList = new ArrayList<>();
    }

    public List<AMultiSelect.AMultiWrapper> getMultiWrapperList() {
        return multiWrapperList;
    }

    public void setMultiWrapperList(List<AMultiSelect.AMultiWrapper> multiWrapperList) {
        this.multiWrapperList = multiWrapperList;
        notifyDataSetChanged();
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public List<Integer> getParamResult() {
        List<Integer> integers = new ArrayList<>();
        for (AMultiSelect.AMultiWrapper wrapper : multiWrapperList) {
            if (wrapper.checked) {
                integers.add(wrapper.id);
            }
        }
        return integers;
    }

    @Override
    public StyleCheckPartnerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new StyleCheckPartnerHolder(LayoutInflater.from(App.getAppContext()).inflate(R.layout.item_check_partner_style, parent, false));

    }

    @Override
    public void onBindViewHolder(StyleCheckPartnerHolder holder, int position) {
        holder.bind(multiWrapperList.get(position), position);
    }

    @Override
    public int getItemCount() {
        return multiWrapperList.size();
    }

    class StyleCheckPartnerHolder extends RecyclerView.ViewHolder implements CompoundButton.OnCheckedChangeListener {

        @BindView(R.id.cbCheck)
        CheckBox cbCheck;
        int position;

        public StyleCheckPartnerHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            cbCheck.setOnCheckedChangeListener(this);
            position = 0;
        }

        public void bind(AMultiSelect.AMultiWrapper wrapper, int position) {
            this.position = position;
            cbCheck.setText(wrapper.title);
            cbCheck.setTag(wrapper);
        }

        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            AMultiSelect.AMultiWrapper wrapper = (AMultiSelect.AMultiWrapper) cbCheck.getTag();
            if (wrapper != null) {
                wrapper.checked = b;
                multiWrapperList.set(position, wrapper);
            }
        }
    }
}
