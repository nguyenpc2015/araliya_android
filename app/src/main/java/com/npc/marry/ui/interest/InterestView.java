package com.npc.marry.ui.interest;

import com.npc.marry.model.UserData;
import com.npc.marry.ui.adapter.UserManagerAdapter;

import java.util.List;

/**
 * Created by Lenovo on 3/29/2017.
 */

public interface InterestView extends UserManagerAdapter.RemoveListner {
    void onError(String msg);

    void onGetUserList(List<UserData> userDataList, int totalPage);

    void onGetUserListNext(List<UserData> userDataList, int totalPage);

    void onRemoveSuccess(String msg, String user_id);
}
