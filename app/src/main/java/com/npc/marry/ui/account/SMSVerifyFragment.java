package com.npc.marry.ui.account;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.npc.marry.R;
import com.npc.marry.base.BaseActivity;
import com.npc.marry.base.BaseFragment;
import com.npc.marry.model.User;
import com.npc.marry.ui.custom.BlackEditText;
import com.npc.marry.ui.custom.BlackTextView;
import com.rilixtech.Country;
import com.rilixtech.CountryCodePicker;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by nguyen tran on 2/17/2018.
 */

public class SMSVerifyFragment extends BaseFragment implements SMSVerifyView {
    @BindView(R.id.tvVerifySuccess)
    BlackTextView tvVerifySuccess;
    @BindView(R.id.llVerify)
    LinearLayout llVerify;
    @BindView(R.id.edtMobile)
    BlackEditText edtMobile;
    @BindView(R.id.ccp)
    CountryCodePicker cpp;
    @BindView(R.id.llSendCode)
    LinearLayout llSendCode;
    @BindView(R.id.edtCode)
    BlackEditText edtCode;

    SMSVerifyPresenter presenter;

    int countryCode = 94;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sms_verify, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter = new SMSVerifyPresenter(this, compositeSubscription, getContext());
        presenter.getUser();
        ((BaseActivity) getActivity()).showProgressDialog(false);

    }

    @OnClick(R.id.btnDone)
    public void onDone() {
        if (edtCode.getText().toString().isEmpty()) {
            edtCode.setError("Please enter your activation code");
        } else {
            presenter.validateSMS(edtCode.getText().toString().trim());
            ((BaseActivity) getActivity()).showProgressDialog(false);
        }
    }

    @OnClick(R.id.btnVerify)
    public void onVerify() {
        if (edtMobile.getText().toString().isEmpty()) {
            edtMobile.setError("Please enter your mobile number");
        } else {
            presenter.sendSMSVerify(countryCode, edtMobile.getText().toString().trim());
            ((BaseActivity) getActivity()).showProgressDialog(false);
        }
    }

    @Override
    public void onError(String msg) {
        ((BaseActivity) getActivity()).dismissProgressDialog();
        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSentSMSSuccess(String msg) {
        ((BaseActivity) getActivity()).dismissProgressDialog();
        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
        llVerify.setVisibility(View.GONE);
        llSendCode.setVisibility(View.VISIBLE);
    }

    @Override
    public void onValidateSuccess(String msg) {
        ((BaseActivity) getActivity()).dismissProgressDialog();
        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
        llSendCode.setVisibility(View.GONE);
        tvVerifySuccess.setVisibility(View.VISIBLE);
    }

    @Override
    public void onGetUser(User user) {
        ((BaseActivity) getActivity()).dismissProgressDialog();
        llSendCode.setVisibility(View.GONE);
        if (user.verified == 0) {
            tvVerifySuccess.setVisibility(View.GONE);
            llVerify.setVisibility(View.VISIBLE);
            cpp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
                @Override
                public void onCountrySelected(Country country) {
                    countryCode = Integer.valueOf(country.getPhoneCode());
                }
            });
        } else {
            tvVerifySuccess.setVisibility(View.VISIBLE);
            llVerify.setVisibility(View.GONE);
        }
    }
}
