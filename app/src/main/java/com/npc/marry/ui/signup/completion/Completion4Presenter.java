package com.npc.marry.ui.signup.completion;

import android.content.Context;

import com.npc.marry.api.server_api.AppServicesImpl;
import com.npc.marry.app.AppSetting;
import com.npc.marry.base.BasePresenter;
import com.npc.marry.model.request.Completion4Request;
import com.npc.marry.model.request.UserLoginRequest;
import com.npc.marry.model.response.StyleResponse;
import com.npc.marry.model.response.UserLoginResponse;
import com.npc.marry.model.response.UserRegisterResponse;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Lenovo on 2/25/2017.
 */

public class Completion4Presenter extends BasePresenter<Completion4View> {

    AppServicesImpl appServices;

    public Completion4Presenter(Completion4View view, Context context, CompositeSubscription compositeSubscription) {
        super(view, compositeSubscription);
        appServices = AppServicesImpl.getInstance(context);
    }

    public void getStyle() {
        compositeSubscription.add(appServices.getStyle()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<StyleResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(StyleResponse styleResponse) {
                        if (view != null) {
                            view.onGetStyleSuccess(styleResponse);
                        }
                    }
                }));
    }

    public void sendProfile(int smokingid, int drinkingid, int dietid, int personalid, int firstid, int livingid) {
        compositeSubscription.add(appServices.completeProfile4(new Completion4Request(Integer.parseInt(AppSetting.getInstance().getUser().user_id), smokingid, drinkingid, dietid, personalid, firstid, livingid))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<UserRegisterResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(UserRegisterResponse userRegisterResponse) {
                        AppSetting.getInstance().setUser(userRegisterResponse.user);
                        if (view != null) {
                            view.onCompleteStep();
                        }
                    }
                }));
    }

    public void login(final String username, String password) {
        compositeSubscription.add(appServices.login(new UserLoginRequest(username, password))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<UserLoginResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onLoginFail(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(UserLoginResponse user) {
                        AppSetting.getInstance().setUser(user.getUser());
                        AppSetting.getInstance().setToken(user.getToken());
                        if (view != null) {
                            view.onLoginSucess();
                        }
                    }
                }));
    }


}
