package com.npc.marry.ui.login;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;
import com.npc.marry.R;
import com.npc.marry.base.BaseActivity;
import com.npc.marry.ui.custom.BlackEditText;
import com.npc.marry.ui.forgot_password.ForgotPasswordActivity;
import com.npc.marry.ui.main.MainActivity;
import com.npc.marry.ui.signup.ConfirmSignupActivity;
import com.npc.marry.ui.signup.SignupActivity;
import com.npc.marry.ui.splash.SplashActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import mehdi.sakout.fancybuttons.FancyButton;

public class LoginActivity extends BaseActivity implements LoginView {

    @NotEmpty
    @BindView(R.id.edtUsername)
    BlackEditText edtUsernamne;

    @Password(min = 4)
    @BindView(R.id.edtPassword)
    BlackEditText edtPassword;

    @BindView(R.id.btnLogin)
    FancyButton btnLogin;

    Validator validator;
    LoginPresenter loginPresenter;

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            boolean result = intent.getBooleanExtra("result", false);
            if (result) {
                startActivity(MainActivity.class, null, true);
            } else {
                Toast.makeText(LoginActivity.this, "Can not register your device", Toast.LENGTH_SHORT).show();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        validator = new Validator(this);
        validator.setValidationListener(this);
        loginPresenter = new LoginPresenter(this, this, compositeSubscription);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getApplicationContext().registerReceiver(mReceiver, new IntentFilter("com.result.notification"));
    }

    @Override
    protected void onPause() {
        super.onPause();
        getApplicationContext().unregisterReceiver(mReceiver);
    }

    @OnClick(R.id.ivBack)
    public void onBack() {
        startActivity(SplashActivity.class, null, true);
    }

    @OnClick(R.id.btnLogin)
    public void onLogin() {
        validator.validate();
    }

    @OnClick(R.id.tvRegister)
    public void onSignup() {
        startActivity(SignupActivity.class, null, true);
    }

    @OnClick(R.id.tvForgotPassword)
    public void onForgotPassword() {
        startActivity(ForgotPasswordActivity.class, null, false);
    }

    @Override
    public void onValidationSucceeded() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String username = edtUsernamne.getText().toString().trim();
                String password = edtPassword.getText().toString().trim();
                if (username.length() < 3) {
                    edtUsernamne.setError("Please enter at least 3 characters");
                } else {
                    showProgressDialog(false);
                    loginPresenter.login(username, password);

                }
            }
        });
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onLoginSucess() {
        loginPresenter.sendToken();
    }

    @Override
    public void onLoginFail(final String msg) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dismissProgressDialog();
                showDialog(msg);
            }
        });

    }

    @Override
    public void onSendTokenSuccess() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dismissProgressDialog();
                startActivity(MainActivity.class, null, true);
            }
        });

    }

    @Override
    public void onError(final String msg) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dismissProgressDialog();
                showDialog(msg);
            }
        });

    }

    @Override
    public void onActiveAccount() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dismissProgressDialog();
                Toast.makeText(LoginActivity.this, "please activate your account", Toast.LENGTH_SHORT).show();
                startActivity(ConfirmSignupActivity.class, null, false);
            }
        });

    }
}
