package com.npc.marry.ui.account;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;

import com.npc.marry.R;
import com.npc.marry.base.BaseActivity;
import com.npc.marry.ui.adapter.ViewPagerFragmentAdapter;

import butterknife.BindView;
import butterknife.OnClick;

public class MyAccountActivity extends BaseActivity {
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.viewpager)
    ViewPager viewPager;

    ViewPagerFragmentAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_account);
        adapter = new ViewPagerFragmentAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        adapter.addFragment(new EditAccountFragment(), "Edit Account");
        adapter.addFragment(new SMSVerifyFragment(), "SMS Verification");

    }

    @OnClick(R.id.ivBack)
    public void onBack() {
        finish();
    }
}
