package com.npc.marry.ui.search;

import android.content.Context;

import com.npc.marry.api.server_api.AppServicesImpl;
import com.npc.marry.app.AppSetting;
import com.npc.marry.base.BasePresenter;
import com.npc.marry.model.request.BasicSearchRequest;
import com.npc.marry.model.response.CityResponse;
import com.npc.marry.model.response.CountryResponse;
import com.npc.marry.model.response.StateResponse;
import com.npc.marry.model.response.UserListResponse;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Lenovo on 3/10/2017.
 */

public class SearchPresenter extends BasePresenter<SearchView> {

    AppServicesImpl appServices;

    public SearchPresenter(SearchView view, Context context, CompositeSubscription compositeSubscription) {
        super(view, compositeSubscription);
        appServices = AppServicesImpl.getInstance(context);
    }

    public void getCountry() {
        compositeSubscription.add(appServices.getCountry()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CountryResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(CountryResponse countryResponse) {
                        if (view != null) {
                            view.onGetCountrySuccuess(countryResponse.countries);
                        }
                    }
                }));
    }

    public void getState(int country_id) {
        compositeSubscription.add(appServices.getState(country_id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<StateResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(StateResponse stateResponse) {
                        if (view != null) {
                            view.onGetStateSuccess(stateResponse.states);
                        }
                    }
                }));
    }

    public void getCity(int country_id, int city_id) {
        compositeSubscription.add(appServices.getCity(country_id, city_id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CityResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(CityResponse cityResponse) {
                        if (view != null) {
                            view.onGetCitySuccess(cityResponse.cities);
                        }
                    }
                }));
    }

    public void searchByName(final int page, String keyword) {
        compositeSubscription.add(appServices.searchByName(AppSetting.getInstance().getUser().user_id, keyword, String.valueOf(page))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<UserListResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(UserListResponse userListResponse) {
                        if (view != null) {
                            if (page == 1) {
                                view.onGetSearchResult(userListResponse.users, userListResponse.total_pages);
                            } else {
                                view.onGetSearchResultNext(userListResponse.users, userListResponse.total_pages);
                            }

                        }
                    }
                }));
    }

    public void search(final int page, int country_id, int state_id, int city_id, int status, int photo, int age_from, int age_to) {
        appServices.search(page, new BasicSearchRequest(String.valueOf(AppSetting.getInstance().getUser().user_id), country_id, state_id, city_id, status, photo, age_from, age_to, ""))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<UserListResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(UserListResponse userListResponse) {
                        if (view != null) {
                            if (page == 1) {
                                view.onGetSearchResult(userListResponse.users, userListResponse.total_pages);
                            } else {
                                view.onGetSearchResultNext(userListResponse.users, userListResponse.total_pages);
                            }

                        }
                    }
                });
    }
}
