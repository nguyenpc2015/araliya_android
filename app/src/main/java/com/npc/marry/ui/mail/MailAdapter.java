package com.npc.marry.ui.mail;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.npc.marry.R;
import com.npc.marry.model.Mail;
import com.npc.marry.ui.custom.BlackTextView;
import com.npc.marry.utils.DateUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by nguyen tran on 2/22/2018.
 */

public class MailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int HEADER_TYPE = 1;
    private static final int ITEM_TYPE = 2;
    private static final int FOOTER_TYPE = 3;

    List<Mail> mailList;

    MailListener mailListener;

    public MailAdapter() {
        mailList = new ArrayList<>();
    }

    public void clear() {
        this.mailList.clear();
        notifyDataSetChanged();
    }

    public void setMailList(List<Mail> mailList) {
        if (this.mailList.size() == 0) {
            if (mailList != null && mailList.size() > 0) {
                this.mailList = mailList;
            } else {
                this.mailList.add(null);
            }
            notifyDataSetChanged();
        } else {
            if (mailList != null && mailList.size() > 0) {
                int size = this.mailList.size();
                this.mailList.remove(size - 1);
                notifyItemRemoved(size - 1);
                this.mailList.addAll(mailList);
                notifyItemRangeInserted(size - 1, mailList.size());
            }
        }
    }

    public void setMailListener(MailListener mailListener) {
        this.mailListener = mailListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case HEADER_TYPE:
                return new HeaderHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_header, parent, false));
            case FOOTER_TYPE:
                return new FooterHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.footer_user_list, parent, false));
            default:
                return new MailHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_mail, parent, false));
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof MailHolder) {
            ((MailHolder) holder).bind(mailList.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return mailList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0 && mailList.get(position) == null && mailList.size() == 1) {
            return HEADER_TYPE;
        } else if (position > 0 && mailList.size() > 0 && mailList.get(position) == null && position == mailList.size() - 1) {
            return FOOTER_TYPE;
        } else {
            return ITEM_TYPE;
        }
    }

    interface MailListener {
        void onMailClick(Mail mail);
    }

    class MailHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.ivUser)
        CircleImageView ivUser;
        @BindView(R.id.tvUser)
        BlackTextView tvUser;
        @BindView(R.id.tvTime)
        BlackTextView tvTime;
        @BindView(R.id.tvTitle)
        BlackTextView tvTitle;
        @BindView(R.id.tvDetail)
        BlackTextView tvDetail;

        public MailHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        public void bind(Mail mail) {
            if (mail != null) {
                tvTime.setText(DateUtils.getDisplayTime(tvTime.getContext(), mail.date));
                tvTitle.setText(mail.subject);
                tvDetail.setText(mail.text);
                tvUser.setText(mail.user_name);
                Picasso.with(ivUser.getContext()).load(mail.photo).into(ivUser);
                tvUser.setTag(mail);
                if (mail._new.equals("Y")) {
                    tvTitle.setTextColor(tvTitle.getResources().getColor(R.color.colorPrimary));
                } else {
                    tvTitle.setTextColor(Color.parseColor("#000000"));
                }
            }
        }


        @Override
        public void onClick(View view) {
            Mail mail = (Mail) tvUser.getTag();
            if (mail != null) {
                if (mailListener != null) {
                    mailListener.onMailClick(mail);
                }
            }
        }
    }

    class FooterHolder extends RecyclerView.ViewHolder {

        public FooterHolder(View itemView) {
            super(itemView);
        }
    }

    class HeaderHolder extends RecyclerView.ViewHolder {

        public HeaderHolder(View itemView) {
            super(itemView);
        }
    }
}
