package com.npc.marry.ui.account;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.Password;
import com.npc.marry.R;
import com.npc.marry.app.AppSetting;
import com.npc.marry.base.BaseActivity;
import com.npc.marry.base.BaseFragment;
import com.npc.marry.model.City;
import com.npc.marry.model.Country;
import com.npc.marry.model.State;
import com.npc.marry.model.Text;
import com.npc.marry.ui.custom.BlackEditText;
import com.npc.marry.ui.custom.BlackTextView;
import com.npc.marry.utils.UIUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by nguyen tran on 2/17/2018.
 */

public class EditAccountFragment extends BaseFragment implements EditAccountView {

    @BindView(R.id.edtEmail)
    BlackEditText edtEmail;
    @BindView(R.id.spCountry)
    Spinner spCountry;
    @BindView(R.id.spState)
    Spinner spState;
    @BindView(R.id.spCity)
    Spinner spCity;
    @Password(min = 6, scheme = Password.Scheme.ALPHA_NUMERIC)
    @BindView(R.id.edtPassword)
    BlackEditText edtPassword;
    @ConfirmPassword
    @BindView(R.id.edtRPassword)
    BlackEditText edtRPassword;
    @BindView(R.id.tvYear)
    BlackTextView tvYear;
    @BindView(R.id.spMonth)
    Spinner spMonth;
    @BindView(R.id.spDate)
    Spinner spDate;

    EditAccountPresenter completion1Presenter;

    List<Country> countryList = null;
    List<State> stateList = null;
    List<City> cityList = null;

    Country country = null;
    State state = null;
    City city = null;

    Validator validator;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_edit_account, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        spCountry.setOnItemSelectedListener(this);
        spState.setOnItemSelectedListener(this);
        spCity.setOnItemSelectedListener(this);
        spMonth.setOnItemSelectedListener(this);
        spDate.setOnItemSelectedListener(this);
        edtEmail.setText(AppSetting.getInstance().getUserData().mail);
        completion1Presenter = new EditAccountPresenter(this, getContext(), compositeSubscription);
        completion1Presenter.getCountry();
        validator = new Validator(this);
        validator.setValidationListener(this);
        String birth = AppSetting.getInstance().getUserData().birth;
        String[] births = birth.split("-");
        tvYear.setText(births[0]);
        List<String> months = new ArrayList<>();
        for (int i = 1; i <= 12; ++i) {
            months.add(String.valueOf(i));
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, months);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spMonth.setAdapter(dataAdapter);
        List<String> dates = new ArrayList<>();
        for (int i = 1; i <= 31; ++i) {
            dates.add(String.valueOf(i));
        }
        ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, dates);
        dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spDate.setAdapter(dataAdapter1);
        spMonth.setSelection(Integer.valueOf(births[1]) - 1);
        spDate.setSelection(Integer.valueOf(births[2]) - 1);
    }

    @Override
    public void onError(String msg) {
        ((BaseActivity) getActivity()).dismissProgressDialog();
        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onGetCountrySuccuess(List<Country> countries) {
        this.countryList = countries;
        List<String> datas = new ArrayList<>();
        datas.add("Please Choose");
        for (Country country : countries) {
            datas.add(country.country_title);
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, datas);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCountry.setAdapter(dataAdapter);
        ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, new ArrayList<String>());
        dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spState.setAdapter(dataAdapter1);
        ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, new ArrayList<String>());
        dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCity.setAdapter(dataAdapter2);
        int country_id = AppSetting.getInstance().getUser().country_id;
        for (Country country : countries) {
            if (country.country_id.equals(String.valueOf(country_id))) {
                spCountry.setSelection(countries.indexOf(country) + 1);
                break;
            }
        }
    }

    @OnClick(R.id.btnUpdatePassword)
    public void onPasswordUpdate() {
        validator.validate();
    }

    @OnClick(R.id.btnUpdateEmail)
    public void onEmailUpdate() {
        if (edtEmail.getText().toString().isEmpty()) {
            edtEmail.setError("please enter your email");
        } else if (!UIUtils.emailValidator(edtEmail.getText().toString())) {
            edtEmail.setError("invalid email format");
        } else {
            completion1Presenter.updateEmail(edtEmail.getText().toString());
            ((BaseActivity) getActivity()).showProgressDialog(false);
        }
    }

    @OnClick(R.id.btnUpdatLocation)
    public void onLocationUpdate() {
        if (country == null) {
            Toast.makeText(getContext(), "please select country", Toast.LENGTH_SHORT).show();
        } else if (state == null) {
            Toast.makeText(getContext(), "please select state", Toast.LENGTH_SHORT).show();
        } else if (city == null) {
            Toast.makeText(getContext(), "please select city", Toast.LENGTH_SHORT).show();
        } else {
            completion1Presenter.updateLocation(Integer.valueOf(country.country_id), Integer.valueOf(state.state_id), Integer.valueOf(city.city_id));
            ((BaseActivity) getActivity()).showProgressDialog(false);
        }
    }

    @OnClick(R.id.btnUpdatBirth)
    public void onBirthdateUpdate() {
        if (spMonth.getTag() == null) {
            Toast.makeText(getContext(), "please select your birth month", Toast.LENGTH_SHORT).show();
        } else if (spDate.getTag() == null) {
            Toast.makeText(getContext(), "please select your birth day", Toast.LENGTH_SHORT).show();
        } else {
            completion1Presenter.updateBirthdate(tvYear.getText().toString() + "-" + spMonth.getTag() + "-" + spDate.getTag());
            ((BaseActivity) getActivity()).showProgressDialog(false);
        }
    }

    @Override
    public void onGetStateSuccess(List<State> states) {
        this.stateList = states;
        List<String> datas = new ArrayList<>();
        datas.add("Please Choose");
        for (State state : states) {
            datas.add(state.state_title);
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, datas);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spState.setAdapter(dataAdapter);
        ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, new ArrayList<String>());
        dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCity.setAdapter(dataAdapter1);
        int state_id = AppSetting.getInstance().getUser().state_id;
        for (State state : states) {
            if (state.state_id.equals(String.valueOf(state_id))) {
                spState.setSelection(states.indexOf(state) + 1);
                break;
            }
        }
    }

    @Override
    public void onGetCitySuccess(List<City> cities) {
        this.cityList = cities;
        List<String> datas = new ArrayList<>();
        datas.add("Please Choose");
        for (City city : cities) {
            datas.add(city.city_title);
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, datas);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCity.setAdapter(dataAdapter);
        int city_id = AppSetting.getInstance().getUser().city_id;
        for (City city : cities) {
            if (city.city_id.equals(String.valueOf(city_id))) {
                spCity.setSelection(cities.indexOf(city) + 1);
                break;
            }
        }
    }

    @Override
    public void onCompleteStep() {

    }

    @Override
    public void onGetHeadline(Text text) {

    }

    @Override
    public void onUpdatePasswordSuccess(String msg) {
        ((BaseActivity) getActivity()).dismissProgressDialog();
        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onUpdateLocationSuccess(String msg) {
        ((BaseActivity) getActivity()).dismissProgressDialog();
        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onUpdateEmailSuccess(String msg) {
        ((BaseActivity) getActivity()).dismissProgressDialog();
        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onUpdateBirthdateSuccess(String msg) {
        ((BaseActivity) getActivity()).dismissProgressDialog();
        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            switch (adapterView.getId()) {
                case R.id.spCountry:
                    if (i > 0) {
                        if (countryList != null) {
                            country = countryList.get(i - 1);
                            completion1Presenter.getState(Integer.parseInt(country.country_id));
                        }
                    }
                    break;
                case R.id.spState:
                    if (i > 0) {
                        if (stateList != null) {
                            state = stateList.get(i - 1);
                            completion1Presenter.getCity(Integer.parseInt(country.country_id), Integer.parseInt(state.state_id));
                        }
                    }
                    break;
                case R.id.spCity:
                    if (i > 0) {
                        if (cityList != null) {
                            city = cityList.get(i - 1);
                        }
                    }
                    break;
                case R.id.spMonth:
                    spMonth.setTag(i + 1);
                    break;
                case R.id.spDate:
                    spDate.setTag(i + 1);
                    break;
            }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onValidationSucceeded() {
        completion1Presenter.updatePassword(edtPassword.getText().toString());
        ((BaseActivity) getActivity()).showProgressDialog(false);

    }

    @Override
    public void onValidationFailed(List<ValidationError> list) {
        for (ValidationError error : list) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getContext());
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
            }
        }
    }
}
