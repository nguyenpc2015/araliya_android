package com.npc.marry.ui.chat;

import android.content.Context;

import com.npc.marry.api.server_api.AppServicesImpl;
import com.npc.marry.app.AppSetting;
import com.npc.marry.base.BasePresenter;
import com.npc.marry.model.response.UserListResponse;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Lenovo on 4/20/2017.
 */

public class ChatOnlineMemberPresenter extends BasePresenter<ChatOnlineMemberView> {
    AppServicesImpl appServices;

    public ChatOnlineMemberPresenter(ChatOnlineMemberView view, CompositeSubscription compositeSubscription, Context context) {
        super(view, compositeSubscription);
        appServices = AppServicesImpl.getInstance(context);
    }

    public void getOnlineUser(final int page) {
        compositeSubscription.add(appServices.getOnlineUser(String.valueOf(AppSetting.getInstance().getUser().user_id), String.valueOf(page))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<UserListResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(UserListResponse userListResponse) {
                        if (view != null) {
                            if (page == 1) {
                                view.onGetUsersSuccess(userListResponse.users, userListResponse.total_pages);
                            } else {
                                view.onGetUsersSuccessNext(userListResponse.users, userListResponse.total_pages);
                            }
                        }
                    }
                }));
    }
}
