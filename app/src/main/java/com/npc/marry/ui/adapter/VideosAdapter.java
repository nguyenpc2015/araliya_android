package com.npc.marry.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.npc.marry.R;
import com.npc.marry.app.AppSetting;
import com.npc.marry.app.Constants;
import com.npc.marry.model.Video;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mehdi.sakout.fancybuttons.FancyButton;

//import com.google.android.gms.games.video.Video;

/**
 * Created by Lenovo on 6/10/2017.
 */

public class VideosAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int ITEM_TYPE = 1;
    private static final int FOOTER_TYPE = 2;

    List<com.npc.marry.model.Video> videoList;

    private VideoListener videoListener;

    public VideosAdapter() {
        videoList = new ArrayList<>();
    }

    public void setVideoListener(VideoListener videoListener) {
        this.videoListener = videoListener;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == ITEM_TYPE) {
            return new VideoHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_video, parent, false));
        } else {
            return new FooterHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.footer_user_list, parent, false));
        }
    }

    public int getItemViewType(int position) {
        if (videoList.size() > 0 && videoList.size() > Constants.PAGE_SIZE &&
                position == videoList.size() - 1 && videoList.get(position) == null) {
            return FOOTER_TYPE;
        }

        return ITEM_TYPE;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof VideoHolder) {
            VideoHolder videoHolder = (VideoHolder) holder;
            videoHolder.bind(videoList.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return videoList.size();
    }

    public void setVideoList(List<com.npc.marry.model.Video> videoList) {
        this.videoList = videoList;
        notifyDataSetChanged();
    }

    public void addVideoList(List<com.npc.marry.model.Video> videos) {
        int size = this.videoList.size();
        this.videoList.remove(size - 1);
        notifyItemRemoved(size - 1);
        size = this.videoList.size();
        this.videoList.addAll(videos);
        notifyItemRangeInserted(size, videos.size());
    }

    public interface VideoListener {
        void onVideoClick(com.npc.marry.model.Video video);

        void onVideoDelete(Video video);
    }

    class VideoHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvName)
        TextView tvName;

        @BindView(R.id.ivThumb)
        ImageView ivThumb;

        @BindView(R.id.btnDelete)
        FancyButton btnDelete;

        @BindView(R.id.llAudit)
        LinearLayout llAudit;

        View itemView;

        public VideoHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            ButterKnife.bind(this, itemView);
        }

        public void bind(final com.npc.marry.model.Video video) {
            tvName.setText(video == null ? "" : video.audio_name);
            if (video.audio_code != null) {
                String url = "https://i3.ytimg.com/vi/" + video.audio_code + "/default.jpg";
                Picasso.with(ivThumb.getContext()).load(url).error(R.drawable.ic_video).into(ivThumb);
            }
            ivThumb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (videoListener != null) {
                        videoListener.onVideoClick(video);
                    }
                }
            });
            llAudit.setVisibility(video.visible == 'Y' ? View.GONE : View.VISIBLE);
            btnDelete.setVisibility(AppSetting.getInstance().getUser().user_id.equals(video.user_id) ? View.VISIBLE : View.GONE);
            ivThumb.setTag(video);
        }

        @OnClick(R.id.btnDelete)
        public void onDeleteVideo() {
            Video video = (Video) ivThumb.getTag();
            if (video != null) {
                videoListener.onVideoDelete(video);
            }
        }
    }

    class FooterHolder extends RecyclerView.ViewHolder {

        public FooterHolder(View itemView) {
            super(itemView);
        }
    }
}
