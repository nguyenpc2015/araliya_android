package com.npc.marry.ui.match;

import com.npc.marry.model.UserData;

import java.util.List;

/**
 * Created by Lenovo on 3/29/2017.
 */

public interface MatchView {
    void onError(String msg);

    void onGetUserSuccess(List<UserData> userDataList, int totalPage);

    void onGetUserSuccessNext(List<UserData> userDataList, int totalPage);
}
