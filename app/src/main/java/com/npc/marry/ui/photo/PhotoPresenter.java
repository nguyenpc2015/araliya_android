package com.npc.marry.ui.photo;

import android.content.Context;

import com.npc.marry.api.server_api.AppServicesImpl;
import com.npc.marry.app.AppSetting;
import com.npc.marry.base.BasePresenter;
import com.npc.marry.model.request.PhotoRequest;
import com.npc.marry.model.response.BaseResponse;
import com.npc.marry.model.response.PhotoResponse;
import com.npc.marry.model.response.UploadPhotoResponse;
import com.npc.marry.model.response.UserPhotoResponse;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Lenovo on 3/13/2017.
 */

public class PhotoPresenter extends BasePresenter<PhotoView> {
    AppServicesImpl appServices;

    public PhotoPresenter(PhotoView view, Context context, CompositeSubscription compositeSubscription) {
        super(view, compositeSubscription);
        appServices = AppServicesImpl.getInstance(context);
    }

    public void deletePhoto(String photo_id) {
        compositeSubscription.add(appServices.deletePhoto(photo_id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<BaseResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(BaseResponse baseResponse) {
                        if (view != null) {
                            view.onDeleteResult(baseResponse.message);
                        }
                    }
                }));
    }

    public void getAllPhoto(final String page) {
        compositeSubscription.add(
                appServices.getAllPhotos(AppSetting.getInstance().getUser().user_id, page)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<UserPhotoResponse>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable e) {
                                if (view != null) {
                                    view.onError(e.getMessage());
                                }
                            }

                            @Override
                            public void onNext(UserPhotoResponse userPhotoResponse) {
                                if (view != null) {
                                    if (page.equals("1")) {
                                        view.onGetAllPhotos(userPhotoResponse.items, userPhotoResponse.total_pages, userPhotoResponse.total_items);
                                    } else {
                                        view.onGetAllPhotosNext(userPhotoResponse.items, userPhotoResponse.total_pages);
                                    }
                                }

                            }
                        })
        );
    }

    public void getPhoto(final String user_id, final String page) {
        compositeSubscription.add(appServices.getPhoto(user_id, page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<PhotoResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(PhotoResponse photoResponse) {
                        if (view != null) {
                            if (page.equals("1")) {
                                view.onGetPhotos(photoResponse.items, photoResponse.total_pages, photoResponse.total_items);
                            } else {
                                view.onGetPhotosNext(photoResponse.items, photoResponse.total_pages);
                            }
                        }
                    }
                }));
    }

    public void uploadPhoto(File file) {
        RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("Filedata", file.getName(), requestFile);
        compositeSubscription.add(appServices.uploadPhoto(body, AppSetting.getInstance().getUser().user_id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<UploadPhotoResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(UploadPhotoResponse uploadPhotoResponse) {
                        if (view != null) {
                            view.onUploadSuccess(uploadPhotoResponse.getRef());
                        }
                    }
                }));
    }

    public void savePhoto(String name, String description, String ref, boolean default_photo) {
        compositeSubscription.add(appServices.savePhoto(new PhotoRequest(AppSetting.getInstance().getUser().user_id, name, description, default_photo ? 1 : 0, ref))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<BaseResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(BaseResponse baseResponse) {
                        if (view != null) {
                            view.onSaveSuccess(baseResponse.message);
                        }
                    }
                }));
    }


}
