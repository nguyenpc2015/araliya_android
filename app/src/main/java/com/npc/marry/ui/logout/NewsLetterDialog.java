package com.npc.marry.ui.logout;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.npc.marry.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by nguyen tran on 2/28/2018.
 */

public class NewsLetterDialog extends DialogFragment {

    @BindView(R.id.cbCheck)
    CheckBox cbCheck;
    NewsletterListener newsletterListener;

    public void setNewsletterListener(NewsletterListener newsletterListener) {
        this.newsletterListener = newsletterListener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_newletter, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @OnClick(R.id.btnUpdate)
    public void onUpdate() {
        if (newsletterListener != null) {
            newsletterListener.onSubscribe(cbCheck.isChecked() ? 1 : 0);
            dismiss();
        }
    }


    public interface NewsletterListener {
        void onSubscribe(int check);
    }
}
