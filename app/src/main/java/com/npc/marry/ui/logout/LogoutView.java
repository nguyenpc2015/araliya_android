package com.npc.marry.ui.logout;

import com.mobsandgeeks.saripaar.Validator;

/**
 * Created by Lenovo on 2/21/2017.
 */

public interface LogoutView extends Validator.ValidationListener {
    void onError(String msg);

    void onLogoutSuccess();

    void onLogoutFail(String msg);

    void onUpdateNewsletterSuccess(String msg);

    void onDeleteAccountSuccess(String msg);
}
