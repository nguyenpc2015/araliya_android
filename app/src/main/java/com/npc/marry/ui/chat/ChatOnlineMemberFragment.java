package com.npc.marry.ui.chat;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.npc.marry.R;
import com.npc.marry.app.AppSetting;
import com.npc.marry.base.BaseActivity;
import com.npc.marry.base.BaseFragment;
import com.npc.marry.model.Conversation;
import com.npc.marry.model.UserData;
import com.npc.marry.ui.adapter.ContactAdapter;
import com.npc.marry.ui.mail.MailComposeActivity;
import com.npc.marry.ui.payment.PaymentActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Lenovo on 3/3/2017.
 */

public class ChatOnlineMemberFragment extends BaseFragment implements ChatOnlineMemberView {

    @BindView(R.id.rvUser)
    RecyclerView rvUser;

    @BindView(R.id.pbProgress)
    ProgressBar pbProgress;

    @BindView(R.id.swipe)
    SwipeRefreshLayout swipeRefreshLayout;

    ContactAdapter userListAdapter;
    ChatOnlineMemberPresenter homePresenter;
    int currentPage = 0;

    boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;

    boolean isNext;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chat_online_member, container, false);
        ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        isNext = false;
        setUpRecycleView();
        homePresenter = new ChatOnlineMemberPresenter(this, compositeSubscription, getContext());
        currentPage = 1;
        homePresenter.getOnlineUser(currentPage);
        pbProgress.setVisibility(View.VISIBLE);

        swipeRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onError(String msg) {
        swipeRefreshLayout.setRefreshing(false);
        pbProgress.setVisibility(View.GONE);
        ((BaseActivity) getActivity()).showDialog(msg);
    }

    @Override
    public void onGetUsersSuccess(List<UserData> userDataList, int totalPage) {
        swipeRefreshLayout.setRefreshing(false);
        pbProgress.setVisibility(View.GONE);
        this.loading = true;
        if (userDataList != null) {
            isNext = currentPage < totalPage;
            if (isNext && userDataList.size() == 0) {
                userDataList.add(null);
            }
            userListAdapter.setUserList(userDataList);
        }
    }

    @Override
    public void onGetUsersSuccessNext(List<UserData> userDataList, int totalPage) {
        this.loading = true;
        if (userDataList != null) {
            isNext = currentPage < totalPage;
            if (isNext) {
                userDataList.add(null);
                userListAdapter.addMediaList(userDataList);
            } else {
                userListAdapter.addMediaList(userDataList);
            }
        }
    }

    private void setUpRecycleView() {
        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rvUser.setLayoutManager(mLayoutManager);
        userListAdapter = new ContactAdapter();
        userListAdapter.setRemoveListner(this);
        rvUser.setAdapter(userListAdapter);
        rvUser.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) { //check for scroll down{
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            if (isNext) {
                                currentPage++;
                                homePresenter.getOnlineUser(currentPage);
                            }
                        }
                    }
                }
            }
        });
    }

    @Override
    public void onItemMessage(UserData userData) {
        if (AppSetting.getInstance().getUserData() != null && AppSetting.getInstance().getUserData().gold_days > 0) {
            Conversation conversation = new Conversation();
            conversation.to_user_name = userData.name;
            conversation.photo_to = userData.default_photo;
            conversation.from_user_id = AppSetting.getInstance().getUserData().user_id;
            conversation.msg = "";
            conversation.from_user_name = AppSetting.getInstance().getUserData().name;
            conversation.photo_from = AppSetting.getInstance().getUserData().default_photo;
            conversation.to_user_id = userData.user_id;
            ((BaseActivity) getActivity()).startActivity(ChatActivity.class, conversation, false);

        } else {
            Toast.makeText(getActivity(), "Please upgrade to use this feature", Toast.LENGTH_SHORT).show();
            ((BaseActivity) getActivity()).startActivity(PaymentActivity.class, null, false);
        }
    }

    @Override
    public void onItemMail(UserData userData) {
        Intent intent = new Intent(getContext(), MailComposeActivity.class);
        intent.putExtra("user_to", userData.user_id);
        intent.putExtra("user_name", userData.name);
        startActivity(intent);
    }

    @Override
    public void onRefresh() {
        currentPage = 1;
        homePresenter.getOnlineUser(currentPage);
    }
}
