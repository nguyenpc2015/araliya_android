package com.npc.marry.ui.favorite;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.npc.marry.R;
import com.npc.marry.base.BaseActivity;
import com.npc.marry.model.UserData;
import com.npc.marry.ui.adapter.UserManagerAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class FavoriteActivity extends BaseActivity implements FavoriteView {
    @BindView(R.id.rvUser)
    RecyclerView rvUser;

    @BindView(R.id.pbProgress)
    ProgressBar pbProgress;

    UserManagerAdapter userListAdapter;
    FavoritePresenter homePresenter;
    int currentPage = 0;

    boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;

    boolean isNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite);
        isNext = false;
        setUpRecycleView();
        homePresenter = new FavoritePresenter(this, compositeSubscription, this);
        currentPage = 1;
        homePresenter.getFavoriteUser(currentPage);
        pbProgress.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.ivBack)
    public void onBack() {
        finish();
    }

    @Override
    public void onError(String msg) {
        pbProgress.setVisibility(View.GONE);
        dismissProgressDialog();
        showDialog(msg);
    }


    @Override
    public void onGetUserSuccess(List<UserData> userDataList, int totalPage) {
        pbProgress.setVisibility(View.GONE);
        this.loading = true;
        if (userDataList != null) {
            isNext = currentPage < totalPage;
            if (isNext || userDataList.size() == 0) {
                userDataList.add(null);
            }
            userListAdapter.setUserList(userDataList);
        }
    }

    @Override
    public void onGetUserSuccessNext(List<UserData> userDataList, int totalPage) {
        this.loading = true;
        if (userDataList != null && userDataList.size() > 0) {
            isNext = currentPage < totalPage;
            if (isNext) {
                userDataList.add(null);
                userListAdapter.addMediaList(userDataList);
            } else {
                userListAdapter.addMediaList(userDataList);
            }
        }
    }

    @Override
    public void onRemoveSuccess(String msg, String user_id) {
        dismissProgressDialog();
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        userListAdapter.removeUser(user_id);
    }

    private void setUpRecycleView() {
        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        rvUser.setLayoutManager(mLayoutManager);
        userListAdapter = new UserManagerAdapter();
        userListAdapter.setRemoveListner(this);
        rvUser.setAdapter(userListAdapter);
        rvUser.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) { //check for scroll down{
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            if (isNext) {
                                currentPage++;
                                homePresenter.getFavoriteUser(currentPage);
                            }
                        }
                    }
                }
            }
        });
    }

    @Override
    public void onItemRemove(UserData userData) {
        homePresenter.removeFavorite(userData.user_id);
        showProgressDialog(false);
    }
}
