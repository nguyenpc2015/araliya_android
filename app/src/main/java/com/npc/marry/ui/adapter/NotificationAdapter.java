package com.npc.marry.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.npc.marry.R;
import com.npc.marry.app.Constants;
import com.npc.marry.model.Notification;
import com.npc.marry.ui.custom.BlackTextView;
import com.npc.marry.ui.custom.BoldTextView;
import com.npc.marry.utils.DateUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Lenovo on 3/11/2017.
 */

public class NotificationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int ITEM_TYPE = 1;
    private static final int ITEM_VIEWED_TYPE = 2;
    private static final int FOOTER_TYPE = 3;

    List<Notification> notificationList;
    NotificationListener notificationListener;

    public NotificationAdapter() {
        notificationList = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case ITEM_TYPE:
                return new ItemHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notification, parent, false));
            case ITEM_VIEWED_TYPE:
                return new ItemViewedHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notification_viewed, parent, false));
            default:
                return new FooterHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.footer_user_list, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemHolder) {
            ItemHolder itemHolder = (ItemHolder) holder;
            itemHolder.bind(notificationList.get(position));
        } else if (holder instanceof ItemViewedHolder) {
            ItemViewedHolder itemViewedHolder = (ItemViewedHolder) holder;
            itemViewedHolder.bind(notificationList.get(position));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (notificationList.size() > 0 && notificationList.size() > Constants.PAGE_SIZE &&
                position == notificationList.size() - 1 && notificationList.get(position) == null) {
            return FOOTER_TYPE;
        } else if (notificationList.get(position).getViewed() == 1) {
            return ITEM_TYPE;
        }
        return ITEM_VIEWED_TYPE;
    }

    @Override
    public int getItemCount() {
        return notificationList.size();
    }

    public void addNotificationList(List<Notification> notifications) {
        int size = this.notificationList.size();
        this.notificationList.remove(size - 1);
        notifyItemRemoved(size - 1);
        size = this.notificationList.size();
        this.notificationList.addAll(notifications);
        notifyItemRangeInserted(size, notifications.size());
    }

    public void setNotificationList(List<Notification> notificationList) {
        this.notificationList = notificationList;
        notifyDataSetChanged();
    }

    public void setNotificationListener(NotificationListener notificationListener) {
        this.notificationListener = notificationListener;
    }

    public interface NotificationListener {
        void onItemClick(Notification notification);
    }

    class ItemHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.ivUser)
        CircleImageView ivUser;
        @BindView(R.id.tvName)
        BoldTextView tvName;
        @BindView(R.id.tvTime)
        BoldTextView tvTime;
        @BindView(R.id.tvContent)
        BoldTextView tvContent;

        Context context;
        Notification notification;

        public ItemHolder(View itemView) {
            super(itemView);
            this.context = itemView.getContext();
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (notificationListener != null && notification != null) {
                        notificationListener.onItemClick(notification);
                    }
                }
            });
            notification = null;
        }

        public void bind(Notification notification) {
            this.notification = notification;
            if (notification != null) {
                tvName.setText(notification.getName());
                tvContent.setText(notification.getContent());
                tvTime.setText(DateUtils.getDisplayTime(context, notification.getDate(), DateUtils.DATE_FORMAT_ISO_8601));
                if (notification.getPhoto() != null) {
                    Picasso.with(context).load(notification.getPhoto()).into(ivUser);
                }
            }
        }
    }

    class ItemViewedHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.ivUser)
        CircleImageView ivUser;
        @BindView(R.id.tvName)
        BlackTextView tvName;
        @BindView(R.id.tvTime)
        BlackTextView tvTime;
        @BindView(R.id.tvContent)
        BlackTextView tvContent;

        Context context;

        Notification notification;

        public ItemViewedHolder(View itemView) {
            super(itemView);
            this.context = itemView.getContext();
            ButterKnife.bind(this, itemView);
            notification = null;
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (notificationListener != null && notification != null) {
                        notificationListener.onItemClick(notification);
                    }
                }
            });
        }

        public void bind(Notification notification) {
            this.notification = notification;
            if (notification != null) {
                tvName.setText(notification.getName());
                tvContent.setText(notification.getContent());
                tvTime.setText(DateUtils.getDisplayTime(context, notification.getDate(), DateUtils.DATE_FORMAT_ISO_8601));
                if (notification.getPhoto() != null) {
                    Picasso.with(context).load(notification.getPhoto()).into(ivUser);
                }
            }
        }
    }

    class FooterHolder extends RecyclerView.ViewHolder {

        public FooterHolder(View itemView) {
            super(itemView);
        }
    }


}
