package com.npc.marry.ui.payment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.npc.marry.R;
import com.npc.marry.model.PaymentPlan;
import com.npc.marry.utils.UIUtils;

import org.parceler.Parcels;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Lenovo on 3/17/2017.
 */

public class PaymentPlanDialog extends DialogFragment {

    @BindView(R.id.llPlan)
    RadioGroup llPlan;
    PaymentPlan paymentPlan;
    PaymentPlanListener paymentPlanListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_payment_plan, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view1, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view1, savedInstanceState);
        List<PaymentPlan> paymentPlanList = Parcels.unwrap(getArguments().getParcelable("plans"));
        for (final PaymentPlan paymentPlan : paymentPlanList) {
            final RadioButton radioButton = new RadioButton(getActivity());
            RadioGroup.LayoutParams layoutParams = new RadioGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.topMargin = UIUtils.dpToPx(getActivity(), 10);
            layoutParams.bottomMargin = UIUtils.dpToPx(getActivity(), 10);
            radioButton.setLayoutParams(layoutParams);
            radioButton.setText(paymentPlan.item_name + " - $ " + paymentPlan.amount);
            radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        PaymentPlanDialog.this.paymentPlan = paymentPlan;
                    }
                }
            });
            llPlan.addView(radioButton);
        }

    }

    @OnClick(R.id.btnSelect)
    public void onSelect() {
        if (paymentPlan != null) {
            paymentPlanListener.onChoosePlan(paymentPlan);
            dismiss();
        } else {
            Toast.makeText(getActivity(), "Please select the plan", Toast.LENGTH_SHORT).show();
        }
    }

    public void setPaymentPlanListener(PaymentPlanListener paymentPlanListener) {
        this.paymentPlanListener = paymentPlanListener;
    }

    public interface PaymentPlanListener {
        void onChoosePlan(PaymentPlan paymentPlan);
    }
}
