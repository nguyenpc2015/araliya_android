package com.npc.marry.ui.custom;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Lenovo on 2/20/2017.
 */

public class BlackTextView extends TextView {
    public BlackTextView(Context context) {
        super(context);
    }

    public BlackTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        Typeface typeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/roboto_regula.ttf");
        setTypeface(typeface);
    }

    public BlackTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
