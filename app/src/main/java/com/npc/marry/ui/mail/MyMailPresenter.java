package com.npc.marry.ui.mail;

import android.content.Context;

import com.npc.marry.api.server_api.AppServicesImpl;
import com.npc.marry.app.AppSetting;
import com.npc.marry.base.BasePresenter;
import com.npc.marry.model.response.MailFileResponse;
import com.npc.marry.model.response.MailsResponse;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by nguyen tran on 2/21/2018.
 */

public class MyMailPresenter extends BasePresenter<MyMailView> {
    AppServicesImpl appServices;

    public MyMailPresenter(MyMailView view, CompositeSubscription compositeSubscription, Context context) {
        super(view, compositeSubscription);
        appServices = AppServicesImpl.getInstance(context);
    }

    public void getMailFiles(String mail_id) {
        compositeSubscription.add(
                appServices.getMailFiels(mail_id)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<MailFileResponse>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable throwable) {
                                if (view != null) {
                                    view.onError(throwable.getMessage());
                                }
                            }

                            @Override
                            public void onNext(MailFileResponse baseResponse) {
                                if (view != null) {
                                    view.onGetMailFiles(baseResponse.files);
                                }
                            }
                        })
        );
    }

    public void getMailInbox(int page) {
        compositeSubscription.add(
                appServices.getMailSent(AppSetting.getInstance().getUser().user_id, String.valueOf(page))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<MailsResponse>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable throwable) {
                                if (view != null) {
                                    view.onError(throwable.getMessage());
                                }
                            }

                            @Override
                            public void onNext(MailsResponse baseResponse) {
                                if (view != null) {
                                    view.onGetMailList(baseResponse.mails, baseResponse.total_pages);
                                }
                            }
                        })
        );
    }

    public void getMailTrash(int page) {
        compositeSubscription.add(
                appServices.getMailTrash(AppSetting.getInstance().getUser().user_id, String.valueOf(page))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<MailsResponse>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable throwable) {
                                if (view != null) {
                                    view.onError(throwable.getMessage());
                                }
                            }

                            @Override
                            public void onNext(MailsResponse baseResponse) {
                                if (view != null) {
                                    view.onGetMailList(baseResponse.mails, baseResponse.total_pages);
                                }
                            }
                        })
        );
    }

    public void getMails(int page) {
        compositeSubscription.add(
                appServices.getMails(AppSetting.getInstance().getUser().user_id, String.valueOf(page))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<MailsResponse>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable throwable) {
                                if (view != null) {
                                    view.onError(throwable.getMessage());
                                }
                            }

                            @Override
                            public void onNext(MailsResponse baseResponse) {
                                if (view != null) {
                                    view.onGetMailList(baseResponse.mails, baseResponse.total_pages);
                                }
                            }
                        })
        );
    }
}
