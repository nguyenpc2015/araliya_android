package com.npc.marry.ui.mail;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.npc.marry.R;
import com.npc.marry.base.BaseActivity;
import com.npc.marry.base.BaseFragment;
import com.npc.marry.model.File;
import com.npc.marry.model.Mail;
import com.npc.marry.ui.custom.DividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by nguyen tran on 2/26/2018.
 */

public class MailSentFragment extends BaseFragment implements MyMailView, MailAdapter.MailListener {
    @BindView(R.id.swipe)
    SwipeRefreshLayout swipe;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.rvMail)
    RecyclerView rvMail;

    MailAdapter mailAdapter;
    MyMailPresenter presenter;

    int currentPage = 1;

    boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;

    boolean isNext;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mail_sent, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setUpRecycleView();
        presenter = new MyMailPresenter(this, compositeSubscription, this.getContext());
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                currentPage = 1;
                presenter.getMailInbox(currentPage);
            }
        });
        progressBar.setVisibility(View.VISIBLE);
        currentPage = 1;
        presenter.getMailInbox(currentPage);
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onError(String msg) {
        progressBar.setVisibility(View.GONE);
        swipe.setRefreshing(false);
        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onGetMailList(List<Mail> mailList, int totalPage) {
        progressBar.setVisibility(View.GONE);
        ((BaseActivity) getActivity()).dismissProgressDialog();
        swipe.setRefreshing(false);
        loading = true;
        isNext = currentPage < totalPage;
        if (mailList == null || mailList.size() == 0) {
            mailList = new ArrayList<>();
        } else if (isNext) {
            mailList.add(null);
        }
        if (currentPage == 1) {
            mailAdapter.clear();
        }
        mailAdapter.setMailList(mailList);
    }

    @Override
    public void onGetMailFiles(List<File> fileList) {

    }

    private void setUpRecycleView() {
        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rvMail.setLayoutManager(mLayoutManager);
        rvMail.addItemDecoration(new DividerItemDecoration(getContext()));
        mailAdapter = new MailAdapter();
        mailAdapter.setMailListener(this);
        rvMail.setAdapter(mailAdapter);
        rvMail.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) { //check for scroll down{
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            if (isNext) {
                                currentPage++;
                                presenter.getMailInbox(currentPage);
                            }
                        }
                    }
                }
            }
        });
    }

    @Override
    public void onMailClick(Mail mail) {
        if (mail != null) {
            Intent intent = new Intent(getContext(), MailDetailActivity.class);
            intent.putExtra("mail", mail);
            intent.putExtra("mode", 3);
            getActivity().startActivity(intent);
        }
    }
}
