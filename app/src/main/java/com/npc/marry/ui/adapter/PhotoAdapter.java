package com.npc.marry.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.npc.marry.R;
import com.npc.marry.app.AppSetting;
import com.npc.marry.app.Constants;
import com.npc.marry.model.Photo;
import com.npc.marry.model.PhotoWrapper;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by Lenovo on 3/13/2017.
 */

public class PhotoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int ITEM_TYPE = 1;
    private static final int FOOTER_TYPE = 2;

    List<Photo> photoList;

    PhotoListener photoListener;

    public PhotoAdapter() {
        photoList = new ArrayList<>();
    }

    public void setPhotoListener(PhotoListener photoListener) {
        this.photoListener = photoListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == ITEM_TYPE) {
            return new PhotoHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_photo, parent, false));
        } else {
            return new FooterHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.footer_user_list, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof PhotoHolder) {
            PhotoHolder photoHolder = (PhotoHolder) holder;
            photoHolder.bind(photoList.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return photoList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (photoList.size() > 0 && photoList.size() > Constants.PAGE_SIZE &&
                position == photoList.size() - 1 && photoList.get(position) == null) {
            return FOOTER_TYPE;
        }

        return ITEM_TYPE;
    }

    public void addMediaList(List<Photo> photos) {
        int size = this.photoList.size();
        this.photoList.remove(size - 1);
        notifyItemRemoved(size - 1);
        size = this.photoList.size();
        this.photoList.addAll(photos);
        notifyItemRangeInserted(size, photos.size());
    }

    public void setPhotoList(List<Photo> photoList) {
        this.photoList = photoList;
        notifyDataSetChanged();
    }


    public interface PhotoListener {
        void onPhotoWrapperClick(PhotoWrapper photoWrapper, List<PhotoWrapper> photoWrapperList);

        void onDeletePhoto(Photo photo);

        void onPhotoClick(Photo photo, List<Photo> photoList);
    }

    class PhotoHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.ivPhoto)
        ImageView ivPhoto;
        @BindView(R.id.llAudit)
        LinearLayout llaudit;
        @BindView(R.id.btnDelete)
        FancyButton btnDelete;


        Context context;

        public PhotoHolder(View itemView) {
            super(itemView);
            context = itemView.getContext();
            ButterKnife.bind(this, itemView);
        }

        public void bind(final Photo photo) {
            if (photo != null) {
                if (photo.ref != null) {
                    String url = Constants.BASE_PHOTO_URL + photo.ref + "_b.jpg";
                    Picasso.with(context).load(url).into(ivPhoto);
                }
                if (photo.visible.equals("N")) {
                    llaudit.setVisibility(View.VISIBLE);
                }
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (photoListener != null) {
                            if (photoList.size() > 0)
                                photoListener.onPhotoClick(photo, photoList);
                        }
                    }
                });
                if (photo.user_id.equals(AppSetting.getInstance().getUserData().user_id)) {
                    btnDelete.setVisibility(View.VISIBLE);
                }
                btnDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (photoListener != null) {
                            photoListener.onDeletePhoto(photo);
                        }
                    }
                });
            }
        }
    }

    class FooterHolder extends RecyclerView.ViewHolder {

        public FooterHolder(View itemView) {
            super(itemView);
        }
    }
}
