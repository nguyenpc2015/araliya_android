package com.npc.marry.ui.mail;

import com.npc.marry.model.File;
import com.npc.marry.model.Mail;

/**
 * Created by nguyen tran on 2/26/2018.
 */

public interface MailComposeView {
    void onError(String msg);

    void onUploadFileSuccess(File file, String msg);
    void onSendMailSuccess(Mail mail);

    void onRemoveAttachmentSuccess(String msg);
}
