package com.npc.marry.ui.profile;

import android.content.Context;

import com.npc.marry.api.server_api.AppServicesImpl;
import com.npc.marry.app.AppSetting;
import com.npc.marry.base.BasePresenter;
import com.npc.marry.model.UserData;
import com.npc.marry.model.request.InterestRequest;
import com.npc.marry.model.response.BaseResponse;
import com.npc.marry.model.response.PhotoResponse;
import com.npc.marry.model.response.VideosResponse;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Lenovo on 3/6/2017.
 */

public class ProfilePresenter extends BasePresenter<ProfileView> {
    AppServicesImpl appServices;
    AppSetting appSetting;

    public ProfilePresenter(ProfileView view, Context context, CompositeSubscription subscription) {
        super(view, subscription);
        appServices = AppServicesImpl.getInstance(context);
        appSetting = AppSetting.getInstance();
    }

    public void getUserData(String user_id) {
        compositeSubscription.add(appServices.getUser(user_id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<UserData>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(UserData userData) {
                        //appSetting.setUserData(userData);
                        if (view != null) {
                            view.onGetDataSuccess(userData);
                        }
                    }
                }));
    }

    public void sendFavorite(int user_id_to) {
        compositeSubscription.add(appServices.addFavorite(new InterestRequest(Integer.valueOf(AppSetting.getInstance().getUser().user_id), user_id_to))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<BaseResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(BaseResponse baseResponse) {
                        if (view != null) {
                            view.onAddFavoriteuccess(baseResponse.message);
                        }
                    }
                }));
    }

    public void blockUser(int user_id_to) {
        compositeSubscription.add(appServices.blockUser(new InterestRequest(Integer.valueOf(AppSetting.getInstance().getUser().user_id), user_id_to))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<BaseResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(BaseResponse baseResponse) {
                        if (view != null) {
                            view.onBlockResult(baseResponse.message);
                        }
                    }
                }));
    }

    public void sendInterest(int user_id_to) {
        compositeSubscription.add(appServices.sendInterest(new InterestRequest(Integer.valueOf(AppSetting.getInstance().getUser().user_id), user_id_to))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<BaseResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(BaseResponse baseResponse) {
                        if (view != null) {
                            view.onSendInterestSuccess(baseResponse.message);
                        }
                    }
                }));
    }

    public void viewUser(String user_id) {
        InterestRequest interestRequest = new InterestRequest(Integer.valueOf(AppSetting.getInstance().getUserData().user_id), Integer.valueOf(user_id));
        compositeSubscription.add(appServices.viewUser(interestRequest).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<BaseResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(BaseResponse baseResponse) {

                    }
                }));
    }

    public void getPhoto(final String user_id, final String page) {
        compositeSubscription.add(appServices.getPhoto(user_id, page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<PhotoResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(PhotoResponse photoResponse) {
                        if (view != null) {
                            if (page.equals("1")) {
                                view.onGetPhotos(photoResponse.items, photoResponse.total_pages, photoResponse.total_items);
                            } else {
                                view.onGetPhotosNext(photoResponse.items, photoResponse.total_pages);
                            }
                        }
                    }
                }));
    }

    public void getVideos(String user_id) {
        compositeSubscription.add(appServices.getVideos(user_id)
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<VideosResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(VideosResponse videosResponse) {
                        if (view != null) {
                            view.onGetVideoList(videosResponse.videos);
                        }
                    }
                }));
    }
}
