package com.npc.marry.ui.video;

import com.npc.marry.model.Video;
import com.npc.marry.model.VideoWrapper;
import com.npc.marry.ui.adapter.VideoWrapperAdapter;
import com.npc.marry.ui.adapter.VideosAdapter;

import java.util.List;

/**
 * Created by Lenovo on 6/10/2017.
 */

public interface VideoView extends VideosAdapter.VideoListener, VideoWrapperAdapter.VideoListener, UploadVideoDialog.SaveVideoListener {
    void onError(String msg);

    void onGetVideoList(List<Video> videoList);

    void onGetVideoList(List<VideoWrapper> videoList, int totalPage, int items);

    void onGetVideoNext(List<VideoWrapper> videos, int totalPage);

    void onDeleteVideoSuccess(String msg);

    void onUploadVideoSuccess(String msg);
}
