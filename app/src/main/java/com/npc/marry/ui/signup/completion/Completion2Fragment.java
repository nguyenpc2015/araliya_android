package com.npc.marry.ui.signup.completion;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.npc.marry.R;
import com.npc.marry.app.AppSetting;
import com.npc.marry.base.BaseActivity;
import com.npc.marry.base.BaseFragment;
import com.npc.marry.model.Appear;
import com.npc.marry.model.Body;
import com.npc.marry.model.Complex;
import com.npc.marry.model.Height;
import com.npc.marry.model.RelationShip;
import com.npc.marry.model.Weight;
import com.npc.marry.model.response.SpecialResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by Lenovo on 2/25/2017.
 */

public class Completion2Fragment extends BaseFragment implements Completion2View {

    @BindView(R.id.spRelationship)
    Spinner spRelationship;
    @BindView(R.id.spFrom)
    Spinner spFrom;
    @BindView(R.id.spTo)
    Spinner spTo;
    @BindView(R.id.spHeight)
    Spinner spHeight;
    @BindView(R.id.spWeight)
    Spinner spWeight;
    @BindView(R.id.spBody)
    Spinner spBody;
    @BindView(R.id.spAppeat)
    Spinner spAppear;
    @BindView(R.id.spComplex)
    Spinner spComplex;
    @BindView(R.id.btnDone)
    FancyButton btnDone;

    List<Height> heights;
    List<Weight> weights;
    List<Body> bodies;
    List<Appear> appears;
    List<Complex> complices;
    List<RelationShip> relationShips;

    Height height;
    Weight weight;
    Body body;
    Appear appear;
    Complex complex;
    RelationShip relationShip;
    int ageFrom = 0;
    int ageTo = 0;


    String mode = null;

    Completion2Presenter completion2Presenter;
    boolean isDone = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_completion_2, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        completion2Presenter = new Completion2Presenter(this, getContext(), compositeSubscription);
        spRelationship.setOnItemSelectedListener(this);
        spFrom.setOnItemSelectedListener(this);
        spTo.setOnItemSelectedListener(this);
        spAppear.setOnItemSelectedListener(this);
        spComplex.setOnItemSelectedListener(this);
        spBody.setOnItemSelectedListener(this);
        spHeight.setOnItemSelectedListener(this);
        spWeight.setOnItemSelectedListener(this);
        completion2Presenter.getSpecial();
        completion2Presenter.getRelationship();
        if (getArguments() != null) {
            mode = getArguments().getString("mode");
        } else {
            btnDone.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.btnNext)
    public void onNext() {
        if (height != null && weight != null && body != null &&
                appear != null && complex != null && relationShip != null &&
                ageFrom > 0 && ageTo > 0) {
            completion2Presenter.sendProfile(Integer.parseInt(relationShip.id), ageFrom, ageTo, Integer.parseInt(height.id),
                    Integer.parseInt(weight.id), Integer.parseInt(body.id), Integer.parseInt(appear.id), Integer.parseInt(complex.id));
            ((BaseActivity) getActivity()).showProgressDialog(false);
        } else {
            Toast.makeText(getActivity(), "Please select all fields", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.btnDone)
    public void onDone() {
        isDone = true;
        onNext();
    }

    @Override
    public void onError(String msg) {
        ((BaseActivity) getActivity()).dismissProgressDialog();
        new AlertDialog.Builder(getActivity())
                .setTitle("Alert")
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    @Override
    public void onGetRelationshipSuccess(List<RelationShip> relationShipList) {
        this.relationShips = relationShipList;
        List<String> datas = new ArrayList<>();
        datas.add("Please Choose");
        for (RelationShip relationShip : relationShipList) {
            datas.add(relationShip.title);
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, datas);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spRelationship.setAdapter(dataAdapter);
        datas = new ArrayList<>();
        for (int i = 18; i < 56; ++i) {
            datas.add(String.valueOf(i));
        }
        ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, datas);
        dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spFrom.setAdapter(dataAdapter1);
        ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, datas);
        dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spTo.setAdapter(dataAdapter2);
        spTo.setSelection(37);
        if (mode != null) {
            String relation = String.valueOf(AppSetting.getInstance().getUser().relation);
            for (RelationShip relationShip : relationShipList) {
                if (relationShip.id.equals(relation)) {
                    spRelationship.setSelection(relationShipList.indexOf(relationShip) + 1);
                    break;
                }
            }
            int ageFrom = AppSetting.getInstance().getUser().p_age_from;
            int ageTo = AppSetting.getInstance().getUser().p_age_to;
            spFrom.setSelection(ageFrom - 18);
            spTo.setSelection(ageTo - 18);
        }

    }

    @Override
    public void onGetSpecialSuccess(SpecialResponse specialResponses) {
        this.heights = specialResponses.heights;
        this.weights = specialResponses.weights;
        this.bodies = specialResponses.bodies;
        this.appears = specialResponses.appears;
        this.complices = specialResponses.complexs;
        List<String> data1 = new ArrayList<>();
        data1.add("Please Choose");
        for (Height height : specialResponses.heights) {
            data1.add(height.title);
        }
        ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, data1);
        dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spHeight.setAdapter(dataAdapter1);

        List<String> data2 = new ArrayList<>();
        data2.add("Please Choose");
        for (Weight weight : specialResponses.weights) {
            data2.add(weight.title);
        }
        ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, data2);
        dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spWeight.setAdapter(dataAdapter2);

        List<String> data3 = new ArrayList<>();
        data3.add("Please Choose");
        for (Body body : specialResponses.bodies) {
            data3.add(body.title);
        }
        ArrayAdapter<String> dataAdapter3 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, data3);
        dataAdapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spBody.setAdapter(dataAdapter3);

        List<String> data4 = new ArrayList<>();
        data4.add("Please Choose");
        for (Appear appear : specialResponses.appears) {
            data4.add(appear.title);
        }
        ArrayAdapter<String> dataAdapter4 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, data4);
        dataAdapter4.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spAppear.setAdapter(dataAdapter4);

        List<String> data5 = new ArrayList<>();
        data5.add("Please Choose");
        for (Complex complex : specialResponses.complexs) {
            data5.add(complex.title);
        }
        ArrayAdapter<String> dataAdapter5 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, data5);
        dataAdapter5.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spComplex.setAdapter(dataAdapter5);

        if (mode != null) {
            String height = String.valueOf(AppSetting.getInstance().getUser().height);
            String weight = String.valueOf(AppSetting.getInstance().getUser().weight);
            String body = String.valueOf(AppSetting.getInstance().getUser().body);
            String appearance = String.valueOf(AppSetting.getInstance().getUser().appearance);
            String complexion = String.valueOf(AppSetting.getInstance().getUser().complexion);
            for (Height height1 : specialResponses.heights) {
                if (height1.id.equals(height)) {
                    spHeight.setSelection(specialResponses.heights.indexOf(height1) + 1);
                    break;
                }
            }
            for (Weight weight1 : specialResponses.weights) {
                if (weight1.id.equals(weight)) {
                    spWeight.setSelection(specialResponses.weights.indexOf(weight1) + 1);
                    break;
                }
            }
            for (Body body1 : specialResponses.bodies) {
                if (body1.id.equals(body)) {
                    spBody.setSelection(specialResponses.bodies.indexOf(body1) + 1);
                    break;
                }
            }

            for (Appear appear : specialResponses.appears) {
                if (appear.id.equals(appearance)) {
                    spAppear.setSelection(specialResponses.appears.indexOf(appear) + 1);
                    break;
                }
            }

            for (Complex complex : specialResponses.complexs) {
                if (complex.id.equals(complexion)) {
                    spComplex.setSelection(specialResponses.complexs.indexOf(complex) + 1);
                    break;
                }
            }
        }
    }

    @Override
    public void onComppleteStep() {
        ((BaseActivity) getActivity()).dismissProgressDialog();
        if (isDone) {
            Intent intent = new Intent();
            intent.setAction("com.npc.complete");
            intent.putExtra("step", 4);
            getContext().sendBroadcast(intent);
        } else {
            Intent intent = new Intent();
            intent.setAction("com.npc.complete");
            intent.putExtra("step", 2);
            getContext().sendBroadcast(intent);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {
            case R.id.spHeight:
                if (heights != null && position > 0) {
                    height = heights.get(position - 1);
                }
                break;
            case R.id.spWeight:
                if (weights != null && position > 0) {
                    weight = weights.get(position - 1);
                }
                break;
            case R.id.spBody:
                if (bodies != null && position > 0) {
                    body = bodies.get(position - 1);
                }
                break;
            case R.id.spAppeat:
                if (appears != null && position > 0) {
                    appear = appears.get(position - 1);
                }
                break;
            case R.id.spComplex:
                if (complices != null && position > 0) {
                    complex = complices.get(position - 1);
                }
                break;
            case R.id.spRelationship:
                if (relationShips != null && position > 0) {
                    relationShip = relationShips.get(position - 1);
                }
                break;
            case R.id.spFrom:
                ageFrom = position + 18;
                break;
            case R.id.spTo:
                ageTo = position + 18;
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
