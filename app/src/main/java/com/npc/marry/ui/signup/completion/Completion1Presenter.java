package com.npc.marry.ui.signup.completion;

import android.content.Context;

import com.npc.marry.api.server_api.AppServicesImpl;
import com.npc.marry.app.AppSetting;
import com.npc.marry.base.BasePresenter;
import com.npc.marry.model.request.Complete1Request;
import com.npc.marry.model.response.BaseResponse;
import com.npc.marry.model.response.CityResponse;
import com.npc.marry.model.response.CountryResponse;
import com.npc.marry.model.response.StateResponse;
import com.npc.marry.model.response.TextResponse;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Lenovo on 2/27/2017.
 */

public class Completion1Presenter extends BasePresenter<Completion1View> {

    AppSetting appSetting;
    AppServicesImpl appServices;

    public Completion1Presenter(Completion1View view, Context context, CompositeSubscription compositeSubscription) {
        super(view, compositeSubscription);
        appServices = AppServicesImpl.getInstance(context);
        appSetting = AppSetting.getInstance();
    }

    public void getCountry() {
        compositeSubscription.add(appServices.getCountry()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CountryResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(CountryResponse countryResponse) {
                        if (view != null) {
                            view.onGetCountrySuccuess(countryResponse.countries);
                        }
                    }
                }));
    }

    public void getState(int country_id) {
        compositeSubscription.add(appServices.getState(country_id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<StateResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(StateResponse stateResponse) {
                        if (view != null) {
                            view.onGetStateSuccess(stateResponse.states);
                        }
                    }
                }));
    }

    public void getCity(int country_id, int city_id) {
        compositeSubscription.add(appServices.getCity(country_id, city_id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CityResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(CityResponse cityResponse) {
                        if (view != null) {
                            view.onGetCitySuccess(cityResponse.cities);
                        }
                    }
                }));
    }

    public void sendProfule1(String headline, String essay, int country_id, int state_id, int city_id) {
        compositeSubscription.add(appServices.completeProfile1(new Complete1Request(Integer.parseInt(appSetting.getUser().user_id), headline, essay, country_id, state_id, city_id))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<BaseResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(BaseResponse baseResponse) {
                        if (view != null) {
                            view.onCompleteStep();
                        }
                    }
                }));
    }

    public void getHeadline() {
        compositeSubscription.add(appServices.getHeadline(AppSetting.getInstance().getUser().user_id)
                .observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(new Observer<TextResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) {
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onNext(TextResponse textResponse) {
                        if (view != null && textResponse.text != null) {
                            view.onGetHeadline(textResponse.text);
                        }
                    }
                }));
    }
}
