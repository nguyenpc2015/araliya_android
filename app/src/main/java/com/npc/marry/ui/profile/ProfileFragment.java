package com.npc.marry.ui.profile;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.Toast;

import com.npc.marry.R;
import com.npc.marry.app.AppSetting;
import com.npc.marry.base.BaseActivity;
import com.npc.marry.base.BaseFragment;
import com.npc.marry.model.Conversation;
import com.npc.marry.model.Photo;
import com.npc.marry.model.UserData;
import com.npc.marry.model.Video;
import com.npc.marry.ui.chat.ChatActivity;
import com.npc.marry.ui.custom.BlackTextView;
import com.npc.marry.ui.mail.MailComposeActivity;
import com.npc.marry.ui.payment.PaymentActivity;
import com.npc.marry.ui.photo_view.PhotoViewerActivity;
import com.npc.marry.ui.signup.completion.ProfileCompletionActivity;
import com.npc.marry.utils.UIUtils;
import com.squareup.picasso.Picasso;

import org.parceler.Parcels;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by Lenovo on 6/8/2017.
 */

public class ProfileFragment extends BaseFragment implements ProfileView {

    @BindView(R.id.tvName)
    BlackTextView tvName;
    @BindView(R.id.rbRating)
    RatingBar rbRating;
    @BindView(R.id.profile_image)
    CircleImageView ivProfile;

    @BindView(R.id.cvControl)
    CardView cvControl;

    @BindView(R.id.tvAge)
    BlackTextView tvAge;
    @BindView(R.id.tvGender)
    BlackTextView tvGender;
    @BindView(R.id.tvActivity)
    BlackTextView tvActivity;
    @BindView(R.id.tvCity)
    BlackTextView tvCity;
    @BindView(R.id.tvState)
    BlackTextView tvState;
    @BindView(R.id.tvCountry)
    BlackTextView tvCountry;
    @BindView(R.id.tvRating)
    BlackTextView tvRating;

    @BindView(R.id.btnEdit)
    FancyButton btnEdit;

    @BindView(R.id.tvTitleAbout)
    BlackTextView tvTitleAbout;

    @BindView(R.id.tvHeadline)
    BlackTextView tvHeadline;

    @BindView(R.id.tvEssay)
    BlackTextView tvEssay;

    @BindView(R.id.tvHeight)
    BlackTextView tvHeight;
    @BindView(R.id.tvWeight)
    BlackTextView tvWeight;
    @BindView(R.id.tvBody)
    BlackTextView tvBody;
    @BindView(R.id.tvAppearance)
    BlackTextView tvAppearance;
    @BindView(R.id.tvComplexion)
    BlackTextView tvComplexion;
    @BindView(R.id.tvStatus)
    BlackTextView tvStatus;

    @BindView(R.id.tvEducation)
    BlackTextView tvEducatiob;
    @BindView(R.id.tvCareer)
    BlackTextView tvCareer;

    @BindView(R.id.tvReligion)
    BlackTextView tvReligion;
    @BindView(R.id.tvEthnicity)
    BlackTextView tvEthnicity;
    @BindView(R.id.tvCaste)
    BlackTextView tvCaste;
    @BindView(R.id.tvClass)
    BlackTextView tvClass;
    @BindView(R.id.tvResidency)
    BlackTextView tvResidency;
    @BindView(R.id.tvFamily)
    BlackTextView tvFamily;

    @BindView(R.id.tvSmoking)
    BlackTextView tvSmoking;
    @BindView(R.id.tvDrinking)
    BlackTextView tvDrinking;
    @BindView(R.id.tvDiet)
    BlackTextView tvDiet;
    @BindView(R.id.tvPeronality)
    BlackTextView tvPeronal;
    @BindView(R.id.tvFirst)
    BlackTextView tvFirst;
    @BindView(R.id.btnPhoto)
    FancyButton btnPhoto;
    @BindView(R.id.btnVideo)
    FancyButton btnVideo;

    List<Photo> photoList;

    String user_id;


    UserData userData = null;

    ProfilePresenter profilePresenter;

    public static ProfileFragment newInstance(String user_id) {
        Bundle bundle = new Bundle();
        bundle.putString("data", user_id);
        ProfileFragment profileFragment = new ProfileFragment();
        profileFragment.setArguments(bundle);
        return profileFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_profile, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        user_id = getArguments().getString("data");
        profilePresenter = new ProfilePresenter(this, getContext(), compositeSubscription);
    }

    @Override
    public void onResume() {
        super.onResume();
        profilePresenter.getUserData(user_id);
        ((BaseActivity) getActivity()).showProgressDialog(false);
    }


    @OnClick(R.id.tvMessage)
    public void onMessage() {
        if (AppSetting.getInstance().getUserData().gender.equals(userData.gender)) {
            Toast.makeText(getContext(), "Opps… Something is not right here...!", Toast.LENGTH_SHORT).show();
        } else if (AppSetting.getInstance().getUserData() != null && AppSetting.getInstance().getUserData().gold_days > 0) {
            Conversation conversation = new Conversation();
            conversation.to_user_name = userData.name;
            conversation.photo_to = userData.default_photo;
            conversation.from_user_id = AppSetting.getInstance().getUser().user_id;
            conversation.msg = "";
            conversation.from_user_name = AppSetting.getInstance().getUserData().name;
            conversation.photo_from = AppSetting.getInstance().getUserData().default_photo;
            conversation.to_user_id = userData.user_id;
            ((BaseActivity) getActivity()).startActivity(ChatActivity.class, conversation, false);

        } else {
            Toast.makeText(getActivity(), "Please upgrade to use this feature", Toast.LENGTH_SHORT).show();
            ((BaseActivity) getActivity()).startActivity(PaymentActivity.class, null, false);
        }
    }

    @OnClick(R.id.tvMail)
    public void omMail() {
        if (AppSetting.getInstance().getUserData() != null && AppSetting.getInstance().getUserData().gold_days > 0) {
            Intent intent = new Intent(getContext(), MailComposeActivity.class);
            intent.putExtra("user_to", userData.user_id);
            intent.putExtra("user_name", userData.name);
            startActivity(intent);
        } else {
            Toast.makeText(getActivity(), "Please upgrade to use this feature", Toast.LENGTH_SHORT).show();
            ((BaseActivity) getActivity()).startActivity(PaymentActivity.class, null, false);
        }
    }
    @OnClick(R.id.tvInterest)
    public void onInterest() {
        if (userData != null) {
            profilePresenter.sendInterest(Integer.valueOf(userData.user_id));
            ((BaseActivity) getActivity()).showProgressDialog(false);
        }
    }

    @OnClick(R.id.profile_image)
    public void onProfileClick() {
        if (photoList != null && photoList.size() > 0) {
            Photo photo = new Photo();
            photo.ref = userData.default_photo;

            Intent intent = new Intent(getActivity(), PhotoViewerActivity.class);
            intent.putExtra("photo", Parcels.wrap(photo));
            intent.putExtra("photo-list", Parcels.wrap(photoList));
            startActivity(intent);
        } else {
            Toast.makeText(getActivity(), "There is no photo", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.tvBlock)
    public void onBlock() {
        if (userData != null) {
            profilePresenter.blockUser(Integer.valueOf(userData.user_id));
        }
    }

    @OnClick(R.id.tvFavorite)
    public void onFavorite() {
        if (userData != null) {
            profilePresenter.sendFavorite(Integer.valueOf(userData.user_id));
            ((BaseActivity) getActivity()).showProgressDialog(false);
        }
    }

    @OnClick(R.id.btnEdit)
    public void onEdit() {
        ((BaseActivity) getActivity()).startActivity(ProfileCompletionActivity.class, "1", false);
    }

    @OnClick(R.id.btnPhoto)
    public void onPhoto() {
        Intent intent = new Intent("com.gallery");
        intent.putExtra("cmd", 0);
        intent.putExtra("data", userData.user_id);
        getActivity().getApplicationContext().sendBroadcast(intent);
    }

    @OnClick(R.id.btnVideo)
    public void onVideo() {
        Intent intent = new Intent("com.gallery");
        intent.putExtra("cmd", 1);
        intent.putExtra("data", userData.user_id);
        getActivity().getApplicationContext().sendBroadcast(intent);
    }

    @Override
    public void onError(String msg) {
        ((BaseActivity) getActivity()).dismissProgressDialog();
        ((BaseActivity) getActivity()).showDialog(msg);
    }

    @Override
    public void onGetDataSuccess(UserData userData) {
        ((BaseActivity) getActivity()).dismissProgressDialog();
        if (!AppSetting.getInstance().getUserData().user_id.equals(userData.user_id)) {
            profilePresenter.viewUser(userData.user_id);
        }
        profilePresenter.getVideos(userData.user_id);
        profilePresenter.getPhoto(userData.user_id, String.valueOf(1));
        this.userData = userData;
        if (userData.user_id.equals(AppSetting.getInstance().getUserData().user_id)) {
            cvControl.setVisibility(View.GONE);
            btnEdit.setVisibility(View.VISIBLE);
        } else {
            btnEdit.setVisibility(View.GONE);
            cvControl.setVisibility(View.VISIBLE);
        }
        Picasso.with(getActivity()).load(userData.default_photo).resize(UIUtils.dpToPx(getActivity(), 96), UIUtils.dpToPx(getActivity(), 96)).centerCrop().into(ivProfile);
        tvHeadline.setText(userData.headline);
        tvEssay.setText(Html.fromHtml(userData.essay));
        tvName.setText(userData.name);
        rbRating.setRating(userData.rating);
        tvRating.setText(userData.rating + "/5");
        tvAge.setText(userData.age + "");
        tvActivity.setText(userData.online.equals("1") ? "Online" : userData.last_visit);
        tvCity.setText(userData.city);
        tvState.setText(userData.state);
        tvCountry.setText(userData.country);
        tvGender.setText(userData.gender);

        tvTitleAbout.setText("About " + userData.name);
        tvHeight.setText(userData.height);
        tvWeight.setText(userData.weight);
        tvBody.setText(userData.body);
        tvAppearance.setText(userData.appearance);
        tvComplexion.setText(userData.complexion);
        tvStatus.setText(userData.status);

        tvEducatiob.setText(userData.education);
        tvCareer.setText(userData.career);

        tvReligion.setText(userData.religion);
        tvEthnicity.setText(userData.ethnicity);
        tvCaste.setText(userData.cast);
        tvResidency.setText(userData.residency);
        tvClass.setText(userData.classs);
        tvFamily.setText(userData.family);

        tvSmoking.setText(userData.smoking);
        tvDrinking.setText(userData.drinking);
        tvDiet.setText(userData.diet);
        tvPeronal.setText(userData.humor);
        tvFirst.setText(userData.first_date);
    }

    @Override
    public void onSendInterestSuccess(String msg) {
        ((BaseActivity) getActivity()).dismissProgressDialog();
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onAddFavoriteuccess(String msg) {
        ((BaseActivity) getActivity()).dismissProgressDialog();
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBlockResult(String msg) {
        ((BaseActivity) getActivity()).dismissProgressDialog();
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onGetPhotos(List<Photo> photoList, int totalPage, int totalItem) {
        this.photoList = photoList;
        if (photoList == null || photoList.size() == 0) {
            btnPhoto.setVisibility(View.GONE);
        } else {
            btnPhoto.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onGetPhotosNext(List<Photo> photoList, int totalPage) {

    }

    @Override
    public void onGetVideoList(List<Video> videos) {
        if (videos == null || videos.size() == 0) {
            btnVideo.setVisibility(View.GONE);
        }
    }


}
