package com.npc.marry.ui.profile;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.npc.marry.R;
import com.npc.marry.base.BaseActivity;
import com.npc.marry.model.UserData;
import com.npc.marry.ui.adapter.ViewPagerFragmentAdapter;
import com.npc.marry.ui.photo.PhotoActivity;
import com.npc.marry.ui.video.VideoActivity;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class ProfileActivity extends BaseActivity {

    @BindView(R.id.viewpager)
    ViewPager viewPager;

    List<UserData> userDataList;
    ViewPagerFragmentAdapter viewPagerFragmentAdapter;

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int cmd = intent.getIntExtra("cmd", 0);
            String user_id = intent.getStringExtra("data");
            switch (cmd) {
                case 0:
                    PhotoActivity photoActivity = new PhotoActivity();
                    Bundle bundle = new Bundle();
                    bundle.putString("data", user_id);
                    bundle.putInt("mode", 1);
                    photoActivity.setArguments(bundle);
                    viewPager.setVisibility(View.GONE);
                    getSupportFragmentManager().beginTransaction().add(R.id.llGallery, photoActivity).addToBackStack(null).commit();
                    break;
                case 1:
                    VideoActivity videoActivity = new VideoActivity();
                    Bundle bundle1 = new Bundle();
                    bundle1.putString("data", user_id);
                    bundle1.putInt("mode", 1);
                    videoActivity.setArguments(bundle1);
                    viewPager.setVisibility(View.GONE);
                    getSupportFragmentManager().beginTransaction().add(R.id.llGallery, videoActivity).addToBackStack(null).commit();
                    break;
            }
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_wrapper);
        viewPagerFragmentAdapter = new ViewPagerFragmentAdapter(getSupportFragmentManager());
        viewPager.setAdapter(viewPagerFragmentAdapter);

        String user_id = null;
        user_id = getIntent().getStringExtra("data");
        if (user_id != null) {
            viewPagerFragmentAdapter.addFragment(ProfileFragment.newInstance(user_id), "t");
        } else {
            Object object = Parcels.unwrap(getIntent().getParcelableExtra("data"));
            if (object instanceof UserData) {
                userDataList = new ArrayList<>();
                userDataList.add((UserData) object);
            } else {
                userDataList = (List<UserData>) object;
            }

            for (UserData userData : userDataList) {
                if (userData != null)
                    viewPagerFragmentAdapter.addFragment(ProfileFragment.newInstance(userData.user_id), "t");
            }

            if (userDataList.size() > 1) {
                UserData userData = Parcels.unwrap(getIntent().getParcelableExtra("data1"));
                for (int i = 0; i < userDataList.size(); ++i) {
                    if (userDataList.get(i).user_id.equals(userData.user_id)) {
                        viewPager.setCurrentItem(i);
                        break;
                    }
                }
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter("com.gallery");
        getApplicationContext().registerReceiver(receiver, intentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        getApplicationContext().unregisterReceiver(receiver);
    }

    @OnClick(R.id.ivBack)
    public void onBack() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
            viewPager.setVisibility(View.VISIBLE);
        } else {
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
            viewPager.setVisibility(View.VISIBLE);
        } else {
            super.onBackPressed();
        }
    }
}
