package com.npc.marry.ui.profile;

import com.npc.marry.model.Photo;
import com.npc.marry.model.UserData;
import com.npc.marry.model.Video;

import java.util.List;

/**
 * Created by Lenovo on 3/6/2017.
 */

public interface ProfileView {
    void onError(String msg);

    void onGetDataSuccess(UserData userData);

    void onSendInterestSuccess(String msg);

    void onAddFavoriteuccess(String msg);

    void onBlockResult(String msg);

    void onGetPhotos(List<Photo> photoList, int totalPage, int totalItem);

    void onGetPhotosNext(List<Photo> photoList, int totalPage);

    void onGetVideoList(List<Video> videos);
}
